package com.uh.homepage.fragment;

import io.xlink.wifi.sdk.XDevice;
import io.xlink.wifi.sdk.XlinkAgent;
import io.xlink.wifi.sdk.XlinkCode;
import io.xlink.wifi.sdk.bean.EventNotify;
import io.xlink.wifi.sdk.listener.ConnectDeviceListener;
import io.xlink.wifi.sdk.listener.ScanDeviceListener;
import io.xlink.wifi.sdk.listener.SetDeviceAccessKeyListener;
import io.xlink.wifi.sdk.listener.SubscribeDeviceListener;
import io.xlink.wifi.sdk.listener.XlinkNetListener;
import io.xlink.wifi.ui.Constant;
import io.xlink.wifi.ui.MyApp;
import io.xlink.wifi.ui.activity.BaseFragmentActivity;
import io.xlink.wifi.ui.activity.DeviceListActivity;
import io.xlink.wifi.ui.adapter.DeviceAdapter;
import io.xlink.wifi.ui.bean.Device;
import io.xlink.wifi.ui.bean.LocalInfo;
import io.xlink.wifi.ui.bean.NetType;
import io.xlink.wifi.ui.db.Xlink_Db_Helper;
import io.xlink.wifi.ui.fragment.BaseFragment;
import io.xlink.wifi.ui.fragment.DeviceListTab;
import io.xlink.wifi.ui.http.HttpAgent;
import io.xlink.wifi.ui.manage.DeviceManage;
import io.xlink.wifi.ui.util.SharedPreferencesUtil;
import io.xlink.wifi.ui.util.XlinkUtils;

import java.util.ArrayList;

import org.apache.http.Header;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.annotation.SuppressLint;
import android.app.AlertDialog.Builder;
import android.app.Dialog;
import android.content.ContentValues;
import android.content.Context;
import android.content.DialogInterface;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v4.widget.SwipeRefreshLayout;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.AdapterView.OnItemLongClickListener;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.loopj.android.http.TextHttpResponseHandler;
import com.youhone.weatherform.WeatherForm;
import com.youhone.weatherquery.WeatherQueryManageImpl;
import com.youhone.xlink.superapp.R;

public class DeviceListFragment extends BaseFragment implements
		OnItemClickListener, XlinkNetListener, OnItemLongClickListener,
		SwipeRefreshLayout.OnRefreshListener {
	/** The Constant TAG. */
	private static final String TAG = "DeviceListFragment";
	private static final int SETLIST = 100;
	private static final int UPDATALIST = 101;
	private static final int TIMEOUT = 102;
	private static final int SETWEATHER = 103;
	private ListView lvDevices;
	private SwipeRefreshLayout mSwipeLayout;
	private DeviceAdapter mDeviceListAdapter;
	private Dialog progressDialog;
	private Dialog Intent_progressDialog;
	private FragmentDeviceAuthorizeListener listener;
	private int receiveCount = 0;
	private int appid;
	private boolean Refresh_Finish = false;
	// 天气相关
	private TextView city, temp, pm25, quality, date, week, home_shidu;
	private ImageView home_weather_img;
	private LinearLayout weather_data;
	private RelativeLayout weather_lay, list_lay;
	private TextView no_city_tip;
	private WeatherForm[] weathers;
	/**
	 * 后台获取到的设备列表
	 */
	ArrayList<XDevice> devices = new ArrayList<XDevice>();
	/**
	 * 扫描到的设备列表，通过mac地址进行对比，从而得到后台获取到的设备的状态
	 */
	ArrayList<XDevice> scan_devices = new ArrayList<XDevice>();
	/**
	 * 储存设备名称，解决乱码问题
	 */
	ArrayList<String> local_devices_name = new ArrayList<String>();

	@SuppressLint("HandlerLeak")
	private Handler uiHandler = new Handler() {
		public void handleMessage(android.os.Message msg) {
			switch (msg.what) {
			case SETWEATHER:
				weathers = (WeatherForm[]) msg.obj;
				if (weathers != null && weathers.length >= 1) {
					System.out.println("天气显示成功1");
					no_city_tip.setVisibility(View.GONE);
					weather_data.setVisibility(View.VISIBLE);
					WeatherForm firstDay = weathers[0];
					System.out.println("天气显示成功2" + firstDay.toString());
					city.setText(firstDay.getName());
					temp.setText(firstDay.getCurrent_temp() + "℃");
					date.setText(firstDay.getDdate());
					week.setText("星期" + firstDay.getWeek());
					pm25.setText(firstDay.getPm25());
					String air_quality = firstDay.getQuality();
					quality.setText("空气质量：" + air_quality);
					if (air_quality.contains("优")) {
						Constant.WEATHER_QUALITY = 1;
						list_lay.setBackgroundResource(R.drawable.pollution_low);
						quality.setBackgroundResource(R.drawable.bg_quality_you);
					} else if (air_quality.contains("良")) {
						Constant.WEATHER_QUALITY = 2;
						list_lay.setBackgroundResource(R.drawable.pollution_low);
						quality.setBackgroundResource(R.drawable.bg_quality_liang);
					} else if (air_quality.contains("轻度")) {
						Constant.WEATHER_QUALITY = 3;
						quality.setBackgroundResource(R.drawable.bg_quality_qingdu);
						list_lay.setBackgroundResource(R.drawable.pollution_middle);
					} else if (air_quality.contains("中度")) {
						Constant.WEATHER_QUALITY = 4;
						quality.setBackgroundResource(R.drawable.bg_quality_zhongdu);
						list_lay.setBackgroundResource(R.drawable.pollution_middle);
					} else if (air_quality.contains("差")
							|| air_quality.contains("重度")) {
						Constant.WEATHER_QUALITY = 5;
						quality.setBackgroundResource(R.drawable.bg_quality_heavy);
						list_lay.setBackgroundResource(R.drawable.pollution_high);
					} else if (air_quality.contains("差")
							|| air_quality.contains("严重")) {
						Constant.WEATHER_QUALITY = 6;
						quality.setBackgroundResource(R.drawable.bg_quality_yanzhong);
						list_lay.setBackgroundResource(R.drawable.pollution_high);
					} else {
						Constant.WEATHER_QUALITY = 1;
						quality.setBackgroundResource(R.drawable.bg_quality_you);
						list_lay.setBackgroundResource(R.drawable.pollution_low);
					}
					home_shidu.setText(firstDay.getHumidity() + "%");
					home_weather_img.setImageResource(firstDay.getImg_id());
					weather_lay.setBackgroundResource(firstDay.getBg_id());
					System.out.println("天气显示成功3" + firstDay.getTemp() + "℃");

				} else {
					weather_data.setVisibility(View.GONE);
					no_city_tip.setVisibility(View.VISIBLE);
				}
				System.out.println("天气显示成功");
				break;
			case SETLIST:// 获取到新的设备，重置适配器
				Log.d(TAG, "列表设备长度  " + devices.size());
				mDeviceListAdapter = new DeviceAdapter(getActivity(), devices,
						local_devices_name);
				lvDevices.setAdapter(mDeviceListAdapter);
				DeviceListActivity.firstLoad = false;
				break;
			case UPDATALIST:// 更新状态
				System.out.println("updata   ");
				int msize = devices.size();
				int scan_size = scan_devices.size();
				System.out.println("receiveCount:" + receiveCount
						+ "   scan_size" + scan_size + "    msize" + msize);
				// 重新排序，在线的排在前面，背景为白色，离线的在下面，背景为灰色
				if (receiveCount >= msize || LocalInfo.NetType != NetType.NET) {
					int onlineCount = 0;
					for (int i = 0; i < msize; i++) {
						XDevice xdevice = devices.get(i);
						String mac = xdevice.getMacAddress();
						for (int j = 0; j < scan_size; j++) {
							if (mac.equals(scan_devices.get(j).getMacAddress())) {// 在线
								System.out.println(xdevice.getMacAddress());
								devices.remove(i);
								devices.add(xdevice);
								local_devices_name.remove(i);
								local_devices_name.add(xdevice.getDeviceName());
								scan_devices.remove(j);
								scan_size--;
								i--;
								msize--;
								onlineCount++;
								break;
							}
						}
					}
					mSwipeLayout.setRefreshing(false);
					// lvDevices.completeRefreshing();
					if (progressDialog.isShowing()) {
						System.out.println("dismiss1");
						progressDialog.dismiss();
					}

					mDeviceListAdapter.setOnlineCount(onlineCount);
					mDeviceListAdapter.notifyDataSetChanged();
					Refresh_Finish = true;
					receiveCount = 0;
				}

				break;
			case TIMEOUT:
				if (devices.size() == 0)
					mSwipeLayout.setRefreshing(false);
				// lvDevices.completeRefreshing();
				if (progressDialog.isShowing()) {
					System.out.println("dismiss2");
					progressDialog.dismiss();
				}

				if (!Refresh_Finish) {
					mDeviceListAdapter.setOnlineCount(0);
					mDeviceListAdapter.notifyDataSetChanged();
				}
				Refresh_Finish = true;
				break;

			default:
				break;
			}

		};
	};

	public void onCreate(Bundle savedInstanceState) {
		Log("onCreate");
		super.onCreate(savedInstanceState);
	}

	public void onResume() {
		Log("onResume11");
		downLoadDevice();
		getWeather();
		super.onResume();
	}

	public void onDestroyView() {
		Log("onDestroyView");
		super.onDestroyView();
		devices.clear();
		scan_devices.clear();
		local_devices_name.clear();
		receiveCount = 0;
		DeviceManage.getInstance().clearAllDevice();
		uiHandler.removeCallbacksAndMessages(null);
		responseHandler.onCancel();
		lvDevices.setAdapter(null);
	}

	@Override
	public void onSaveInstanceState(Bundle savedInstanceState) {
		// Save away the original text, so we still have it if the activity
		// needs to be killed while paused.
		super.onSaveInstanceState(savedInstanceState);
		// downLoadDevice();
		if (null != Intent_progressDialog)
			Intent_progressDialog.dismiss();
		Log.e(TAG, "onSaveInstanceState");
	}

	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View view = inflater.inflate(R.layout.xlink_device_list, container,
				false);
		Log("onCreateView");
		initDialog();
		quality = (TextView) view.findViewById(R.id.home_air_quality);
		date = (TextView) view.findViewById(R.id.home_weather_date);
		week = (TextView) view.findViewById(R.id.home_weather_week);
		city = (TextView) view.findViewById(R.id.home_city_name);
		temp = (TextView) view.findViewById(R.id.home_temp);
		pm25 = (TextView) view.findViewById(R.id.home_pm25);
		home_weather_img = (ImageView) view.findViewById(R.id.home_weather_img);
		home_shidu = (TextView) view.findViewById(R.id.home_shidu);
		no_city_tip = (TextView) view.findViewById(R.id.no_city_tip);
		weather_lay = (RelativeLayout) view.findViewById(R.id.weather_lay);
		list_lay = (RelativeLayout) view.findViewById(R.id.list_lay);
		weather_data = (LinearLayout) view.findViewById(R.id.weather_data);
		lvDevices = (ListView) view.findViewById(R.id.lvDevices);
		mSwipeLayout = (SwipeRefreshLayout) view.findViewById(R.id.id_swipe_ly);
		weather_lay.setOnClickListener(new BtnListener());
		lvDevices.setOnItemClickListener(this);
		// lvDevices.setOnItemLongClickListener(this);
		mDeviceListAdapter = new DeviceAdapter(getActivity(), devices,
				local_devices_name);
		lvDevices.setAdapter(mDeviceListAdapter);
		// lvDevices.setOnRefreshListener(refreshListener);
		mSwipeLayout.setOnRefreshListener(this);
		mSwipeLayout.setRefreshing(true);
		mSwipeLayout.setColorScheme(android.R.color.holo_blue_bright,
				android.R.color.holo_green_light,
				android.R.color.holo_orange_light,
				android.R.color.holo_red_light);
		// mSwipeLayout.post(new Runnable() {
		//
		// @Override
		// public void run() {
		// mSwipeLayout.setRefreshing(true);
		// }
		// });
		// this.onRefresh();
		devices.clear();
		scan_devices.clear();
		local_devices_name.clear();
		receiveCount = 0;
		if (XlinkUtils.isWifi()) {
			XlinkAgent.getInstance().start();
		}
		XlinkAgent.getInstance().addXlinkListener(this);
		appid = SharedPreferencesUtil.queryIntValue(Constant.APP_ID);

		return view;
	}

	private void initDialog() {
		progressDialog = ((BaseFragmentActivity) getActivity())
				.createProgressDialog("", "正在获取设备列表，请稍等");
		Intent_progressDialog = ((BaseFragmentActivity) getActivity())
				.createProgressDialog("", "正在连接设备，请稍等");
		progressDialog.setCanceledOnTouchOutside(false);
		Intent_progressDialog.setCanceledOnTouchOutside(true);
		Intent_progressDialog.dismiss();
		progressDialog.dismiss();
	}

	TextHttpResponseHandler responseHandler = new TextHttpResponseHandler() {

		@Override
		public void onSuccess(int arg0, Header[] arg1, String str) {
			doConnectSuccess(arg0, arg1, str);
		}

		@Override
		public void onFailure(int arg0, Header[] arg1, String arg2,
				Throwable arg3) {
			doConnectFail(arg0, arg1, arg2, arg3);

		}
	};

	private void doConnectSuccess(int arg0, Header[] arg1, String str) {
		if (getActivity() == null)
			return;
		Xlink_Db_Helper mDb_Helper = new Xlink_Db_Helper(getActivity());
		ContentValues values = new ContentValues();
		if (mDb_Helper == null)
			return;
		SQLiteDatabase db = mDb_Helper.getWritableDatabase();
		JSONObject json;
		try {
			json = new JSONObject(str);
			Log.d(TAG, "回来的数据  ");
			Log.d(TAG, str);
			JSONArray results = json.getJSONArray("results");
			final int results_length = results.length();
			Log.d(TAG, "数据长度    " + results_length);
			if (results.length() == 0) {
				mSwipeLayout.setRefreshing(false);
				// lvDevices.completeRefreshing();
				progressDialog.dismiss();
				return;
			}
			mSwipeLayout.setRefreshing(false);
			// lvDevices.completeRefreshing();
			scan_devices.clear();
			devices.clear();
			local_devices_name.clear();
			if (LocalInfo.NetType == NetType.NET)
				clearDB();
			// 对结果进行循环
			for (int i = 0; i < results_length; i++) {
				final int j = i;
				JSONObject data = results.getJSONObject(i)
						.getJSONObject("data");
				local_devices_name.add(data.getString("device_name"));
				// 将回来的Json转换为Device
				final Device device_item = new Device(
						XlinkAgent.JsonToDevice(data));
				// device_item.setPassword("Constant.passwrod");
				XDevice Xdevice = device_item.getXDevice();
				if (!devices.contains(Xdevice)) {
					devices.add(Xdevice);
				}
				// 调用connectDevice前需要初始化设备
				// if (!Xdevice.isInit())
				XlinkAgent.getInstance().initDevice(Xdevice);

				Log.d(TAG, "设备名称    " + Xdevice.getDeviceName());
				if (LocalInfo.NetType != NetType.NET) {
					scan();
				} else {
					values.clear();
					values.put("id", appid + "#" + Xdevice.getMacAddress());
					values.put("data", data.toString());
					db.insert("device_db", null, values);
					try {
						// 通过这个方法判断此设备是否在线
						XlinkAgent.getInstance().connectDevice(Xdevice,
								Constant.password, new ConnectDeviceListener() {
									@Override
									public void onConnectDevice(XDevice arg0,
											int arg1) {
										Log.d(TAG, "连接返回代码    " + arg1);
										if ((arg1 == 1 || arg1 == 0)
												&& arg0.getMacAddress()
														.equals(device_item
																.getMacAddress())) {// 利用MAC地址判断是否未同一个设备
											if (scan_devices.size() == 0) {
												arg0.setDeviceName(device_item
														.getDName());
												scan_devices.add(arg0);
											}
											for (XDevice xdevice : scan_devices) {
												if ((!xdevice
														.getMacAddress()
														.equals(arg0
																.getMacAddress()))
														&& !scan_devices
																.contains(arg0)) {// 如果扫描列表中已经有，则不添加
													arg0.setDeviceName(device_item
															.getDName());
													scan_devices.add(arg0);
													break;
												}
											}
											if (j <= results_length - 1) {// 解决乱码问题
												XDevice mdevice = devices
														.get(j);
												mdevice.setDeviceName(local_devices_name
														.get(j));
												devices.set(j, mdevice);
											}
										}
										receiveCount++;
										uiHandler.sendEmptyMessage(UPDATALIST);
									}
								});
					} catch (Exception e) {

					}

				}
			}

			// 设置列表显示，并扫描设备进行判断状态
			uiHandler.sendEmptyMessage(SETLIST);
			Bundle saveBundle = new Bundle();
			saveBundle.putBoolean("isLoad", true);
			onSaveInstanceState(saveBundle);
			// scan();
		} catch (JSONException e) {
			progressDialog.dismiss();
			mSwipeLayout.setRefreshing(false);
			// lvDevices.completeRefreshing();
			XlinkUtils.shortTips("数据解析错误");
			e.printStackTrace();
		} finally {
			db.close();
			mDb_Helper.close();
		}

	}

	private void doConnectFail(int arg0, Header[] arg1, String arg2,
			Throwable arg3) {
		XlinkUtils.shortTips("获取设备失败，请重试");
		progressDialog.dismiss();
		mSwipeLayout.setRefreshing(false);
		// lvDevices.completeRefreshing();
	}

	private void clearDB() {
		Xlink_Db_Helper mDb_Helper = new Xlink_Db_Helper(getActivity());
		SQLiteDatabase db = mDb_Helper.getWritableDatabase();
		String delete = "Delete from device_db where id like \"" + appid
				+ "#%\"";
		db.execSQL(delete);
		db.close();
		mDb_Helper.close();
	}

	private void scan() {
		int ret[] = new int[Constant.PRODUCTID.length];
		for (int i = 0; i < ret.length; i++) {
			ret[i] = XlinkAgent.getInstance().scanDeviceByProductId(
					Constant.PRODUCTID[i], scanListener);
			if (ret[i] < 0) {
				progressDialog.dismiss();
				switch (ret[i]) {
				case XlinkCode.NO_CONNECT_SERVER:
					if (XlinkUtils.isWifi()) {
						XlinkAgent.getInstance().start();
					}
					break;
				case XlinkCode.NETWORD_UNAVAILABLE:
					break;
				default:
					break;
				}
			}
		}

	}

	private ScanDeviceListener scanListener = new ScanDeviceListener() {

		@Override
		public void onGotDeviceByScan(XDevice xdevice) {
			if (!scan_devices.contains(xdevice)) {
				for (int i = 0; i < Constant.PRODUCTID.length; i++) {
					if (xdevice.getProductId().equals(Constant.PRODUCTID[i])) {
						scan_devices.add(xdevice);
					}
				}

			}
			uiHandler.sendEmptyMessage(UPDATALIST);
		}
	};

	/**
	 * 获取天气
	 */
	private void getWeather() {
		new Thread(new Runnable() {
			@Override
			public void run() {
				Message msg = new Message();
				WeatherQueryManageImpl WQM = new WeatherQueryManageImpl();
				// UserCityManageImpl dbManage = UserCityManageImpl
				// .getDBInstance(getActivity());
				String city = getActivity()
						.getSharedPreferences("city", Context.MODE_PRIVATE)
						.getString(MyApp.getApp().getAppid() + "", "").trim()
						.toString();
				if (city.equals(""))
					city = "广州";
				if (!city.equals("")) {
					// 查询天气，返回3天的天气信息
					msg.obj = WQM.weatherquery(city);
					msg.what = SETWEATHER;
					uiHandler.sendMessage(msg);
				}
			}
		}).start();
	}

	/**
	 * 获取后台储存的设备
	 */
	private void downLoadDevice() {
		System.out.println("调用了" + DeviceListTab.position);
		Refresh_Finish = false;
		// if (DeviceListTab.position == 0)
		progressDialog.show();
		uiHandler.removeMessages(TIMEOUT);
		uiHandler.sendEmptyMessageDelayed(TIMEOUT, 12 * 1000);// 12S超时
		final int appid = SharedPreferencesUtil.queryIntValue(Constant.APP_ID);
		System.out.println("网络类型    " + LocalInfo.NetType);
		System.out.println("获取设备时的appid=" + appid);
		switch (LocalInfo.NetType) {
		case NetType.LAN:
		case NetType.NET:
			JSONObject data = new JSONObject();
			try {
				data.put("appid", appid);
			} catch (JSONException e) {
				e.printStackTrace();
			}
			HttpAgent.getInstance().queryData(Constant.TABLE_USER,
					responseHandler, data);
			break;
		default:
			Xlink_Db_Helper mDb_Helper = new Xlink_Db_Helper(getActivity());
			SQLiteDatabase db = mDb_Helper.getWritableDatabase();
			String query = "Select * from device_db where id like ?";
			Cursor mCursor = db.rawQuery(query, new String[] { appid + "#%" });
			System.out.println("手机离线，在本地数据库获取到的设备数量   " + mCursor.getCount());
			JSONObject json = new JSONObject();
			JSONArray result = new JSONArray();
			while (mCursor.moveToNext()) {
				JSONObject data2 = new JSONObject();
				try {
					data2.put(
							"data",
							new JSONObject(mCursor.getString(mCursor
									.getColumnIndex("data"))));
				} catch (JSONException e) {
					e.printStackTrace();
				}
				result.put(data2);
			}
			try {
				json.put("results", result);
				Header[] arg1 = null;
				doConnectSuccess(0, arg1, json.toString());
			} catch (JSONException e) {
				e.printStackTrace();
			}
			mCursor.close();
			db.close();
			mDb_Helper.close();
			break;
		}

	}

	private class BtnListener implements OnClickListener {
		@Override
		public void onClick(View v) {
			int id = v.getId();
			if (id == R.id.weather_lay) {
				getAct().openWeatherDetail(weathers);
			}
		}

	}

	/**
	 * 设备列表点击
	 */
	@Override
	public void onItemClick(AdapterView<?> parent, View view, int position,
			long id) {
		if (position == mDeviceListAdapter.getCount() - 1) {
			getAct().openSearchDevice();
			return;
		}
		if (LocalInfo.NetType == NetType.NO_NET) {
			Toast.makeText(getAct(), "当前无网络连接", Toast.LENGTH_SHORT).show();
			return;
		}
		XDevice tempDevice = mDeviceListAdapter
				.getDeviceByPosition(position + 1);
		if (tempDevice == null) {
			System.out.println("点击得到的device为空");
			return;
		}
		if (!mDeviceListAdapter.getState(position)) {
			Toast.makeText(getAct(), "该设备离线", Toast.LENGTH_SHORT).show();
			DeviceManage.getInstance().clearAllDevice();// 清除之前连接过的设备
			DeviceManage.getInstance().addDevice(tempDevice);
			for (int i = 0; i < Constant.PRODUCTID.length; i++) {
				if (tempDevice.getProductId().equals(Constant.PRODUCTID[i])) {
					((DeviceListActivity) getActivity()).openDeviceActiviy(i,
							false);
					break;
				}
			}
			return;
		}
		Intent_progressDialog.show();
		Log.d(TAG, "点击的设备   " + tempDevice.getDeviceName());
		// 初始化设备
		XlinkAgent.getInstance().initDevice(tempDevice);
		// 验证设备
		if (tempDevice.isInit())
			SUCCEED(tempDevice, Constant.password);
		else
			setDevicePassword(Constant.password, tempDevice);

	}

	/**
	 * 进行密码连接或设置初始密码
	 * 
	 * @param password
	 * @return
	 */
	private int setDevicePassword(final int password, XDevice xdevice) {
		int ret = XlinkAgent.getInstance().setDeviceAccessKey(xdevice,
				password, new SetDeviceAccessKeyListener() {
					@Override
					public void onSetLocalDeviceAccessKey(XDevice xdevice,
							int code, int msgId) {
						switch (code) {
						case XlinkCode.SUCCEED:
							Intent_progressDialog.dismiss();
							SUCCEED(xdevice, password);
							break;
						case XlinkCode.SERVER_CODE_UNAUTHORIZED:// 不存在订阅关系
							XlinkAgent.getInstance().subscribeDevice(xdevice,
									password, new SubscribeDeviceListener() {

										@Override
										public void onSubscribeDevice(
												XDevice xdevice, int code) {

											// stub
											switch (code) {
											case XlinkCode.SUCCEED:
												Intent_progressDialog.dismiss();
												SUCCEED(xdevice, password);
												break;
											default:
												Intent_progressDialog.dismiss();
												fail(code, xdevice);
												break;
											}

										}
									});

							break;
						default:
							fail(code, xdevice);
							break;
						}
						Log("设置默认密码:" + code);
					}
				});
		System.out.println(ret);
		return ret;
	}

	/**
	 * 认证成功
	 * 
	 * @param xd
	 * @param pwd
	 */
	private void SUCCEED(XDevice xd, int pwd) {
		// XlinkUtils.shortTips("认证设备成功");
		Device device = new Device(xd);
		// device.setPassword(pwd);

		DeviceManage.getInstance().clearAllDevice();// 清除之前连接过的设备
		DeviceManage.getInstance().addDevice(device);
		System.out.println("设备名称1" + device.getName());
		System.out.println("设备名称2" + device.getDName());
		if (getActivity() instanceof DeviceListActivity) {
			for (int i = 0; i < Constant.PRODUCTID.length; i++) {
				if (device.getXDevice().getProductId()
						.equals(Constant.PRODUCTID[i])) {
					((DeviceListActivity) getActivity()).openDeviceActiviy(i,
							false);
					break;
				}
			}
		} else {
			if (listener != null) {
				listener.onSetLocalDeviceAuthorizeCode(xd, 0);
			}
		}
	}

	public interface FragmentDeviceAuthorizeListener {
		void onSetLocalDeviceAuthorizeCode(XDevice xdevice, int code);
	}

	private void fail(int code, XDevice device) {
		String tips = "";
		if (getActivity() instanceof DeviceListActivity) {
			if (device.isInit()) {
				tips = "设备认证失败,错误码：" + code;

			} else {
				tips = "设置初始密码失败,错误码：" + code;
			}
		} else {
			tips = "设备认证失败,错误码：" + code;
		}
		Intent_progressDialog.dismiss();
		progressDialog.dismiss();
		mSwipeLayout.setRefreshing(false);
		// lvDevices.completeRefreshing();
		Toast.makeText(getActivity(), "连接错误，请重试", Toast.LENGTH_SHORT).show();
		Log(tips);
	}

	// private void linkDeviceError(String msg) {
	// CustomDialog dialog = ((BaseFragmentActivity) getActivity())
	// .createTipsDialog("设备认证", msg);
	// dialog.show();
	// }

	private DeviceListActivity getAct() {
		return (DeviceListActivity) getActivity();
	}

	@Override
	public void onDataPointUpdate(XDevice arg0, int arg1, Object arg2,
			int arg3, int arg4) {
		// TODO Auto-generated method stub
	}

	@Override
	public void onDeviceStateChanged(XDevice arg0, int arg1) {
		// TODO Auto-generated method stub
	}

	@Override
	public void onDisconnect(int arg0) {
		progressDialog.dismiss();
	}

	@Override
	public void onLocalDisconnect(int arg0) {
		progressDialog.dismiss();
	}

	@Override
	public void onLogin(int code) {
		if (code != XlinkCode.SUCCEED) {
			progressDialog.dismiss();
		}
		if (code == XlinkCode.SUCCEED) {
			// XlinkUtils.shortTips("云端网络已可用");
		} else if (code == XlinkCode.CLOUD_CONNECT_NO_NETWORK
				|| XlinkUtils.isConnected()) {

		} else {

		}
	}

	@Override
	public void onRecvPipeData(XDevice arg0, byte flags, byte[] arg1) {
		// TODO Auto-generated method stub
	}

	@Override
	public void onRecvPipeSyncData(XDevice arg0, byte flags, byte[] arg1) {
		// TODO Auto-generated method stub
	}

	@Override
	public void onStart(int arg0) {
		System.out.println("onStart");
		// downLoadDevice();
	}

	@Override
	public boolean onItemLongClick(AdapterView<?> parent, View view,
			int position, long id) {
		// System.out.println("长按的设备名字：       "
		// + position
		// + "   "
		// + mDeviceListAdapter.getDeviceByPosition(position)
		// .getDeviceName());
		// if (null != mDeviceListAdapter.getDeviceByPosition(position))
		// showDeleteDialog(mDeviceListAdapter.getDeviceByPosition(position));
		return true;
	}

	private void showDeleteDialog(final XDevice xdevice) {
		Builder builder = new Builder(getActivity());
		builder.setCancelable(false);
		builder.setMessage("确认删除该设备吗？");
		builder.setTitle("提示");
		builder.setNegativeButton("确定",
				new android.content.DialogInterface.OnClickListener() {
					@Override
					public void onClick(DialogInterface dialog, int which) {
						deleteDevice(xdevice.getMacAddress());
					}
				});
		builder.setNeutralButton("取消", null);
		builder.create().show();
	}

	private void deleteDevice(final String mac) {
		final int appid = SharedPreferencesUtil.queryIntValue(MyApp.getApp()
				.getAppid() + "");
		HttpAgent.getInstance().deleteData(Constant.TABLE_USER,
				new TextHttpResponseHandler() {
					@Override
					public void onSuccess(int arg0, Header[] arg1, String arg2) {
						XlinkUtils.shortTips("删除成功");
						DeviceManage.getInstance().clearAllDevice();
						Xlink_Db_Helper mDb_Helper = new Xlink_Db_Helper(
								getActivity());
						SQLiteDatabase db = mDb_Helper.getWritableDatabase();
						String delete = "Delete from device_db where id = \""
								+ appid + "#" + mac + "\"";
						db.execSQL(delete);
						db.close();
						mDb_Helper.close();
						getActivity().runOnUiThread(new Runnable() {

							@Override
							public void run() {
								downLoadDevice();
							}
						});

					}

					@Override
					public void onFailure(int arg0, Header[] arg1, String arg2,
							Throwable arg3) {
						XlinkUtils.shortTips("删除失败");
					}
				}, appid + "#" + mac);
	}

	@Override
	public void onEventNotify(EventNotify arg0) {
		// TODO Auto-generated method stub

	}

	@Override
	public void onRefresh() {
		if (XlinkAgent.getInstance().isConnectedLocal()
				|| XlinkAgent.getInstance().isConnectedOuterNet()) {
			devices.clear();
			scan_devices.clear();
			local_devices_name.clear();
			receiveCount = 0;
			mDeviceListAdapter = new DeviceAdapter(getActivity(), devices,
					local_devices_name);
			lvDevices.setAdapter(mDeviceListAdapter);
			downLoadDevice();
		} else {
			XlinkAgent.getInstance().start();
		}

	}

}
