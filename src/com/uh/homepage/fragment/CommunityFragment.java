package com.uh.homepage.fragment;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.youhone.xlink.superapp.R;

import io.xlink.wifi.ui.fragment.BaseFragment;

public class CommunityFragment  extends BaseFragment{

	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View view = inflater.inflate(R.layout.fragment_community, container,
				false);
		Log("onCreateView");
		return view;
	}
}
