package com.uh.autoupdate;

import io.xlink.wifi.ui.http.HttpAgent;

import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;
import org.json.JSONObject;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.ComponentName;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.ServiceConnection;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.PackageManager.NameNotFoundException;
import android.os.Handler;
import android.os.IBinder;
import android.os.Message;

import com.uh.autoupdate.DownloadService.DownloadBinder;

public class UpdateUtil {
	
	
	public static String APK_URL;
	public static String UPDATE_URL;

	private DownloadBinder binder;
	private boolean isBinded;

	private Context context;

	/**
	 * 
	 * @param context
	 *            当前Activity对象
	 * @param UPDATE_URL
	 *            检查版本更新时的url
	 * @param APK_URL
	 *            apk存放地址
	 */
	public UpdateUtil(Context context) {
		this.context = context;
		
		ApplicationInfo appInfo = null;
		try {
			appInfo = context.getPackageManager().getApplicationInfo(
					context.getPackageName(), PackageManager.GET_META_DATA);
		} catch (NameNotFoundException e) {
			e.printStackTrace();
		}
		// 通过manifest 里配置的meta-data 获取 secret key 和 access id和pid
		APK_URL = appInfo.metaData
				.getString("APK_URL");
		UPDATE_URL = appInfo.metaData.getString("UPDATE_URL");
	}

	public UpdateUtil() {
	};
	public Handler handler=new Handler(){
		public void handleMessage(Message msg) {
			showUpdateDialog();
		};
	};

	public void checkUpdate() {

		new Thread(new Runnable() {

			@Override
			public void run() {
				try {
					// TODO Auto-generated method stub
					HttpGet httpRequest = new HttpGet(UPDATE_URL);
					HttpResponse httpResponse;

					httpResponse = new DefaultHttpClient().execute(httpRequest);
					System.out
							.println("httpResponse.getStatusLine().getStatusCode():"
									+ httpResponse.getStatusLine()
											.getStatusCode());
					if (httpResponse.getStatusLine().getStatusCode() == 200) {

						String strResult = EntityUtils.toString(httpResponse
								.getEntity());
						System.out.println("strResult=" + strResult);
						JSONObject jb = new JSONObject(strResult);
						int webVersion = jb.getInt("version");

						PackageManager manager = context.getPackageManager();
						try {
							PackageInfo info = manager.getPackageInfo(
									context.getPackageName(), 0);
							String appVersion = info.versionName; // 版本名
							int currentVersionCode = info.versionCode; // 版本号
							System.out.println("currentVersionCode:"
									+ currentVersionCode + " webVersion:"
									+ webVersion);
							if (webVersion > currentVersionCode) {

//								Intent intent = new Intent(context,
//										UpdateNoticeBrocast.class);
//								context.sendBroadcast(intent);
								System.out.println("showUpdateDialog");
//								showUpdateDialog();
								handler.sendEmptyMessage(1);
							}

						} catch (NameNotFoundException e) {
							// TODO Auto-generated catch blockd
							e.printStackTrace();
						}

					}
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}

		}).start();

	}

	public void showUpdateDialog() {
		AlertDialog.Builder builder = new AlertDialog.Builder(context);
		builder.setTitle("检测到新版本");
		builder.setMessage("是否下载更新?");
		builder.setPositiveButton("下载", new DialogInterface.OnClickListener() {

			@Override
			public void onClick(DialogInterface dialog, int which) {

				Intent it = new Intent(context, DownloadService.class);

				context.startService(it);
				context.bindService(it, conn, Context.BIND_AUTO_CREATE);
				dialog.dismiss();

			}
		}).setNegativeButton("取消", new DialogInterface.OnClickListener() {

			@Override
			public void onClick(DialogInterface dialog, int which) {
				// TODO Auto-generated method stub
				dialog.dismiss();
			}
		});
		builder.show();
	}

	ServiceConnection conn = new ServiceConnection() {

		@Override
		public void onServiceDisconnected(ComponentName name) {
			// TODO Auto-generated method stub
			isBinded = false;
		}

		@Override
		public void onServiceConnected(ComponentName name, IBinder service) {
			// TODO Auto-generated method stub
			binder = (DownloadBinder) service;
			binder.setContext(context);
			System.out.println("服务启动!!!");
			// 开始下载
			isBinded = true;
			binder.addCallback(callback);
			binder.start();

		}
	};

	private ICallbackResult callback = new ICallbackResult() {

		@Override
		public void OnBackResult(Object result) {
			// TODO Auto-generated method stub
			if ("finish".equals(result)) {
				((Activity) context).finish();
				return;
			}

		}

	};

	public interface ICallbackResult {
		public void OnBackResult(Object result);
	}

}
