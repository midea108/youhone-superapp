package com.youhone.device.qizhen;

import io.xlink.wifi.sdk.XlinkAgent;
import io.xlink.wifi.ui.activity.DeviceListActivity;
import io.xlink.wifi.ui.fragment.BaseFragment;
import io.xlink.wifi.ui.view.IconPagerAdapter;
import io.xlink.wifi.ui.view.IconTabPageIndicator;
import io.xlink.wifi.ui.view.LazyViewPager;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.List;

import android.app.AlertDialog.Builder;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewConfiguration;
import android.view.ViewGroup;

import com.youhone.xlink.superapp.R;

public class Qizhen_Main_Tab extends BaseFragment {

	public static int position = 0;
	private LazyViewPager mViewPager;
	private IconTabPageIndicator mIndicator;
	private List<Fragment> mTabs = new ArrayList<Fragment>();
	private FragmentAdapter mAdapter;

	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View view = inflater.inflate(R.layout.device_tab_control, container, false);
		setOverflowShowingAlways();
		mViewPager = (LazyViewPager) view.findViewById(R.id.view_pager);
		mIndicator = (IconTabPageIndicator) view.findViewById(R.id.indicator);
		initDatas(view);
		mAdapter = new FragmentAdapter(mTabs, getFragmentManager());
		mViewPager.setOffscreenPageLimit(0);
		mViewPager.setAdapter(mAdapter);
		mIndicator.setViewPager(mViewPager, 1);
		return view;
	}

	private void initDatas(View view) {

		Fragment pmFragment = new QizhenFragment_Timer();
		Bundle pmargs = new Bundle();
		pmargs.clear();
		pmargs.putString("title", "定时");
		pmFragment.setArguments(pmargs);
		((QizhenFragment_Timer) pmFragment).setIconId(R.drawable.tab_timer);
		((QizhenFragment_Timer) pmFragment).setTitle("定时");
		mTabs.add(pmFragment);

		Fragment deviceFragment = new QizhenFragment_Control_New();
		Bundle deviceargs = new Bundle();
		deviceargs.putString("title", "控制页");
		deviceFragment.setArguments(deviceargs);
		((QizhenFragment_Control_New) deviceFragment)
				.setIconId(R.drawable.tab_control);
		((QizhenFragment_Control_New) deviceFragment).setTitle("控制");
		mTabs.add(deviceFragment);

		Fragment filterFragment = new QizhenFragment_Filter();
		Bundle filterargs = new Bundle();
		filterargs.putString("title", "滤芯信息");
		filterFragment.setArguments(filterargs);
		((QizhenFragment_Filter) filterFragment)
				.setIconId(R.drawable.tab_filter);
		((QizhenFragment_Filter) filterFragment).setTitle("滤芯");
		mTabs.add(filterFragment);

		Fragment dataFragment = new QizhenFragment_Data();
		Bundle dataargs = new Bundle();
		dataargs.putString("title", "数据");
		dataFragment.setArguments(filterargs);
		((QizhenFragment_Data) dataFragment).setIconId(R.drawable.tab_data);
		((QizhenFragment_Data) dataFragment).setTitle("数据");
		mTabs.add(dataFragment);

	}

	class FragmentAdapter extends FragmentStatePagerAdapter implements
			IconPagerAdapter {
		private List<Fragment> mFragments;

		public FragmentAdapter(List<Fragment> fragments, FragmentManager fm) {
			super(fm);
			mFragments = fragments;
		}

		@Override
		public Fragment getItem(int i) {
			return mFragments.get(i);
		}

		@Override
		public int getIconResId(int index) {

			switch (index) {
			case 0:
				return ((QizhenFragment_Timer) mFragments.get(index))
						.getIconId();
			case 1:
				return ((QizhenFragment_Control_New) mFragments.get(index))
						.getIconId();
			case 2:
				return ((QizhenFragment_Filter) mFragments.get(index))
						.getIconId();
			case 3:
				return ((QizhenFragment_Data) mFragments.get(index))
						.getIconId();
			}
			return index;
		}

		@Override
		public int getCount() {
			return mFragments.size();
		}

		@Override
		public CharSequence getPageTitle(int position) {
			switch (position) {
			case 0:
				return ((QizhenFragment_Timer) mFragments.get(position))
						.getTitle();
			case 1:
				return ((QizhenFragment_Control_New) mFragments.get(position))
						.getTitle();
			case 2:
				return ((QizhenFragment_Filter) mFragments.get(position))
						.getTitle();
			case 3:
				return ((QizhenFragment_Data) mFragments.get(position))
						.getTitle();
			}
			return null;

		}
	}

	private void setOverflowShowingAlways() {
		try {
			// true if a permanent menu key is present, false otherwise.
			ViewConfiguration config = ViewConfiguration.get(getActivity());
			Field menuKeyField = ViewConfiguration.class
					.getDeclaredField("sHasPermanentMenuKey");
			menuKeyField.setAccessible(true);
			menuKeyField.setBoolean(config, false);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private DeviceListActivity getAct() {
		return (DeviceListActivity) getActivity();
	}

	public void showExitDialog() {
		Builder builder = new Builder(getAct());
		builder.setCancelable(false);
		builder.setMessage("退出程序吗？");
		builder.setTitle("提示");
		builder.setNegativeButton("确定",
				new android.content.DialogInterface.OnClickListener() {
					@Override
					public void onClick(DialogInterface dialog, int which) {
						dialog.dismiss();
						XlinkAgent.getInstance().stop();
						getAct().finish();
						System.exit(0);
					}
				});
		builder.setNeutralButton("取消", null);
		builder.create().show();
	}

}
