package com.youhone.device.qizhen;

import io.xlink.wifi.ui.util.XlinkUtils;

import java.util.Timer;
import java.util.TimerTask;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.youhone.activity.MainActivity;
import com.youhone.activity.MainActivity.ReceiveDataListener;
import com.youhone.utils.qizhen.Qizhen_OrderUtils;
import com.youhone.utils.qizhen.Qizhen_Status;
import com.youhone.xlink.superapp.R;

public class QizhenFragment_Filter extends Fragment implements
		ReceiveDataListener {
	private Timer timer;
	private TimerTask timerTask;
	private Button qizhen_buy_filter;
	private Qizhen_Status status;
	private TextView qizhen_filter_left_time;

	public QizhenFragment_Filter() {
	}

	public void onDestroyView() {
		stopTimer();
		super.onDestroyView();
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View contentView = inflater.inflate(R.layout.fragment_qizhen_filter,
				container, false);
		((MainActivity) getActivity()).setOnReceive(this,
				QizhenFragment_Filter.this);
		qizhen_buy_filter = (Button) contentView
				.findViewById(R.id.qizhen_buy_filter);
		qizhen_filter_left_time = (TextView) contentView
				.findViewById(R.id.qizhen_filter_left_time);
		qizhen_buy_filter.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				Intent intent = new Intent();
				intent.setAction("android.intent.action.VIEW");
				Uri content_url = Uri
						.parse("https://wap.koudaitong.com/v2/goods/3f0bh4wmtme3l?reft=1452910795965_1452910810457&spm=f32294156_t76710536");
				intent.setData(content_url);
				startActivity(intent);
			}
		});
		startTimer();
		return contentView;
	}

	// Tab相关
	private String title;
	private int iconId;

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public int getIconId() {
		return iconId;
	}

	public void setIconId(int iconId) {
		this.iconId = iconId;
	}

	/**
	 * 启动定时器,开始倒计时
	 */
	private void startTimer() {
		if (timer == null) {
			timer = new Timer();
		}
		if (timerTask == null) {
			timerTask = new TimerTask() {

				@Override
				public void run() {
					((MainActivity) getActivity()).sendData(Qizhen_OrderUtils
							.getStatus());
				}
			};
			if (timer != null && timerTask != null) {
				timer.schedule(timerTask, 300, 6000);
			}
		}

	}

	/**
	 * 停止定时器
	 */
	private void stopTimer() {
		if (timerTask != null) {
			timerTask.cancel();
			timerTask = null;
		}

		if (timer != null) {
			timer.cancel();
			timer = null;
		}
	}

	@Override
	public void receviceData(byte[] data) {
		System.out.println("收到pipe：" + XlinkUtils.getHexBinString(data));
		status = Qizhen_OrderUtils.receive(data);
		if (status != null) {
			updataStatus(status);
		} else {
			Log.e("", "返回数据错误");
		}
	}

	private void updataStatus(Qizhen_Status status) {
		qizhen_filter_left_time.setText(status.getLvxin_remaining_time() + "");
	}

}
