package com.youhone.device.qizhen;

import io.xlink.wifi.sdk.XDevice;
import io.xlink.wifi.sdk.XlinkAgent;
import io.xlink.wifi.sdk.XlinkCode;
import io.xlink.wifi.sdk.listener.SendPipeListener;
import io.xlink.wifi.ui.activity.ActivityManager;
import io.xlink.wifi.ui.manage.DeviceManage;
import io.xlink.wifi.ui.util.XlinkUtils;

import java.util.Calendar;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.AlertDialog.Builder;
import android.app.TimePickerDialog;
import android.app.TimePickerDialog.OnTimeSetListener;
import android.content.ContentValues;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import com.youhone.bean.TimerItem;
import com.youhone.db.Timer_Db_Helper;
import com.youhone.utils.qizhen.Qizhen_OrderUtils;
import com.youhone.xlink.superapp.R;

public class QizhenActivity_Timer_User extends Activity implements
		OnClickListener {
	private TextView timer_user_timeoff, timer_user_timeon,
			qizhen_timer_model_text, qizhen_timer_gear_text;
	private TextView qizhen_cancel_timer;
	private Button timer_title_back;
	private ImageView timer_on_set, timer_off_set;
	private RelativeLayout qizhen_timer_model, qizhen_timer_gear;
	private EditText qizhen_timer_user_name;
	private Button qizhen_timer_save;
	private Button qizhen_timer_switch;
	private int hour, min;
	private int position = 3;
	private int gear = 0;
	private int model = 0;
	private Intent intent;
	private boolean isadd = false;
	private String[] gears = { "自动档", "一档", "二档", "三档", "四档" };
	private String[] models = { "智能模式", "新风模式", "内循环模式", "恒温模式", "恒温恒湿模式" };
	private boolean time_on_ison = false;
	private boolean time_off_ison = false;
	private boolean time_ison = false;

	@Override
	protected void onCreate(Bundle arg0) {
		super.onCreate(arg0);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.activity_qizhen_timer_user);
		ActivityManager manager = new ActivityManager();
		manager.applyKitKatTranslucency(QizhenActivity_Timer_User.this,
				getResources().getColor(R.color.main_color));
		setResult(RESULT_OK);
		intent = getIntent();

		position = intent.getIntExtra("position", 0);
		isadd = intent.getBooleanExtra("isadd", false);
		time_on_ison = intent.getBooleanExtra("time_on_ison", false);
		time_off_ison = intent.getBooleanExtra("time_off_ison", false);
		qizhen_timer_switch = (Button) findViewById(R.id.qizhen_timer_switch);
		qizhen_cancel_timer = (TextView) findViewById(R.id.qizhen_cancel_timer);
		qizhen_timer_model_text = (TextView) findViewById(R.id.qizhen_timer_model_text);
		qizhen_timer_gear_text = (TextView) findViewById(R.id.qizhen_timer_gear_text);
		qizhen_timer_save = (Button) findViewById(R.id.qizhen_timer_save);
		qizhen_timer_user_name = (EditText) findViewById(R.id.qizhen_timer_user_name);
		qizhen_timer_gear = (RelativeLayout) findViewById(R.id.qizhen_timer_gear);
		qizhen_timer_model = (RelativeLayout) findViewById(R.id.qizhen_timer_model);
		timer_on_set = (ImageView) findViewById(R.id.timer_on_set);
		timer_off_set = (ImageView) findViewById(R.id.timer_off_set);
		timer_user_timeon = (TextView) findViewById(R.id.timer_user_timeon);
		timer_user_timeoff = (TextView) findViewById(R.id.timer_user_timeoff);
		timer_title_back = (Button) findViewById(R.id.timer_title_back);
		qizhen_timer_switch.setOnClickListener(this);
		qizhen_timer_save.setOnClickListener(this);
		timer_on_set.setOnClickListener(this);
		timer_off_set.setOnClickListener(this);
		qizhen_timer_gear.setOnClickListener(this);
		timer_user_timeon.setOnClickListener(this);
		timer_user_timeoff.setOnClickListener(this);
		qizhen_timer_model.setOnClickListener(this);
		timer_title_back.setOnClickListener(this);
		qizhen_cancel_timer.setOnClickListener(this);
		Calendar calendar = Calendar.getInstance();
		hour = calendar.get(Calendar.HOUR_OF_DAY);
		min = calendar.get(Calendar.MINUTE);
		if (!isadd) {
			qizhen_timer_switch.setVisibility(View.VISIBLE);
			qizhen_cancel_timer.setVisibility(View.VISIBLE);
			TimerItem item = new TimerItem();
			item = (TimerItem) intent.getSerializableExtra("timerItem");
			time_on_ison = item.Time_on_ison();
			time_off_ison = item.Time_off_ison();
			Timer_Db_Helper myDBHelper = new Timer_Db_Helper(
					QizhenActivity_Timer_User.this);
			SQLiteDatabase dbwrite = myDBHelper.getWritableDatabase();
			Cursor c = dbwrite.query("qizhen_timer_db", null, null, null, null,
					null, null);
			c.moveToPosition(position);
			timer_user_timeon.setText(c.getString(c.getColumnIndex("time_on")));
			timer_user_timeoff
					.setText(c.getString(c.getColumnIndex("time_off")));
			gear = Integer
					.parseInt(c.getString(c.getColumnIndex("timer_gear")));
			model = Integer.parseInt(c.getString(c
					.getColumnIndex("timer_model")));
			qizhen_timer_model_text.setText(models[model]);
			qizhen_timer_gear_text.setText(gears[gear]);
			qizhen_timer_user_name.setText(c.getString(c
					.getColumnIndex("timer_name")));
			timer_on_set.setSelected(time_on_ison);
			timer_off_set.setSelected(time_off_ison);
			time_ison = item.isOn();
			if (time_ison) {
				qizhen_timer_switch.setText("关闭定时");
			} else {
				qizhen_timer_switch.setText("开启定时");
			}
		} else {
			qizhen_timer_switch.setVisibility(View.GONE);
			qizhen_cancel_timer.setVisibility(View.GONE);
		}
	}

	@Override
	public void onClick(View arg0) {
		switch (arg0.getId()) {
		case R.id.qizhen_timer_gear:
			AlertDialog.Builder builder = new AlertDialog.Builder(
					QizhenActivity_Timer_User.this);
			builder.setTitle("请选择风速");
			builder.setSingleChoiceItems(gears, 0,
					new DialogInterface.OnClickListener() {
						@Override
						public void onClick(DialogInterface dialog, int which) {
							gear = which;
						}
					});
			builder.setPositiveButton("确定",
					new DialogInterface.OnClickListener() {
						@Override
						public void onClick(DialogInterface dialog, int which) {
							qizhen_timer_gear_text.setText(gears[gear]);
						}
					});
			builder.setNegativeButton("取消",
					new DialogInterface.OnClickListener() {
						@Override
						public void onClick(DialogInterface dialog, int which) {
						}
					});
			builder.show();
			break;
		case R.id.qizhen_timer_model:
			AlertDialog.Builder builder_model = new AlertDialog.Builder(
					QizhenActivity_Timer_User.this);
			builder_model.setTitle("请选择风速");
			builder_model.setSingleChoiceItems(models, 0,
					new DialogInterface.OnClickListener() {
						@Override
						public void onClick(DialogInterface dialog, int which) {
							model = which;
						}
					});
			builder_model.setPositiveButton("确定",
					new DialogInterface.OnClickListener() {
						@Override
						public void onClick(DialogInterface dialog, int which) {
							qizhen_timer_model_text.setText(models[model]);
						}
					});
			builder_model.setNegativeButton("取消",
					new DialogInterface.OnClickListener() {
						@Override
						public void onClick(DialogInterface dialog, int which) {
						}
					});
			builder_model.show();
			break;
		case R.id.timer_on_set:
			timer_on_set.setSelected(!timer_on_set.isSelected());
			break;
		case R.id.timer_off_set:
			timer_off_set.setSelected(!timer_off_set.isSelected());
			break;
		case R.id.timer_user_timeon:
			TimePickerDialog datePickerDialog_on = new TimePickerDialog(
					QizhenActivity_Timer_User.this, new OnTimeSetListener() {
						@Override
						public void onTimeSet(TimePicker view, int hourOfDay,
								int minute) {
							timer_user_timeon.setText((hourOfDay < 10 ? "0"
									+ hourOfDay : hourOfDay)
									+ ":"
									+ (minute < 10 ? "0" + minute : minute));
						}
					}, hour, min, true);
			datePickerDialog_on.show();
			break;
		case R.id.timer_user_timeoff:
			TimePickerDialog datePickerDialog_off = new TimePickerDialog(
					QizhenActivity_Timer_User.this, new OnTimeSetListener() {

						@Override
						public void onTimeSet(TimePicker view, int hourOfDay,
								int minute) {
							timer_user_timeoff.setText((hourOfDay < 10 ? "0"
									+ hourOfDay : hourOfDay)
									+ ":"
									+ (minute < 10 ? "0" + minute : minute));

						}
					}, hour, min, true);
			datePickerDialog_off.show();
			break;
		case R.id.qizhen_timer_save:
			Timer_Db_Helper myDBHelper = new Timer_Db_Helper(
					QizhenActivity_Timer_User.this);
			SQLiteDatabase dbwrite = myDBHelper.getWritableDatabase();
			Cursor c = dbwrite.query("qizhen_timer_db", null, null, null, null,
					null, null);
			ContentValues value1 = new ContentValues();
			value1.put("time_on", timer_user_timeon.getText().toString());
			value1.put("time_off", timer_user_timeoff.getText().toString());
			value1.put("ison", "false");
			value1.put("time_on_ison", timer_on_set.isSelected() ? "true"
					: "false");
			value1.put("time_off_ison", timer_off_set.isSelected() ? "true"
					: "false");
			value1.put("timer_name", qizhen_timer_user_name.getText()
					.toString());
			value1.put("timer_note", "完全定制属于您的个性化模式");
			value1.put("timer_model", model);
			value1.put("timer_gear", gear);
			if (isadd) {
				dbwrite.insert("qizhen_timer_db", null, value1);
			} else {
				c.moveToPosition(position);
				dbwrite.update("qizhen_timer_db", value1, "id=?",
						new String[] { c.getString(c.getColumnIndex("id")) });
			}
			c.close();
			dbwrite.close();
			myDBHelper.close();
			Toast.makeText(QizhenActivity_Timer_User.this, "保存成功",
					Toast.LENGTH_SHORT).show();
			// finish();
			break;
		case R.id.qizhen_timer_switch:
			if (time_ison) {// 关闭定时
				closeTimer();
			} else {// 打开定时
				openTimer();
			}
			break;
		case R.id.qizhen_cancel_timer:
			showDeleteDialog(position);
			break;
		case R.id.timer_title_back:
			finish();
			break;

		default:
			break;
		}
	}

	private void showDeleteDialog(final int position) {
		Builder builder = new Builder(QizhenActivity_Timer_User.this);
		builder.setCancelable(false);
		builder.setMessage("确认删除该定时吗？");
		builder.setTitle("提示");
		builder.setNegativeButton("确定",
				new android.content.DialogInterface.OnClickListener() {
					@Override
					public void onClick(DialogInterface dialog, int which) {
						Timer_Db_Helper myDBHelper = new Timer_Db_Helper(
								QizhenActivity_Timer_User.this);
						SQLiteDatabase dbwrite = myDBHelper
								.getWritableDatabase();
						Cursor c = dbwrite.query("qizhen_timer_db", null, null,
								null, null, null, null);
						c.moveToPosition(position);
						dbwrite.delete("qizhen_timer_db", "id=?",
								new String[] { c.getString(c
										.getColumnIndex("id")) });
						c.close();
						dbwrite.close();
						myDBHelper.close();
						finish();
					}
				});
		builder.setNeutralButton("取消", null);
		builder.create().show();
	}

	private void closeTimer() {
		System.out.println("关闭定时");
		qizhen_timer_switch.setText("打开定时");
		Qizhen_OrderUtils.setTimerOnoff((byte) 1);// 关定时
		sendData(Qizhen_OrderUtils.getTimerbyte());
		updateDb(false);
		time_ison = false;
		Toast.makeText(QizhenActivity_Timer_User.this, "关闭定时成功",
				Toast.LENGTH_SHORT).show();
	}

	private void openTimer() {
		// 打开定时
		time_ison = true;
		System.out.println("打开定时");
		Calendar c = Calendar.getInstance();
		int current_hour = c.get(Calendar.HOUR_OF_DAY);
		int current_min = c.get(Calendar.MINUTE);
		int begin_hour = 0, begin_min = 0;
		int end_hour = 0, end_min = 0;
		String timer_on = timer_user_timeon.getText().toString();
		String timer_off = timer_user_timeoff.getText().toString();
		if (timer_on.split(":").length > 1) {
			begin_hour = Integer.parseInt(timer_on.split(":")[0]);
			begin_min = Integer.parseInt(timer_on.split(":")[1]);
		}
		if (timer_off.split(":").length > 1) {
			end_hour = Integer.parseInt(timer_off.split(":")[0]);
			end_min = Integer.parseInt(timer_off.split(":")[1]);
		}
		System.out.println("设定的开机的时间" + timer_on);
		System.out.println("设定的关机的时间" + timer_off);
		// 计算开机的时间
		int send_begin_hour = begin_hour - current_hour;
		int send_begin_min = begin_min - current_min;
		if (send_begin_min < 0) {
			send_begin_min += 60;
			send_begin_hour--;
		}
		if (send_begin_hour < 0) {
			send_begin_hour += 24;
		}
		send_begin_min = (send_begin_min + (send_begin_hour) * 60);
		// 计算关机的时间
		int send_end_hour = end_hour - current_hour;
		int send_end_min = end_min - current_min;
		if (send_end_min < 0) {
			send_end_min += 60;
			send_end_hour--;
		}
		if (send_end_hour < 0) {
			send_end_hour += 24;
		}
		send_end_min = (send_end_min + (send_end_hour) * 60);
		System.out.println("距离开机的时间" + send_begin_min);
		System.out.println("距离关机的时间" + send_end_min);
		boolean time_on_ison = timer_on_set.isSelected() ? true : false;
		boolean time_off_ison = timer_off_set.isSelected() ? true : false;
		Qizhen_OrderUtils.setTimerOnoff((byte) 0);// 开定时
		if (position < 3)// 固定的模式
		{
			System.out.println("固定模式的定时");
			Qizhen_OrderUtils.setTimerOn(send_begin_min);
			Qizhen_OrderUtils.setTimerOff(send_end_min);
			Qizhen_OrderUtils.setTimerGear(0);
		} else {
			if (time_on_ison && time_off_ison) {
				System.out.println("双定");
				System.out.println("模式==" + model + "   档位=" + gear);
				Qizhen_OrderUtils.setTimerOn(send_begin_min);
				Qizhen_OrderUtils.setTimerOff(send_end_min);
				Qizhen_OrderUtils.setTimerGear(gear);
			}
			if (time_on_ison && !time_off_ison) {
				System.out.println("只定时开");
				Qizhen_OrderUtils.setTimerOn(send_begin_min);
				Qizhen_OrderUtils.setTimerOff(0);
				Qizhen_OrderUtils.setTimerGear(gear);
			}

			if (time_off_ison && !time_on_ison) {
				System.out.println("只定时关");
				Qizhen_OrderUtils.setTimerOn(0);
				Qizhen_OrderUtils.setTimerOff(send_end_min);
				Qizhen_OrderUtils.setTimerGear(gear);
			}
		}
		if (time_on_ison || time_off_ison)
			sendData(Qizhen_OrderUtils.getTimerbyte());
		updateDb(true);
		finish();
	}

	private void updateDb(boolean onoff) {
		Timer_Db_Helper myDBHelper = new Timer_Db_Helper(
				QizhenActivity_Timer_User.this);
		SQLiteDatabase dbwrite = myDBHelper.getWritableDatabase();
		Cursor cursor = dbwrite.query("qizhen_timer_db", null, null, null,
				null, null, null);
		ContentValues value = new ContentValues();
		while (cursor.moveToNext()) {
			if (cursor.getPosition() == position) {
				value.clear();
				value.put("ison", onoff ? "true" : "false");
				System.out.println("更新数据库" + value.getAsString("ison"));
				dbwrite.update("qizhen_timer_db", value, "id=?",
						new String[] { cursor.getString(cursor
								.getColumnIndex("id")) });
			} else {
				value.clear();
				value.put("ison", "false");
				dbwrite.update("qizhen_timer_db", value, "id=?",
						new String[] { cursor.getString(cursor
								.getColumnIndex("id")) });
			}
		}

		cursor.close();
		dbwrite.close();
		myDBHelper.close();
	}

	public boolean sendData(final byte[] bs) {
		System.out.println("发送命令");
		for (byte b : bs) {
			System.out.format("%02X ", b);
		}
		System.out.println("");
		int ret = XlinkAgent.getInstance().sendPipeData(
				DeviceManage.getInstance().getDevices().get(0).getXDevice(),
				bs, new SendPipeListener() {
					@Override
					public void onSendLocalPipeData(XDevice xdevice, int code,
							int msgId) {
						// setDeviceStatus(false);
						switch (code) {
						case XlinkCode.SUCCEED:
							Log.e("model",
									"发送数据" + XlinkUtils.getHexBinString(bs)
											+ "成功");
							break;
						case XlinkCode.TIMEOUT:
							// 重新调用connect
							Log.e("model",
									"发送数据超时：" + XlinkUtils.getHexBinString(bs));
							// XlinkUtils.shortTips("发送数据超时："
							// + );

							break;
						case XlinkCode.SERVER_CODE_UNAUTHORIZED:
							// XlinkUtils.shortTips("控制设备失败,当前帐号未订阅此设备，请重新订阅");
							Log.e("model", "控制设备失败,当前帐号未订阅此设备，请重新订阅");

							break;
						case XlinkCode.SERVER_DEVICE_OFFLIEN:
							XlinkUtils.shortTips("设备不在线");
							break;
						default:
							XlinkUtils.shortTips("控制设备其他错误码:" + code);
							Log.e("model", "控制设备其他错误码:" + code);
							break;
						}
					}
				});
		if (ret < 0) {

			Log.e("model", "发送数据失败，错误码：" + ret);
			return false;
		}
		return true;
	}
}
