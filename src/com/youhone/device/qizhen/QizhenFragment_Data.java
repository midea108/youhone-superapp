package com.youhone.device.qizhen;

import io.xlink.wifi.ui.util.XlinkUtils;

import java.math.BigDecimal;
import java.util.Timer;
import java.util.TimerTask;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.youhone.activity.MainActivity;
import com.youhone.activity.MainActivity.ReceiveDataListener;
import com.youhone.utils.qizhen.Qizhen_OrderUtils;
import com.youhone.utils.qizhen.Qizhen_Status;
import com.youhone.xlink.superapp.R;

public class QizhenFragment_Data extends Fragment implements
		ReceiveDataListener {
	private Timer timer;
	private TimerTask timerTask;
	private Qizhen_Status status;
	private TextView data_pm25_weight, data_rice, data_left_time,
			data_work_time, data_pm25_volume, data_air_box, data_air_volume,
			data_pm25_unit, data_pm25_unit2;
	private ImageView data_p1, data_p2, data_p3, data_p4, data_p5, data_p6,
			data_p7, data_p8, data_p9, data_p10, data_p11, data_p12, data_p13,
			data_p14, data_p15, data_p16, data_p17, data_p18, data_p19,
			data_p20, data_p21, data_p22, data_p23, data_p24, data_p25,
			data_p26, data_p27, data_p28, data_p29, data_p30;
	private ImageView imgs[] = { data_p1, data_p2, data_p3, data_p4, data_p5,
			data_p6, data_p7, data_p8, data_p9, data_p10, data_p11, data_p12,
			data_p13, data_p14, data_p15, data_p16, data_p17, data_p18,
			data_p19, data_p20, data_p21, data_p22, data_p23, data_p24,
			data_p25, data_p26, data_p27, data_p28, data_p29, data_p30 };
	private int Img_ids[] = { R.id.data_p1, R.id.data_p2, R.id.data_p3,
			R.id.data_p4, R.id.data_p5, R.id.data_p6, R.id.data_p7,
			R.id.data_p8, R.id.data_p9, R.id.data_p10, R.id.data_p11,
			R.id.data_p12, R.id.data_p13, R.id.data_p14, R.id.data_p15,
			R.id.data_p16, R.id.data_p17, R.id.data_p18, R.id.data_p19,
			R.id.data_p20, R.id.data_p21, R.id.data_p22, R.id.data_p23,
			R.id.data_p24, R.id.data_p25, R.id.data_p26, R.id.data_p27,
			R.id.data_p28, R.id.data_p29, R.id.data_p30 };

	public QizhenFragment_Data() {
	}

	public void onDestroyView() {
		stopTimer();
		super.onDestroyView();
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View contentView = inflater.inflate(R.layout.fragment_qizhen_data,
				container, false);
		((MainActivity) getActivity()).setOnReceive(this,
				QizhenFragment_Data.this);
		data_pm25_weight = (TextView) contentView
				.findViewById(R.id.data_pm25_weight);
		data_rice = (TextView) contentView.findViewById(R.id.data_rice);
		data_work_time = (TextView) contentView
				.findViewById(R.id.data_work_time);
		data_left_time = (TextView) contentView
				.findViewById(R.id.data_left_time);
		data_pm25_volume = (TextView) contentView
				.findViewById(R.id.data_pm25_volume);
		data_air_box = (TextView) contentView.findViewById(R.id.data_air_box);
		data_air_volume = (TextView) contentView
				.findViewById(R.id.data_air_volume);
		data_pm25_unit = (TextView) contentView
				.findViewById(R.id.data_pm25_unit);
		data_pm25_unit2 = (TextView) contentView
				.findViewById(R.id.data_pm25_unit2);
		for (int i = 0; i < imgs.length; i++) {
			imgs[i] = (ImageView) contentView.findViewById(Img_ids[i]);
		}
		startTimer();
		return contentView;
	}

	// Tab相关
	private String title;
	private int iconId;

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public int getIconId() {
		return iconId;
	}

	public void setIconId(int iconId) {
		this.iconId = iconId;
	}

	/**
	 * 启动定时器,开始倒计时
	 */
	private void startTimer() {
		if (timer == null) {
			timer = new Timer();
		}
		if (timerTask == null) {
			timerTask = new TimerTask() {

				@Override
				public void run() {
					((MainActivity) getActivity()).sendData(Qizhen_OrderUtils
							.getStatus());
				}
			};
			if (timer != null && timerTask != null) {
				timer.schedule(timerTask, 300, 6000);
			}
		}

	}

	/**
	 * 停止定时器
	 */
	private void stopTimer() {
		if (timerTask != null) {
			timerTask.cancel();
			timerTask = null;
		}

		if (timer != null) {
			timer.cancel();
			timer = null;
		}
	}

	@Override
	public void receviceData(byte[] data) {
		System.out.println("收到pipe：" + XlinkUtils.getHexBinString(data));
		status = Qizhen_OrderUtils.receive(data);
		if (status != null) {
			updataStatus(status);
		} else {
			Log.e("", "返回数据错误");
		}

	}

	private void updataStatus(Qizhen_Status status) {
		updataWorkTime(status.getWorkTime());
		data_work_time.setText(status.getWorkTime() + "");
		data_left_time.setText(status.getLvxin_remaining_time() + "");
		// System.out.println("分数+"+status.getLvxin_remaining_time() /
		// MainActivity.QIZHEN_LVXIN_WORKTIM);
		int left_img = (status.getLvxin_remaining_time() * 30 / MainActivity.QIZHEN_LVXIN_WORKTIM);
		System.out.println("剩余的格数" + left_img);
		for (int i = 0; i < imgs.length; i++) {
			if (i < left_img)
				imgs[i].setImageResource(R.drawable.data_progress_blue);
			else
				imgs[i].setImageResource(R.drawable.data_progress_gray);
		}
	}

	private void updataWorkTime(int time) {
		BigDecimal bd = new BigDecimal(time);
		double hour = bd.setScale(2, BigDecimal.ROUND_HALF_UP).doubleValue();
		data_pm25_weight.setText(new BigDecimal(hour * 17).setScale(2,
				BigDecimal.ROUND_HALF_UP).doubleValue()
				+ "");
		data_rice.setText("——相当于"
				+ new BigDecimal(hour * 17 / 20).setScale(1,
						BigDecimal.ROUND_HALF_UP).doubleValue() + "颗大米——");
		data_work_time.setText(hour + "");
		// 立方米
		double pm25_volume = new BigDecimal(hour * 200).setScale(2,
				BigDecimal.ROUND_HALF_UP).doubleValue();
		System.out.println("立方米" + pm25_volume);
		double box = new BigDecimal(pm25_volume / 26).setScale(2,
				BigDecimal.ROUND_HALF_UP).doubleValue();
		data_air_box.setText(box + "");
		if (pm25_volume > 100000) {
			pm25_volume = pm25_volume / 10000;
			data_pm25_unit.setText("万立方米");
			data_pm25_unit2.setText("万立方米");
		} else {
			data_pm25_unit.setText("立方米");
			data_pm25_unit2.setText("立方米");
		}
		data_air_volume.setText(pm25_volume + "");
		data_pm25_volume.setText(new BigDecimal(pm25_volume).setScale(1,
				BigDecimal.ROUND_HALF_UP).doubleValue()
				+ "");
	}
}
