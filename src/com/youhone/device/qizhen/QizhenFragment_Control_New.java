package com.youhone.device.qizhen;

import io.xlink.wifi.ui.Constant;
import io.xlink.wifi.ui.util.XlinkUtils;

import java.math.BigDecimal;
import java.util.Timer;
import java.util.TimerTask;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.youhone.activity.MainActivity;
import com.youhone.activity.MainActivity.ReceiveDataListener;
import com.youhone.utils.qizhen.Qizhen_OrderUtils;
import com.youhone.utils.qizhen.Qizhen_Static;
import com.youhone.utils.qizhen.Qizhen_Status;
import com.youhone.xlink.superapp.R;

public class QizhenFragment_Control_New extends Fragment implements
		ReceiveDataListener {
	private int gear = 0;
	private Button btn_onoff;
	private boolean isMore = false;
	private ImageView gear1_img, gear2_img, gear3_img, gear4_img, auto_img,
			more_img;
	private ImageView[] gear_imgs = new ImageView[6];
	private int[] gearImgOn_ids = new int[] { R.drawable.qizhen_auto_on,
			R.drawable.qizhen_gear1_on, R.drawable.qizhen_gear2_on,
			R.drawable.qizhen_gear3_on, R.drawable.qizhen_gear4_on,
			R.drawable.qizhen_more_on };

	private int[] gearImgOff_ids = new int[] { R.drawable.qizhen_auto_off,
			R.drawable.qizhen_gear1_off, R.drawable.qizhen_gear2_off,
			R.drawable.qizhen_gear3_off, R.drawable.qizhen_gear4_off,
			R.drawable.qizhen_more_on };
	private LinearLayout back_lay;
	private ImageView back_img;
	private TextView pm25_value, temp_in, shidu_in, pm25_weight, qizhen_rice;
	private ImageView qizhen_quality;
	private TextView qizhen_tip1, qizhen_tip2;
	private int pm25 = 0;
	private BtnListener btnlistener = new BtnListener();
	private Qizhen_Status status;
	private boolean isOn = false;
	private boolean isNegative = false;
	private boolean isSleep = false;
	private boolean isLock = false;
	private boolean isShajun = false;
	private boolean isFanOn = false;
	private boolean isLightOn = false;

	private Timer timer;
	private TimerTask timerTask;

	private Toast t;

	@Override
	@Nullable
	public View onCreateView(LayoutInflater inflater,
			@Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
		View contentView = inflater.inflate(
				R.layout.fragment_qizhen_control_new, container, false);
		initView(contentView);
		((MainActivity) getActivity()).setOnReceive(this,
				QizhenFragment_Control_New.this);
		startTimer();
		return contentView;
	}

	private void initView(View contentView) {
		qizhen_quality = (ImageView) contentView
				.findViewById(R.id.qizhen_quality);
		back_lay = (LinearLayout) contentView.findViewById(R.id.back_lay);
		back_img = (ImageView) contentView.findViewById(R.id.back_img);
		qizhen_rice = (TextView) contentView.findViewById(R.id.qizhen_rice);
		pm25_weight = (TextView) contentView.findViewById(R.id.pm25_weight);
		qizhen_tip1 = (TextView) contentView.findViewById(R.id.qizhen_tip1);
		qizhen_tip2 = (TextView) contentView.findViewById(R.id.qizhen_tip2);
		shidu_in = (TextView) contentView.findViewById(R.id.shidu_in);
		temp_in = (TextView) contentView.findViewById(R.id.temp_in);
		pm25_value = (TextView) contentView.findViewById(R.id.pm25_value);
		btn_onoff = (Button) contentView.findViewById(R.id.device_onoff);
		auto_img = (ImageView) contentView.findViewById(R.id.auto_img);
		gear1_img = (ImageView) contentView.findViewById(R.id.gear1_img);
		gear2_img = (ImageView) contentView.findViewById(R.id.gear2_img);
		gear3_img = (ImageView) contentView.findViewById(R.id.gear3_img);
		gear4_img = (ImageView) contentView.findViewById(R.id.gear4_img);
		more_img = (ImageView) contentView.findViewById(R.id.more_img);
		gear_imgs[1] = gear1_img;
		gear_imgs[2] = gear2_img;
		gear_imgs[3] = gear3_img;
		gear_imgs[4] = gear4_img;
		gear_imgs[0] = auto_img;
		gear_imgs[5] = more_img;
		btn_onoff.setOnClickListener(btnlistener);
		auto_img.setOnClickListener(btnlistener);
		gear1_img.setOnClickListener(btnlistener);
		gear2_img.setOnClickListener(btnlistener);
		gear3_img.setOnClickListener(btnlistener);
		gear4_img.setOnClickListener(btnlistener);
		more_img.setOnClickListener(btnlistener);
		back_img.setOnClickListener(btnlistener);
		// updateGearLay(gear, isOn);
		if (isOn) {
			btn_onoff.setBackgroundResource(R.drawable.b_xinfeng_off);
		} else {
			btn_onoff.setBackgroundResource(R.drawable.b_xinfeng_on);
		}
		t = new Toast(getActivity());

	}

	@Override
	public void onActivityCreated(@Nullable Bundle savedInstanceState) {

		super.onActivityCreated(savedInstanceState);
	}

	public void onResume() {
		super.onResume();
	}

	public void onDestroyView() {
		stopTimer();
		super.onDestroyView();
	}

	private void showOffLine() {
		t.cancel();
		t.makeText(getActivity(), "请先开启设备", Toast.LENGTH_SHORT).show();
	}

	private class BtnListener implements OnClickListener {
		@Override
		public void onClick(View v) {

			switch (v.getId()) {
			case R.id.device_onoff:
				if (isOn) {
					btn_onoff.setBackgroundResource(R.drawable.b_xinfeng_on);
					sendOFF();
				} else {
					sendON();
					btn_onoff.setBackgroundResource(R.drawable.b_xinfeng_off);
				}
				isOn = !isOn;
				break;
			case R.id.more_img:
				if (isMore) {// 灯带
					if (isOn) {
						isLightOn = !isLightOn;
						sendLight(isLightOn);
						updateLightLay(isLightOn, isOn);
					} else {
						showOffLine();
					}
				} else {
					isMore = true;
					back_lay.setVisibility(View.VISIBLE);
					updateView();
				}
				break;
			case R.id.gear1_img:
				if (isOn) {
					if (isMore) {// 童锁
						isLock = !isLock;
						sendLock(isLock);
						updateLock(isLock, isOn);
					} else {
						gear = 1;
						sendGear(gear);
						updateGearLay(gear, isOn);
					}
				} else {
					showOffLine();
				}
				break;
			case R.id.gear2_img:
				if (isOn) {
					if (isMore) {// 睡眠
						isSleep = !isSleep;
						sendSleep(isSleep);
						updateSleepLay(isSleep, isOn);
					} else {
						gear = 2;
						sendGear(gear);
						updateGearLay(gear, isOn);
					}
				} else {
					showOffLine();
				}
				break;
			case R.id.gear3_img:
				if (isOn) {
					if (isMore) {// 负离子
						isNegative = !isNegative;
						sendNegative(isNegative);
						updateNegativeLay(isNegative, isOn);
					} else {
						gear = 3;
						sendGear(gear);
						updateGearLay(gear, isOn);
					}

				} else {
					showOffLine();
				}
				break;
			case R.id.gear4_img:
				if (isOn) {
					if (isMore) {// 杀菌
						isShajun = !isShajun;
						sendShajun(isShajun);
						updateShajunLay(isShajun, isOn);
					} else {
						gear = 4;
						sendGear(gear);
						updateGearLay(gear, isOn);
					}
				} else {
					showOffLine();
				}
				break;
			case R.id.auto_img:
				if (isOn) {
					if (isMore) {// 风叶
						isFanOn = !isFanOn;
						sendFan(isFanOn);
						updateFanLay(isFanOn, isOn);
					} else {
						gear = 0;
						sendGear(gear);
						updateGearLay(gear, isOn);
					}
				} else {
					showOffLine();
				}
				break;
			case R.id.back_img:
				back_lay.setVisibility(View.GONE);
				isMore = false;
				updateGearLay(gear, isOn);
				break;

			}
		}
	}

	private void sendGear(int gear) {
		((MainActivity) getActivity()).sendData(Qizhen_OrderUtils.SendBase(
				Qizhen_Static.GEAR, (byte) gear));

	}

	private void sendSleep(boolean isSleep) {
		((MainActivity) getActivity()).sendData(Qizhen_OrderUtils.SendBase(
				Qizhen_Static.SLEEP, isSleep ? (byte) 1 : 0));
	}

	private void sendLock(boolean isLock) {
		((MainActivity) getActivity()).sendData(Qizhen_OrderUtils.SendBase(
				Qizhen_Static.LOCK, isLock ? (byte) 1 : 0));
	}

	private void sendNegative(boolean negative) {
		((MainActivity) getActivity()).sendData(Qizhen_OrderUtils.SendBase(
				Qizhen_Static.NEGATIVE, negative ? (byte) 1 : 0));
	}

	private void sendShajun(boolean shajun) {
		((MainActivity) getActivity()).sendData(Qizhen_OrderUtils.SendBase(
				Qizhen_Static.STERILIZATION, shajun ? (byte) 1 : 0));
	}

	private void sendFan(boolean fan) {
		((MainActivity) getActivity()).sendData(Qizhen_OrderUtils.SendBase(
				Qizhen_Static.FAN, fan ? (byte) 1 : 0));
	}

	private void sendLight(boolean light) {
		((MainActivity) getActivity()).sendData(Qizhen_OrderUtils.SendBase(
				Qizhen_Static.LIGHT, light ? (byte) 1 : 0));
	}

	/**
	 * 发送开机命令
	 */
	private void sendON() {
		((MainActivity) getActivity()).sendData(Qizhen_OrderUtils.SendBase(
				Qizhen_Static.DEVICE_SWITCH, (byte) 1));
	}

	/**
	 * 发送关机命令
	 */
	private void sendOFF() {
		((MainActivity) getActivity()).sendData(Qizhen_OrderUtils.SendBase(
				Qizhen_Static.DEVICE_SWITCH, (byte) 0));
	}

	@Override
	public void receviceData(byte[] data) {
		System.out.println("收到pipe：" + XlinkUtils.getHexBinString(data));
		status = Qizhen_OrderUtils.receive(data);
		if (status != null) {
			synchronized (status) {
				if (getActivity() != null)
					updataStatus(status);
			}
		} else {
			Log.e("", "返回数据错误");
		}
	}

	private void updataStatus(Qizhen_Status status) {
		isOn = status.isOn();
		if (isOn) {
			temp_in.setText(status.getTemp() + "℃");
			shidu_in.setText(status.getHumidity() + "％");
			pm25_value.setText(status.getPm25() + "");
		} else {
			temp_in.setText("- -");
			shidu_in.setText("- -");
			pm25_value.setText("--");
		}
		pm25 = status.getPm25();
		isLock = status.isTongsuoOn();
		isSleep = status.isSleep();
		isNegative = status.isNegativeOn();
		isShajun = status.isSterilizeOn();
		isFanOn = status.isFanOn();
		isLightOn = status.isLightOn();
		gear = status.getGear();
		setTips(getTips(status));
		updataWorkTime(status.getWorkTime());
		updateView();
	}

	private void updateView() {
		if (isOn) {
			btn_onoff.setBackgroundResource(R.drawable.b_xinfeng_off);
		} else {
			btn_onoff.setBackgroundResource(R.drawable.b_xinfeng_on);
		}
		updataQualityLay(pm25);
		if (isMore) {
			back_lay.setVisibility(View.VISIBLE);
			updateLock(isLock, isOn);
			updateSleepLay(isSleep, isOn);
			updateNegativeLay(isNegative, isOn);
			updateShajunLay(isShajun, isOn);
			updateFanLay(isFanOn, isOn);
			updateLightLay(isLightOn, isOn);
		} else {
			back_lay.setVisibility(View.GONE);
			if (gear < 5 && gear >= 0) {
				updateGearLay(gear, isOn);
			}
		}
	}

	private void updataWorkTime(int time) {
		BigDecimal bd = new BigDecimal(time);
		double hour = bd.setScale(2, BigDecimal.ROUND_HALF_UP).doubleValue();
		pm25_weight.setText(new BigDecimal(hour * 17).setScale(2,
				BigDecimal.ROUND_HALF_UP).doubleValue()
				+ "");
		qizhen_rice.setText("——相当于"
				+ new BigDecimal(hour * 17 / 20).setScale(1,
						BigDecimal.ROUND_HALF_UP).doubleValue() + "颗大米——");
	}

	private void updateLock(boolean on, boolean enable) {// 一档位置
		if (enable) {
			gear1_img.setImageResource(on ? R.drawable.qizhen_lock_on
					: R.drawable.qizhen_lock_off);
		} else {
			gear1_img.setImageResource(R.drawable.qizhen_lock_off);
		}
	}

	private void updateNegativeLay(boolean on, boolean enable) {// 3档位置
		if (enable) {
			gear3_img.setImageResource(on ? R.drawable.qizhen_negative_on
					: R.drawable.qizhen_negative_off);
		} else {
			gear3_img.setImageResource(R.drawable.qizhen_negative_off);
		}
	}

	private void updateSleepLay(boolean on, boolean enable) {// 2档位置
		System.out.println("睡眠更新状态" + enable);
		if (enable) {
			gear2_img.setImageResource(on ? R.drawable.qizhen_sleep_on
					: R.drawable.qizhen_sleep_off);
		} else {
			gear2_img.setImageResource(R.drawable.qizhen_sleep_off);
		}
	}

	private void updateShajunLay(boolean on, boolean enable) {// 4档位置
		if (enable) {
			gear4_img.setImageResource(on ? R.drawable.qizhen_shajun_on
					: R.drawable.qizhen_shajun_off);
		} else {
			gear4_img.setImageResource(R.drawable.qizhen_shajun_off);
		}
	}

	private void updateFanLay(boolean on, boolean enable) {// 自动档位置
		if (enable) {
			auto_img.setImageResource(on ? R.drawable.qizhen_fan_on
					: R.drawable.qizhen_fan_off);
		} else {
			auto_img.setImageResource(R.drawable.qizhen_fan_off);
		}
	}

	private void updateLightLay(boolean on, boolean enable) {// 更多位置
		if (enable) {
			more_img.setImageResource(on ? R.drawable.qizhen_light_on
					: R.drawable.qizhen_light_off);
		} else {
			more_img.setImageResource(R.drawable.qizhen_light_off);
		}
	}

	private void updateGearLay(int gear, boolean enable) {
		for (int i = 0; i < gear_imgs.length; i++) {
			if (enable) {
				if (i != (gear)) {
					gear_imgs[i].setImageResource(gearImgOff_ids[i]);
				} else {
					gear_imgs[i].setImageResource(gearImgOn_ids[i]);
				}
			} else {
				gear_imgs[i].setImageResource(gearImgOff_ids[i]);
			}
		}
	}

	private void updataQualityLay(int quality) {
		if (quality < 50) {
			qizhen_quality.setImageResource(R.drawable.qizhen_you);
		} else if (quality < 90) {
			qizhen_quality.setImageResource(R.drawable.qizhen_liang);
		} else if (quality < 120) {
			qizhen_quality.setImageResource(R.drawable.qizhen_qingdu);
		} else if (quality < 150) {
			qizhen_quality.setImageResource(R.drawable.qizhen_middle);
		} else if (quality < 250) {
			qizhen_quality.setImageResource(R.drawable.qizhen_zhongdu);
		} else if (quality >= 250) {
			qizhen_quality.setImageResource(R.drawable.qizhen_yanzhong);
		}
	}

	private void setTips(String tips) {
		System.out.println("使用建议：" + tips);
		String[] array = tips.split("、");
		if (array.length == 2) {
			qizhen_tip1.setText(array[0]);
			qizhen_tip2.setText(array[1]);
		}
	}

	private String getTips(Qizhen_Status status) {
		String tips = "";
		int pm25 = status.getPm25();
		if (isOn && pm25 < 50 && Constant.WEATHER_QUALITY == 1) {
			tips = "室内空气洁净、请保持净化器开启";
		} else if (isOn && (pm25 >= 50 && pm25 < 120)
				&& Constant.WEATHER_QUALITY == 1) {
			tips = "室内空气洁净、请保持净化器开启";
		} else if (isOn && (pm25 >= 50 && pm25 < 120)
				&& Constant.WEATHER_QUALITY == 2) {
			tips = "室内空气良好、请保持净化器开启";
		} else if (isOn && pm25 < 50 && Constant.WEATHER_QUALITY == 2) {
			tips = "室内空气洁净、请保持净化器开启";
		} else if (isOn && pm25 < 120 && Constant.WEATHER_QUALITY == 3) {
			tips = "关闭门窗、请保持净化器开启";
		} else if (isOn && pm25 < 120 && Constant.WEATHER_QUALITY == 4) {
			tips = "关闭门窗、请保持净化器开启";
		} else if (isOn && pm25 < 120 && Constant.WEATHER_QUALITY == 5) {
			tips = "关闭门窗、请保持净化器开启";
		} else if (isOn && pm25 < 120 && Constant.WEATHER_QUALITY == 6) {
			tips = "关闭门窗、请保持净化器开启";
		} else if (isOn && pm25 >= 150) {
			tips = "室内空气一般、请保持净化器运行";
		} else if (!isOn
				&& (Constant.WEATHER_QUALITY == 1 || Constant.WEATHER_QUALITY == 2)) {
			tips = "暂无数据、  ";
		} else if (!isOn && Constant.WEATHER_QUALITY == 3) {
			tips = "室外空气轻度污染、请开启空气净化器";
		} else if (!isOn && Constant.WEATHER_QUALITY == 4) {
			tips = "室外空气中度污染、请开启空气净化器";
		} else if (!isOn && Constant.WEATHER_QUALITY == 5) {
			tips = "室外空气重度污染、请开启空气净化器";
		} else if (!isOn && Constant.WEATHER_QUALITY == 6) {
			tips = "室外空气严重污染、请开启空气净化器";
		} else if (isOn && Constant.WEATHER_QUALITY == 6 && pm25 < 90) {
			tips = "室外空气严重污染、请停止户外活动";
		} else {
			tips = "室内空气清新、请保持净化器运行";
		}
		return tips;
	}

	/**
	 * 启动定时器,开始倒计时
	 */
	private void startTimer() {
		if (timer == null) {
			timer = new Timer();
		}
		if (timerTask == null) {
			timerTask = new TimerTask() {

				@Override
				public void run() {
					((MainActivity) getActivity()).sendData(Qizhen_OrderUtils
							.getStatus());
				}
			};
			if (timer != null && timerTask != null) {
				timer.schedule(timerTask, 100, 6000);
			}
		}

	}

	/**
	 * 停止定时器
	 */
	private void stopTimer() {
		if (timerTask != null) {
			timerTask.cancel();
			timerTask = null;
		}

		if (timer != null) {
			timer.cancel();
			timer = null;
		}
	}

	// Tab相关
	private String title;
	private int iconId;

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public int getIconId() {
		return iconId;
	}

	public void setIconId(int iconId) {
		this.iconId = iconId;
	}
}
