package com.youhone.device.tianjun;

import io.xlink.wifi.ui.util.XlinkUtils;

import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.youhone.activity.MainActivity.ReceiveDataListener;
import com.youhone.utils.tianjun.DensityUtils;
import com.youhone.utils.tianjun.FanLight_OrderUtils;
import com.youhone.utils.tianjun.FanLight_Static;
import com.youhone.utils.tianjun.FanLight_Status;
import com.youhone.utils.tianjun.Utils;
import com.youhone.widget.RadialMenuWidget;
import com.youhone.widget.RadialMenuWidget.RadialMenuEntry;
import com.youhone.xlink.superapp.R;

public class FanLightFragment extends Fragment implements ReceiveDataListener {
	private static final int TIMEOUT_MAX = 90;
	private int timeout = 0;
	private Button btn_switch_light;
	private Button btn_switch_fan;
	private BtnListener btnlistener = new BtnListener();
	private RadialMenuWidget menu;
	private InitBtn high, middle, low, off;
	private Button times[] = new Button[4];
	private boolean onoff = false;
	private Button btn_onoff;
	private FanLight_Status status;
	private TextView left_time;
	private int timeHour, timeMin, timeSec;
	private Timer timer;
	private TimerTask timerTask;
	private int fan_mode = 3;

	@Override
	@Nullable
	public View onCreateView(LayoutInflater inflater,
			@Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
		View contentView = inflater.inflate(R.layout.fragment_fan_light,
				container, false);
		left_time = (TextView) contentView.findViewById(R.id.left_time);
		btn_onoff = (Button) contentView.findViewById(R.id.btn_onoff);
		btn_switch_light = (Button) contentView
				.findViewById(R.id.btn_switch_light);
		btn_switch_fan = (Button) contentView.findViewById(R.id.btn_switch_fan);
		btn_onoff.setOnClickListener(btnlistener);
		btn_switch_light.setOnClickListener(btnlistener);
		btn_switch_fan.setOnClickListener(btnlistener);
		initBtn(contentView);
		((com.youhone.activity.MainActivity) getActivity()).setOnReceive(this,
				FanLightFragment.this);
		return contentView;
	}

	private void initBtn(View contentView) {
		times[0] = (Button) contentView.findViewById(R.id.timer_1h);
		times[1] = (Button) contentView.findViewById(R.id.timer_2h);
		times[2] = (Button) contentView.findViewById(R.id.timer_4h);
		times[3] = (Button) contentView.findViewById(R.id.timer_8h);
		times[0].setOnClickListener(btnlistener);
		times[1].setOnClickListener(btnlistener);
		times[2].setOnClickListener(btnlistener);
		times[3].setOnClickListener(btnlistener);
	}

	@Override
	public void onActivityCreated(@Nullable Bundle savedInstanceState) {
		menu = new RadialMenuWidget(getActivity());
		addPieMenuOpen(getView());
		// menu.setEnable(onoff);
		super.onActivityCreated(savedInstanceState);
	}

	public void onResume() {
		super.onResume();
	}

	public void onDestroyView() {
		super.onDestroyView();
		stopTimer();
	}

	private class BtnListener implements OnClickListener {
		@Override
		public void onClick(View v) {
			if (v.getId() == R.id.btn_onoff) {
				if (onoff) {
					fan_mode = 0;
					sendOFF();
					onoff = false;
					// menu.setEnable(onoff);
					btn_switch_light.setSelected(false);
					btn_switch_fan.setSelected(false);
					menu.ResetState();
					cleanBtnState();
					addPieMenuOpen(getView());
				} else {
					sendON();
					onoff = true;
					// menu.setEnable(onoff);
					if (!btn_switch_fan.isSelected()) {
						fan_mode = 3;
						menu.setTop();
					}
					btn_switch_light.setSelected(true);
					btn_switch_fan.setSelected(true);
					// addPieMenuClose(getView());
				}
				return;

			} else if (v.getId() == R.id.btn_switch_light) {
				if (btn_switch_light.isSelected()) {
					light_off();
					onoff = false;
					if (!btn_switch_fan.isSelected()) {
						cleanBtnState();
					}
				} else {
					light_on();
					if (btn_switch_light.isSelected()) {
						onoff = true;
					}
				}
				btn_switch_light.setSelected(!btn_switch_light.isSelected());
				return;

			} else if (v.getId() == R.id.btn_switch_fan) {
				if (btn_switch_fan.isSelected()) {
					Log.d("  ", "off   " + fan_mode);
					fan_off();
					onoff = false;
					menu.ResetState();
					// menu.setEnable(false);
					if (!btn_switch_light.isSelected()) {
						cleanBtnState();
					}
				} else {
					Log.d("  ", "on   " + fan_mode);
					fan_on();
					// setFan(fan_mode);
					// menu.setMiddle();
					switch (fan_mode) {
					case 1:
						menu.setBottom();
						break;
					case 2:
						menu.setMiddle();
						break;
					case 3:
						menu.setTop();
						break;

					default:
						break;
					}
					// menu.setEnable(true);
					if (btn_switch_light.isSelected()) {
						onoff = true;
					}
				}

				btn_switch_fan.setSelected(!btn_switch_fan.isSelected());
				Log.e("  ", btn_switch_fan.isSelected() + "");
				return;
			} else {

				if (btn_switch_fan.isSelected()
						|| btn_switch_light.isSelected()) {
					switch (v.getId()) {
					case R.id.timer_1h:
						setBtnState(R.id.timer_1h, false);
						setTimer(1);
						break;
					case R.id.timer_2h:
						setBtnState(R.id.timer_2h, false);
						setTimer(2);
						break;
					case R.id.timer_4h:
						setBtnState(R.id.timer_4h, false);
						setTimer(4);
						break;
					case R.id.timer_8h:
						setBtnState(R.id.timer_8h, false);
						setTimer(8);
						break;
					default:
						break;
					}
				} else {
					XlinkUtils.showShortToast("请先打开设备");
				}
			}
		}
	}

	/**
	 * 关机状态的圆圈菜单，中间放开机按钮
	 * 
	 * @param contentView
	 */
	private void addPieMenuOpen(View contentView) {
		ViewGroup layout_top = (ViewGroup) contentView
				.findViewById(R.id.layout_top);
		layout_top.removeAllViewsInLayout();
		menu.removeAllView();
		off = new InitBtn(null);
		off.setIcon(R.drawable.logo);
		high = new InitBtn("低");
		middle = new InitBtn("中");
		low = new InitBtn("高");
		// 标题栏高度60dp
		int labelBarHight = DensityUtils.dp2px(getActivity(), 50);
		// 获取状态栏高度，px
		int statusBarHeight = Utils.getStatusBarHeight(getActivity());
		// 状态栏和标题栏总高度
		int topHight = labelBarHight + statusBarHeight;

		Log.d("labelBarHight", labelBarHight + "");
		Log.d("statusBarHeight", statusBarHeight + "");
		int width = getResources().getDisplayMetrics().widthPixels / 2;
		int height = (getResources().getDisplayMetrics().heightPixels - topHight) * 15 / 56;
		int outerRadius = (width < height ? width : height) * 7 / 8;
		int innerRadius = (width < height ? width : height) * 2 / 5;
		Log.d("innerRadius", innerRadius + "");
		menu.setIconSize((innerRadius - 5) * 2, (innerRadius - 5) * 2);
		Log.d("UI", width + " , " + height);
		menu.setCenterLocation(width, height);
		menu.setInnerRingRadius(innerRadius, outerRadius);
		menu.setCenterCircleRadius(innerRadius - 10);
		menu.setTextSize(20);
		menu.setCenterCircle(off);
		menu.addMenuEntry(middle);
		menu.addMenuEntry(low);
		menu.addMenuEntry(high);
		layout_top.addView(menu);

	}

	/**
	 * 开机状态的圆圈菜单，中间放关机按钮
	 * 
	 * @param contentView
	 */
	private void addPieMenuClose(View contentView) {
		ViewGroup layout_top = (ViewGroup) contentView
				.findViewById(R.id.layout_top);
		layout_top.removeAllViewsInLayout();
		menu.removeAllView();
		off = new InitBtn(null);
		off.setIcon(R.drawable.logo);
		high = new InitBtn("低");
		middle = new InitBtn("中");
		low = new InitBtn("高");
		// // 标题栏高度60dp
		int labelBarHight = DensityUtils.dp2px(getActivity(), 50);
		// // 获取状态栏高度，px
		int statusBarHeight = Utils.getStatusBarHeight(getActivity());
		// // 状态栏和标题栏总高度
		int topHight = labelBarHight + statusBarHeight;
		int width = getResources().getDisplayMetrics().widthPixels / 2;
		int height = (getResources().getDisplayMetrics().heightPixels - topHight) * 15 / 56;
		int outerRadius = (width < height ? width : height) * 7 / 8;
		int innerRadius = (width < height ? width : height) * 2 / 5;

		menu.setIconSize((innerRadius - 5) * 2, (innerRadius - 5) * 2);
		Log.d("UI", width + " , " + height);
		menu.setCenterLocation(width, height);
		menu.setInnerRingRadius(innerRadius, outerRadius);
		menu.setCenterCircleRadius(innerRadius - 10);
		menu.setTextSize(20);
		menu.removeAllView();
		menu.setCenterCircle(off);
		menu.addMenuEntry(middle);
		menu.addMenuEntry(low);
		menu.addMenuEntry(high);
		layout_top.addView(menu);
	}

	private class InitBtn implements RadialMenuEntry {
		private String label;
		private int Icon;

		public InitBtn(String label) {
			this.label = label;
		}

		@Override
		public String getName() {
			return label;
		}

		@Override
		public String getLabel() {
			return label;
		}

		@Override
		public int getIcon() {
			return Icon;
		}

		public void setIcon(int icon) {
			this.Icon = icon;
		}

		@Override
		public List<RadialMenuEntry> getChildren() {
			return null;
		}

		@Override
		public void menuActiviated() {
			if (label != null) {
				switch (label) {
				case "低":
					setFan(1);
					btn_switch_fan.setSelected(true);
					break;
				case "中":
					btn_switch_fan.setSelected(true);
					setFan(2);
					break;
				case "高":
					btn_switch_fan.setSelected(true);
					setFan(3);
					break;
				default:
					break;
				}
			}
			Log.d("  ", "" + fan_mode);

		}

	}

	/**
	 * 按钮状态更新
	 * 
	 * @param id
	 */
	private void setBtnState(int id, boolean receive) {
		for (int i = 0; i < times.length; i++) {
			if (times[i].getId() != id) {
				times[i].setSelected(false);
			} else {
				if (receive) {
					times[i].setSelected(true);
				} else {
					times[i].setSelected(true);
				}

			}
		}

	}

	/**
	 * 清除按钮状态
	 */
	private void cleanBtnState() {
		for (int i = 0; i < times.length; i++) {
			times[i].setSelected(false);
		}
		// btn_switch_light.setSelected(false);
		// btn_switch_fan.setSelected(false);
		stopTimer();
	}

	/**
	 * 发送开机命令
	 */
	private void sendON() {
		((com.youhone.activity.MainActivity) getActivity())
				.sendData(FanLight_OrderUtils.send(FanLight_Static.TYPE_BOTH,
						FanLight_Static.CTRL_OPEN));
	}

	/**
	 * 发送关机命令
	 */
	private void sendOFF() {
		((com.youhone.activity.MainActivity) getActivity())
				.sendData(FanLight_OrderUtils.send(FanLight_Static.TYPE_BOTH,
						FanLight_Static.CTRL_CLOSE));
	}

	/**
	 * 发送开灯命令
	 */
	private void light_on() {
		((com.youhone.activity.MainActivity) getActivity())
				.sendData(FanLight_OrderUtils.send(FanLight_Static.TYPE_LIGHT,
						FanLight_Static.CTRL_OPEN));
	}

	/**
	 * 发送关灯命令
	 */
	private void light_off() {
		((com.youhone.activity.MainActivity) getActivity())
				.sendData(FanLight_OrderUtils.send(FanLight_Static.TYPE_LIGHT,
						FanLight_Static.CTRL_CLOSE));
	}

	/**
	 * 发送开风扇命令
	 */
	private void fan_on() {
		((com.youhone.activity.MainActivity) getActivity())
				.sendData(FanLight_OrderUtils.send(FanLight_Static.TYPE_FAN,
						FanLight_Static.CTRL_LEVEL3));
	}

	/**
	 * 发送关风扇命令
	 */
	private void fan_off() {
		((com.youhone.activity.MainActivity) getActivity())
				.sendData(FanLight_OrderUtils.send(FanLight_Static.TYPE_FAN,
						FanLight_Static.CTRL_CLOSE));
	}

	/**
	 * 发送定时命令
	 */
	private void setTimer(int time) {
		((com.youhone.activity.MainActivity) getActivity())
				.sendData(FanLight_OrderUtils.send(FanLight_Static.TYPE_TIMER,
						time));
	}

	/**
	 * 发送风扇档位命令
	 */
	private void setFan(int mode) {
		fan_mode = mode;

		if (btn_switch_light.isSelected()) {
			onoff = true;
		}

		btn_switch_fan.setSelected(true);
		((com.youhone.activity.MainActivity) getActivity())
				.sendData(FanLight_OrderUtils.send(FanLight_Static.TYPE_FAN,
						mode));
	}

	@Override
	public void receviceData(byte[] data) {
		System.out.println("收到pipe：" + XlinkUtils.getHexBinString(data));
		if (data.length >= 6) {
			status = FanLight_OrderUtils.receive(data);
			if (status != null) {
				timeout = 0;
				updataStatus(status);
			} else {
				Log.e("", "返回数据错误");
			}
		} else {
			Log.e("", "返回数据错误");
		}
	}

	private void updataStatus(FanLight_Status status) {
		if (status.getFan() != 0 && status.getLight() != 0) {
			onoff = true;
		} else {
			onoff = false;
		}

		switch (status.getFan()) {
		case 0:
			fan_mode = 0;
			menu.ResetState();
			break;
		case 1:
			menu.ResetState();
			fan_mode = 1;
			menu.setBottom();
			break;
		case 2:
			menu.ResetState();
			fan_mode = 2;
			menu.setMiddle();
			break;
		case 3:
			menu.ResetState();
			fan_mode = 3;
			menu.setTop();
			break;
		default:
			break;
		}
		btn_switch_fan.setSelected(status.getFan() != 0);
		// menu.setEnable(status.getFan() != 0);
		btn_switch_light.setSelected(status.getLight() != 0);

		if (status.isTiming()) {

			if (Math.abs(timeSec - status.getSec()) > 3
					|| Math.abs(timeMin - status.getMin()) > 0
					|| Math.abs(timeHour - status.getHour()) > 0) {
				timeHour = status.getHour();
				timeMin = status.getMin();
				timeSec = status.getSec();
				startTimer();
			}
			if (timeHour >= 0
					&& ((timeHour < 1) || (timeHour <= 1 && timeMin == 0))) {
				setBtnState(R.id.timer_1h, true);
			} else if (timeHour >= 1
					&& ((timeHour < 2) || (timeHour <= 2 && timeMin == 0))) {
				setBtnState(R.id.timer_2h, true);
			} else if (timeHour >= 2
					&& ((timeHour < 4) || (timeHour <= 4 && timeMin == 0))) {
				setBtnState(R.id.timer_4h, true);
			} else if (timeHour >= 4) {
				setBtnState(R.id.timer_8h, true);
			}

		} else {
			stopTimer();
			cleanBtnState();
			left_time.setText("无定时");
		}
	}

	/**
	 * 启动定时器,开始倒计时
	 */
	private void startTimer() {
		if (timer == null) {
			timer = new Timer();
		}
		if (timerTask == null) {
			timerTask = new TimerTask() {

				@Override
				public void run() {
					if (++timeout > TIMEOUT_MAX) {
						stopTimer();
						return;
					}
					if (timeSec > 0) {// 秒还大于0
						timeSec--;
					} else {
						if (timeMin > 0) {
							timeSec = 59;
							timeMin--;
						} else if (timeHour > 0) {
							timeMin = 59;
							timeSec = 59;
							timeHour--;

						} else {
							stopTimer();
						}

					}
					getActivity().runOnUiThread(new Runnable() {

						@Override
						public void run() {
							left_time.setText((timeHour < 10 ? "0" + timeHour
									: timeHour)
									+ ":"
									+ (timeMin < 10 ? "0" + timeMin : timeMin)
									+ ":"
									+ (timeSec < 10 ? "0" + timeSec : timeSec));
						}
					});
				}
			};
			if (timer != null && timerTask != null) {
				timer.schedule(timerTask, 0, 1000);
			}
		}

	}

	/**
	 * 停止定时器
	 */
	private void stopTimer() {
		if (timerTask != null) {
			timerTask.cancel();
			timerTask = null;
		}

		if (timer != null) {
			timer.cancel();
			timer = null;
		}
		getActivity().runOnUiThread(new Runnable() {

			@Override
			public void run() {
				left_time.setText("无定时");
			}
		});

	}

}
