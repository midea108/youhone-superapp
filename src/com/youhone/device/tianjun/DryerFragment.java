package com.youhone.device.tianjun;

import io.xlink.wifi.ui.util.XlinkUtils;

import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.youhone.activity.MainActivity.ReceiveDataListener;
import com.youhone.utils.tianjun.DensityUtils;
import com.youhone.utils.tianjun.Dryer_Status;
import com.youhone.utils.tianjun.Dryer_Utils;
import com.youhone.utils.tianjun.Utils;
import com.youhone.widget.RadialMenuWidget;
import com.youhone.widget.RadialMenuWidget.RadialMenuEntry;
import com.youhone.xlink.superapp.R;

public class DryerFragment extends Fragment implements ReceiveDataListener {
	private static final int TIMEOUT_MAX = 90;
	private int timeout = 0;
	private Button btn_disinfect;
	private boolean onoff;
	private TextView txt_left_time;
	private Button time_add, time_dec;
	private InitBtn shirt, socks, pillow, towel, center_onoff;
	private RadialMenuWidget menu;
	private BtnListener listerner = new BtnListener();
	private int[] timerList = { 0, 30, 60, 90, 120, 150 };
	private int timerLevel = 0;
	private int timeHour, timeMin, timeSec;
	private Timer timer;
	private TimerTask timerTask;

	@Override
	@Nullable
	public View onCreateView(LayoutInflater inflater,
			@Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
		View contentView = inflater.inflate(R.layout.fragment_dryer, container,
				false);
		time_add = (Button) contentView.findViewById(R.id.timmer_add);
		time_dec = (Button) contentView.findViewById(R.id.timmer_dec);
		txt_left_time = (TextView) contentView.findViewById(R.id.left_time);
		btn_disinfect = (Button) contentView.findViewById(R.id.btn_disinfect);
		time_add.setOnClickListener(listerner);
		time_dec.setOnClickListener(listerner);
		btn_disinfect.setOnClickListener(listerner);
		((com.youhone.activity.MainActivity) getActivity()).setOnReceive(this, DryerFragment.this);
		return contentView;
	}

	private void setType(int type) {
		switch (type) {
		case 0:

			break;
		case 1:
			break;
		case 2:
			break;
		case 3:
			break;

		default:

			break;
		}
	}

	private class BtnListener implements OnClickListener {

		@Override
		public void onClick(View v) {
			if (onoff) {
				switch (v.getId()) {
				case R.id.timmer_add:
					if (timerLevel < 6) {
						timerLevel++;
					}
					setTimer(timerList[timerLevel]);
					XlinkUtils.showShortToast("定时时间：" + timerList[timerLevel]);
					break;
				case R.id.timmer_dec:
					if (timerLevel > 0) {
						timerLevel--;
					}
					setTimer(timerList[timerLevel]);
					XlinkUtils.showShortToast(timerLevel == 0 ? "定时已关闭"
							: "定时时间：" + timerList[timerLevel]);
					break;

				case R.id.btn_disinfect:
					if (btn_disinfect.isSelected()) {// 已经打开消毒，则发关闭命令
						btn_disinfect.setSelected(false);
					} else {// 已经关闭消毒，则发打开命令
						btn_disinfect.setSelected(true);
					}
					break;

				default:
					break;
				}
			} else {
				XlinkUtils.showShortToast("请先打开设备");
			}

		}
	}

	@Override
	public void onActivityCreated(@Nullable Bundle savedInstanceState) {
		menu = new RadialMenuWidget(getActivity());
		addPieMenu(getView(), false);
		super.onActivityCreated(savedInstanceState);
	}

	private void addPieMenu(View contentView, boolean onoff) {
		ViewGroup layout_top = (ViewGroup) contentView
				.findViewById(R.id.layout_top);

		layout_top.removeAllViewsInLayout();
		menu.removeAllView();

		center_onoff = new InitBtn(null);
		if (!onoff) {
			center_onoff.setIcon(R.drawable.btn_off);
		} else {
			center_onoff.setIcon(R.drawable.btn_on);
		}

		shirt = new InitBtn("衬衫");
		socks = new InitBtn("内衣\n袜子");
		pillow = new InitBtn("枕套\n被套");
		towel = new InitBtn("毛巾\n牛仔");
		// 标题栏高度60dp
		int labelBarHight = DensityUtils.dp2px(getActivity(), 50);
		// 获取状态栏高度，px
		int statusBarHeight = Utils.getStatusBarHeight(getActivity());
		// 状态栏和标题栏总高度
		int topHight = labelBarHight + statusBarHeight;

		int width = getResources().getDisplayMetrics().widthPixels / 2;
		int height = (getResources().getDisplayMetrics().heightPixels - topHight) * 3 / 5 / 2;

		int outerRadius = (width < height ? width : height) * 13 / 16;
		int innerRadius = (width < height ? width : height) * 2 / 5;
		menu.setIconSize((innerRadius - 5) * 2, (innerRadius - 5) * 2);
		Log.d("UI", width + " , " + height);

		menu.setCenterLocation(width, height);
		menu.setInnerRingRadius(innerRadius, outerRadius);
		menu.setCenterCircleRadius(innerRadius - 10);
		menu.setTextSize(17);

		menu.setTextColor(0xffffff, 0xff);
		menu.setCenterCircle(center_onoff);
		menu.addMenuEntry(shirt);
		menu.addMenuEntry(socks);
		menu.addMenuEntry(pillow);
		menu.addMenuEntry(towel);

		layout_top.addView(menu);
	}

	private class InitBtn implements RadialMenuEntry {
		private String label;
		private int Icon;

		public InitBtn(String label) {
			this.label = label;
		}

		@Override
		public String getName() {
			return label;
		}

		@Override
		public String getLabel() {
			return label;
		}

		@Override
		public int getIcon() {
			return Icon;
		}

		public void setIcon(int icon) {
			this.Icon = icon;
		}

		@Override
		public List<RadialMenuEntry> getChildren() {
			return null;
		}

		@Override
		public void menuActiviated() {
			// TODO

			if (label != null && onoff) {
				switch (label) {
				case "衬衫":
					sendControl(1);
					break;
				case "内衣\n袜子":
					sendControl(2);
					break;
				case "枕套\n被套":
					sendControl(3);
					break;
				case "毛巾\n牛仔":
					sendControl(4);
					break;

				default:
					break;
				}
			} else if (label == null) {
				switch (Icon) {
				case R.drawable.btn_on:
					menu.ResetState();
					sendOnoff(false);
					onoff = false;
					menu.setEnable(onoff);
					addPieMenu(getView(), false);
					btn_disinfect.setSelected(false);
					break;
				case R.drawable.btn_off:
					onoff = true;
					sendOnoff(true);
					addPieMenu(getView(), true);
					break;
				default:
					break;
				}
			}

		}
	}

	@Override
	public void receviceData(byte[] data) {
		System.out.println("收到pipe：" + XlinkUtils.getHexBinString(data));
		if (data.length >= 6) {
			Dryer_Status status = Dryer_Utils.receive(data);
			if (status != null) {
				timeout = 0;
				updataStatus(status);
			} else {
				Log.e("", "返回数据错误");
			}
		} else {
			Log.e("", "返回数据错误");
		}
	}

	private void updataStatus(Dryer_Status status) {
		// TODO
		onoff = status.isOnoff();

		switch (status.getFunc()) {
		case 0:
			menu.ResetState();
			break;
		case 1:
			menu.ResetState();
			break;
		case 2:
			menu.ResetState();
			break;
		case 3:
			menu.ResetState();
			break;
		default:
			break;
		}

		btn_disinfect.setSelected(status.isSter());

		if (status.isTiming()) {
			timeHour = status.getHour();
			timeMin = status.getMin();
			timeSec = status.getSec();
			startTimer();
		} else {
			stopTimer();
			txt_left_time.setText("无定时");
		}

	}

	private void sendOnoff(boolean onoff) {
		send(Dryer_Utils.sendOnoff(onoff));
	}

	private void sendControl(int func) {
		send(Dryer_Utils.sendControl(func));
	}

	private void sendSter(boolean isOn) {
		send(Dryer_Utils.sendSter(isOn));
	}

	private void setTimer(int time) {
		send(Dryer_Utils.setTimer(time));
	}

	private void send(byte[] buff) {
		((com.youhone.activity.MainActivity) getActivity()).sendData(buff);
	}

	/**
	 * 启动定时器,开始倒计时
	 */
	private void startTimer() {
		if (timer == null) {
			timer = new Timer();
		}
		if (timerTask == null) {
			timerTask = new TimerTask() {

				@Override
				public void run() {
					if (++timeout > TIMEOUT_MAX) {
						stopTimer();
						return;
					}
					if (timeSec > 0) {// 秒还大于0
						timeSec--;
					} else {
						if (timeMin > 0) {
							timeSec = 59;
							timeMin--;
						} else if (timeHour > 0) {
							timeMin = 59;
							timeSec = 59;
							timeHour--;

						} else {
							stopTimer();
						}

					}
					getActivity().runOnUiThread(new Runnable() {

						@Override
						public void run() {
							txt_left_time.setText((timeHour < 10 ? "0"
									+ timeHour : timeHour)
									+ ":"
									+ (timeMin < 10 ? "0" + timeMin : timeMin)
									+ ":"
									+ (timeSec < 10 ? "0" + timeSec : timeSec));
						}
					});
				}
			};
			if (timer != null && timerTask != null) {
				timer.schedule(timerTask, 0, 1000);
			}
		}

	}

	/**
	 * 停止定时器
	 */
	private void stopTimer() {
		if (timerTask != null) {
			timerTask.cancel();
			timerTask = null;
		}

		if (timer != null) {
			timer.cancel();
			timer = null;
		}
		getActivity().runOnUiThread(new Runnable() {

			@Override
			public void run() {
				txt_left_time.setText("无定时");
			}
		});

	}
}
