package com.youhone.device.tianjun;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.SeekBar;
import android.widget.SeekBar.OnSeekBarChangeListener;
import android.widget.TextView;

import com.youhone.xlink.superapp.R;

public class TimerDialog extends Dialog {
	private Button btn_confirm, btn_cancel;
	private TextView txt_time;
	private SeekBar sb;
	private BtnListener btnListener = new BtnListener();
	private SbListener sbListener = new SbListener();
	private OnConfirmListener listener;

	private int[] time = { 30, 60, 90, 120, 150 };

	public TimerDialog(Context context) {
		super(context);
	}

	public TimerDialog(Context context, int theme) {
		super(context, theme);
	}

	public TimerDialog(Context context, boolean cancelable,
			OnCancelListener cancelListener) {
		super(context, cancelable, cancelListener);
	}

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.dialog_timer);
		init();
		super.onCreate(savedInstanceState);
	}

	@Override
	protected void onStart() {
		sb.setProgress(0);
		txt_time.setText(String.valueOf(time[0]));
		super.onStart();
	}

	private void init() {
		btn_confirm = (Button) findViewById(R.id.btn_confirm);
		btn_cancel = (Button) findViewById(R.id.btn_cancel);
		sb = (SeekBar) findViewById(R.id.sb_time);
		txt_time = (TextView) findViewById(R.id.txt_time);

		btn_confirm.setOnClickListener(btnListener);
		btn_cancel.setOnClickListener(btnListener);
		sb.setOnSeekBarChangeListener(sbListener);
	}

	private class BtnListener implements android.view.View.OnClickListener {
		@Override
		public void onClick(View v) {
			switch (v.getId()) {
			case R.id.btn_confirm:
				int timer = time[sb.getProgress()];

				if (listener != null) {
					listener.onConfirm(timer);
				}
				
				dismiss();
				break;
			case R.id.btn_cancel:
				dismiss();
				break;
			}
		}
	}

	private class SbListener implements OnSeekBarChangeListener {
		@Override
		public void onProgressChanged(SeekBar seekBar, int progress,
				boolean fromUser) {
			txt_time.setText(String.valueOf(time[progress]));
		}

		@Override
		public void onStartTrackingTouch(SeekBar seekBar) {
		}

		@Override
		public void onStopTrackingTouch(SeekBar seekBar) {
		}
	}

	public void setOnConfirmListener(OnConfirmListener l) {
		this.listener = l;
	}

	public interface OnConfirmListener {
		/**
		 * Called when the confirm button has been clicked.
		 */
		void onConfirm(int time);
	}

}
