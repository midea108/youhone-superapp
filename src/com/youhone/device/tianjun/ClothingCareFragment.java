package com.youhone.device.tianjun;

import io.xlink.wifi.ui.util.XlinkUtils;

import java.util.Timer;
import java.util.TimerTask;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.youhone.activity.MainActivity.ReceiveDataListener;
import com.youhone.device.tianjun.TimerDialog.OnConfirmListener;
import com.youhone.utils.tianjun.ClothingCare_OrderUtils;
import com.youhone.utils.tianjun.ClothingCare_Static;
import com.youhone.utils.tianjun.ClothingCare_Status;
import com.youhone.xlink.superapp.R;

public class ClothingCareFragment extends Fragment implements
		ReceiveDataListener {
	private static final int TIMEOUT_MAX = 6;
	private int timeout = 0;
	private int[] functionIds = { R.id.iv_suit, R.id.iv_sweater, R.id.iv_sport,
			R.id.iv_hat, R.id.iv_dry_sweater, R.id.iv_dry_shirt,
			R.id.iv_dry_rain, R.id.iv_dry_timing, R.id.iv_sterilize_normal,
			R.id.iv_sterilize_bedding, R.id.iv_sterilize_underwear,
			R.id.iv_sterilize_toy };
	private int[] stateIds = { R.id.iv_ready, R.id.iv_steam, R.id.iv_fresh,
			R.id.iv_dry, R.id.iv_sterilize };

	private TextView hour, min, sec, function;
	private ImageView play, pause, stop;
	private View layout_error;

	private FunctionListener listener = new FunctionListener();
	private BtnListener btnListener = new BtnListener();

	private int func = ClothingCare_Static.OTHERS;
	private boolean isRunning = false;
	private int timeHour, timeMin, timeSec;
	private Timer timer;
	private TimerTask timerTask;
	private Timer cycleTimer;
	private TimerTask cycleTimerTask;
	private boolean isPause = false;

	private TimerDialog dialog;

	@Override
	@Nullable
	public View onCreateView(LayoutInflater inflater,
			@Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
		View contentView = inflater.inflate(R.layout.fragment_clothing_care,
				container, false);
		((com.youhone.activity.MainActivity) getActivity()).setOnReceive(this,
				ClothingCareFragment.this);
		return contentView;
	}

	@Override
	public void onActivityCreated(@Nullable Bundle savedInstanceState) {
		init(getView());
		super.onActivityCreated(savedInstanceState);
	}

	@Override
	public void onResume() {
		startCycleTimer();
		super.onResume();
	}

	private void init(View view) {
		for (int i = 0; i < functionIds.length; i++) {
			view.findViewById(functionIds[i]).setOnClickListener(listener);
		}

		layout_error = (View) view.findViewById(R.id.layout_error);

		hour = (TextView) view.findViewById(R.id.hour);
		min = (TextView) view.findViewById(R.id.min);
		sec = (TextView) view.findViewById(R.id.sec);
		function = (TextView) view.findViewById(R.id.func);
		play = (ImageView) view.findViewById(R.id.btn_play);
		pause = (ImageView) view.findViewById(R.id.btn_pause);
		stop = (ImageView) view.findViewById(R.id.btn_stop);
		play.setOnClickListener(btnListener);
		pause.setOnClickListener(btnListener);
		stop.setOnClickListener(btnListener);

		stop.setSelected(true);
		layout_error.setOnTouchListener(new OnTouchListener() {
			@Override
			public boolean onTouch(View v, MotionEvent event) {
				v.performClick();
				return true;
			}
		});
	}

	private class BtnListener implements OnClickListener {
		@Override
		public void onClick(View v) {
			switch (v.getId()) {
			case R.id.btn_play:
				isPause = false;
				sendStart();
				break;
			case R.id.btn_pause:
				isPause = true;
				sendPause();
				break;
			case R.id.btn_stop:
				isPause = false;
				sendStop();
				break;
			default:
				break;
			}
		}
	}

	private class FunctionListener implements OnClickListener {
		@Override
		public void onClick(View v) {
			if (isRunning) {
				Toast.makeText(getActivity(), "请先停止当前任务", Toast.LENGTH_SHORT)
						.show();
				return;
			}

			if (v.isSelected()) {
				v.setSelected(false);
				func = ClothingCare_Static.OTHERS;
				return;
			}

			int id = v.getId();
			switch (id) {
			case R.id.iv_suit:
				func = ClothingCare_Static.SUIT;
				break;
			case R.id.iv_sweater:
				func = ClothingCare_Static.SWEATER;
				break;
			case R.id.iv_sport:
				func = ClothingCare_Static.SPORT;
				break;
			case R.id.iv_hat:
				func = ClothingCare_Static.HAT;
				break;
			case R.id.iv_dry_sweater:
				func = ClothingCare_Static.DRY_SWEATER;
				break;
			case R.id.iv_dry_shirt:
				func = ClothingCare_Static.DRY_SHIRT;
				break;
			case R.id.iv_dry_rain:
				func = ClothingCare_Static.DRY_RAIN;
				break;
			case R.id.iv_dry_timing:
				if (dialog == null) {
					dialog = new TimerDialog(getActivity());
					dialog.setOnConfirmListener(timerListener);
				}
				dialog.show();
				break;
			case R.id.iv_sterilize_normal:
				func = ClothingCare_Static.STERILIZE_NORMAL;
				break;
			case R.id.iv_sterilize_bedding:
				func = ClothingCare_Static.STERILIZE_BEDDING;
				break;
			case R.id.iv_sterilize_underwear:
				func = ClothingCare_Static.STERILIZE_UNDERWEAR;
				break;
			case R.id.iv_sterilize_toy:
				func = ClothingCare_Static.STERILIZE_TOY;
				break;

			default:
				break;
			}

			if (id != R.id.iv_dry_timing)
				changeBtnState(id);
		}
	}

	/**
	 * 发送启动指令
	 */
	private void sendStart() {
		((com.youhone.activity.MainActivity) getActivity())
				.sendData(ClothingCare_OrderUtils.sendControl(func,
						ClothingCare_Static.START));
	}

	/**
	 * 发送暂停指令
	 */
	private void sendPause() {
		if (isRunning) {
			((com.youhone.activity.MainActivity) getActivity())
					.sendData(ClothingCare_OrderUtils.sendControl(func,
							ClothingCare_Static.PAUSE));
		} else {
			Toast.makeText(getActivity(), "当前没有执行任何任务", Toast.LENGTH_SHORT)
					.show();
		}
	}

	/**
	 * 发送停止指令
	 */
	private void sendStop() {
		if (isRunning) {
			((com.youhone.activity.MainActivity) getActivity())
					.sendData(ClothingCare_OrderUtils.sendControl(func,
							ClothingCare_Static.STOP));
		} else {
			Toast.makeText(getActivity(), "当前没有执行任何任务", Toast.LENGTH_SHORT)
					.show();
		}
	}

	private void sendTimer(int time) {
		((com.youhone.activity.MainActivity) getActivity())
				.sendData(ClothingCare_OrderUtils.setTimer(time));
	}

	/**
	 * 设置过程状态
	 */
	private void setState(int where) {
		for (int i = 0; i < stateIds.length; i++) {
			if (i == where) {
				if (!getView().findViewById(stateIds[i]).isSelected())
					getView().findViewById(stateIds[i]).setSelected(true);
			} else {
				if (getView().findViewById(stateIds[i]).isSelected())
					getView().findViewById(stateIds[i]).setSelected(false);
			}
		}
	}

	/**
	 * 改变按钮状态
	 * 
	 * @param id
	 */
	private void changeBtnState(int id) {
		for (int i = 0; i < functionIds.length; i++) {
			if (functionIds[i] == id) {
				getView().findViewById(functionIds[i]).setSelected(true);
			} else {
				getView().findViewById(functionIds[i]).setSelected(false);
			}
		}
	}

	/**
	 * 启动定时器,开始倒计时
	 */
	private void startTimer() {
		if (timer == null) {
			timer = new Timer();
		}
		if (timerTask == null) {
			timerTask = new TimerTask() {

				@Override
				public void run() {

					if (timeSec > 0) {// 秒还大于0
						timeSec--;
					} else {
						if (timeMin > 0) {
							timeSec = 59;
							timeMin--;
						} else if (timeHour > 0) {
							timeMin = 59;
							timeSec = 59;
							timeHour--;
						} else {
							stopTimer();
						}

					}
					getActivity().runOnUiThread(new Runnable() {

						@Override
						public void run() {
							updateTimer();
						}
					});
				}
			};
			if (timer != null && timerTask != null) {
				timer.schedule(timerTask, 0, 1000);
			}
		}

	}

	private void startCycleTimer() {
		if (cycleTimer == null) {
			cycleTimer = new Timer();
		}
		if (cycleTimerTask == null) {
			cycleTimerTask = new TimerTask() {

				@Override
				public void run() {
					if (++timeout > TIMEOUT_MAX) {
						getActivity().runOnUiThread(new Runnable() {

							@Override
							public void run() {
								layout_error.setVisibility(View.VISIBLE);
								TextView txt = (TextView) getView()
										.findViewById(R.id.tv_error);
								txt.setText("设备无法连接");
								txt.setOnClickListener(new OnClickListener() {
									@Override
									public void onClick(View v) {
										getActivity().finish();
									}
								});
							}
						});
						stopTimer();
						stopCycleTimer();
						return;
					}
					((com.youhone.activity.MainActivity) getActivity())
							.sendData(ClothingCare_OrderUtils.getStatus());
				}
			};
			if (cycleTimer != null && cycleTimerTask != null) {
				cycleTimer.schedule(cycleTimerTask, 4000, 5000);
			}
		}

	}

	private void stopCycleTimer() {
		if (cycleTimerTask != null) {
			cycleTimerTask.cancel();
			cycleTimerTask = null;
		}

		if (cycleTimer != null) {
			cycleTimer.cancel();
			cycleTimer = null;
		}
	}

	/**
	 * 停止定时器
	 */
	private void stopTimer() {
		if (timerTask != null) {
			timerTask.cancel();
			timerTask = null;
		}

		if (timer != null) {
			timer.cancel();
			timer = null;
		}

		getActivity().runOnUiThread(new Runnable() {

			@Override
			public void run() {
				if (!isPause) {
					timeHour = 0;
					timeMin = 0;
					timeSec = 0;
				}
				updateTimer();
			}
		});

	}

	/**
	 * 根据回来的数据更新状态
	 * 
	 * @param status
	 */
	private void updataStatus(ClothingCare_Status status) {
		// stopTimer();

		if (status.isError()) {
			layout_error.setVisibility(View.VISIBLE);
			((TextView) getView().findViewById(R.id.tv_error)).setText(status
					.getError());
			return;
		} else {
			layout_error.setVisibility(View.GONE);
		}
		int fPara = status.getfPara();
		setfPara(fPara);
		if ((!isRunning && (fPara == ClothingCare_Static.PAUSE || fPara == ClothingCare_Static.START))
				|| (isRunning && fPara == ClothingCare_Static.STOP)) {// 与当前状态相反则改变按钮状态
			func = status.getFunc();
			changeBtnState(func != ClothingCare_Static.OTHERS ? functionIds[func
					- ClothingCare_Static.SUIT]
					: 0);

			function.setText(getResources().getStringArray(R.array.func)[func]
					+ "...");
		}
		switch (fPara) {
		case ClothingCare_Static.PAUSE:
		case ClothingCare_Static.START:
			isRunning = true;
			break;
		case ClothingCare_Static.STOP:
			isRunning = false;
			break;
		}
		setState(status.getfStep() - 1);

		if (status.isTiming()) {
			System.out.println("本地定时器" + timeHour + ":" + timeMin + ":"
					+ timeSec);
			System.out.println("获取的时间" + status.getHour() + ":"
					+ status.getMin() + ":" + status.getSec());
			if (Math.abs(timeSec - status.getSec()) > 3
					|| Math.abs(timeMin - status.getMin()) > 0
					|| Math.abs(timeHour - status.getHour()) > 0) {

				timeHour = status.getHour();
				timeMin = status.getMin();
				timeSec = status.getSec();
				updateTimer();
				if (isRunning)
					startTimer();
				System.out.println("超出范围");
			}
		} else {
			stopTimer();
			updateTimer();
		}

	}

	/**
	 * 数据接收
	 * 
	 * @param data
	 */
	@Override
	public void receviceData(byte[] data) {
		if (data.length < 6)
			return;
		System.out.println("收到pipe：" + XlinkUtils.getHexBinString(data));
		ClothingCare_Status status = ClothingCare_OrderUtils.receive(data);
		if (status != null) {
			Log.e("返回", "获得返回数据");
			timeout = 0;
			updataStatus(status);
		} else {
			Log.e("", "返回数据错误");
		}

	}

	/**
	 * 更新倒计时时间
	 */
	private void updateTimer() {
		hour.setText("" + (timeHour < 10 ? "0" + timeHour : timeHour));
		min.setText("" + (timeMin < 10 ? "0" + timeMin : timeMin));
		sec.setText("" + (timeSec < 10 ? "0" + timeSec : timeSec));
	}

	/**
	 * 定时窗口设置回调
	 */
	private OnConfirmListener timerListener = new OnConfirmListener() {
		@Override
		public void onConfirm(int time) {
			func = ClothingCare_Static.DRY_TIMING;
			sendTimer(time);
			changeBtnState(R.id.iv_dry_timing);
			Toast.makeText(getActivity(), "定时时间设置成功", Toast.LENGTH_SHORT)
					.show();
		}
	};

	/**
	 * 设置三个控制键
	 */
	private void setfPara(int fPara) {
		isPause = fPara == ClothingCare_Static.PAUSE;
		if (isPause) {
			stopTimer();
		}
		play.setSelected(fPara == ClothingCare_Static.START);
		pause.setSelected(fPara == ClothingCare_Static.PAUSE);
		stop.setSelected(fPara == ClothingCare_Static.STOP);
	}

	public void onDestroyView() {
		super.onDestroyView();
		stopTimer();
		stopCycleTimer();
	}
}