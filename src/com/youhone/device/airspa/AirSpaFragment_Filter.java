package com.youhone.device.airspa;

import io.xlink.wifi.ui.util.XlinkUtils;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.youhone.activity.MainActivity;
import com.youhone.activity.MainActivity.ReceiveDataListener;
import com.youhone.utils.airspa.AirSpa_OrderUtils;
import com.youhone.utils.airspa.AirSpa_Status;
import com.youhone.xlink.superapp.R;

public class AirSpaFragment_Filter extends Fragment implements
		ReceiveDataListener {
	private Button xinfeng_buy_filter;
	private TextView xinfeng_filter_tip;
	private AirSpa_Status status;

	public AirSpaFragment_Filter() {
	}

	public void onDestroyView() {
		super.onDestroyView();
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View contentView = inflater.inflate(R.layout.fragment_xinfeng_filter,
				container, false);
		xinfeng_buy_filter = (Button) contentView
				.findViewById(R.id.xinfeng_buy_filter);
		xinfeng_filter_tip = (TextView) contentView
				.findViewById(R.id.xinfeng_filter_tip);
		((MainActivity) getActivity()).setOnReceive(this,
				AirSpaFragment_Filter.this);
		((MainActivity) getActivity()).sendData(AirSpa_OrderUtils.GET_QUERY());
		xinfeng_buy_filter.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				Intent intent = new Intent();
				intent.setAction("android.intent.action.VIEW");
				Uri content_url = Uri
						.parse("https://item.taobao.com/item.htm?spm=0.0.0.0.7A3MJv&id=526901559432");
				intent.setData(content_url);
				startActivity(intent);

			}
		});
		return contentView;
	}

	@Override
	public void receviceData(byte[] data) {
		System.out.println("收到pipe：" + XlinkUtils.getHexBinString(data));
		status = AirSpa_OrderUtils.receive(data);
		if (status != null) {
			updataStatus(status);
		} else {
			Log.e("", "返回数据错误");
		}
	}

	private void updataStatus(AirSpa_Status status) {
		if (null != status) {
			if (status.isAlarmCode()) {
				System.out.println("故障信息");
				return;
			}
			if (status.isStatusQuery()) {
				System.out.println("滤芯界面状态信息");
				if (status.isNeedClean()) {// 是否需要清洁
					xinfeng_filter_tip.setVisibility(View.VISIBLE);
				} else {
					xinfeng_filter_tip.setVisibility(View.INVISIBLE);
				}

			}

		}

	}

	// Tab相关
	private String title;
	private int iconId;

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public int getIconId() {
		return iconId;
	}

	public void setIconId(int iconId) {
		this.iconId = iconId;
	}
}
