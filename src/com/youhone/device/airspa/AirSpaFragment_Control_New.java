package com.youhone.device.airspa;

import io.xlink.wifi.ui.Constant;
import io.xlink.wifi.ui.util.XlinkUtils;

import java.math.BigDecimal;
import java.util.Timer;
import java.util.TimerTask;

import android.app.AlertDialog.Builder;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.youhone.activity.MainActivity;
import com.youhone.activity.MainActivity.ReceiveDataListener;
import com.youhone.activity.TimerDialog;
import com.youhone.utils.airspa.AirSpa_OrderUtils;
import com.youhone.utils.airspa.AirSpa_Status;
import com.youhone.xlink.superapp.R;

public class AirSpaFragment_Control_New extends Fragment implements
		ReceiveDataListener {
	private int gear = 1;
	private Button btn_onoff;
	private TextView xinfeng_tip1, xinfeng_tip2;
	private ImageView gear1_img, gear2_img, gear3_img, gear4_img, gear5_img,
			timer_img, auto_img, negative_img, sleep_img;
	private ImageView[] gear_imgs = new ImageView[6];
	private int[] gearImgOn_ids = new int[] { R.drawable.xinfeng_auto_on,
			R.drawable.qizhen_gear1_on, R.drawable.qizhen_gear2_on,
			R.drawable.qizhen_gear3_on, R.drawable.qizhen_gear4_on,
			R.drawable.xinfeng_gear5_on };
	private int[] gearImgOff_ids = new int[] { R.drawable.xinfeng_auto_off,
			R.drawable.qizhen_gear1_off, R.drawable.qizhen_gear2_off,
			R.drawable.qizhen_gear3_off, R.drawable.qizhen_gear4_off,
			R.drawable.xinfeng_gear5_off };
	private TextView co2_value, temp_in, shidu_in, pm25_value, pm25_weight,
			xinfeng_rice;
	private ImageView xinfeng_quality;
	private BtnListener btnlistener = new BtnListener();
	private AirSpa_Status status;
	private TimerDialog dialog;
	private int temp_out, shidu_out;

	private boolean isOn = false;
	private boolean isSleepOn = false;
	private boolean isNegative = false;
	private boolean isTimerOn = false;

	private Timer timer;
	private TimerTask timerTask;
	private int time = 0;

	private Toast t;

	@Override
	@Nullable
	public View onCreateView(LayoutInflater inflater,
			@Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
		View contentView = inflater.inflate(
				R.layout.fragment_xinfeng_control_new, container, false);
		initView(contentView);
		((MainActivity) getActivity()).setOnReceive(this,
				AirSpaFragment_Control_New.this);
		((MainActivity) getActivity()).sendData(AirSpa_OrderUtils.GET_QUERY());
		((MainActivity) getActivity()).sendData(AirSpa_OrderUtils
				.QUERT_WORKTIME());
		startTimer();
		return contentView;
	}

	private void initView(View contentView) {
		xinfeng_tip1 = (TextView) contentView.findViewById(R.id.xinfeng_tip1);
		xinfeng_tip2 = (TextView) contentView.findViewById(R.id.xinfeng_tip2);
		xinfeng_rice = (TextView) contentView.findViewById(R.id.xinfeng_rice);
		pm25_weight = (TextView) contentView.findViewById(R.id.pm25_weight);
		pm25_value = (TextView) contentView.findViewById(R.id.pm25_value);
		shidu_in = (TextView) contentView.findViewById(R.id.shidu_in);
		temp_in = (TextView) contentView.findViewById(R.id.temp_in);
		co2_value = (TextView) contentView.findViewById(R.id.co2_value);
		btn_onoff = (Button) contentView.findViewById(R.id.device_onoff);
		xinfeng_quality = (ImageView) contentView
				.findViewById(R.id.xinfeng_control_quality);
		auto_img = (ImageView) contentView.findViewById(R.id.auto_img);
		negative_img = (ImageView) contentView.findViewById(R.id.negative_img);
		timer_img = (ImageView) contentView.findViewById(R.id.timer_img);
		sleep_img = (ImageView) contentView.findViewById(R.id.sleep_img);
		gear1_img = (ImageView) contentView.findViewById(R.id.gear1_img);
		gear2_img = (ImageView) contentView.findViewById(R.id.gear2_img);
		gear3_img = (ImageView) contentView.findViewById(R.id.gear3_img);
		gear4_img = (ImageView) contentView.findViewById(R.id.gear4_img);
		gear5_img = (ImageView) contentView.findViewById(R.id.gear5_img);
		gear_imgs[0] = auto_img;
		gear_imgs[1] = gear1_img;
		gear_imgs[2] = gear2_img;
		gear_imgs[3] = gear3_img;
		gear_imgs[4] = gear4_img;
		gear_imgs[5] = gear5_img;
		btn_onoff.setOnClickListener(btnlistener);
		sleep_img.setOnClickListener(btnlistener);
		auto_img.setOnClickListener(btnlistener);
		timer_img.setOnClickListener(btnlistener);
		negative_img.setOnClickListener(btnlistener);
		gear1_img.setOnClickListener(btnlistener);
		gear2_img.setOnClickListener(btnlistener);
		gear3_img.setOnClickListener(btnlistener);
		gear4_img.setOnClickListener(btnlistener);
		gear5_img.setOnClickListener(btnlistener);
		updateGearLay(gear, isOn);
		updataNegativeLay(isNegative, isOn);
		// updataTimerLay(isTimerOn, isOn);
		if (isOn) {
			btn_onoff.setBackgroundResource(R.drawable.b_xinfeng_off);
		} else {
			btn_onoff.setBackgroundResource(R.drawable.b_xinfeng_on);
		}
		t = new Toast(getActivity());
	}

	@Override
	public void onActivityCreated(@Nullable Bundle savedInstanceState) {

		super.onActivityCreated(savedInstanceState);
	}

	public void onResume() {
		super.onResume();
	}

	public void onDestroyView() {
		stopTimer();
		super.onDestroyView();
	}

	private void showOffLine() {
		t.cancel();
		t.makeText(getActivity(), "请先开启设备", Toast.LENGTH_SHORT).show();
	}

	private void showSleep() {
		t.cancel();
		t.makeText(getActivity(), "请开启设备或关闭睡眠模式", Toast.LENGTH_SHORT).show();
	}

	private class BtnListener implements OnClickListener {
		@Override
		public void onClick(View v) {

			switch (v.getId()) {
			case R.id.device_onoff:
				if (isOn) {
					sendOFF();
					btn_onoff.setBackgroundResource(R.drawable.b_xinfeng_on);
				} else {
					sendON();
					btn_onoff.setBackgroundResource(R.drawable.b_xinfeng_off);
				}
				isOn = !isOn;
				updateGearLay(gear, isOn);
				updataNegativeLay(isNegative, isOn);
				time = 1;
				break;
			// TODO 更多里面的功能
			case R.id.negative_img:
				System.out.println("负离子当前状态" + isNegative);
				if (isOn) {
					isNegative = !isNegative;
					AirSpa_OrderUtils.setNegative(isNegative ? (byte) 1 : 0);
					((MainActivity) getActivity()).sendData(AirSpa_OrderUtils
							.getResult());
				} else {
					showOffLine();
				}
				updataNegativeLay(isNegative, isOn);
				break;
			case R.id.timer_img:// 定时按钮
				String msg = "室外温度： " + temp_out + "℃" + "\n" + "室外湿度： "
						+ shidu_out + "％";
				Builder builder = new Builder(getActivity());
				builder.setCancelable(false);
				builder.setMessage(msg);
				builder.setTitle("室外信息");
				builder.setNegativeButton("确定",
						new DialogInterface.OnClickListener() {
							@Override
							public void onClick(DialogInterface dialog,
									int which) {
								dialog.dismiss();
							}
						});
				builder.create().show();
				break;
			case R.id.auto_img:
				if (isOn) {
					gear = 0;
					sendGear(gear);
					updateGearLay(gear, isOn);
				} else {
					showOffLine();
				}
				break;
			case R.id.sleep_img:
				if (isOn) {
					isSleepOn = !isSleepOn;
					AirSpa_OrderUtils.setSleep(isSleepOn ? (byte) 1 : 0);
					((MainActivity) getActivity()).sendData(AirSpa_OrderUtils
							.getResult());
				} else {
					showOffLine();
				}
				updataSleepLay(isSleepOn, isOn);
				break;
			case R.id.gear1_img:
				if (isOn && !isSleepOn) {
					gear = 1;
					sendGear(gear);
					updateGearLay(gear, isOn);
				} else {
					showSleep();
				}
				break;
			case R.id.gear2_img:
				if (isOn && !isSleepOn) {
					gear = 2;
					sendGear(gear);
					updateGearLay(gear, isOn);
				} else {
					showSleep();
				}
				break;
			case R.id.gear3_img:
				if (isOn && !isSleepOn) {
					gear = 3;
					sendGear(gear);
					updateGearLay(gear, isOn);
				} else {
					showSleep();
				}
				break;
			case R.id.gear4_img:
				if (isOn && !isSleepOn) {
					gear = 4;
					sendGear(gear);
					updateGearLay(gear, isOn);
				} else {
					showSleep();
				}
				break;
			case R.id.gear5_img:
				if (isOn && !isSleepOn) {
					gear = 5;
					sendGear(gear);
					updateGearLay(gear, isOn);
				} else {
					showSleep();
				}
				break;
			}
		}
	}

	private void sendGear(int gear) {
		AirSpa_OrderUtils.setOnOff(isOn ? (byte) 1 : (byte) 0);
		AirSpa_OrderUtils.setGear((byte) gear);
		((MainActivity) getActivity()).sendData(AirSpa_OrderUtils.getResult());

	}

	private void updataNegativeLay(boolean on, boolean enable) {
		if (enable) {
			if (on) {
				negative_img.setImageResource(R.drawable.qizhen_negative_on);
			} else {
				negative_img.setImageResource(R.drawable.qizhen_negative_off);
			}
		} else {
			negative_img.setImageResource(R.drawable.qizhen_negative_off);
		}

	}

	private void updataQualityLay(int quality) {
		switch (quality) {
		case 0:
			xinfeng_quality.setImageResource(R.drawable.qizhen_you);
			break;
		case 1:
			xinfeng_quality.setImageResource(R.drawable.qizhen_liang);
			break;

		case 2:
			xinfeng_quality.setImageResource(R.drawable.qizhen_qingdu);
			break;
		case 3:
			xinfeng_quality.setImageResource(R.drawable.qizhen_zhongdu);
			break;

		default:
			break;
		}
	}

	private void updataWorkTime(int time) {
		BigDecimal bd = new BigDecimal(time);
		double hour = bd.setScale(2, BigDecimal.ROUND_HALF_UP).doubleValue();
		pm25_weight.setText(new BigDecimal(hour * 31.75).setScale(2,
				BigDecimal.ROUND_HALF_UP).doubleValue()
				+ "");
		xinfeng_rice.setText("——相当于"
				+ new BigDecimal(hour * 31.75 / 20).setScale(1,
						BigDecimal.ROUND_HALF_UP).doubleValue() + "颗大米——");
	}

	private void updataSleepLay(boolean on, boolean enable) {
		if (enable) {
			if (on) {
				sleep_img.setImageResource(R.drawable.xinfeng_sleep_on);
			} else {
				sleep_img.setImageResource(R.drawable.xinfeng_sleep_off);
			}
		} else {
			sleep_img.setImageResource(R.drawable.xinfeng_sleep_off);
		}

	}

	private void updataTimerLay(boolean on, boolean enable) {
		if (enable) {
			if (on) {
				timer_img.setImageResource(R.drawable.xinfeng_timer_on);
			} else {
				timer_img.setImageResource(R.drawable.xinfeng_timer_off);
			}
		} else {
			timer_img.setImageResource(R.drawable.xinfeng_timer_off);
		}

	}

	private void updateGearLay(int gear, boolean enable) {
		if (isSleepOn)
			gear = 0;
		for (int i = 0; i < gear_imgs.length; i++) {
			if (enable) {
				if (i != (gear))
					gear_imgs[i].setImageResource(gearImgOff_ids[i]);
				else
					gear_imgs[i].setImageResource(gearImgOn_ids[i]);
			} else {
				gear_imgs[i].setImageResource(gearImgOff_ids[i]);
			}

		}

	}

	/**
	 * 发送开机命令
	 */
	private void sendON() {
		((MainActivity) getActivity()).sendData(AirSpa_OrderUtils.GET_ON());
	}

	/**
	 * 发送关机命令
	 */
	private void sendOFF() {
		((MainActivity) getActivity()).sendData(AirSpa_OrderUtils.GET_OFF());
	}

	@Override
	public void receviceData(byte[] data) {
		System.out.println("收到pipe：" + XlinkUtils.getHexBinString(data));
		status = AirSpa_OrderUtils.receive(data);
		if (status != null) {
			synchronized (status) {
				if (getActivity() != null)
				updataStatus(status);
			}
		} else {
			Log.e("", "返回数据错误");
		}
	}

	private void updataStatus(AirSpa_Status status) {
		if (null != status) {
			if (status.isAlarmCode()) {
				System.out.println("故障信息");
				return;
			}
			if (status.isSwitch()) {
				System.out.println("开关信息");
				isOn = status.isOn();
				if (isOn) {
					btn_onoff.setBackgroundResource(R.drawable.b_xinfeng_off);
				} else {
					btn_onoff.setBackgroundResource(R.drawable.b_xinfeng_on);
				}
				return;
			}
			if (status.isStatusQuery()) {
				System.out.println("状态信息");
				isOn = status.isOn();
				isTimerOn = status.isTimerOn();
				temp_in.setText(status.getTemp_in() + "℃");
				System.out
						.println("temp_in+   " + temp_in.getText().toString());
				shidu_in.setText(status.getHumidity_in() + "％");
				co2_value.setText(status.getCo2() + "");
				pm25_value.setText(status.getPm25() + "");
				isNegative = status.isNegative();
				gear = status.getGear();
				isSleepOn = status.isSleep();
				temp_out = status.getTemp_out();
				shidu_out = status.getHumidity_out();
				updataQualityLay(status.getQuality());
				updataNegativeLay(isNegative, isOn);
				updataSleepLay(isSleepOn, isOn);
				// updataTimerLay(isTimerOn, isOn);
				AirSpa_OrderUtils.setNegative(isNegative ? (byte) 1 : 0);
				AirSpa_OrderUtils.setSleep(isSleepOn ? (byte) 1 : 0);
				AirSpa_OrderUtils.setTimer(isTimerOn ? (byte) 1 : 0);
				AirSpa_OrderUtils.setGear((byte) gear);
				AirSpa_OrderUtils.setOnOff(isOn ? (byte) 1 : 0);
				if (gear < 8 && gear >= 0) {
					updateGearLay(gear, isOn);
				}
				MainActivity.model = status.getModel();
				AirSpa_OrderUtils.setModel((byte) MainActivity.model);
				if (isOn) {
					btn_onoff.setBackgroundResource(R.drawable.b_xinfeng_off);
				} else {
					btn_onoff.setBackgroundResource(R.drawable.b_xinfeng_on);
				}
				setTips(getTips(status));
				return;
			}
			if (status.isControlBack()) {
				isTimerOn = status.isTimerOn();
				isOn = status.isOn();
				isNegative = status.isNegative();
				gear = status.getGear();
				isSleepOn = status.isSleep();
				updataNegativeLay(isNegative, isOn);
				updataSleepLay(isSleepOn, isOn);
				// updataTimerLay(isTimerOn, isOn);
				AirSpa_OrderUtils.setNegative(isNegative ? (byte) 1 : 0);
				AirSpa_OrderUtils.setSleep(isSleepOn ? (byte) 1 : 0);
				AirSpa_OrderUtils.setTimer(isTimerOn ? (byte) 1 : 0);
				AirSpa_OrderUtils.setGear((byte) gear);
				AirSpa_OrderUtils.setOnOff(isOn ? (byte) 1 : 0);
				if (gear < 8 && gear >= 0) {
					updateGearLay(gear, isOn);
				}
				MainActivity.model = status.getModel();
				AirSpa_OrderUtils.setModel((byte) MainActivity.model);
				return;
			}
			if (status.isWorkTimeBck()) {
				updataWorkTime(status.getWork_time());
			}
			if (status.isNeedClean()) {// 是否需要清洁
			} else {
			}
			if (status.isAlarm()) {// 是否有报警
			} else {
			}
		}

	}

	private void setTips(String tips) {
		System.out.println("使用建议：" + tips);
		String[] array = tips.split("、");
		if (array.length == 2) {
			xinfeng_tip1.setText(array[0]);
			xinfeng_tip2.setText(array[1]);
		}
	}

	private String getTips(AirSpa_Status status) {
		String tips = "";
		int co2 = status.getCo2();
		int pm25 = status.getPm25();
		if (!isOn && co2 < 1000 && pm25 < 50) {
			tips = "室内空气清新、适宜室内活动";
		} else if (!isOn && (co2 >= 1000 && co2 < 2000)) {
			tips = "室内空气混浊、请开启新风";
		} else if (isOn && (co2 >= 1000 && co2 < 2000)) {
			tips = "室内空气混浊、请保持新风开启";
		} else if (!isOn && co2 >= 2000) {
			tips = "空气十分混浊、请开启新风";
		} else if (isOn && co2 >= 2000) {
			tips = "空气十分混浊、请保持新风开启";
		} else if (!isOn && co2 < 1000 && pm25 >= 50 && pm25 < 100) {
			tips = "室内空气一般、请开启新风";
		} else if (isOn && co2 < 1000 && pm25 >= 50 && pm25 < 100) {
			tips = "室内空气一般、请保持新风开启";
		} else if (!isOn && co2 < 1000 && pm25 >= 100) {
			tips = "室内空气混浊、请开启新风";
		} else if (isOn && co2 < 1000 && pm25 >= 100) {
			tips = "室内空气混浊、请保持新风开启";
		} else if (isOn && Constant.WEATHER_QUALITY == 2 && co2 < 1000
				&& pm25 < 50) {
			tips = "室外空气良、请保持新风开启";
		} else if (isOn && Constant.WEATHER_QUALITY == 3 && co2 < 1000
				&& pm25 < 50) {
			tips = "室外空气轻度污染、儿童老人减少户外活动";
		} else if (isOn && Constant.WEATHER_QUALITY == 4 && co2 < 1000
				&& pm25 < 50) {
			tips = "室外空气中度污染、请较少户外活动";
		} else if (isOn && Constant.WEATHER_QUALITY == 5 && co2 < 1000
				&& pm25 < 50) {
			tips = "室外空气重度污染、请避免户外活动";
		} else if (isOn && Constant.WEATHER_QUALITY == 6 && co2 < 1000
				&& pm25 < 50) {
			tips = "室外空气严重污染、请停止户外活动";
		} else if (isOn && co2 < 1000 && pm25 < 50) {
			tips = "室内空气清新、请保持新风开启";
		} else {
			tips = "室内空气清新、请保持新风开启";
		}
		return tips;
	}

	/**
	 * 启动定时器,开始倒计时
	 */
	private void startTimer() {
		if (timer == null) {
			timer = new Timer();
		}
		if (timerTask == null) {
			timerTask = new TimerTask() {

				@Override
				public void run() {
					if (time % 20 == 2) {
						((MainActivity) getActivity())
								.sendData(AirSpa_OrderUtils.GET_QUERY());
					} else if (time % 20 == 12) {
						((MainActivity) getActivity())
								.sendData(AirSpa_OrderUtils.QUERT_WORKTIME());
					} else if (time % 6 == 4) {
						// ((MainActivity) getActivity())
						// .sendData(AirSpa_OrderUtils.GET_ALQRM());
					}
					if (++time > 20)
						time = 0;
				}
			};
			if (timer != null && timerTask != null) {
				timer.schedule(timerTask, 0, 1000);
			}
		}

	}

	/**
	 * 停止定时器
	 */
	private void stopTimer() {
		if (timerTask != null) {
			timerTask.cancel();
			timerTask = null;
		}

		if (timer != null) {
			timer.cancel();
			timer = null;
		}
	}

	// Tab相关
	private String title;
	private int iconId;

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public int getIconId() {
		return iconId;
	}

	public void setIconId(int iconId) {
		this.iconId = iconId;
	}
}
