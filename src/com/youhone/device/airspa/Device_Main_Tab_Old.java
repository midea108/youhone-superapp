package com.youhone.device.airspa;

import io.xlink.wifi.ui.ChangeColorIconWithTextView;
import io.xlink.wifi.ui.fragment.BaseFragment;
import io.xlink.wifi.ui.view.LazyViewPager;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.List;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewConfiguration;
import android.view.ViewGroup;

import com.youhone.activity.MainActivity;
import com.youhone.activity.MainActivity.ReceiveDataListener;
import com.youhone.fragment.airspa.AirSpaFragment_Timer;
import com.youhone.utils.airspa.AirSpa_OrderUtils;
import com.youhone.utils.airspa.AirSpa_Status;
import com.youhone.xlink.superapp.R;

public class Device_Main_Tab_Old extends BaseFragment implements
		LazyViewPager.OnPageChangeListener, OnClickListener,
		ReceiveDataListener {
	public static int position = 0;
	private LazyViewPager mViewPager;
	private List<Fragment> mTabs = new ArrayList<Fragment>();
	private FragmentStatePagerAdapter mAdapter;

	private List<ChangeColorIconWithTextView> mTabIndicator = new ArrayList<ChangeColorIconWithTextView>();

	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View view = inflater.inflate(R.layout.tab_xinfeng, container, false);
		setOverflowShowingAlways();
		mViewPager = (LazyViewPager) view.findViewById(R.id.id_viewpager);
		initDatas(view);
		mViewPager.setAdapter(mAdapter);
		mViewPager.setOnPageChangeListener(this);
		mViewPager.setCurrentItem(2, false);
		mViewPager.setOffscreenPageLimit(0);
		return view;
	}

	private void initDatas(View view) {

		Fragment modelFragment = new AirSpaFragment_Model();
		Bundle Modelargs = new Bundle();
		Modelargs.clear();
		Modelargs.putString("title", "模式");
		modelFragment.setArguments(Modelargs);
		mTabs.add(modelFragment);

		Fragment pmFragment = new AirSpaFragment_Timer();
		Bundle pmargs = new Bundle();
		pmargs.clear();
		pmargs.putString("title", "定时");
		pmFragment.setArguments(pmargs);
		mTabs.add(pmFragment);

		Fragment deviceFragment = new AirSpaFragment_Control_New();
		Bundle deviceargs = new Bundle();
		deviceargs.putString("title", "控制页");
		deviceFragment.setArguments(deviceargs);
		mTabs.add(deviceFragment);

		Fragment filterFragment = new AirSpaFragment_Filter();
		Bundle filterargs = new Bundle();
		filterargs.putString("title", "滤芯信息");
		filterFragment.setArguments(filterargs);
		mTabs.add(filterFragment);

		Fragment dataFragment = new AirSpaFragment_Record();
		Bundle dataargs = new Bundle();
		dataargs.putString("title", "数据");
		dataFragment.setArguments(filterargs);
		mTabs.add(dataFragment);

		mAdapter = new FragmentStatePagerAdapter(getFragmentManager()) {

			@Override
			public int getCount() {
				return mTabs.size();
			}

			@Override
			public Fragment getItem(int arg0) {
				return mTabs.get(arg0);
			}
		};

		initTabIndicator(view);

	}

	private void initTabIndicator(View view) {
		ChangeColorIconWithTextView one = (ChangeColorIconWithTextView) view
				.findViewById(R.id.id_indicator_model);
		ChangeColorIconWithTextView two = (ChangeColorIconWithTextView) view
				.findViewById(R.id.id_indicator_timer);
		ChangeColorIconWithTextView three = (ChangeColorIconWithTextView) view
				.findViewById(R.id.id_indicator_control);

		ChangeColorIconWithTextView four = (ChangeColorIconWithTextView) view
				.findViewById(R.id.id_indicator_filter);
		ChangeColorIconWithTextView five = (ChangeColorIconWithTextView) view
				.findViewById(R.id.id_indicator_data);

		mTabIndicator.add(one);
		mTabIndicator.add(two);
		mTabIndicator.add(three);
		mTabIndicator.add(four);
		mTabIndicator.add(five);

		one.setOnClickListener(this);
		two.setOnClickListener(this);
		three.setOnClickListener(this);
		four.setOnClickListener(this);
		five.setOnClickListener(this);

		three.setIconAlpha(1.0f);

	}

	@Override
	public void onPageSelected(int arg0) {
		position = arg0;
	}

	@Override
	public void onPageScrolled(int position, float positionOffset,
			int positionOffsetPixels) {
		// Log.e("TAG", "position = " + position + " , positionOffset = "
		// + positionOffset);

		if (positionOffset > 0) {
			ChangeColorIconWithTextView left = mTabIndicator.get(position);
			ChangeColorIconWithTextView right = mTabIndicator.get(position + 1);

			left.setIconAlpha(1 - positionOffset);
			right.setIconAlpha(positionOffset);
		}

	}

	@Override
	public void onPageScrollStateChanged(int state) {

	}

	@Override
	public void onClick(View v) {

		resetOtherTabs();
		int id = v.getId();
		if (id == R.id.id_indicator_control) {
			mTabIndicator.get(2).setIconAlpha(1.0f);
			mViewPager.setCurrentItem(2, false);
		} else if (id == R.id.id_indicator_filter) {
			mTabIndicator.get(3).setIconAlpha(1.0f);
			mViewPager.setCurrentItem(3, false);
		} else if (id == R.id.id_indicator_timer) {
			mTabIndicator.get(1).setIconAlpha(1.0f);
			mViewPager.setCurrentItem(1, false);
		} else if (id == R.id.id_indicator_model) {
			mTabIndicator.get(0).setIconAlpha(1.0f);
			mViewPager.setCurrentItem(0, false);
		} else if (id == R.id.id_indicator_data) {
			mTabIndicator.get(4).setIconAlpha(1.0f);
			mViewPager.setCurrentItem(4, false);
		}

	}

	/**
	 * 重置其他的Tab
	 */
	private void resetOtherTabs() {
		for (int i = 0; i < mTabIndicator.size(); i++) {
			mTabIndicator.get(i).setIconAlpha(0);
		}
	}

	private void setOverflowShowingAlways() {
		try {
			// true if a permanent menu key is present, false otherwise.
			ViewConfiguration config = ViewConfiguration.get(getActivity());
			Field menuKeyField = ViewConfiguration.class
					.getDeclaredField("sHasPermanentMenuKey");
			menuKeyField.setAccessible(true);
			menuKeyField.setBoolean(config, false);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Override
	public void receviceData(byte[] data) {
		AirSpa_Status status = AirSpa_OrderUtils.receive(data);
		if (status != null)
			MainActivity.model = status.getModel();
	}

}
