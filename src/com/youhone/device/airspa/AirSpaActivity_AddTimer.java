package com.youhone.device.airspa;

import io.xlink.wifi.sdk.XDevice;
import io.xlink.wifi.sdk.XlinkAgent;
import io.xlink.wifi.sdk.XlinkCode;
import io.xlink.wifi.sdk.listener.SendPipeListener;
import io.xlink.wifi.ui.activity.ActivityManager;
import io.xlink.wifi.ui.bean.Device;
import io.xlink.wifi.ui.manage.DeviceManage;
import io.xlink.wifi.ui.util.XlinkUtils;

import java.util.Calendar;

import android.app.Activity;
import android.app.Dialog;
import android.app.TimePickerDialog;
import android.content.ContentValues;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.Button;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.TimePicker.OnTimeChangedListener;
import android.widget.Toast;

import com.youhone.activity.MainActivity;
import com.youhone.activity.MainActivity.ReceiveDataListener;
import com.youhone.bean.TimerItem;
import com.youhone.db.Timer_Db_Helper;
import com.youhone.utils.airspa.AirSpa_OrderUtils;
import com.youhone.utils.airspa.AirSpa_Status;
import com.youhone.xlink.superapp.R;



public class AirSpaActivity_AddTimer extends Activity implements
		ReceiveDataListener, OnClickListener, OnTimeChangedListener {
	private Button timer_on ;
	private TextView timer_save;
	private Device device;
	private Button title_back;
	private TimerItem item;
	private Calendar c;
	private TextView begin_timer, end_timer;
	private int begin_hour, begin_min;
	private int end_hour, end_min;
	private boolean isOn = false;
	private boolean isadd = false;
	private int position = 0;
	private TimePicker tp_begin, tp_end;

	@Override
	protected void onCreate(Bundle arg0) {
		super.onCreate(arg0);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.activity_addtimer);
		ActivityManager manager = new ActivityManager();
		manager.applyKitKatTranslucency(AirSpaActivity_AddTimer.this,
				0xff63C7C0);
		setResult(RESULT_OK);
		tp_begin = (TimePicker) findViewById(R.id.tp_begin);
		tp_end = (TimePicker) findViewById(R.id.tp_end);
		tp_begin.setIs24HourView(true);
		tp_end.setIs24HourView(true);
		tp_begin.setOnTimeChangedListener(this);
		tp_end.setOnTimeChangedListener(this);
		begin_timer = (TextView) findViewById(R.id.begin_timer);
		end_timer = (TextView) findViewById(R.id.end_timer);
		timer_on = (Button) findViewById(R.id.timer_on);
		timer_save = (TextView) findViewById(R.id.timer_save);
		title_back = (Button) findViewById(R.id.timer_title_back);
		device = DeviceManage.getInstance().getDevices().get(0);
		Intent intent = getIntent();
		isadd = intent.getBooleanExtra("isadd", false);
		c = Calendar.getInstance();
		if (isadd) {

			tp_begin.setCurrentHour(c.get(Calendar.HOUR_OF_DAY));
			tp_begin.setCurrentMinute(c.get(Calendar.MINUTE));
			tp_end.setCurrentHour(c.get(Calendar.HOUR_OF_DAY));
			tp_end.setCurrentMinute(c.get(Calendar.MINUTE));
		} else {
			position = intent.getIntExtra("position", 0);
			item = (TimerItem) intent.getSerializableExtra("timerItem");
			isOn = item.isOn();
			if (item.isOn()) {
				timer_on.setText("关闭");
				timer_on.setBackgroundResource(R.drawable.btn_model_off);
			} else {
				timer_on.setText("开启");
				timer_on.setBackgroundResource(R.drawable.btn_model_on);
			}
			begin_timer.setText(item.getTime_on());
			end_timer.setText(item.getTime_off());
			if (item.getTime_on().split(" : ").length > 1) {
				begin_hour = Integer
						.parseInt(item.getTime_on().split(" : ")[0]);
				begin_min = Integer.parseInt(item.getTime_on().split(" : ")[1]);
				tp_begin.setCurrentHour(begin_hour);
				tp_begin.setCurrentMinute(begin_min);
			}
			if (item.getTime_off().split(" : ").length > 1) {
				end_hour = Integer.parseInt(item.getTime_off().split(" : ")[0]);
				end_min = Integer.parseInt(item.getTime_off().split(" : ")[1]);
				tp_end.setCurrentHour(end_hour);
				tp_end.setCurrentMinute(end_min);
			}

		}

		timer_save.setOnClickListener(this);
		timer_on.setOnClickListener(this);
		title_back.setOnClickListener(this);
		begin_timer.setOnClickListener(this);
		end_timer.setOnClickListener(this);
	}

	public boolean sendData(final byte[] bs) {
		System.out.println("发送命令");
		for (byte b : bs) {
			System.out.format("%02X ", b);
		}
		System.out.println("");
		int ret = XlinkAgent.getInstance().sendPipeData(device.getXDevice(),
				bs, new SendPipeListener() {
					@Override
					public void onSendLocalPipeData(XDevice xdevice, int code,
							int msgId) {
						// setDeviceStatus(false);
						switch (code) {
						case XlinkCode.SUCCEED:
							Log.e("model",
									"发送数据" + XlinkUtils.getHexBinString(bs)
											+ "成功");
							break;
						case XlinkCode.TIMEOUT:
							// 重新调用connect
							Log.e("model",
									"发送数据超时：" + XlinkUtils.getHexBinString(bs));
							// XlinkUtils.shortTips("发送数据超时："
							// + );

							break;
						case XlinkCode.SERVER_CODE_UNAUTHORIZED:
							// XlinkUtils.shortTips("控制设备失败,当前帐号未订阅此设备，请重新订阅");
							Log.e("model", "控制设备失败,当前帐号未订阅此设备，请重新订阅");

							break;
						case XlinkCode.SERVER_DEVICE_OFFLIEN:
							XlinkUtils.shortTips("设备不在线");
							break;
						default:
							XlinkUtils.shortTips("控制设备其他错误码:" + code);
							Log.e("model", "控制设备其他错误码:" + code);
							break;
						}
					}
				});
		if (ret < 0) {

			Log.e("model", "发送数据失败，错误码：" + ret);
			return false;
		}
		return true;
	}

	@Override
	public void receviceData(byte[] data) {
		AirSpa_Status status = AirSpa_OrderUtils.receive(data);
		if (status != null)
			MainActivity.model = status.getModel();
	}

	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.timer_on:
			if (begin_timer.getText().toString().equals("请选择")
					&& end_timer.getText().toString().equals("请选择")) {
				Toast.makeText(this, "请设置时间", Toast.LENGTH_SHORT).show();
				return;
			}
			if (isOn) {
				timer_on.setText("开启");
				timer_on.setBackgroundResource(R.drawable.btn_model_on);
				item.setOn(false);
				AirSpa_OrderUtils.setTimer((byte) 0);// 关定时
			} else {
				int current_hour = c.get(Calendar.HOUR_OF_DAY);
				int current_min = c.get(Calendar.MINUTE);
				if (!begin_timer.getText().toString().equals("请选择")) {
					int send_begin_hour = begin_hour - current_hour;
					if (send_begin_hour < 0) {
						send_begin_hour += 24;
					}
					int send_begin_min = begin_min - current_min;
					if (send_begin_min < 0) {
						send_begin_min += 60;
						send_begin_hour--;
					}
					send_begin_min = (send_begin_min + (send_begin_hour) * 60) / 6;
					AirSpa_OrderUtils.setTimer((byte) 1);
					AirSpa_OrderUtils.setTimeOn((byte) send_begin_min);
				}

				if (!end_timer.getText().toString().equals("请选择")) {
					int send_end_hour = end_hour - current_hour;
					if (send_end_hour < 0) {
						send_end_hour += 24;
					}
					int send_end_min = end_min - current_min;
					if (send_end_min < 0) {
						send_end_min += 60;
						send_end_hour--;
					}
					send_end_min = (send_end_min + (send_end_hour) * 60) / 6;
					AirSpa_OrderUtils.setTimeOff((byte) send_end_min);
					AirSpa_OrderUtils.setTimer((byte) 2);
				}
				if (!begin_timer.getText().toString().equals("请选择")
						&& !end_timer.getText().toString().equals("请选择")) {
					System.out.println("双定");
					AirSpa_OrderUtils.setTimer((byte) 3);
				}
				timer_on.setText("关闭");
				timer_on.setBackgroundResource(R.drawable.btn_model_off);
			}
			isOn = !isOn;
			onClick(timer_save);
			sendData(AirSpa_OrderUtils.getResult());
			break;
		case R.id.timer_save:
			if (begin_timer.getText().toString().equals("请选择")
					&& end_timer.getText().toString().equals("请选择")) {
				Toast.makeText(this, "请设置时间", Toast.LENGTH_SHORT).show();
				return;
			}
			Timer_Db_Helper myDBHelper = new Timer_Db_Helper(AirSpaActivity_AddTimer.this);
			SQLiteDatabase dbwrite = myDBHelper.getWritableDatabase();
			ContentValues value = new ContentValues();
			value.put("time_on", begin_timer.getText().toString());
			value.put("time_off", end_timer.getText().toString());
			value.put("ison", isOn ? "true" : "false");
			if (isadd) {
				dbwrite.insert("youhone_airspa_timer_db", null, value);
			} else {
				Cursor c = dbwrite.query("youhone_airspa_timer_db", null, null, null, null,
						null, null);
				c.moveToPosition(position);
				dbwrite.update("youhone_airspa_timer_db", value, "id=?",
						new String[] { c.getString(c.getColumnIndex("id")) });
				c.close();
			}
			dbwrite.close();
			myDBHelper.close();
			isadd = false;
			Toast.makeText(this, "保存成功", Toast.LENGTH_SHORT).show();
			break;
		case R.id.timer_title_back:
			setResult(RESULT_OK);
			finish();
			break;
		case R.id.begin_timer:
			showTimerDialog(R.id.begin_timer);
			break;
		case R.id.end_timer:
			showTimerDialog(R.id.end_timer);
			break;

		default:
			break;
		}

	}

	private void showTimerDialog(final int text_id) {
		c = Calendar.getInstance();
		Dialog dialog = new TimePickerDialog(AirSpaActivity_AddTimer.this,
				new TimePickerDialog.OnTimeSetListener() {
					public void onTimeSet(TimePicker view, int hourOfDay,
							int minute) {
						((TextView) (AirSpaActivity_AddTimer.this
								.findViewById(text_id)))
								.setText(hourOfDay < 10 ? "0" + hourOfDay
										: hourOfDay
												+ " : "
												+ (minute < 10 ? "0" + minute
														: minute));
						switch (text_id) {
						case R.id.begin_timer:
							begin_hour = hourOfDay;
							begin_min = minute;
							break;
						case R.id.end_timer:
							end_hour = hourOfDay;
							end_min = minute;
							break;
						default:
							break;
						}
					}
				}, c.get(Calendar.HOUR_OF_DAY), c.get(Calendar.MINUTE), true);

		dialog.show();
	}

	@Override
	public void onTimeChanged(TimePicker view, int hourOfDay, int minute) {
		switch (view.getId()) {
		case R.id.tp_begin:
			begin_timer.setText(hourOfDay < 10 ? "0" + hourOfDay : hourOfDay
					+ " : " + (minute < 10 ? "0" + minute : minute));
			break;
		case R.id.tp_end:
			end_timer.setText(hourOfDay < 10 ? "0" + hourOfDay : hourOfDay
					+ " : " + (minute < 10 ? "0" + minute : minute));
			break;
		default:
			break;
		}

	}

}
