package com.youhone.device.airspa;

import io.xlink.wifi.sdk.XDevice;
import io.xlink.wifi.sdk.XlinkAgent;
import io.xlink.wifi.sdk.XlinkCode;
import io.xlink.wifi.sdk.listener.SendPipeListener;
import io.xlink.wifi.ui.activity.ActivityManager;
import io.xlink.wifi.ui.manage.DeviceManage;
import io.xlink.wifi.ui.util.XlinkUtils;

import java.util.Calendar;

import android.app.Activity;
import android.app.TimePickerDialog;
import android.app.TimePickerDialog.OnTimeSetListener;
import android.content.ContentValues;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import com.youhone.db.Timer_Db_Helper;
import com.youhone.utils.airspa.AirSpa_OrderUtils;
import com.youhone.xlink.superapp.R;

public class XinfengActivity_Timer_Model extends Activity implements
		OnClickListener {
	private Intent intent;
	private ImageView timer_bg;
	private TextView timer_model_text;
	private int position = 0;
	private RelativeLayout timer_model_timeon, timer_model_timeoff;
	private TextView time_on, time_off;
	private int hour, min;
	private Button timer_title_back;
	private TextView text1, text2;
	private Button xinfeng_timer_switch;
	private boolean time_ison = false;

	@Override
	protected void onCreate(Bundle arg0) {
		super.onCreate(arg0);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.activity_xinfeng_timer_model);
		ActivityManager manager = new ActivityManager();
		manager.applyKitKatTranslucency(XinfengActivity_Timer_Model.this,
				getResources().getColor(R.color.main_color));
		intent = getIntent();
		setResult(RESULT_OK);
		xinfeng_timer_switch = (Button) findViewById(R.id.xinfeng_timer_switch);
		timer_bg = (ImageView) findViewById(R.id.timer_bg);
		text1 = (TextView) findViewById(R.id.text1);
		text2 = (TextView) findViewById(R.id.text2);
		time_on = (TextView) findViewById(R.id.time_on);
		time_off = (TextView) findViewById(R.id.time_off);
		timer_model_text = (TextView) findViewById(R.id.timer_model_text);
		timer_model_timeon = (RelativeLayout) findViewById(R.id.timer_model_timeon);
		timer_model_timeoff = (RelativeLayout) findViewById(R.id.timer_model_timeoff);
		timer_model_timeon.setOnClickListener(this);
		timer_model_timeoff.setOnClickListener(this);
		xinfeng_timer_switch.setOnClickListener(this);
		position = intent.getIntExtra("position", 0);
		showView(position);
		timer_title_back = (Button) findViewById(R.id.timer_title_back);
		timer_title_back.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				finish();
			}
		});
	}

	private void showView(int position) {
		switch (position) {
		case 0:
			timer_bg.setImageResource(R.drawable.xinfeng_timer_sleep_bg);
			timer_model_text.setText(R.string.xinfeng_timer_sleep);
			break;
		case 1:
			timer_bg.setImageResource(R.drawable.xinfeng_timer_child_bg);
			timer_model_text.setText(R.string.xinfeng_timer_child);
			break;
		case 2:
			text1.setText("开启");
			text2.setText("关闭");
			timer_bg.setImageResource(R.drawable.xinfeng_timer_weekend_bg);
			timer_model_text.setText(R.string.xinfeng_timer_weekend);
			break;
		default:
			break;
		}
		Timer_Db_Helper myDBHelper = new Timer_Db_Helper(
				XinfengActivity_Timer_Model.this);
		SQLiteDatabase dbwrite = myDBHelper.getWritableDatabase();
		Cursor c = dbwrite.query("airspa_timer_db", null, null, null, null,
				null, null);
		c.moveToPosition(position);
		time_on.setText(c.getString(c.getColumnIndex("time_on")));
		time_off.setText(c.getString(c.getColumnIndex("time_off")));
		time_ison = c.getString(c.getColumnIndex("ison")).equals("true");
		if (time_ison) {
			xinfeng_timer_switch.setText("关闭定时");
		} else {
			xinfeng_timer_switch.setText("开启定时");
		}
		c.close();
		dbwrite.close();
		myDBHelper.close();
	}

	@Override
	public void onClick(View v) {
		Calendar calendar = Calendar.getInstance();
		hour = calendar.get(Calendar.HOUR_OF_DAY);
		min = calendar.get(Calendar.MINUTE);
		switch (v.getId()) {
		case R.id.xinfeng_timer_switch:
			if (time_ison) {// 关闭定时
				closeTimer();
			} else {// 打开定时
				openTimer();
			}
			break;
		case R.id.timer_model_timeon:
			TimePickerDialog datePickerDialog_on = new TimePickerDialog(
					XinfengActivity_Timer_Model.this, new OnTimeSetListener() {

						@Override
						public void onTimeSet(TimePicker view, int hourOfDay,
								int minute) {
							time_on.setText((hourOfDay < 10 ? "0" + hourOfDay
									: hourOfDay)
									+ ":"
									+ (minute < 10 ? "0" + minute : minute));
							Timer_Db_Helper myDBHelper = new Timer_Db_Helper(
									XinfengActivity_Timer_Model.this);
							SQLiteDatabase dbwrite = myDBHelper
									.getWritableDatabase();
							Cursor c = dbwrite.query("airspa_timer_db", null,
									null, null, null, null, null);
							c.moveToPosition(position);
							ContentValues value1 = new ContentValues();
							value1.put("time_on", time_on.getText().toString());
							dbwrite.update("airspa_timer_db", value1, "id=?",
									new String[] { c.getString(c
											.getColumnIndex("id")) });
							c.close();
							dbwrite.close();
							myDBHelper.close();
						}
					}, hour, min, true);
			datePickerDialog_on.show();
			break;
		case R.id.timer_model_timeoff:
			TimePickerDialog datePickerDialog_off = new TimePickerDialog(
					XinfengActivity_Timer_Model.this, new OnTimeSetListener() {

						@Override
						public void onTimeSet(TimePicker view, int hourOfDay,
								int minute) {
							time_off.setText((hourOfDay < 10 ? "0" + hourOfDay
									: hourOfDay)
									+ ":"
									+ (minute < 10 ? "0" + minute : minute));
							Timer_Db_Helper myDBHelper = new Timer_Db_Helper(
									XinfengActivity_Timer_Model.this);
							SQLiteDatabase dbwrite = myDBHelper
									.getWritableDatabase();
							Cursor c = dbwrite.query("airspa_timer_db", null,
									null, null, null, null, null);
							c.moveToPosition(position);
							ContentValues value1 = new ContentValues();
							value1.put("time_off", time_off.getText()
									.toString());
							dbwrite.update("airspa_timer_db", value1, "id=?",
									new String[] { c.getString(c
											.getColumnIndex("id")) });
							c.close();
							dbwrite.close();
							myDBHelper.close();
						}
					}, hour, min, true);
			datePickerDialog_off.show();
			break;
		default:
			break;
		}
	}

	private void closeTimer() {
		System.out.println("关闭定时");
		xinfeng_timer_switch.setText("打开定时");
		AirSpa_OrderUtils.setTimerCount(0);
		sendData(AirSpa_OrderUtils.getTimerByte());
		updateDb(false);
		time_ison = false;
		Toast.makeText(XinfengActivity_Timer_Model.this, "关闭定时成功",
				Toast.LENGTH_SHORT).show();
	}

	private void openTimer() {
		// 打开定时
		time_ison = true;
		System.out.println("打开定时");
		Calendar c = Calendar.getInstance();
		int current_hour = c.get(Calendar.HOUR_OF_DAY);
		int current_min = c.get(Calendar.MINUTE);
		int begin_hour = 0, begin_min = 0;
		int end_hour = 0, end_min = 0;
		String timer_on = time_on.getText().toString();
		String timer_off = time_off.getText().toString();
		if (timer_on.split(":").length > 1) {
			begin_hour = Integer.parseInt(timer_on.split(":")[0]);
			begin_min = Integer.parseInt(timer_on.split(":")[1]);
		}
		if (timer_off.split(":").length > 1) {
			end_hour = Integer.parseInt(timer_off.split(":")[0]);
			end_min = Integer.parseInt(timer_off.split(":")[1]);
		}
		System.out.println("设定的开机的时间" + timer_on);
		System.out.println("设定的关机的时间" + timer_off);
		// 计算开机的时间
		int send_begin_hour = begin_hour - current_hour;
		int send_begin_min = begin_min - current_min;
		if (send_begin_min < 0) {
			send_begin_min += 60;
			send_begin_hour--;
		}
		if (send_begin_hour < 0) {
			send_begin_hour += 24;
		}
		send_begin_min = (send_begin_min + (send_begin_hour) * 60) / 6;
		// 计算关机的时间
		int send_end_hour = end_hour - current_hour;
		int send_end_min = end_min - current_min;
		if (send_end_min < 0) {
			send_end_min += 60;
			send_end_hour--;
		}
		if (send_end_hour < 0) {
			send_end_hour += 24;
		}
		send_end_min = (send_end_min + (send_end_hour) * 60) / 6;
		System.out.println("距离开机的时间" + send_begin_min);
		System.out.println("距离关机的时间" + send_end_min);
		System.out.println("固定模式的定时");

		AirSpa_OrderUtils.setTimerCount(2);
		AirSpa_OrderUtils.setTimer1Onoff((byte) 1);
		AirSpa_OrderUtils.setTimer1On(send_begin_min);
		AirSpa_OrderUtils.setTimer1Model((byte) 0);
		AirSpa_OrderUtils.setTimer1Gear((byte) 0);
		AirSpa_OrderUtils.setTimer1Negative((byte) 0);
		AirSpa_OrderUtils.setTimer1Onoff((byte) 0);
		AirSpa_OrderUtils.setTimer2Off(send_end_min);

		sendData(AirSpa_OrderUtils.getTimerByte());
		updateDb(true);
		finish();
	}

	private void updateDb(boolean onoff) {
		Timer_Db_Helper myDBHelper = new Timer_Db_Helper(
				XinfengActivity_Timer_Model.this);
		SQLiteDatabase dbwrite = myDBHelper.getWritableDatabase();
		Cursor cursor = dbwrite.query("airspa_timer_db", null, null, null,
				null, null, null);
		ContentValues value = new ContentValues();
		while (cursor.moveToNext()) {
			if (cursor.getPosition() == position) {
				value.clear();
				value.put("ison", onoff ? "true" : "false");
				System.out.println("更新数据库" + value.getAsString("ison"));
				dbwrite.update("airspa_timer_db", value, "id=?",
						new String[] { cursor.getString(cursor
								.getColumnIndex("id")) });
			} else {
				value.clear();
				value.put("ison", "false");
				dbwrite.update("airspa_timer_db", value, "id=?",
						new String[] { cursor.getString(cursor
								.getColumnIndex("id")) });
			}
		}

		cursor.close();
		dbwrite.close();
		myDBHelper.close();
	}

	public boolean sendData(final byte[] bs) {
		System.out.println("发送命令");
		for (byte b : bs) {
			System.out.format("%02X ", b);
		}
		System.out.println("");
		int ret = XlinkAgent.getInstance().sendPipeData(
				DeviceManage.getInstance().getDevices().get(0).getXDevice(),
				bs, new SendPipeListener() {
					@Override
					public void onSendLocalPipeData(XDevice xdevice, int code,
							int msgId) {
						// setDeviceStatus(false);
						switch (code) {
						case XlinkCode.SUCCEED:
							Log.e("model",
									"发送数据" + XlinkUtils.getHexBinString(bs)
											+ "成功");
							break;
						case XlinkCode.TIMEOUT:
							// 重新调用connect
							Log.e("model",
									"发送数据超时：" + XlinkUtils.getHexBinString(bs));
							// XlinkUtils.shortTips("发送数据超时："
							// + );

							break;
						case XlinkCode.SERVER_CODE_UNAUTHORIZED:
							// XlinkUtils.shortTips("控制设备失败,当前帐号未订阅此设备，请重新订阅");
							Log.e("model", "控制设备失败,当前帐号未订阅此设备，请重新订阅");

							break;
						case XlinkCode.SERVER_DEVICE_OFFLIEN:
							XlinkUtils.shortTips("设备不在线");
							break;
						default:
							XlinkUtils.shortTips("控制设备其他错误码:" + code);
							Log.e("model", "控制设备其他错误码:" + code);
							break;
						}
					}
				});
		if (ret < 0) {
			Log.e("model", "发送数据失败，错误码：" + ret);
			return false;
		}
		return true;
	}
}
