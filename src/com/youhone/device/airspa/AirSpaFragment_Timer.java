package com.youhone.device.airspa;

import java.util.ArrayList;

import android.app.AlertDialog.Builder;
import android.content.ContentValues;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;

import com.youhone.adapter.Xinfeng_TimerAdapter;
import com.youhone.bean.TimerItem;
import com.youhone.db.Timer_Db_Helper;
import com.youhone.xlink.superapp.R;

public class AirSpaFragment_Timer extends Fragment implements
		OnItemClickListener {
	private int requestCode = 200;
	private ListView timerList;
	private Xinfeng_TimerAdapter mTimerAdapter;
	private ArrayList<TimerItem> timerItem = new ArrayList<TimerItem>();

	public AirSpaFragment_Timer() {
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View contentView = inflater.inflate(R.layout.fragment_timer, container,
				false);
		timerList = (ListView) contentView.findViewById(R.id.timer_list);
		mTimerAdapter = new Xinfeng_TimerAdapter(getActivity(), timerItem);
		timerList.setAdapter(mTimerAdapter);
		timerList.setOnItemClickListener(this);
		addTimer();
		queryTimer();
		return contentView;
	}

	private void addTimer() {
		Timer_Db_Helper myDBHelper = new Timer_Db_Helper(getActivity());
		SQLiteDatabase dbwrite = myDBHelper.getWritableDatabase();
		Cursor c = dbwrite.query("airspa_timer_db", null, null, null, null,
				null, null);
		if (c.getCount() < 3) {
			ContentValues value1 = new ContentValues();
			value1.put("time_on", "23:40");
			value1.put("time_off", "05:30");
			value1.put("ison", "false");
			value1.put("time_on_ison", "true");
			value1.put("time_off_ison", "true");
			value1.put("timer_name", "睡眠模式");
			value1.put("timer_note", "夜色轻裘渐入眠，睡眠天使在身边");
			value1.put("timer_model", 0);
			value1.put("timer_gear", 0);
			dbwrite.insert("airspa_timer_db", null, value1);

			ContentValues value2 = new ContentValues();
			value2.put("time_on", "23:40");
			value2.put("time_off", "05:30");
			value2.put("ison", "false");
			value2.put("time_on_ison", "true");
			value2.put("time_off_ison", "true");
			value2.put("timer_name", "儿童模式");
			value2.put("timer_note", "孩子甜睡cute样，妈妈柔展欢颜");
			value2.put("timer_model", 0);
			value2.put("timer_gear", 0);
			dbwrite.insert("airspa_timer_db", null, value2);

			ContentValues value3 = new ContentValues();
			value3.put("time_on_ison", "true");
			value3.put("time_off_ison", "true");
			value3.put("time_on", "23:40");
			value3.put("time_off", "05:30");
			value3.put("ison", "false");
			value3.put("timer_name", "周末模式");
			value3.put("timer_note", "悠闲周末，安然清新");
			value3.put("timer_model", 0);
			value3.put("timer_gear", 0);
			dbwrite.insert("airspa_timer_db", null, value3);
		}
		c.close();
		dbwrite.close();
		myDBHelper.close();
	}

	private void queryTimer() {
		timerItem.clear();
		Timer_Db_Helper myDBHelper = new Timer_Db_Helper(getActivity());
		SQLiteDatabase dbwrite = myDBHelper.getWritableDatabase();
		Cursor c = dbwrite.query("airspa_timer_db", null, null, null, null,
				null, null);
		System.out.println("定时器数量" + c.getCount());
		while (c.moveToNext()) {
			TimerItem item = new TimerItem();
			item.setTimer_name(c.getString(c.getColumnIndex("timer_name")));
			item.setTimer_note(c.getString(c.getColumnIndex("timer_note")));
			item.setOn(c.getString(c.getColumnIndex("ison")).equals("true"));
			item.setTime_on(c.getString(c.getColumnIndex("time_on")));
			item.setTime_off(c.getString(c.getColumnIndex("time_off")));
			item.setTime_on_ison(c.getString(c.getColumnIndex("time_on_ison"))
					.equals("true"));
			item.setTime_off_ison(c
					.getString(c.getColumnIndex("time_off_ison"))
					.equals("true"));
			item.setModel(Integer.parseInt(c.getString(c
					.getColumnIndex("timer_model"))));
			item.setGear(Integer.parseInt(c.getString(c
					.getColumnIndex("timer_gear"))));
			timerItem.add(item);
		}
		mTimerAdapter.notifyDataSetChanged();
		c.close();
		dbwrite.close();
		myDBHelper.close();
	}

	@Override
	public void onItemClick(AdapterView<?> arg0, View arg1, int arg2, long arg3) {
		Intent intent = new Intent();
		if (arg2 == timerItem.size()) {
			intent.setClass(getActivity(), XinfengActivity_Timer_User.class);
			intent.putExtra("isadd", true);
		} else {
			if (arg2 <= 2) {
				intent.setClass(getActivity(),
						XinfengActivity_Timer_Model.class);
			} else {
				intent.setClass(getActivity(), XinfengActivity_Timer_User.class);
			}
			intent.putExtra("position", arg2);
			intent.putExtra("isadd", false);
			intent.putExtra("timerItem", timerItem.get(arg2));
		}
		startActivityForResult(intent, requestCode);
	}

	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent data) {
		if (requestCode == this.requestCode) {
			queryTimer();
		}
		super.onActivityResult(requestCode, resultCode, data);
	}

	private void showOpenDialog(final int position) {
		Builder builder = new Builder(getActivity());
		builder.setCancelable(false);
		builder.setMessage("确认开启该定时吗？");
		builder.setTitle("提示");
		builder.setNegativeButton("确定",
				new android.content.DialogInterface.OnClickListener() {
					@Override
					public void onClick(DialogInterface dialog, int which) {
						Timer_Db_Helper myDBHelper = new Timer_Db_Helper(getActivity());
						SQLiteDatabase dbwrite = myDBHelper
								.getWritableDatabase();
						Cursor c = dbwrite.query("airspa_timer_db", null, null,
								null, null, null, null);
						c.moveToPosition(position);
						ContentValues content = new ContentValues();
						while (c.moveToNext()) {
							content.clear();
							if (c.getPosition() == position) {
								content.put("ison", "true");
							} else {
								content.put("ison", "false");
							}
							dbwrite.update("airspa_timer_db", content, "id=?",
									new String[] { c.getString(c
											.getColumnIndex("id")) });
						}
						c.close();
						dbwrite.close();
						myDBHelper.close();
						queryTimer();
					}
				});
		builder.setNeutralButton("取消", null);
		builder.create().show();
	}

	private void showDeleteDialog(final int position) {
		Builder builder = new Builder(getActivity());
		builder.setCancelable(false);
		builder.setMessage("确认删除该定时吗？");
		builder.setTitle("提示");
		builder.setNegativeButton("确定",
				new android.content.DialogInterface.OnClickListener() {
					@Override
					public void onClick(DialogInterface dialog, int which) {
						Timer_Db_Helper myDBHelper = new Timer_Db_Helper(getActivity());
						SQLiteDatabase dbwrite = myDBHelper
								.getWritableDatabase();
						Cursor c = dbwrite.query("airspa_timer_db", null, null,
								null, null, null, null);
						System.out.println("长按的位置" + position + "  "
								+ c.getCount());

						c.moveToPosition(position);
						System.out.println(c.getString(c.getColumnIndex("id")));
						dbwrite.delete("airspa_timer_db", "id=?",
								new String[] { c.getString(c
										.getColumnIndex("id")) });
						c.close();
						dbwrite.close();
						myDBHelper.close();
						queryTimer();
					}
				});
		builder.setNeutralButton("取消", null);
		builder.create().show();
	}

	// Tab相关
	private String title;
	private int iconId;

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public int getIconId() {
		return iconId;
	}

	public void setIconId(int iconId) {
		this.iconId = iconId;
	}
}
