package com.youhone.device.airspa;

import java.util.Timer;
import java.util.TimerTask;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.youhone.activity.MainActivity;
import com.youhone.activity.MainActivity.ReceiveDataListener;
import com.youhone.utils.airspa.AirSpa_OrderUtils;
import com.youhone.utils.airspa.AirSpa_Status;
import com.youhone.xlink.superapp.R;

public class AirSpaFragment_Model extends Fragment implements
		ReceiveDataListener {
	private LinearLayout model_smart, model_xinfeng, model_inner_loop,
			model_temp, model_temp_shidu;
	private LinearLayout[] model_lay = new LinearLayout[5];
	private ImageView model_smart_head, model_xinfeng_head,
			model_inner_loop_head, model_temp_head, model_temp_shidu_head;
	private ImageView[] model_head = new ImageView[5];
	private TextView smart_text, xinfeng_text, inner_text, model_temp_text,
			model_temp_shidu_text;
	private TextView[] model_text = new TextView[5];
	private TextView smart_detail, xinfeng_detail, inner_detail,
			model_temp_detail, model_temp_shidu_detail;
	private TextView[] model_detail = new TextView[5];
	private BtnListener listener;
	private AirSpa_Status status;
	private Timer timer;
	private TimerTask timerTask;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View contentView = inflater.inflate(R.layout.fragment_xinfeng_model,
				container, false);
		((MainActivity) getActivity()).setOnReceive(this,
				AirSpaFragment_Model.this);
		model_lay[0] = model_smart = (LinearLayout) contentView
				.findViewById(R.id.model_smart);
		model_lay[1] = model_xinfeng = (LinearLayout) contentView
				.findViewById(R.id.model_xinfeng);
		model_lay[2] = model_inner_loop = (LinearLayout) contentView
				.findViewById(R.id.model_inner_loop);
		model_lay[3] = model_temp = (LinearLayout) contentView
				.findViewById(R.id.model_temp);
		model_lay[4] = model_temp_shidu = (LinearLayout) contentView
				.findViewById(R.id.model_temp_shidu);

		model_head[0] = model_smart_head = (ImageView) contentView
				.findViewById(R.id.model_smart_head);
		model_head[1] = model_xinfeng_head = (ImageView) contentView
				.findViewById(R.id.model_xinfeng_head);
		model_head[2] = model_inner_loop_head = (ImageView) contentView
				.findViewById(R.id.model_inner_head);
		model_head[3] = model_temp_head = (ImageView) contentView
				.findViewById(R.id.model_temp_head);
		model_head[4] = model_temp_shidu_head = (ImageView) contentView
				.findViewById(R.id.model_temp_shidu_head);

		model_text[0] = smart_text = (TextView) contentView
				.findViewById(R.id.smart_text);
		model_text[1] = xinfeng_text = (TextView) contentView
				.findViewById(R.id.xinfeng_text);
		model_text[2] = inner_text = (TextView) contentView
				.findViewById(R.id.inner_text);
		model_text[3] = model_temp_text = (TextView) contentView
				.findViewById(R.id.model_temp_text);
		model_text[4] = model_temp_shidu_text = (TextView) contentView
				.findViewById(R.id.model_temp_shidu_text);

		model_detail[0] = smart_detail = (TextView) contentView
				.findViewById(R.id.smart_detail);
		model_detail[1] = xinfeng_detail = (TextView) contentView
				.findViewById(R.id.xinfeng_detail);
		model_detail[2] = inner_detail = (TextView) contentView
				.findViewById(R.id.inner_detail);
		model_detail[3] = model_temp_detail = (TextView) contentView
				.findViewById(R.id.model_temp_detail);
		model_detail[4] = model_temp_shidu_detail = (TextView) contentView
				.findViewById(R.id.model_temp_shidu_detail);

		listener = new BtnListener();
		model_smart.setOnClickListener(listener);
		model_xinfeng.setOnClickListener(listener);
		model_inner_loop.setOnClickListener(listener);
		model_temp.setOnClickListener(listener);
		model_temp_shidu.setOnClickListener(listener);
		// changeView(MainActivity.model);

		startTimer();
		// Test
		// ((MainActivity) getActivity())
		// .sendData(new byte[] { (byte) 0xF3, (byte) 0XF5, 0X0A, 0X35,
		// 0X00, 0X0A, 0X00, 0X00, 0X00, 0X01, (byte) 0X81, 0X02,
		// 0X00, (byte) 0XCD, (byte) 0XF3, (byte) 0XFB });
		return contentView;
	}

	private void changeView(int model) {
		for (int i = 0; i < 5; i++) {
			if (i == model) {
				model_lay[i].setSelected(true);
				model_head[i].setSelected(true);
				model_detail[i].setTextColor(getActivity().getResources()
						.getColor(R.color.main_color));
				model_text[i].setTextColor(getActivity().getResources()
						.getColor(R.color.main_color));
			} else {
				model_lay[i].setSelected(false);
				model_head[i].setSelected(false);
				model_detail[i].setTextColor(0xff6a6a6a);
				model_text[i].setTextColor(0xff6a6a6a);
			}
		}
	}

	private class BtnListener implements OnClickListener {

		@Override
		public void onClick(View v) {
			int model = 0;
			switch (v.getId()) {
			case R.id.model_smart:
				model = 0;
				break;
			case R.id.model_xinfeng:
				model = 1;
				break;
			case R.id.model_inner_loop:
				model = 2;
				break;
			case R.id.model_temp:
				model = 3;
				break;
			case R.id.model_temp_shidu:
				model = 4;
				break;

			default:
				break;
			}
			// MainActivity.model = model;
			// changeView(model);
			Intent intent = new Intent(getActivity(),
					AirSpaActivity_Model_Introduce.class);
			intent.putExtra("model", model);
			getActivity().startActivity(intent);

		}

	}

	/**
	 * 启动定时器,开始倒计时
	 */
	private void startTimer() {
		if (timer == null) {
			timer = new Timer();
		}
		if (timerTask == null) {
			timerTask = new TimerTask() {

				@Override
				public void run() {
					((MainActivity) getActivity()).sendData(AirSpa_OrderUtils
							.GET_QUERY());
				}
			};
			if (timer != null && timerTask != null) {
				timer.schedule(timerTask, 0, 10000);
			}
		}

	}

	/**
	 * 停止定时器
	 */
	private void stopTimer() {
		if (timerTask != null) {
			timerTask.cancel();
			timerTask = null;
		}

		if (timer != null) {
			timer.cancel();
			timer = null;
		}
	}

	@Override
	public void onDestroyView() {
		stopTimer();
		super.onDestroyView();
	}

	@Override
	public void receviceData(byte[] data) {
		System.out.println("模式界面收到pipe：");
		status = AirSpa_OrderUtils.receive(data);
		if (status != null && getActivity() != null) {
			changeView(status.getModel());
		} else {
			Log.e("airspa_model", "返回数据错误");
		}

	}

	// Tab相关
	private String title;
	private int iconId;

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public int getIconId() {
		return iconId;
	}

	public void setIconId(int iconId) {
		this.iconId = iconId;
	}
}
