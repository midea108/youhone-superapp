package com.youhone.device.airspa;

import io.xlink.wifi.ui.util.XlinkUtils;

import java.math.BigDecimal;
import java.util.Timer;
import java.util.TimerTask;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.youhone.activity.MainActivity;
import com.youhone.activity.MainActivity.ReceiveDataListener;
import com.youhone.utils.airspa.AirSpa_OrderUtils;
import com.youhone.utils.airspa.AirSpa_Status;
import com.youhone.xlink.superapp.R;

public class AirSpaFragment_Record extends Fragment implements
		ReceiveDataListener {

	private TextView data_pm25_weight, data_rice, data_filter_remian,
			data_work_time, data_pm25_volume, data_air_volume, data_air_box,
			data_pm25_unit, data_pm25_unit2;
	private AirSpa_Status status;
	private Timer timer;
	private TimerTask timerTask;

	public AirSpaFragment_Record() {
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View contentView = inflater.inflate(R.layout.fragment_xinfeng_data,
				container, false);
		((MainActivity) getActivity()).setOnReceive(this,
				AirSpaFragment_Record.this);
		data_pm25_weight = (TextView) contentView
				.findViewById(R.id.data_pm25_weight);
		data_rice = (TextView) contentView.findViewById(R.id.data_rice);
		data_filter_remian = (TextView) contentView
				.findViewById(R.id.data_filter_remian);
		data_work_time = (TextView) contentView
				.findViewById(R.id.data_work_time);
		data_pm25_volume = (TextView) contentView
				.findViewById(R.id.data_pm25_volume);
		data_air_volume = (TextView) contentView
				.findViewById(R.id.data_air_volume);
		data_air_box = (TextView) contentView.findViewById(R.id.data_air_box);
		data_pm25_unit = (TextView) contentView
				.findViewById(R.id.data_pm25_unit);
		data_pm25_unit2 = (TextView) contentView
				.findViewById(R.id.data_pm25_unit2);
		startTimer();
		return contentView;
	}

	private void updataWorkTime(int time) {
		BigDecimal bd = new BigDecimal(time);
		double hour = bd.setScale(2, BigDecimal.ROUND_HALF_UP).doubleValue();
		data_pm25_weight.setText(new BigDecimal(hour * 17).setScale(2,
				BigDecimal.ROUND_HALF_UP).doubleValue()
				+ "");
		data_rice.setText("——相当于"
				+ new BigDecimal(hour * 17 / 20).setScale(1,
						BigDecimal.ROUND_HALF_UP).doubleValue() + "颗大米——");
		data_work_time.setText(hour + "");
		// 立方米
		double pm25_volume = new BigDecimal(hour * 200).setScale(2,
				BigDecimal.ROUND_HALF_UP).doubleValue();
		int box = (int) (pm25_volume / 5);
		data_air_box.setText(box + "");
		if (pm25_volume > 100000) {
			pm25_volume = pm25_volume / 10000;
			data_pm25_unit.setText("万立方米");
			data_pm25_unit2.setText("万立方米");
		} else {
			data_pm25_unit.setText("立方米");
			data_pm25_unit2.setText("立方米");
		}
		data_air_volume.setText(pm25_volume + "");
		data_pm25_volume.setText(new BigDecimal(pm25_volume).setScale(1,
				BigDecimal.ROUND_HALF_UP).doubleValue()
				+ "");
	}

	@Override
	public void receviceData(byte[] data) {
		System.out.println("收到pipe：" + XlinkUtils.getHexBinString(data));
		status = AirSpa_OrderUtils.receive(data);
		if (status != null) {
			updataStatus(status);
		} else {
			Log.e("", "返回数据错误");
		}
	}

	private void updataStatus(AirSpa_Status status) {
		if (null != status) {
			if (status.isWorkTimeBck()) {
				System.out.println("工作时间" + status.getWork_time());
				updataWorkTime(status.getWork_time());
			}
		}

	}

	/**
	 * 启动定时器,开始倒计时
	 */
	private void startTimer() {
		if (timer == null) {
			timer = new Timer();
		}
		if (timerTask == null) {
			timerTask = new TimerTask() {

				@Override
				public void run() {
					((MainActivity) getActivity()).sendData(AirSpa_OrderUtils
							.QUERT_WORKTIME());
				}
			};
			if (timer != null && timerTask != null) {
				timer.schedule(timerTask, 0, 10000);
			}
		}

	}

	/**
	 * 停止定时器
	 */
	private void stopTimer() {
		if (timerTask != null) {
			timerTask.cancel();
			timerTask = null;
		}

		if (timer != null) {
			timer.cancel();
			timer = null;
		}
	}

	@Override
	public void onDestroyView() {
		stopTimer();
		super.onDestroyView();
	}

	// Tab相关
	private String title;
	private int iconId;

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public int getIconId() {
		return iconId;
	}

	public void setIconId(int iconId) {
		this.iconId = iconId;
	}
}
