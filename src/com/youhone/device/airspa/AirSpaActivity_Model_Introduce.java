package com.youhone.device.airspa;

import io.xlink.wifi.sdk.XDevice;
import io.xlink.wifi.sdk.XlinkAgent;
import io.xlink.wifi.sdk.XlinkCode;
import io.xlink.wifi.sdk.listener.SendPipeListener;
import io.xlink.wifi.ui.activity.ActivityManager;
import io.xlink.wifi.ui.bean.Device;
import io.xlink.wifi.ui.manage.DeviceManage;
import io.xlink.wifi.ui.util.XlinkUtils;
import android.app.Activity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.youhone.activity.MainActivity;
import com.youhone.activity.MainActivity.ReceiveDataListener;
import com.youhone.utils.airspa.AirSpa_OrderUtils;
import com.youhone.utils.airspa.AirSpa_Status;
import com.youhone.xlink.superapp.R;

public class AirSpaActivity_Model_Introduce extends Activity implements
		ReceiveDataListener {
	private int model = 0;
	private ImageView model_on;
	private Device device;
	private Button title_back;
	private TextView title_text, model_detail_text;
	private ImageView model_detail_bg;

	@Override
	protected void onCreate(Bundle arg0) {
		super.onCreate(arg0);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.activity_xinfeng_model_detail);
		ActivityManager manager = new ActivityManager();
		manager.applyKitKatTranslucency(AirSpaActivity_Model_Introduce.this,
				getResources().getColor(R.color.main_color));
		model_detail_bg = (ImageView) findViewById(R.id.model_detail_bg);
		model_detail_text = (TextView) findViewById(R.id.model_detail_text);
		title_text = (TextView) findViewById(R.id.model_title_text);
		title_back = (Button) findViewById(R.id.model_title_back);
		model_on = (ImageView) findViewById(R.id.model_on);
		device = DeviceManage.getInstance().getDevices().get(0);
		model = getIntent().getIntExtra("model", 0);
		// if (model == MainActivity.model) {
		// model_on.setImageResource(R.drawable.btn_xinfeng_model_off);
		// } else {
		// model_on.setImageResource(R.drawable.btn_xinfeng_model_on);
		// }
		switch (model) {
		case 0:
			title_text.setText("智能模式");
			model_detail_text.setText(R.string.xinfeng_smart_model);
			model_detail_bg.setImageResource(R.drawable.xinfeng_model_smart_bg);
			break;
		case 1:
			title_text.setText("新风模式");
			model_detail_text.setText(R.string.xinfeng_model);
			model_detail_bg
					.setImageResource(R.drawable.xinfeng_model_xinfeng_bg);
			break;
		case 2:
			title_text.setText("内循环模式");
			model_detail_text.setText(R.string.xinfeng_inner);
			model_detail_bg.setImageResource(R.drawable.xinfeng_model_inner_bg);
			break;
		case 3:
			title_text.setText("恒温模式");
			model_detail_text.setText(R.string.xinfeng_model_temp);
			model_detail_bg.setImageResource(R.drawable.xinfeng_model_temp_bg);
			break;
		case 4:
			title_text.setText("恒温恒湿模式");
			model_detail_text.setText(R.string.xinfeng_model_tempshidu);
			model_detail_bg
					.setImageResource(R.drawable.xinfeng_model_tempshi_bg);
			break;
		}
		model_on.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				switch (model) {
				case 0:
				case 1:
				case 2:
				case 3:
				case 4:
					AirSpa_OrderUtils.setOnOff((byte)1);
					AirSpa_OrderUtils.setModel((byte) (model));
					sendData(AirSpa_OrderUtils.getResult());
					MainActivity.model = model;
					finish();
					break;
				}
			}
		});
		title_back.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				finish();
			}
		});
	}

	public boolean sendData(final byte[] bs) {
		System.out.println("发送命令");
		for (byte b : bs) {
			System.out.format("%02X ", b);
		}
		System.out.println("");
		int ret = XlinkAgent.getInstance().sendPipeData(device.getXDevice(),
				bs, new SendPipeListener() {
					@Override
					public void onSendLocalPipeData(XDevice xdevice, int code,
							int msgId) {
						// setDeviceStatus(false);
						switch (code) {
						case XlinkCode.SUCCEED:
							Log.e("model",
									"发送数据" + XlinkUtils.getHexBinString(bs)
											+ "成功");
							break;
						case XlinkCode.TIMEOUT:
							// 重新调用connect
							Log.e("model",
									"发送数据超时：" + XlinkUtils.getHexBinString(bs));
							// XlinkUtils.shortTips("发送数据超时："
							// + );

							break;
						case XlinkCode.SERVER_CODE_UNAUTHORIZED:
							// XlinkUtils.shortTips("控制设备失败,当前帐号未订阅此设备，请重新订阅");
							Log.e("model", "控制设备失败,当前帐号未订阅此设备，请重新订阅");

							break;
						case XlinkCode.SERVER_DEVICE_OFFLIEN:
							XlinkUtils.shortTips("设备不在线");
							break;
						default:
							XlinkUtils.shortTips("控制设备其他错误码:" + code);
							Log.e("model", "控制设备其他错误码:" + code);
							break;
						}
					}
				});
		if (ret < 0) {
			Log.e("model", "发送数据失败，错误码：" + ret);
			return false;
		}
		return true;
	}

	@Override
	public void receviceData(byte[] data) {
		AirSpa_Status status = AirSpa_OrderUtils.receive(data);
		if (status != null)
			MainActivity.model = status.getModel();
	}
	
	
}
