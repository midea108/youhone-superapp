package com.youhone.device.light;

import io.xlink.wifi.ui.util.XlinkUtils;

import java.util.Timer;
import java.util.TimerTask;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;

import com.youhone.activity.MainActivity;
import com.youhone.activity.MainActivity.ReceiveDataListener;
import com.youhone.utils.light.Light_OrderUtils;
import com.youhone.utils.light.Light_Status;
import com.youhone.xlink.superapp.R;

public class LightFragment extends Fragment implements ReceiveDataListener {
	private Button light1_on, light2_on, light3_on, lightall_on;
	private Button lightall_off;
	private BtnListener listener = new BtnListener();
	private Light_Status status;
	private Timer timer;
	private TimerTask timerTask;

	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View view = inflater.inflate(R.layout.fragment_light, container, false);
		listener = new BtnListener();
		light1_on = (Button) view.findViewById(R.id.light1_on);
		light2_on = (Button) view.findViewById(R.id.light2_on);
		light3_on = (Button) view.findViewById(R.id.light3_on);
		lightall_on = (Button) view.findViewById(R.id.lightall_on);

		lightall_off = (Button) view.findViewById(R.id.lightall_off);

		light1_on.setOnClickListener(listener);
		light2_on.setOnClickListener(listener);
		light3_on.setOnClickListener(listener);
		lightall_on.setOnClickListener(listener);

		lightall_off.setOnClickListener(listener);

		((MainActivity) getActivity()).setOnReceive(this, LightFragment.this);
		startTimer();
		return view;
	}

	@Override
	public void onDestroyView() {
		stopTimer();
		super.onDestroyView();
	}

	private class BtnListener implements OnClickListener {

		@Override
		public void onClick(View v) {
			switch (v.getId()) {
			case R.id.light1_on:
				if (light1_on.isSelected()) {
					((com.youhone.activity.MainActivity) getActivity())
							.sendData(Light_OrderUtils.sendControl((byte) 0xC1,
									(byte) 0));
					light1_on.setSelected(false);
				} else {
					((com.youhone.activity.MainActivity) getActivity())
							.sendData(Light_OrderUtils.sendControl((byte) 0xC1,
									(byte) 1));
					light1_on.setSelected(true);
				}

				break;
			case R.id.light2_on:
				if (light2_on.isSelected()) {
					((com.youhone.activity.MainActivity) getActivity())
							.sendData(Light_OrderUtils.sendControl((byte) 0xC2,
									(byte) 0));
					light2_on.setSelected(false);
				} else {
					((com.youhone.activity.MainActivity) getActivity())
							.sendData(Light_OrderUtils.sendControl((byte) 0xC2,
									(byte) 1));
					light2_on.setSelected(true);
				}
				break;
			case R.id.light3_on:
				if (light3_on.isSelected()) {
					((com.youhone.activity.MainActivity) getActivity())
							.sendData(Light_OrderUtils.sendControl((byte) 0xC3,
									(byte) 0));
					light3_on.setSelected(false);
				} else {
					((com.youhone.activity.MainActivity) getActivity())
							.sendData(Light_OrderUtils.sendControl((byte) 0xC3,
									(byte) 1));
					light3_on.setSelected(true);
				}
				break;

			case R.id.lightall_on:
				((com.youhone.activity.MainActivity) getActivity())
						.sendData(Light_OrderUtils.sendControl((byte) 0xC4,
								(byte) 1));
				break;
			case R.id.lightall_off:
				((com.youhone.activity.MainActivity) getActivity())
						.sendData(Light_OrderUtils.sendControl((byte) 0xC4,
								(byte) 0));
				break;

			default:
				break;
			}
		}
	}

	@Override
	public void receviceData(byte[] data) {
		System.out.println("收到pipe：" + XlinkUtils.getHexBinString(data));
		status = Light_OrderUtils.receive(data);
		if (status != null) {
			updataStatus(status);
		} else {
			Log.e("", "返回数据错误");
		}
	}

	private void updataStatus(Light_Status status) {
		System.out.println("status.isLight1On()+  " + status.isLight1On());
		light1_on.setSelected(status.isLight1On());

		light2_on.setSelected(status.isLight2On());

		light3_on.setSelected(status.isLight3On());
	}

	/**
	 * 启动定时器,开始倒计时
	 */
	private void startTimer() {
		if (timer == null) {
			timer = new Timer();
		}
		if (timerTask == null) {
			timerTask = new TimerTask() {

				@Override
				public void run() {
					((MainActivity) getActivity()).sendData(Light_OrderUtils
							.getStatus());
				}
			};
			if (timer != null && timerTask != null) {
				timer.schedule(timerTask, 100, 3000);
			}
		}

	}

	/**
	 * 停止定时器
	 */
	private void stopTimer() {
		if (timerTask != null) {
			timerTask.cancel();
			timerTask = null;
		}

		if (timer != null) {
			timer.cancel();
			timer = null;
		}
	}

}
