package com.youhone.adapter;

import io.xlink.wifi.sdk.XDevice;
import io.xlink.wifi.sdk.XlinkAgent;
import io.xlink.wifi.sdk.XlinkCode;
import io.xlink.wifi.sdk.listener.SendPipeListener;
import io.xlink.wifi.ui.manage.DeviceManage;
import io.xlink.wifi.ui.util.XlinkUtils;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.youhone.activity.MainActivity;
import com.youhone.bean.TimerItem;
import com.youhone.db.Timer_Db_Helper;
import com.youhone.utils.qizhen.Qizhen_OrderUtils;
import com.youhone.xlink.superapp.R;

public class Qizhen_TimerAdapter extends BaseAdapter {

	private Context mContext;
	private List<TimerItem> timerlist;
	private int size = 0;

	public Qizhen_TimerAdapter(Context context, ArrayList<TimerItem> timerlist) {

		this.mContext = context;
		this.timerlist = timerlist;
	}

	public void setDevices(ArrayList<TimerItem> timerlist) {
		this.timerlist = timerlist;
	}

	@Override
	public int getCount() {
		return timerlist.size() + 1;
	}

	@Override
	public Object getItem(int position) {
		return null;
	}

	@Override
	public long getItemId(int position) {
		return position;
	}

	@Override
	public View getView(final int position, View convertView, ViewGroup parent) {
		final ViewHolder holder = new ViewHolder();
		size = timerlist.size();
		convertView = LayoutInflater.from(mContext).inflate(
				R.layout.timelist_item, parent, false);
		holder.timerImg = (ImageView) convertView.findViewById(R.id.timerImg);
		holder.timer_btn = (ImageView) convertView.findViewById(R.id.timer_btn);
		holder.timer_name = (TextView) convertView
				.findViewById(R.id.timer_name);
		holder.timer_note = (TextView) convertView
				.findViewById(R.id.timer_note);
		convertView.setTag(holder);
		if (position != size) {
			final TimerItem item = timerlist.get(position);
			String time_name = item.getTimer_name();
			String time_note = item.getTimer_note();
			final boolean isOn = item.isOn();
			holder.timer_name.setText(time_name);
			holder.timer_note.setText(time_note);
			if (isOn) {
				holder.timerImg
						.setBackgroundResource(R.drawable.timer_icon_on_small);
				holder.timer_btn.setSelected(isOn);
			} else {
				holder.timerImg
						.setBackgroundResource(R.drawable.timer_icon_off_small);
				holder.timer_btn.setSelected(isOn);
			}
			holder.timer_btn.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View v) {
					boolean mIsOn = timerlist.get(position).isOn();
					System.out.println("定时的状态" + mIsOn);
					if (mIsOn) {// 关闭定时
						System.out.println("关闭定时");
						holder.timerImg
								.setBackgroundResource(R.drawable.timer_icon_off_small);
						holder.timer_btn.setSelected(false);
						Qizhen_OrderUtils.setTimerOnoff((byte) 1);// 关定时
						((MainActivity) mContext).sendData(Qizhen_OrderUtils
								.getTimerbyte());
					} else {// 打开定时
						System.out.println("打开定时");
						Qizhen_OrderUtils.setTimerOnoff((byte) 0);// 开定时
						Calendar c = Calendar.getInstance();
						int current_hour = c.get(Calendar.HOUR_OF_DAY);
						int current_min = c.get(Calendar.MINUTE);
						int begin_hour = 0, begin_min = 0;
						int end_hour = 0, end_min = 0;
						String timer_on = item.getTime_on();
						String timer_off = item.getTime_off();
						if (timer_on.split(":").length > 1) {
							begin_hour = Integer.parseInt(timer_on.split(":")[0]);
							begin_min = Integer.parseInt(timer_on.split(":")[1]);
						}
						if (timer_off.split(":").length > 1) {
							end_hour = Integer.parseInt(timer_off.split(":")[0]);
							end_min = Integer.parseInt(timer_off.split(":")[1]);
						}
						System.out.println("设定的开机的时间" + timer_on);
						System.out.println("设定的关机的时间" + timer_off);
						// 计算开机的时间
						int send_begin_hour = begin_hour - current_hour;
						int send_begin_min = begin_min - current_min;
						if (send_begin_min < 0) {
							send_begin_min += 60;
							send_begin_hour--;
						}
						if (send_begin_hour < 0) {
							send_begin_hour += 24;
						}
						send_begin_min = (send_begin_min + (send_begin_hour) * 60);
						// 计算关机的时间
						int send_end_hour = end_hour - current_hour;
						int send_end_min = end_min - current_min;
						if (send_end_min < 0) {
							send_end_min += 60;
							send_end_hour--;
						}
						if (send_end_hour < 0) {
							send_end_hour += 24;
						}
						send_end_min = (send_end_min + (send_end_hour) * 60);
						System.out.println("距离开机的时间" + send_begin_min);
						System.out.println("距离关机的时间" + send_end_min);
						boolean time_on_ison = item.Time_on_ison();
						boolean time_off_ison = item.Time_off_ison();
						if (position < 3)// 固定的模式
						{
							System.out.println("固定模式的定时");
							Qizhen_OrderUtils.setTimerOn(send_begin_min);
							Qizhen_OrderUtils.setTimerOff(send_end_min);
							Qizhen_OrderUtils.setTimerGear(0);
						} else {
							int model = timerlist.get(position).getModel();
							int gear = timerlist.get(position).getGear();
							if (time_on_ison && time_off_ison) {
								// AirSpa_OrderUtils.setTimer((byte) 3);
								// ((MainActivity) mContext)
								// .sendData(AirSpa_OrderUtils.getResult());
								System.out.println("双定");
								System.out.println("模式==" + model + "   档位="
										+ gear);
								Qizhen_OrderUtils.setTimerOn(send_begin_min);
								Qizhen_OrderUtils.setTimerOff(send_end_min);
								Qizhen_OrderUtils.setTimerGear(gear);

							}
							if (time_on_ison && !time_off_ison) {
								System.out.println("只定时开");
								Qizhen_OrderUtils.setTimerOn(send_begin_min);
								Qizhen_OrderUtils.setTimerOff(0);
								Qizhen_OrderUtils.setTimerGear(gear);
							}

							if (time_off_ison && !time_on_ison) {
								System.out.println("只定时关");
								Qizhen_OrderUtils.setTimerOn(0);
								Qizhen_OrderUtils.setTimerOff(send_end_min);
								Qizhen_OrderUtils.setTimerGear(gear);
							}
						}
						holder.timerImg
								.setBackgroundResource(R.drawable.timer_icon_on_small);
						holder.timer_btn.setSelected(true);
						if (time_on_ison || time_off_ison)
							((MainActivity) mContext)
									.sendData(Qizhen_OrderUtils.getTimerbyte());

					}
					Timer_Db_Helper myDBHelper = new Timer_Db_Helper(mContext);
					SQLiteDatabase dbwrite = myDBHelper.getWritableDatabase();
					Cursor cursor = dbwrite.query("qizhen_timer_db", null,
							null, null, null, null, null);
					ContentValues value = new ContentValues();
					while (cursor.moveToNext()) {
						if (cursor.getPosition() == position) {
							value.clear();
							value.put("ison",
									holder.timer_btn.isSelected() ? "true"
											: "false");
							System.out.println("更新数据库"
									+ value.getAsString("ison"));
							dbwrite.update("qizhen_timer_db", value, "id=?",
									new String[] { cursor.getString(cursor
											.getColumnIndex("id")) });
						} else {
							value.clear();
							value.put("ison", "false");
							dbwrite.update("qizhen_timer_db", value, "id=?",
									new String[] { cursor.getString(cursor
											.getColumnIndex("id")) });
						}
					}

					cursor.close();
					dbwrite.close();
					myDBHelper.close();
					queryTimer();
				}
			});
		} else {
			holder.timer_name.setText("自定义模式");
			holder.timer_note.setText("完全定制属于您的个性化模式");
			holder.timerImg
					.setBackgroundResource(R.drawable.timer_icon_add_small);
			holder.timer_btn.setBackgroundColor(0x00000000);
			holder.timer_btn.setImageResource(R.drawable.timer_row);
		}

		return convertView;

	}

	class ViewHolder {
		public ImageView timerImg;
		public ImageView timer_btn;
		public TextView timer_name;
		public TextView timer_note;
	}

	private void queryTimer() {
		timerlist.clear();
		Timer_Db_Helper myDBHelper = new Timer_Db_Helper(mContext);
		SQLiteDatabase dbwrite = myDBHelper.getWritableDatabase();
		Cursor c = dbwrite.query("qizhen_timer_db", null, null, null, null,
				null, null);
		System.out.println("定时器数量" + c.getCount());
		while (c.moveToNext()) {
			TimerItem item = new TimerItem();
			item.setOn(c.getString(c.getColumnIndex("ison")).equals("true"));
			item.setTimer_name(c.getString(c.getColumnIndex("timer_name")));
			item.setTimer_note(c.getString(c.getColumnIndex("timer_note")));
			item.setTime_on(c.getString(c.getColumnIndex("time_on")));
			item.setTime_off(c.getString(c.getColumnIndex("time_off")));
			item.setModel(Integer.parseInt(c.getString(c
					.getColumnIndex("timer_model"))));
			item.setGear(Integer.parseInt(c.getString(c
					.getColumnIndex("timer_gear"))));
			item.setTime_on_ison(c.getString(c.getColumnIndex("time_on_ison"))
					.equals("true"));
			item.setTime_off_ison(c
					.getString(c.getColumnIndex("time_off_ison"))
					.equals("true"));
			timerlist.add(item);
		}
		notifyDataSetChanged();
		c.close();
		dbwrite.close();
		myDBHelper.close();
	}

	public boolean sendData(final byte[] bs) {
		System.out.println("发送命令");
		for (byte b : bs) {
			System.out.format("%02X ", b);
		}
		System.out.println("");
		int ret = XlinkAgent.getInstance().sendPipeData(
				DeviceManage.getInstance().getDevices().get(0).getXDevice(),
				bs, new SendPipeListener() {
					@Override
					public void onSendLocalPipeData(XDevice xdevice, int code,
							int msgId) {
						// setDeviceStatus(false);
						switch (code) {
						case XlinkCode.SUCCEED:
							Log.e("model",
									"发送数据" + XlinkUtils.getHexBinString(bs)
											+ "成功");
							break;
						case XlinkCode.TIMEOUT:
							// 重新调用connect
							Log.e("model",
									"发送数据超时：" + XlinkUtils.getHexBinString(bs));
							// XlinkUtils.shortTips("发送数据超时："
							// + );

							break;
						case XlinkCode.SERVER_CODE_UNAUTHORIZED:
							// XlinkUtils.shortTips("控制设备失败,当前帐号未订阅此设备，请重新订阅");
							Log.e("model", "控制设备失败,当前帐号未订阅此设备，请重新订阅");

							break;
						case XlinkCode.SERVER_DEVICE_OFFLIEN:
							XlinkUtils.shortTips("设备不在线");
							break;
						default:
							XlinkUtils.shortTips("控制设备其他错误码:" + code);
							Log.e("model", "控制设备其他错误码:" + code);
							break;
						}
					}
				});
		if (ret < 0) {

			Log.e("model", "发送数据失败，错误码：" + ret);
			return false;
		}
		return true;
	}
}
