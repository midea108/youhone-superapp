package com.youhone.bean;

import java.io.Serializable;

public class TimerItem implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private boolean on = false;
	private String time_on = "";
	private String time_off = "";
	private String timer_name = "";
	private String timer_note = "";
	private boolean time_on_ison = false;
	private boolean time_off_ison = false;
	private int model = 0;
	private int gear = 0;

	public int getModel() {
		return model;
	}

	public void setModel(int model) {
		this.model = model;
	}

	public int getGear() {
		return gear;
	}

	public void setGear(int gear) {
		this.gear = gear;
	}

	public boolean Time_on_ison() {
		return time_on_ison;
	}

	public void setTime_on_ison(boolean time_on_ison) {
		this.time_on_ison = time_on_ison;
	}

	public boolean Time_off_ison() {
		return time_off_ison;
	}

	public void setTime_off_ison(boolean time_off_ison) {
		this.time_off_ison = time_off_ison;
	}

	public String getTimer_name() {
		return timer_name;
	}

	public void setTimer_name(String timer_name) {
		this.timer_name = timer_name;
	}

	public String getTimer_note() {
		return timer_note;
	}

	public void setTimer_note(String timer_note) {
		this.timer_note = timer_note;
	}

	public boolean isOn() {
		return on;
	}

	public void setOn(boolean on) {
		this.on = on;
	}

	public String getTime_on() {
		return time_on;
	}

	public void setTime_on(String time_on) {
		this.time_on = time_on;
	}

	public String getTime_off() {
		return time_off;
	}

	public void setTime_off(String time_off) {
		this.time_off = time_off;
	}

}
