package com.youhone.about;

import io.xlink.wifi.ui.MyApp;
import io.xlink.wifi.ui.MyApp.userExtresion;
import io.xlink.wifi.ui.activity.ActivityManager;
import android.app.Activity;
import android.app.AlertDialog.Builder;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.Button;

import com.youhone.xlink.superapp.R;

public class ActivityAboutCompany extends Activity implements userExtresion {
	private Button about_title_back;

	@Override
	protected void onCreate(Bundle arg0) {
		super.onCreate(arg0);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.activity_about_text);
		ActivityManager manager = new ActivityManager();
		manager.applyKitKatTranslucency(ActivityAboutCompany.this, getResources()
				.getColor(R.color.main_color));
		MyApp.getApp().setCurrentActivity(ActivityAboutCompany.this);
		about_title_back = (Button) findViewById(R.id.about_title_back);
		about_title_back.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				finish();
			}
		});
	}

	public void showExceptionDialog(String msg,
			android.content.DialogInterface.OnClickListener listener) {
		Builder builder = new Builder(this);
		builder.setCancelable(false);
		builder.setMessage(msg);
		builder.setTitle("登录异常");
		builder.setNegativeButton("退出账号", listener);
		builder.create().show();
	}

	@Override
	public void onExtresion() {
		showExceptionDialog("该账号已在其他地方登录！",
				new android.content.DialogInterface.OnClickListener() {

					@Override
					public void onClick(DialogInterface dialog, int which) {
						dialog.dismiss();
						MyApp.getApp().removeUser(ActivityAboutCompany.this);
					}
				});

	}

}
