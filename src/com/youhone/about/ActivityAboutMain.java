package com.youhone.about;

import io.xlink.wifi.ui.MyApp;
import io.xlink.wifi.ui.MyApp.userExtresion;
import io.xlink.wifi.ui.activity.ActivityManager;
import android.app.Activity;
import android.app.AlertDialog.Builder;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.youhone.xlink.superapp.R;

public class ActivityAboutMain extends Activity implements userExtresion,
		OnClickListener {
	private Button about_title_back;
	private ImageView about_wap, about_shop, about_company, about_tel,
			about_join;
	private TextView verson_text;

	@Override
	protected void onCreate(Bundle arg0) {
		super.onCreate(arg0);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.activity_about_main);
		ActivityManager manager = new ActivityManager();
		manager.applyKitKatTranslucency(ActivityAboutMain.this, getResources()
				.getColor(R.color.main_color));
		MyApp.getApp().setCurrentActivity(ActivityAboutMain.this);
		verson_text = (TextView) findViewById(R.id.verson_text);
		about_title_back = (Button) findViewById(R.id.about_title_back);
		about_wap = (ImageView) findViewById(R.id.about_wap);
		about_shop = (ImageView) findViewById(R.id.about_shop);
		about_company = (ImageView) findViewById(R.id.about_company);
		about_tel = (ImageView) findViewById(R.id.about_tel);
		about_join = (ImageView) findViewById(R.id.about_join);
		about_wap.setOnClickListener(this);
		about_shop.setOnClickListener(this);
		about_company.setOnClickListener(this);
		about_tel.setOnClickListener(this);
		about_join.setOnClickListener(this);
		about_title_back.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				finish();
			}
		});
		try {
			PackageManager manager1 = this.getPackageManager();
			PackageInfo info = manager1
					.getPackageInfo(this.getPackageName(), 0);
			String version = info.versionName;
			verson_text.setText("启振环保智能净化控制系统Android版 " + version + "版");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void showExceptionDialog(String msg,
			android.content.DialogInterface.OnClickListener listener) {
		Builder builder = new Builder(this);
		builder.setCancelable(false);
		builder.setMessage(msg);
		builder.setTitle("登录异常");
		builder.setNegativeButton("退出账号", listener);
		builder.create().show();
	}

	@Override
	public void onExtresion() {
		showExceptionDialog("该账号已在其他地方登录！",
				new android.content.DialogInterface.OnClickListener() {

					@Override
					public void onClick(DialogInterface dialog, int which) {
						dialog.dismiss();
						MyApp.getApp().removeUser(ActivityAboutMain.this);
					}
				});

	}

	@Override
	public void onClick(View v) {
		Intent intent = new Intent();
		int id = v.getId();
		if (id == R.id.about_wap) {
			intent.setAction("android.intent.action.VIEW");
			Uri content_url = Uri.parse("http://www.fsqzhb.com");
			intent.setData(content_url);
			startActivity(intent);

		} else if (id == R.id.about_shop) {
			intent.setAction("android.intent.action.VIEW");
			Uri content_url = Uri
					.parse("https://wap.koudaitong.com/v2/showcase/feature?alias=1i7ch079e");
			intent.setData(content_url);
			startActivity(intent);
		} else if (id == R.id.about_company) {
			intent.setClass(ActivityAboutMain.this, ActivityAboutCompany.class);
			startActivity(intent);
		} else if (id == R.id.about_tel) {
			intent.setAction(Intent.ACTION_DIAL);
			intent.setData(Uri.parse("tel:4008870027"));
			startActivity(intent);
		} else if (id == R.id.about_join) {
			intent.setAction("android.intent.action.VIEW");
			Uri content_url = Uri.parse("http://eqxiu.com/s/yWfDYunw");
			intent.setData(content_url);
			startActivity(intent);
		}
	}

}
