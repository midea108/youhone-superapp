package com.youhone.db;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteDatabase.CursorFactory;
import android.database.sqlite.SQLiteOpenHelper;

public class Timer_Db_Helper extends SQLiteOpenHelper {
	private static final int DATABASE_VERSION = 1;
	private static final String DATABASE_NAME = "AirSpa_DB";

	public Timer_Db_Helper(Context context, String name, CursorFactory factory,
			int version) {
		super(context, name, factory, version);
	}

	public Timer_Db_Helper(Context context) {
		super(context, DATABASE_NAME, null, DATABASE_VERSION);
	}

	@Override
	public void onCreate(SQLiteDatabase db) {
		db.execSQL("create table airspa_timer_db(id integer primary key autoincrement,time_on string,time_off string,ison,time_on_ison,time_off_ison,timer_name string,timer_note string,timer_model integer,timer_gear integer)");
		db.execSQL("create table qizhen_timer_db(id integer primary key autoincrement,time_on string,time_off string,ison,time_on_ison,time_off_ison,timer_name string,timer_note string,timer_model integer,timer_gear integer)");
	}

	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
		// TODO 更新数据库

	}

}
