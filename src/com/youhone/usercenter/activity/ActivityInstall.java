package com.youhone.usercenter.activity;

import io.xlink.wifi.ui.MyApp;
import io.xlink.wifi.ui.activity.ActivityManager;
import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.youhone.xlink.superapp.R;

public class ActivityInstall extends Activity implements OnClickListener {
	private LinearLayout install_q1;
	private TextView install_a1;
	private ImageView install_q1_img;
	private Button help_title_back;

	@Override
	protected void onCreate(Bundle arg0) {
		super.onCreate(arg0);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.activity_help_install);
		ActivityManager manager = new ActivityManager();
		manager.applyKitKatTranslucency(ActivityInstall.this, getResources()
				.getColor(R.color.main_color));
		//MyApp.getApp().setCurrentActivity(ActivityInstall.this);
		install_q1 = (LinearLayout) findViewById(R.id.install_q1);
		install_a1 = (TextView) findViewById(R.id.install_a1);
		install_q1_img = (ImageView) findViewById(R.id.install_q1_img);
		install_q1.setOnClickListener(this);
		help_title_back = (Button) findViewById(R.id.help_title_back);
		help_title_back.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				finish();
			}
		});
	}

	@Override
	public void onClick(View v) {
		int id = v.getId();
		if (id == R.id.install_q1) {
			if (install_a1.getVisibility() == View.VISIBLE) {// 收起回答
				install_a1.setVisibility(View.GONE);
				install_q1_img
						.setImageResource(R.drawable.ic_expand_more_black_16dp);
			} else {// 展开回答
				install_a1.setVisibility(View.VISIBLE);
				install_q1_img
						.setImageResource(R.drawable.ic_expand_less_black_16dp);
			}

		}
	}
}
