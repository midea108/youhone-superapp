package com.youhone.usercenter.activity;

import io.xlink.wifi.ui.MyApp;
import io.xlink.wifi.ui.activity.ActivityManager;
import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.youhone.xlink.superapp.R;

public class ActivityAttention extends Activity implements OnClickListener {
	private LinearLayout attention_q1, attention_q2, attention_q3;
	private TextView attention_a1, attention_a2, attention_a3;
	private ImageView attention_q1_img, attention_q2_img, attention_q3_img;
	private Button help_title_back;

	@Override
	protected void onCreate(Bundle arg0) {
		super.onCreate(arg0);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.activity_help_attention);
		ActivityManager manager = new ActivityManager();
		manager.applyKitKatTranslucency(ActivityAttention.this, getResources()
				.getColor(R.color.main_color));
		//MyApp.getApp().setCurrentActivity(ActivityAttention.this);
		attention_q1 = (LinearLayout) findViewById(R.id.attention_q1);
		attention_q2 = (LinearLayout) findViewById(R.id.attention_q2);
		attention_q3 = (LinearLayout) findViewById(R.id.attention_q3);
		attention_a1 = (TextView) findViewById(R.id.attention_a1);
		attention_a2 = (TextView) findViewById(R.id.attention_a2);
		attention_a3 = (TextView) findViewById(R.id.attention_a3);
		attention_q1_img = (ImageView) findViewById(R.id.attention_q1_img);
		attention_q2_img = (ImageView) findViewById(R.id.attention_q2_img);
		attention_q3_img = (ImageView) findViewById(R.id.attention_q3_img);
		attention_q1.setOnClickListener(this);
		attention_q2.setOnClickListener(this);
		attention_q3.setOnClickListener(this);
		help_title_back = (Button) findViewById(R.id.help_title_back);
		help_title_back.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				finish();
			}
		});
	}

	@Override
	public void onClick(View v) {
		int id = v.getId();
		if (id == R.id.attention_q1) {
			if (attention_a1.getVisibility() == View.VISIBLE) {// 收起回答
				attention_a1.setVisibility(View.GONE);
				attention_q1_img
						.setImageResource(R.drawable.ic_expand_more_black_16dp);
			} else {// 展开回答
				attention_a1.setVisibility(View.VISIBLE);
				attention_q1_img
						.setImageResource(R.drawable.ic_expand_less_black_16dp);
			}

		} else if (id == R.id.attention_q2) {
			if (attention_a2.getVisibility() == View.VISIBLE) {// 收起回答
				attention_a2.setVisibility(View.GONE);
				attention_q2_img
						.setImageResource(R.drawable.ic_expand_more_black_16dp);
			} else {// 展开回答
				attention_a2.setVisibility(View.VISIBLE);
				attention_q2_img
						.setImageResource(R.drawable.ic_expand_less_black_16dp);
			}
		} else if (id == R.id.attention_q3) {
			if (attention_a3.getVisibility() == View.VISIBLE) {// 收起回答
				attention_a3.setVisibility(View.GONE);
				attention_q3_img
						.setImageResource(R.drawable.ic_expand_more_black_16dp);
			} else {// 展开回答
				attention_a3.setVisibility(View.VISIBLE);
				attention_q3_img
						.setImageResource(R.drawable.ic_expand_less_black_16dp);
			}
		}
	}
}
