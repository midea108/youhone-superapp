package com.youhone.usercenter.activity;

import io.xlink.wifi.ui.MyApp;
import io.xlink.wifi.ui.activity.ActivityManager;
import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.youhone.xlink.superapp.R;

public class ActivityTurnOn extends Activity implements OnClickListener {
	private LinearLayout turnon_q1, turnon_q2, turnon_q3;
	private TextView turnon_a1, turnon_a2, turnon_a3;
	private ImageView turnon_q1_img, turnon_q2_img, turnon_q3_img;
	private Button help_title_back;

	@Override
	protected void onCreate(Bundle arg0) {
		super.onCreate(arg0);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.activity_help_turnon);
		ActivityManager manager = new ActivityManager();
		manager.applyKitKatTranslucency(ActivityTurnOn.this, getResources()
				.getColor(R.color.main_color));
		//MyApp.getApp().setCurrentActivity(ActivityTurnOn.this);
		turnon_q1 = (LinearLayout) findViewById(R.id.turnon_q1);
		turnon_q2 = (LinearLayout) findViewById(R.id.turnon_q2);
		turnon_q3 = (LinearLayout) findViewById(R.id.turnon_q3);
		turnon_a1 = (TextView) findViewById(R.id.turnon_a1);
		turnon_a2 = (TextView) findViewById(R.id.turnon_a2);
		turnon_a3 = (TextView) findViewById(R.id.turnon_a3);
		turnon_q1_img = (ImageView) findViewById(R.id.turnon_q1_img);
		turnon_q2_img = (ImageView) findViewById(R.id.turnon_q2_img);
		turnon_q3_img = (ImageView) findViewById(R.id.turnon_q3_img);
		turnon_q1.setOnClickListener(this);
		turnon_q2.setOnClickListener(this);
		turnon_q3.setOnClickListener(this);
		help_title_back = (Button) findViewById(R.id.help_title_back);
		help_title_back.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				finish();
			}
		});
	}

	@Override
	public void onClick(View v) {
		int id = v.getId();
		if (id == R.id.turnon_q1) {
			if (turnon_a1.getVisibility() == View.VISIBLE) {// 收起回答
				turnon_a1.setVisibility(View.GONE);
				turnon_q1_img
						.setImageResource(R.drawable.ic_expand_more_black_16dp);
			} else {// 展开回答
				turnon_a1.setVisibility(View.VISIBLE);
				turnon_q1_img
						.setImageResource(R.drawable.ic_expand_less_black_16dp);
			}

		} else if (id == R.id.turnon_q2) {
			if (turnon_a2.getVisibility() == View.VISIBLE) {// 收起回答
				turnon_a2.setVisibility(View.GONE);
				turnon_q2_img
						.setImageResource(R.drawable.ic_expand_more_black_16dp);
			} else {// 展开回答
				turnon_a2.setVisibility(View.VISIBLE);
				turnon_q2_img
						.setImageResource(R.drawable.ic_expand_less_black_16dp);
			}
		} else if (id == R.id.turnon_q3) {
			if (turnon_a3.getVisibility() == View.VISIBLE) {// 收起回答
				turnon_a3.setVisibility(View.GONE);
				turnon_q3_img
						.setImageResource(R.drawable.ic_expand_more_black_16dp);
			} else {// 展开回答
				turnon_a3.setVisibility(View.VISIBLE);
				turnon_q3_img
						.setImageResource(R.drawable.ic_expand_less_black_16dp);
			}
		}
	}
}
