package com.youhone.usercenter.activity;

import io.xlink.wifi.ui.MyApp;
import io.xlink.wifi.ui.activity.ActivityManager;
import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.youhone.xlink.superapp.R;

public class ActivityFilter extends Activity implements OnClickListener {
	private LinearLayout filter_q1, filter_q2, filter_q3, filter_q4, filter_q5,
			filter_q6;
	private TextView filter_a1, filter_a2, filter_a3, filter_a4, filter_a5,
			filter_a6;
	private ImageView filter_q1_img, filter_q2_img, filter_q3_img,
			filter_q4_img, filter_q5_img, filter_q6_img;
	private Button help_title_back;

	@Override
	protected void onCreate(Bundle arg0) {
		super.onCreate(arg0);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.activity_help_filter);
		ActivityManager manager = new ActivityManager();
		manager.applyKitKatTranslucency(ActivityFilter.this, getResources()
				.getColor(R.color.main_color));
		//MyApp.getApp().setCurrentActivity(ActivityFilter.this);
		filter_q1 = (LinearLayout) findViewById(R.id.filter_q1);
		filter_q2 = (LinearLayout) findViewById(R.id.filter_q2);
		filter_q3 = (LinearLayout) findViewById(R.id.filter_q3);
		filter_q4 = (LinearLayout) findViewById(R.id.filter_q4);
		filter_q5 = (LinearLayout) findViewById(R.id.filter_q5);
		filter_q6 = (LinearLayout) findViewById(R.id.filter_q6);
		filter_a1 = (TextView) findViewById(R.id.filter_a1);
		filter_a2 = (TextView) findViewById(R.id.filter_a2);
		filter_a3 = (TextView) findViewById(R.id.filter_a3);
		filter_a4 = (TextView) findViewById(R.id.filter_a4);
		filter_a5 = (TextView) findViewById(R.id.filter_a5);
		filter_a6 = (TextView) findViewById(R.id.filter_a6);
		filter_q1_img = (ImageView) findViewById(R.id.filter_q1_img);
		filter_q2_img = (ImageView) findViewById(R.id.filter_q2_img);
		filter_q3_img = (ImageView) findViewById(R.id.filter_q3_img);
		filter_q4_img = (ImageView) findViewById(R.id.filter_q4_img);
		filter_q5_img = (ImageView) findViewById(R.id.filter_q5_img);
		filter_q6_img = (ImageView) findViewById(R.id.filter_q6_img);
		filter_q1.setOnClickListener(this);
		filter_q2.setOnClickListener(this);
		filter_q3.setOnClickListener(this);
		filter_q4.setOnClickListener(this);
		filter_q5.setOnClickListener(this);
		filter_q6.setOnClickListener(this);
		help_title_back = (Button) findViewById(R.id.help_title_back);
		help_title_back.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				finish();
			}
		});
	}

	@Override
	public void onClick(View v) {
		int id = v.getId();
		if (id == R.id.filter_q1) {
			if (filter_a1.getVisibility() == View.VISIBLE) {// 收起回答
				filter_a1.setVisibility(View.GONE);
				filter_q1_img
						.setImageResource(R.drawable.ic_expand_more_black_16dp);
			} else {// 展开回答
				filter_a1.setVisibility(View.VISIBLE);
				filter_q1_img
						.setImageResource(R.drawable.ic_expand_less_black_16dp);
			}

		} else if (id == R.id.filter_q2) {
			if (filter_a2.getVisibility() == View.VISIBLE) {// 收起回答
				filter_a2.setVisibility(View.GONE);
				filter_q2_img
						.setImageResource(R.drawable.ic_expand_more_black_16dp);
			} else {// 展开回答
				filter_a2.setVisibility(View.VISIBLE);
				filter_q2_img
						.setImageResource(R.drawable.ic_expand_less_black_16dp);
			}
		} else if (id == R.id.filter_q3) {
			if (filter_a3.getVisibility() == View.VISIBLE) {// 收起回答
				filter_a3.setVisibility(View.GONE);
				filter_q3_img
						.setImageResource(R.drawable.ic_expand_more_black_16dp);
			} else {// 展开回答
				filter_a3.setVisibility(View.VISIBLE);
				filter_q3_img
						.setImageResource(R.drawable.ic_expand_less_black_16dp);
			}

		} else if (id == R.id.filter_q4) {
			if (filter_a4.getVisibility() == View.VISIBLE) {// 收起回答
				filter_a4.setVisibility(View.GONE);
				filter_q4_img
						.setImageResource(R.drawable.ic_expand_more_black_16dp);
			} else {// 展开回答
				filter_a4.setVisibility(View.VISIBLE);
				filter_q4_img
						.setImageResource(R.drawable.ic_expand_less_black_16dp);
			}
		} else if (id == R.id.filter_q5) {
			if (filter_a5.getVisibility() == View.VISIBLE) {// 收起回答
				filter_a5.setVisibility(View.GONE);
				filter_q5_img
						.setImageResource(R.drawable.ic_expand_more_black_16dp);
			} else {// 展开回答
				filter_a5.setVisibility(View.VISIBLE);
				filter_q5_img
						.setImageResource(R.drawable.ic_expand_less_black_16dp);
			}
		} else if (id == R.id.filter_q6) {
			if (filter_a6.getVisibility() == View.VISIBLE) {// 收起回答
				filter_a6.setVisibility(View.GONE);
				filter_q6_img
						.setImageResource(R.drawable.ic_expand_more_black_16dp);
			} else {// 展开回答
				filter_a6.setVisibility(View.VISIBLE);
				filter_q6_img
						.setImageResource(R.drawable.ic_expand_less_black_16dp);
			}
		}
	}
}
