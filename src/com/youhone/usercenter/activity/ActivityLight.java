package com.youhone.usercenter.activity;

import io.xlink.wifi.ui.MyApp;
import io.xlink.wifi.ui.activity.ActivityManager;
import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.youhone.xlink.superapp.R;

public class ActivityLight extends Activity implements OnClickListener {
	private LinearLayout light_q1, light_q2, light_q3;
	private TextView light_a1, light_a2, light_a3;
	private ImageView light_q1_img, light_q2_img, light_q3_img;
	private Button help_title_back;

	@Override
	protected void onCreate(Bundle arg0) {
		super.onCreate(arg0);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.activity_help_light);
		ActivityManager manager = new ActivityManager();
		manager.applyKitKatTranslucency(ActivityLight.this, getResources()
				.getColor(R.color.main_color));
		//MyApp.getApp().setCurrentActivity(ActivityLight.this);
		light_q1 = (LinearLayout) findViewById(R.id.light_q1);
		light_q2 = (LinearLayout) findViewById(R.id.light_q2);
		light_q3 = (LinearLayout) findViewById(R.id.light_q3);
		light_a1 = (TextView) findViewById(R.id.light_a1);
		light_a2 = (TextView) findViewById(R.id.light_a2);
		light_a3 = (TextView) findViewById(R.id.light_a3);
		light_q1_img = (ImageView) findViewById(R.id.light_q1_img);
		light_q2_img = (ImageView) findViewById(R.id.light_q2_img);
		light_q3_img = (ImageView) findViewById(R.id.light_q3_img);
		light_q1.setOnClickListener(this);
		light_q2.setOnClickListener(this);
		light_q3.setOnClickListener(this);
		help_title_back = (Button) findViewById(R.id.help_title_back);
		help_title_back.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				finish();
			}
		});
	}

	@Override
	public void onClick(View v) {
		int id = v.getId();
		if (id == R.id.light_q1) {
			if (light_a1.getVisibility() == View.VISIBLE) {// 收起回答
				light_a1.setVisibility(View.GONE);
				light_q1_img
						.setImageResource(R.drawable.ic_expand_more_black_16dp);
			} else {// 展开回答
				light_a1.setVisibility(View.VISIBLE);
				light_q1_img
						.setImageResource(R.drawable.ic_expand_less_black_16dp);
			}
		} else if (id == R.id.light_q2) {
			if (light_a2.getVisibility() == View.VISIBLE) {// 收起回答
				light_a2.setVisibility(View.GONE);
				light_q2_img
						.setImageResource(R.drawable.ic_expand_more_black_16dp);
			} else {// 展开回答
				light_a2.setVisibility(View.VISIBLE);
				light_q2_img
						.setImageResource(R.drawable.ic_expand_less_black_16dp);
			}
		} else if (id == R.id.light_q3) {
			if (light_a3.getVisibility() == View.VISIBLE) {// 收起回答
				light_a3.setVisibility(View.GONE);
				light_q3_img
						.setImageResource(R.drawable.ic_expand_more_black_16dp);
			} else {// 展开回答
				light_a3.setVisibility(View.VISIBLE);
				light_q3_img
						.setImageResource(R.drawable.ic_expand_less_black_16dp);
			}
		}
	}
}
