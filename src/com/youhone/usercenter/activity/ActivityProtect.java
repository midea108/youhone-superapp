package com.youhone.usercenter.activity;

import io.xlink.wifi.ui.MyApp;
import io.xlink.wifi.ui.activity.ActivityManager;
import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.youhone.xlink.superapp.R;

public class ActivityProtect extends Activity implements OnClickListener {
	private LinearLayout protect_q1, protect_q2;
	private TextView protect_a1, protect_a2;
	private ImageView protect_q1_img, protect_q2_img;
	private Button help_title_back;

	@Override
	protected void onCreate(Bundle arg0) {
		super.onCreate(arg0);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.activity_help_protect);
		ActivityManager manager = new ActivityManager();
		manager.applyKitKatTranslucency(ActivityProtect.this, getResources()
				.getColor(R.color.main_color));
		//MyApp.getApp().setCurrentActivity(ActivityProtect.this);
		protect_q1 = (LinearLayout) findViewById(R.id.protect_q1);
		protect_q2 = (LinearLayout) findViewById(R.id.protect_q2);
		protect_a1 = (TextView) findViewById(R.id.protect_a1);
		protect_a2 = (TextView) findViewById(R.id.protect_a2);
		protect_q1_img = (ImageView) findViewById(R.id.protect_q1_img);
		protect_q2_img = (ImageView) findViewById(R.id.protect_q2_img);
		protect_q1.setOnClickListener(this);
		protect_q2.setOnClickListener(this);
		help_title_back = (Button) findViewById(R.id.help_title_back);
		help_title_back.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				finish();
			}
		});
	}

	@Override
	public void onClick(View v) {
		int id = v.getId();
		if (id == R.id.protect_q1) {
			if (protect_a1.getVisibility() == View.VISIBLE) {// 收起回答
				protect_a1.setVisibility(View.GONE);
				protect_q1_img
						.setImageResource(R.drawable.ic_expand_more_black_16dp);
			} else {// 展开回答
				protect_a1.setVisibility(View.VISIBLE);
				protect_q1_img
						.setImageResource(R.drawable.ic_expand_less_black_16dp);
			}

		} else if (id == R.id.protect_q2) {
			if (protect_a2.getVisibility() == View.VISIBLE) {// 收起回答
				protect_a2.setVisibility(View.GONE);
				protect_q2_img
						.setImageResource(R.drawable.ic_expand_more_black_16dp);
			} else {// 展开回答
				protect_a2.setVisibility(View.VISIBLE);
				protect_q2_img
						.setImageResource(R.drawable.ic_expand_less_black_16dp);
			}
		}
	}
}
