package com.youhone.activity;

import io.xlink.wifi.sdk.XDevice;
import io.xlink.wifi.sdk.XlinkAgent;
import io.xlink.wifi.sdk.XlinkCode;
import io.xlink.wifi.sdk.listener.ConnectDeviceListener;
import io.xlink.wifi.sdk.listener.SendPipeListener;
import io.xlink.wifi.sdk.listener.SetDeviceAccessKeyListener;
import io.xlink.wifi.sdk.listener.SubscribeDeviceListener;
import io.xlink.wifi.sdk.util.MyLog;
import io.xlink.wifi.ui.Constant;
import io.xlink.wifi.ui.MyApp;
import io.xlink.wifi.ui.activity.ActivityManager;
import io.xlink.wifi.ui.activity.BaseFragmentActivity;
import io.xlink.wifi.ui.bean.Device;
import io.xlink.wifi.ui.db.Xlink_Db_Helper;
import io.xlink.wifi.ui.fragment.DeviceListTab;
import io.xlink.wifi.ui.http.HttpAgent;
import io.xlink.wifi.ui.manage.DeviceManage;
import io.xlink.wifi.ui.util.SharedPreferencesUtil;
import io.xlink.wifi.ui.util.XlinkUtils;
import io.xlink.wifi.ui.view.CustomDialog;

import org.apache.http.Header;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.AlertDialog.Builder;
import android.app.Dialog;
import android.content.BroadcastReceiver;
import android.content.ContentValues;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.drawable.BitmapDrawable;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.text.TextUtils;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.Window;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.PopupWindow;
import android.widget.TextView;

import com.loopj.android.http.TextHttpResponseHandler;
import com.youhone.device.airspa.Device_Main_Tab;
import com.youhone.device.light.LightFragment;
import com.youhone.device.qizhen.Qizhen_Main_Tab;
import com.youhone.device.tianjun.ClothingCareFragment;
import com.youhone.device.tianjun.DryerFragment;
import com.youhone.device.tianjun.FanLightFragment;
import com.youhone.utils.airspa.AirSpa_OrderUtils;
import com.youhone.xlink.superapp.R;

public class MainActivity extends BaseFragmentActivity implements
		OnClickListener {
	public static final int QIZHEN_LVXIN_WORKTIM = 3000;
	private FragmentManager fragmentManager;
	private String mCurrentFragment;
	private final String TAG = "DeviceActivity";
	// 当前操作的设备
	public Device device;
	// 界面是否可见
	private boolean isRun;
	// 是否销毁
	private boolean isDestroy;
	View title_back;
	View title_more;
	TextView title_text;
	boolean isShowLoginDialog = false;
	private Dialog loginDialog;
	private Dialog connectDialog;
	public Dialog alterProgressDialog;
	private boolean isRegisterBroadcast = false;
	byte[] pipeData;
	private CustomDialog oldTipsDialog;
	public boolean isOnline;
	private ReceiveDataListener onReceiveListener;
	private Intent intent;
	private int flag;
	private boolean isFirst = false;
	private String currentName = "";
	private boolean isDelete = false;
	private byte[] reSend = AirSpa_OrderUtils.GET_QUERY();
	public static int model = 0;

	public interface ReceiveDataListener {
		public abstract void receviceData(byte[] data);

	}

	public void setOnReceive(ReceiveDataListener reListener, Fragment f) {
		this.onReceiveListener = reListener;
	}

	@Override
	protected void onCreate(Bundle arg0) {
		isDestroy = false;
		super.onCreate(arg0);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.xlink_activity_main);
		Log("onCreate");
		MyApp.getApp().setCurrentActivity(MainActivity.this);
		intent = getIntent();
		flag = intent.getFlags();
		isFirst = intent.getBooleanExtra("isFirst", false);
		if (!(XlinkAgent.getInstance().isConnectedOuterNet() || XlinkAgent
				.getInstance().isConnectedLocal())) {
			MyApp.getApp().login();
			isShowLoginDialog = true;
			showLoginDialog();
		}
		ActivityManager manager = new ActivityManager();
		manager.applyKitKatTranslucency(MainActivity.this, getResources()
				.getColor(R.color.main_color));
		device = DeviceManage.getInstance().getDevices().get(0);
		XlinkAgent.getInstance().initDevice(device.getXDevice());
		initView();
		replaceFragment(Constant.PRODUCT_NAME[flag]);
	}

	private void initView() {
		title_back = findViewById(R.id.title_back);
		title_back.setOnClickListener(this);
		title_text = (TextView) findViewById(R.id.title_text);
		title_more = findViewById(R.id.title_more);
		title_more.setOnClickListener(this);
		fragmentManager = getSupportFragmentManager();
		/**
		 * 监听广播
		 */
		IntentFilter myIntentFilter = new IntentFilter();
		myIntentFilter.addAction(Constant.BROADCAST_RECVPIPE);
		myIntentFilter.addAction(Constant.BROADCAST_ON_LOGIN);
		myIntentFilter.addAction(Constant.BROADCAST_DEVICE_CHANGED);
		myIntentFilter.addAction(Constant.BROADCAST_DEVICE_SYNC);
		isRegisterBroadcast = true;
		// 注册广播
		registerReceiver(mBroadcastReceiver, myIntentFilter);
		Log.e(TAG, "名字是否乱码？" + XlinkUtils.isMessyCode(device.getDName()));
		if (XlinkUtils.isMessyCode(device.getDName())) {
			String name = Constant.PRODUCT_NAME[flag];
			DeviceManage.getInstance().getDevices().get(0).setName(name);
			DeviceManage.getInstance().getDevices().get(0).getXDevice()
					.setDeviceName(name);
			device = DeviceManage.getInstance().getDevices().get(0);
			device.getXDevice().setDeviceName(name);
			setTitleText(name);
		} else {
			if (isFirst) {
				device.getXDevice().setDeviceName(Constant.PRODUCT_NAME[flag]);
				setTitleText(device.getDName());
			} else {
				setTitleText(device.getDName());
			}

		}
		currentName = device.getDName();
		if (XlinkAgent.getInstance().isConnectedOuterNet()
				|| XlinkAgent.getInstance().isConnectedLocal()) {
			connectDevice();
		}
	}

	/**
	 * 连接设备
	 */
	private void connectDevice() {
		final int ret = XlinkAgent.getInstance().connectDevice(
				device.getXDevice(), Constant.password, connectDeviceListener);
		System.out.println("连接失败代码  ：  " + ret);
		runOnUiThread(new Runnable() {
			@Override
			public void run() {
				if (ret < 0) {// 调用设备失败
					switch (ret) {
					case XlinkCode.INVALID_DEVICE_ID:
						XlinkUtils.shortTips("无效的设备ID，请先联网激活设备");
						break;
					case XlinkCode.NO_CONNECT_SERVER:
						XlinkUtils.shortTips("连接设备失败，手机未连接服务器");
						if (XlinkUtils.isConnected()) {
							XlinkAgent.getInstance().start();
							MyApp.getApp().login();
						}
						break;
					case XlinkCode.NETWORD_UNAVAILABLE:
						XlinkUtils.shortTips("当前网络不可用,无法连接设备");
						break;
					case XlinkCode.NO_DEVICE:
						XlinkUtils.shortTips("未找到设备");
						XlinkAgent.getInstance()
								.initDevice(device.getXDevice());
						break;
					// 重复调用了连接设备接口
					case XlinkCode.ALREADY_EXIST:
						// XlinkUtils.shortTips("设备已离线，正在重连设备");
						break;
					default:
						// XlinkUtils.shortTips("连接设备" + device.getMacAddress()
						// + "失败:" + ret);
						break;
					}
				} else {
					showConnectDialog();
				}
			}
		});

	}

	/**
	 * 连接设备回调。该回调在主程序，可直接更改ui
	 */
	private ConnectDeviceListener connectDeviceListener = new ConnectDeviceListener() {

		@Override
		public void onConnectDevice(XDevice xDevice, int result) {
			System.out.println("连接监听result  ：  " + result);
			if (connectDialog != null && !isDestroy) {
				connectDialog.dismiss();
			}
			String tips;
			switch (result) {
			// 连接设备成功 设备处于内网
			case XlinkCode.DEVICE_STATE_LOCAL_LINK:
				// 连接设备成功，成功后
				setDeviceStatus(true);
				xDevice.setDeviceName(currentName);
				DeviceManage.getInstance().updateDevice(xDevice);
				tips = "正在局域网控制设备(" + xDevice.getMacAddress() + "   "
						+ xDevice.getDeviceName() + ")";
				Log.e(TAG, tips);
				XlinkAgent.getInstance().sendProbe(xDevice);
				new Thread(new Runnable() {
					@Override
					public void run() {
						if (isFirst) {
							device.getXDevice().setDeviceName(
									Constant.PRODUCT_NAME[flag]);
							uploadDevice(device, Constant.PRODUCT_NAME[flag]);
						} else {
							uploadDevice(device, device.getDName());
						}

					}
				}).run();
				if (reSend != null && reSend.length != 0) {
					sendData(reSend);
				}
				break;
			// 连接设备成功 设备处于云端
			case XlinkCode.DEVICE_STATE_OUTER_LINK:
				setDeviceStatus(true);
				xDevice.setDeviceName(currentName);
				DeviceManage.getInstance().updateDevice(xDevice);
				tips = "正在通过云端控制设备(" + xDevice.getMacAddress() + "   "
						+ xDevice.getDeviceName() + ")";
				Log.e(TAG, tips);
				XlinkAgent.getInstance().sendProbe(xDevice);
				new Thread(new Runnable() {
					@Override
					public void run() {
						if (isFirst) {
							device.getXDevice().setDeviceName(
									Constant.PRODUCT_NAME[flag]);
							uploadDevice(device, Constant.PRODUCT_NAME[flag]);
						} else {
							uploadDevice(device, device.getDName());
						}

					}
				}).run();
				if (reSend != null && reSend.length != 0) {
					sendData(reSend);
				}
				break;
			// 设备授权码错误
			case XlinkCode.CONNECT_DEVICE_INVALID_KEY:
				setDeviceStatus(false);
				Log.e(TAG, "Device:" + xDevice.getMacAddress() + "设备认证失败");
				XlinkUtils.shortTips("设备认证失败");
				break;
			// 设备不在线
			case XlinkCode.CONNECT_DEVICE_OFFLINE:
				setDeviceStatus(false);
				connectDeviceTipsDialog("设备不在线");
				break;
			// 连接设备超时了，（设备未应答，或者服务器未应答）
			case XlinkCode.CONNECT_DEVICE_TIMEOUT:
				connectDeviceTipsDialog("连接设备超时");
				setDeviceStatus(false);
				break;
			case XlinkCode.CONNECT_DEVICE_SERVER_ERROR:
				setDeviceStatus(false);
				connectDeviceTipsDialog("连接设备失败，服务器内部错误");
				break;
			case XlinkCode.CONNECT_DEVICE_OFFLINE_NO_LOGIN:
				setDeviceStatus(false);
				connectDeviceTipsDialog("连接设备失败，当前无云端环境");
				break;
			default:
				setDeviceStatus(false);
				connectDeviceTipsDialog("连接设备失败，其他错误码:" + result);
				break;
			}

		}
	};

	private void connectDeviceTipsDialog(String msg) {
		try {
			System.out.println("msg     " + msg);
			if (oldTipsDialog != null && oldTipsDialog.isShowing()) {
				oldTipsDialog.dismiss();
			}
			CustomDialog dialog = CustomDialog.createErrorDialog(this, "连接设备",
					msg, new OnClickListener() {

						@Override
						public void onClick(View v) {
							openDeviceListActivity();
						}
					});
			dialog.show();
			oldTipsDialog = dialog;
		} catch (Exception e) {

		}

	}

	/**
	 * 设置设备状态
	 * 
	 * @param iso
	 */
	private void setDeviceStatus(boolean iso) {
		this.isOnline = iso;
	}

	/**
	 * 监听的广播
	 */
	private BroadcastReceiver mBroadcastReceiver = new BroadcastReceiver() {

		@Override
		public void onReceive(Context context, Intent intent) {
			String action = intent.getAction();
			System.out.println("action  " + action);
			if (action.equals(Constant.BROADCAST_ON_LOGIN)) {
				int code = intent.getIntExtra(Constant.STATUS, -1);

				if (code == XlinkCode.SERVER_CODE_INVALID_KEY) {
					// 用户认证失败
					return;
				}
				if (!isOnline && isShowLoginDialog) {
					connectDevice();
				}
				if (isShowLoginDialog) {
					loginDialog.dismiss();
					isShowLoginDialog = false;
				}
				return;
			}

			String mac = intent.getStringExtra(Constant.DEVICE_MAC);
			if (mac == null
					|| !mac.equals(MainActivity.this.device.getMacAddress())) {
				return;
			}
			setDeviceStatus(true);
			// 收到pipe包
			if (action.equals(Constant.BROADCAST_RECVPIPE)) {
				byte[] data = intent.getByteArrayExtra(Constant.DATA);
				Log("收到pipe：" + XlinkUtils.getHexBinString(data));
				pipeData = data;
				if (null != onReceiveListener && null != pipeData) {
					onReceiveListener.receviceData(pipeData);
				}
				if (!isRun) { // 当界面不可见，把pipeData存起来，然后等onResume再更新界面
					return;
				}
			} else if (action.equals(Constant.BROADCAST_DEVICE_CHANGED)) {
				int status = intent.getIntExtra(Constant.STATUS, -1);
				if (status == XlinkCode.DEVICE_CHANGED_CONNECTING) {
				} else if (status == XlinkCode.DEVICE_CHANGED_CONNECT_SUCCEED) {
					setDeviceStatus(true);
				} else if (status == XlinkCode.DEVICE_CHANGED_OFFLINE) {
					setDeviceStatus(false);
					connectDeviceTipsDialog("设备已离线");
				}
			} // sync
			else if (action.equals(Constant.BROADCAST_DEVICE_SYNC)) {
				int key = intent.getIntExtra(Constant.KEY, -1);
				int type = intent.getIntExtra(Constant.TYPE, -1);
				Object value = null;
				switch (type) {
				case XlinkCode.POINT_TYPE_BOOLEAN:
					value = intent.getBooleanExtra(Constant.DATA, false);
					break;
				case XlinkCode.POINT_TYPE_BYTE:
					value = intent.getByteExtra(Constant.DATA, (byte) 0x00);
					break;
				case XlinkCode.POINT_TYPE_SHORT:
					value = intent.getShortExtra(Constant.DATA, (short) 0);
					break;
				case XlinkCode.POINT_TYPE_INT:
					value = intent.getIntExtra(Constant.DATA, 0);
					break;
				default:
					break;
				}
			} else if (action.equals(Constant.BROADCAST_DEVICE_SYNC)) {
			} else if (action.equals(Constant.BROADCAST_EXIT)) {
				finish();
			}
		}
	};

	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) {
		if (event.getKeyCode() == KeyEvent.KEYCODE_BACK) {
			if (event.getAction() == KeyEvent.ACTION_DOWN
					&& event.getRepeatCount() == 0) {
				back();
			}
			return true;
		} else if (keyCode == KeyEvent.KEYCODE_MENU) {

		}
		return super.onKeyDown(keyCode, event);
	}

	public void showExitDialog() {
		Builder builder = new Builder(this);
		builder.setCancelable(false);
		builder.setMessage("退出程序吗？");
		builder.setTitle("提示");
		builder.setNegativeButton("确定",
				new android.content.DialogInterface.OnClickListener() {
					@Override
					public void onClick(DialogInterface dialog, int which) {
						dialog.dismiss();
						XlinkAgent.getInstance().stop();
						finish();
						System.exit(0);
					}
				});
		builder.setNeutralButton("取消", null);
		builder.create().show();
	}

	public void showTipsDialog(String msg,
			android.content.DialogInterface.OnClickListener listener) {
		Builder builder = new Builder(this);
		builder.setCancelable(false);
		builder.setMessage(msg);
		builder.setTitle("提示");
		builder.setNegativeButton("确定", listener);
		builder.setNeutralButton("取消", null);
		builder.create().show();
	}

	@Override
	protected void onDestroy() {
		if (!isDelete) {
			device.getXDevice().setDeviceName(currentName);
			DeviceManage.getInstance().updateDevice(device);
			uploadDevice(device, device.getDName());
		}
		isDestroy = true;
		super.onDestroy();
		if (isRegisterBroadcast) {
			unregisterReceiver(mBroadcastReceiver);
		}
		Log("onDestroy");
		DeviceListTab.position = 0;
	}

	@Override
	protected void onNewIntent(Intent intent) {
		super.onNewIntent(intent);
		Log("-----onNewIntent()");
	}

	public boolean sendData(final byte[] bs) {
		reSend = bs;
		if (!isOnline) {
			// connectDevice();
			System.out.println(XlinkUtils.getHexBinString(bs));
			System.out.println("5");
			return false;
		}
		int ret = XlinkAgent.getInstance().sendPipeData(device.getXDevice(),
				bs, new SendPipeListener() {
					@Override
					public void onSendLocalPipeData(XDevice xdevice, int code,
							int msgId) {
						switch (code) {
						case XlinkCode.SUCCEED:
							reSend = null;
							if (!isOnline) {
								setDeviceStatus(true);
							}
							Log("发送数据" + XlinkUtils.getHexBinString(bs) + "成功");
							break;
						case XlinkCode.TIMEOUT:
							// 重新调用connect
							Log("发送数据超时：" + XlinkUtils.getHexBinString(bs));
							break;
						case XlinkCode.SERVER_CODE_UNAUTHORIZED:
							// XlinkUtils.shortTips("控制设备失败,当前帐号未订阅此设备，请重新订阅");
							Log("控制设备失败,当前帐号未订阅此设备，请重新订阅");
							XlinkAgent.getInstance().subscribeDevice(xdevice,
									Constant.password,
									new SubscribeDeviceListener() {
										@Override
										public void onSubscribeDevice(
												XDevice xdevice, int code) {
											switch (code) {
											case XlinkCode.SUCCEED:
												connectDevice();
												break;
											default:
												break;
											}
										}
									});

							break;
						case XlinkCode.SERVER_DEVICE_OFFLIEN:
							XlinkUtils.shortTips("设备不在线");
							connectDeviceTipsDialog("设备不在线");
							setDeviceStatus(false);
							break;
						case XlinkCode.INVALID_DEVICE_ID:
							setDeviceStatus(false);
							connectDeviceTipsDialog("设备未录入后台");
							break;
						default:
							XlinkUtils.shortTips("控制设备其他错误码:" + code);
							connectDeviceTipsDialog("控制设备其他错误码:" + code);
							Log("控制设备其他错误码:" + code);
							setDeviceStatus(false);
							break;
						}
					}
				});
		if (ret < 0) {
			setDeviceStatus(false);
			switch (ret) {
			case XlinkCode.NO_CONNECT_SERVER:
				XlinkUtils.shortTips("发送数据失败，手机未连接服务器");
				break;
			case XlinkCode.NETWORD_UNAVAILABLE:
				XlinkUtils.shortTips("当前网络不可用,发送数据失败");
				break;
			case XlinkCode.NO_DEVICE:
				XlinkUtils.shortTips("未找到设备");
				XlinkAgent.getInstance().initDevice(device.getXDevice());
				break;
			default:
				XlinkUtils.shortTips("发送数据失败，错误码：" + ret);
				break;
			}
			Log("发送数据失败，错误码：" + ret);
			return false;
		}
		return true;
	}

	private void Log(String msg) {
		MyLog.e(TAG, msg);
	}

	@Override
	protected void onRestart() {
		super.onRestart();
	}

	@Override
	protected void onResume() {
		super.onResume();
		Log("onResume");
		// MyApp.getApp().setCurrentActivity(this);
		isRun = true;
	}

	@Override
	protected void onPause() {
		super.onPause();
		isRun = false;
		Log("onPause");
	}

	Fragment currentFr;

	/**
	 * 切换fragment内容呈现
	 * 
	 * @param fragment
	 * @param tag
	 */
	public void replaceFragment(String tag) {
		Log.d("UI", "切换fragment " + tag);

		// 收起输入法
		InputMethodManager imm = (InputMethodManager) getSystemService(INPUT_METHOD_SERVICE);
		if (imm != null) {
			imm.hideSoftInputFromWindow(getWindow().getDecorView()
					.getWindowToken(), 0);
		}

		if (tag == mCurrentFragment) {
			return;
		}

		FragmentTransaction mFragmentTransaction = fragmentManager
				.beginTransaction();

		// 隐藏当前fragment
		if (mCurrentFragment != null) {
			mFragmentTransaction.hide(fragmentManager
					.findFragmentByTag(mCurrentFragment));
		}

		Fragment fragment = fragmentManager.findFragmentByTag(tag);
		currentFr = fragment;
		if (fragment != null) {
			mFragmentTransaction.show(fragment);
		} else {
			fragment = this.getFragment(tag);
			if (fragment != null) {
				mFragmentTransaction.replace(R.id.ll_main, fragment, tag);
			} else {
				Log.e("MainActivity", "没有取到对应的fragment");
			}
		}
		mFragmentTransaction.addToBackStack(null);
		currentFr = fragment;
		mCurrentFragment = tag;
		mFragmentTransaction.commit();
		Log.d("UI", "提交fragment " + tag);
	}

	private Fragment getFragment(String tag) {
		Fragment fragment = null;
		// TODO 不同控制界面这里要做适配
		Fragment fragments[] = new Fragment[] { new Device_Main_Tab(),
				new Qizhen_Main_Tab(), new FanLightFragment(),
				new DryerFragment(), new ClothingCareFragment(),new LightFragment() };
		for (int i = 0; i < Constant.PRODUCT_NAME.length; i++) {
			if (tag.equals(Constant.PRODUCT_NAME[i])) {
				fragment = fragments[i];
				break;
			}
		}

		return fragment;
	}

	EditText device_name_edit;
	CustomDialog device_name_dialog;

	private void updateName() {
		device_name_dialog = CustomDialog.createMicroDialog(this,
				R.layout.update_device_name);
		device_name_edit = (EditText) device_name_dialog
				.findViewById(R.id.device_edit);
		device_name_dialog.findViewById(R.id.update_save).setOnClickListener(
				this);
		device_name_dialog.show();
	}

	/**
	 * 上传设备至后台数据库
	 */
	private void uploadDevice(Device device, String name) {
		final int appid = SharedPreferencesUtil.queryIntValue(Constant.APP_ID);
		String id = "";
		final XDevice xdevice = device.getXDevice();
		final JSONObject data = XlinkAgent.deviceToJson(xdevice);
		try {
			data.put("appid", appid);
			id = appid + "#" + xdevice.getMacAddress();
			data.put("id", xdevice.getMacAddress());
			XlinkAgent.getInstance().initDevice(xdevice);
			if (xdevice.getProductId().equals(Constant.PRODUCTID)) {
				data.put("type", Constant.PRODUCT_NAME);
			}
			data.put("device_name", name);
			xdevice.setDeviceName(name);
			xdevice.setAuthkey(SharedPreferencesUtil
					.queryValue(Constant.APP_KEY));
			currentName = name;
		} catch (JSONException e) {
			e.printStackTrace();
		}
		HttpAgent.getInstance().putData(Constant.TABLE_USER, data,
				new TextHttpResponseHandler() {
					@Override
					public void onFailure(int arg0, Header[] arg1, String arg2,
							Throwable arg3) {
						// TODO Auto-generated method stub
						// XlinkUtils.shortTips("修改失败");
					}

					@Override
					public void onSuccess(int arg0, Header[] arg1, String arg2) {
						String id = appid + "#" + xdevice.getMacAddress();
						boolean isExist = false;
						Xlink_Db_Helper mDb_Helper = new Xlink_Db_Helper(
								MainActivity.this);
						SQLiteDatabase db = mDb_Helper.getWritableDatabase();
						ContentValues values = new ContentValues();
						String query = "Select * from device_db where id like ?";
						Cursor mCursor = db.rawQuery(query,
								new String[] { appid + "#%" });
						try {
							data.put("protocol", 1);
							values.put("data", data.toString());
						} catch (JSONException e) {
							e.printStackTrace();
							return;
						}
						while (mCursor.moveToNext()) {
							if (mCursor.getString(mCursor.getColumnIndex("id"))
									.equals(id)) {
								isExist = true;
								break;
							}
						}
						if (isExist) {
							db.update("device_db", values, "id=?",
									new String[] { id });
						} else {
							values.put("id", id);
							db.insert("device_db", null, values);
						}
						db.close();
						mDb_Helper.close();
					}
				}, id);
	}

	private void deleteDevice(final String mac) {
		final Dialog dialog = createProgressDialog("请稍等", "正在删除设备...");
		dialog.setCancelable(false);
		dialog.setCanceledOnTouchOutside(false);
		dialog.show();
		final int appid = SharedPreferencesUtil.queryIntValue(Constant.APP_ID);
		HttpAgent.getInstance().deleteData(Constant.TABLE_USER,
				new TextHttpResponseHandler() {

					@Override
					public void onSuccess(int arg0, Header[] arg1, String arg2) {
						// TODO Auto-generated method stub
						XlinkUtils.shortTips("删除成功");
						dialog.dismiss();
						DeviceManage.getInstance().clearAllDevice();
						Xlink_Db_Helper mDb_Helper = new Xlink_Db_Helper(
								MainActivity.this);
						SQLiteDatabase db = mDb_Helper.getWritableDatabase();
						String delete = "Delete from device_db where id = \""
								+ appid + "#" + mac + "\"";
						db.execSQL(delete);
						db.close();
						mDb_Helper.close();
						isDelete = true;
						openDeviceListActivity();
					}

					@Override
					public void onFailure(int arg0, Header[] arg1, String arg2,
							Throwable arg3) {
						dialog.dismiss();
						XlinkUtils.shortTips("删除失败");
					}
				}, appid + "#" + mac);
	}

	private void openDeviceListActivity() {
		finish();
	}

	private void back() {
		openDeviceListActivity();
	}

	@Override
	public void onClick(View v) {
		if (mPopupWindow != null)
			mPopupWindow.dismiss();
		switch (v.getId()) {
		case R.id.update_save:
			final String updatename = device_name_edit.getText().toString();
			if (TextUtils.isEmpty(updatename)) {
				XlinkUtils.shortTips("请输入正确的名称");
				return;
			}
			device_name_dialog.dismiss();
			device.getXDevice().setDeviceName(updatename);
			setTitleText(updatename);
			DeviceManage.getInstance().updateDevice(device);
			new Thread(new Runnable() {
				@Override
				public void run() {
					uploadDevice(device, updatename);
				}
			}).run();
			break;
		case R.id.title_back:
			back();
			break;
		case R.id.title_more:
			showMore();
			break;
		case R.id.more_relative_1:
			// 修改设备名称
			updateName();
			break;
		case R.id.more_relative_4:
			// 退出帐号
			showTipsDialog("确定退出当前帐号吗？",
					new android.content.DialogInterface.OnClickListener() {

						@Override
						public void onClick(DialogInterface dialog, int which) {
							dialog.dismiss();
							MyApp.getApp().removeUser(MainActivity.this);
						}
					});
			break;
		case R.id.more_relative_2:
			if (!isOnline) {
				connectDevice();
				return;
			}
			// 修改访问密码
			alterAuth();
			break;
		case R.id.more_relative_3:
			// 删除设备
			showTipsDialog("确定删除设备吗",
					new android.content.DialogInterface.OnClickListener() {

						@Override
						public void onClick(DialogInterface dialog, int which) {
							dialog.dismiss();
							deleteDevice(device.getMacAddress());

						}
					});
			break;

		// 修改密码确定按钮
		case R.id.alter_auth_svae:
			String oldAuth = oldAuth_edit.getText().toString().trim();
			int newAuth;
			int compare;
			if (oldAuth.equals(device.getPassword())) {
				try {
					newAuth = Integer.parseInt(newAuth_edit.getText()
							.toString().trim());
					compare = Integer.parseInt(compare_edit.getText()
							.toString().trim());
				} catch (Exception e) {
					XlinkUtils.shortTips("请输入设备密码，密码小于900000000");
					return;
				}
				if ((newAuth > 900000000) || (newAuth < 0)) {
					XlinkUtils.shortTips("请输入设备密码，密码小于900000000");
					return;
				}
				if (newAuth != compare) {
					XlinkUtils.shortTips("2次密码不一致，请重新输入");
					return;
				}
				setDevicePassword(oldAuth, newAuth);

			} else {
				oldAuth_edit.setText("");
				XlinkUtils.shortTips("原设备密码不匹配");
			}
			break;
		default:
			break;
		}
	}

	private CustomDialog alterAuthDialog;
	private EditText oldAuth_edit, newAuth_edit, compare_edit;

	private void alterAuth() {
		alterAuthDialog = CustomDialog.createStandardDialog(this,
				R.layout.alter_device_auth);
		alterAuthDialog.show();
		alterAuthDialog.findViewById(R.id.alter_auth_svae).setOnClickListener(
				this);
		oldAuth_edit = (EditText) alterAuthDialog
				.findViewById(R.id.alter_old_edit);
		newAuth_edit = (EditText) alterAuthDialog
				.findViewById(R.id.alter_new_edit);

		compare_edit = (EditText) alterAuthDialog
				.findViewById(R.id.alter_compare_edit);

	}

	private void setDevicePassword(String oldPw, final int newPw) {

		int ret = XlinkAgent.getInstance().setDeviceAccessKey(
				device.getXDevice(), newPw, new SetDeviceAccessKeyListener() {
					@Override
					public void onSetLocalDeviceAccessKey(XDevice xdevice,
							int code, int msgId) {
						if (alterProgressDialog != null && !isDestroy) {
							alterProgressDialog.dismiss();
						}
						if (alterAuthDialog != null && !isDestroy)
							alterAuthDialog.dismiss();
						switch (code) {
						case XlinkCode.SUCCEED:
							XlinkUtils.shortTips("修改初始密码成功");
							// device.getXDevice().getAccessKey()
							DeviceManage.getInstance().updateDevice(xdevice);
							break;
						default:
							XlinkUtils.shortTips("设置密码失败");
							break;
						}
						Log("设置默认密码:" + code);

					}
				});
		if (ret < 0) {
			XlinkUtils.shortTips("修改密码失败" + ret);
		} else {
			showAlterDialog();

		}
	}

	PopupWindow mPopupWindow;

	public void showSetting(View v, boolean b1, boolean b2) {
		if (mPopupWindow == null) {
			LayoutInflater layoutInflater = LayoutInflater.from(this);
			View myview = layoutInflater.inflate(R.layout.device_popup_more,
					null);
			mPopupWindow = new PopupWindow(myview, myview.getWidth(),
					myview.getHeight());
			View v1, v2;
			v1 = myview.findViewById(R.id.more_relative_1);
			if (b1) {
				v1.setOnClickListener(this);
			} else {
				v1.setVisibility(View.GONE);
			}
			myview.findViewById(R.id.more_relative_3).setOnClickListener(this);
			myview.findViewById(R.id.more_relative_4).setOnClickListener(this);

			mPopupWindow.setWindowLayoutMode(
					ViewGroup.LayoutParams.WRAP_CONTENT,
					ViewGroup.LayoutParams.WRAP_CONTENT);
			mPopupWindow.setFocusable(true);
			// 设置点击弹框外部，弹框消失
			mPopupWindow.setOutsideTouchable(true);
			// 设置一个透明的背景，不然无法实现点击弹框外，弹框消失
			mPopupWindow.setBackgroundDrawable(new BitmapDrawable());
			mPopupWindow.setTouchable(true);

			// 获取popwidow的宽高
		}

		if (!mPopupWindow.isShowing()) {

			mPopupWindow.showAsDropDown(v);
		} else {

			mPopupWindow.dismiss();

		}
	}

	private void showConnectDialog() {
		if (connectDialog == null) {
			connectDialog = createProgressDialog("连接设备", "正在连接设备...");
		}
		if (!connectDialog.isShowing())
			connectDialog.show();
	}

	private void showLoginDialog() {
		if (loginDialog == null) {
			loginDialog = createProgressDialog("登录", "正在登录服务器...");
		}
		if (!loginDialog.isShowing())
			loginDialog.show();
	}

	public void showAlterDialog() {
		if (alterProgressDialog == null) {
			alterProgressDialog = createProgressDialog("修改授权码", "正在修改授权码...");
		}
		if (!alterProgressDialog.isShowing())
			alterProgressDialog.show();
	}

	private void setTitleText(String text) {
		title_text.setText(text);
		title_more.setVisibility(View.VISIBLE);
	}

	@SuppressWarnings("unused")
	private void showBack() {
		title_back.setVisibility(View.VISIBLE);
	}

	private void gongBack() {
		title_back.setVisibility(View.GONE);
	}

	private void showMore() {
		showSetting(title_more, true, true);
	}

}
