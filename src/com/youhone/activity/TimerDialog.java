package com.youhone.activity;

import java.util.Calendar;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RadioGroup.OnCheckedChangeListener;
import android.widget.TimePicker;

import com.youhone.utils.airspa.AirSpa_OrderUtils;
import com.youhone.xlink.superapp.R;

public class TimerDialog extends Dialog {
	private Button btn_confirm, btn_cancel;
	private BtnListener btnListener = new BtnListener();
	private OnConfirmListener listener;
	private RadioGroup dialog_timer_rg;
	private RadioButton dialog_timer_on, dialog_timer_off;
	private TimePicker control_time_picker;
	private boolean isOn = true;//是否为定时开机
	private Context mContext;

	public TimerDialog(Context context) {
		super(context);
		mContext = context;
	}

	public TimerDialog(Context context, int theme) {
		super(context, theme);
		mContext = context;
	}

	public TimerDialog(Context context, boolean cancelable,
			OnCancelListener cancelListener) {
		super(context, cancelable, cancelListener);
		mContext = context;
	}

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.dialog_timer);
		init();
		super.onCreate(savedInstanceState);
	}

	@Override
	protected void onStart() {
		super.onStart();
	}

	private void init() {
		control_time_picker = (TimePicker) findViewById(R.id.control_time_picker);
		btn_confirm = (Button) findViewById(R.id.btn_confirm);
		btn_cancel = (Button) findViewById(R.id.btn_cancel);
		dialog_timer_rg = (RadioGroup) findViewById(R.id.dialog_timer_rg);
		dialog_timer_on = (RadioButton) findViewById(R.id.dialog_timer_on);
		dialog_timer_off = (RadioButton) findViewById(R.id.dialog_timer_off);
		dialog_timer_rg
				.setOnCheckedChangeListener(new RadioCheckChangeListener());
		btn_confirm.setOnClickListener(btnListener);
		btn_cancel.setOnClickListener(btnListener);
		control_time_picker.setIs24HourView(true);
	}

	private class RadioCheckChangeListener implements OnCheckedChangeListener {

		@Override
		public void onCheckedChanged(RadioGroup group, int checkedId) {
			int id = checkedId;
			if (id == R.id.dialog_timer_on) {
				isOn = true;
			} else if (id == R.id.dialog_timer_off) {
				isOn = false;
			}
		}

	}

	private class BtnListener implements android.view.View.OnClickListener {
		@Override
		public void onClick(View v) {
			switch (v.getId()) {
			case R.id.btn_confirm:
				int hour = control_time_picker.getCurrentHour();
				int min = control_time_picker.getCurrentMinute();
				Calendar calendar = Calendar.getInstance();
				int current_hour = calendar.get(Calendar.HOUR_OF_DAY);
				int current_min = calendar.get(Calendar.MINUTE);
				// 计算定时的时间
				int send_begin_hour = hour - current_hour;
				if (send_begin_hour < 0) {
					send_begin_hour += 24;
				}
				int send_begin_min = min - current_min;
				if (send_begin_min < 0) {
					send_begin_min += 60;
					send_begin_hour--;
				}
				System.out.println("距离执行还有"+send_begin_hour+"小时"+send_begin_min+"分钟");
				send_begin_min = (send_begin_min + (send_begin_hour) * 60) / 6;
				// AirSpa_OrderUtils.setTimer((byte) 3);
				// ((MainActivity) mContext).sendData(AirSpa_OrderUtils
				// .getResult());
				AirSpa_OrderUtils.setTimerCount(1);
				AirSpa_OrderUtils.setTimer1Onoff(isOn ? (byte) 1 : (byte) 0);
				AirSpa_OrderUtils.setTimer1On(send_begin_min);
				AirSpa_OrderUtils.setTimer1Gear((byte) 0);
				AirSpa_OrderUtils.setTimer1Model((byte) 0);
				((MainActivity) mContext).sendData(AirSpa_OrderUtils
						 .getTimerByte());
				dismiss();
				break;
			case R.id.btn_cancel:
				dismiss();
				break;
			}
		}
	}

	public void setOnConfirmListener(OnConfirmListener l) {
		this.listener = l;
	}

	public interface OnConfirmListener {
		/**
		 * Called when the confirm button has been clicked.
		 */
		void onConfirm(int time);
	}

}
