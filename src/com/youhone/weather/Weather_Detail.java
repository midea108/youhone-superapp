package com.youhone.weather;

import io.xlink.wifi.ui.activity.DeviceListActivity;
import io.xlink.wifi.ui.fragment.BaseFragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.youhone.weatherform.WeatherForm;
import com.youhone.xlink.superapp.R;

public class Weather_Detail extends BaseFragment {
	// 天气相关
	private TextView city, temp, pm25, quality, date, shidu, fengxiang,
			current_temp;
	private ImageView detail_weather_img;
	private LinearLayout current_weather, yubao_weather;
	private TextView no_city_tip;
	private WeatherForm[] weathers;
	private BtnListener btnListener = new BtnListener();

	private LinearLayout weather_lay1, weather_lay2, weather_lay3;
	private TextView week1, week2, week3;
	private TextView quality1, quality2, quality3;
	private ImageView weather_img1, weather_img2, weather_img3;
	private TextView temp1, temp2, temp3;
	private TextView weather_text1, weather_text2, weather_text3;

	public Weather_Detail(WeatherForm[] weathers) {
		this.weathers = weathers;
	}

	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
	}

	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View view = inflater.inflate(R.layout.fragment_weather_detail,
				container, false);
		initView(view);
		showWeather(weathers);
		return view;
	}

	private void initView(View view) {
		weather_lay1 = (LinearLayout) view.findViewById(R.id.weather_lay1);
		weather_lay2 = (LinearLayout) view.findViewById(R.id.weather_lay2);
		weather_lay3 = (LinearLayout) view.findViewById(R.id.weather_lay3);
		week1 = (TextView) view.findViewById(R.id.week1);
		week2 = (TextView) view.findViewById(R.id.week2);
		week3 = (TextView) view.findViewById(R.id.week3);
		quality1 = (TextView) view.findViewById(R.id.quality1);
		quality2 = (TextView) view.findViewById(R.id.quality2);
		quality3 = (TextView) view.findViewById(R.id.quality3);
		weather_img1 = (ImageView) view.findViewById(R.id.weather_img1);
		weather_img2 = (ImageView) view.findViewById(R.id.weather_img2);
		weather_img3 = (ImageView) view.findViewById(R.id.weather_img3);
		temp1 = (TextView) view.findViewById(R.id.temp1);
		temp2 = (TextView) view.findViewById(R.id.temp2);
		temp3 = (TextView) view.findViewById(R.id.temp3);
		weather_text1 = (TextView) view.findViewById(R.id.weather_text1);
		weather_text2 = (TextView) view.findViewById(R.id.weather_text2);
		weather_text3 = (TextView) view.findViewById(R.id.weather_text3);
		current_temp = (TextView) view.findViewById(R.id.detail_current_temp);
		fengxiang = (TextView) view.findViewById(R.id.fengxiang);
		shidu = (TextView) view.findViewById(R.id.detail_shidu);
		date = (TextView) view.findViewById(R.id.weather_text);
		quality = (TextView) view.findViewById(R.id.detail_air_quality);
		city = (TextView) view.findViewById(R.id.detail_city_name);
		temp = (TextView) view.findViewById(R.id.detail_temp);
		// pm25 = (TextView) view.findViewById(R.id.detail_pm25);
		no_city_tip = (TextView) view.findViewById(R.id.detail_no_city_tip);
		detail_weather_img = (ImageView) view
				.findViewById(R.id.detail_weather_img);
		current_weather = (LinearLayout) view
				.findViewById(R.id.current_weather);
		yubao_weather = (LinearLayout) view.findViewById(R.id.yubao_weather);

		no_city_tip.setOnClickListener(btnListener);
	}

	private void showWeather(WeatherForm[] weathers) {
		if (weathers != null) {
			int length = weathers.length;
			System.out.println("天气的长度" + length);
			if (length >= 1) {
				no_city_tip.setVisibility(View.GONE);
				current_weather.setVisibility(View.VISIBLE);
				yubao_weather.setVisibility(View.VISIBLE);
				WeatherForm firstDay = weathers[0];
				date.setText(firstDay.getWeather());
				city.setText(firstDay.getName());
				temp.setText(firstDay.getCurrent_temp() + "℃");
				// pm25.setText(firstDay.getPm25());
				fengxiang.setText(firstDay.getWind());
				current_temp.setText(firstDay.getCurrent_temp() + "℃");
				String air_quality = firstDay.getQuality();
				quality.setText("空气质量：" + air_quality);
				if (air_quality.contains("优")) {
					quality.setBackgroundResource(R.drawable.bg_quality_you);
				} else if (air_quality.contains("良")) {
					quality.setBackgroundResource(R.drawable.bg_quality_liang);
				} else if (air_quality.contains("轻度")) {
					quality.setBackgroundResource(R.drawable.bg_quality_qingdu);
				} else if (air_quality.contains("中度")) {
					quality.setBackgroundResource(R.drawable.bg_quality_zhongdu);
				} else if (air_quality.contains("差")
						|| air_quality.contains("重度")) {
					quality.setBackgroundResource(R.drawable.bg_quality_heavy);
				} else if (air_quality.contains("差")
						|| air_quality.contains("严重")) {
					quality.setBackgroundResource(R.drawable.bg_quality_yanzhong);
				} else {
					quality.setBackgroundResource(R.drawable.bg_quality_you);
				}
				shidu.setText(firstDay.getHumidity() + "%");
				detail_weather_img.setImageResource(firstDay.getHigh_img_id());
			}
			if (length >= 4) {
				WeatherForm weather1 = weathers[1];
				WeatherForm weather2 = weathers[2];
				WeatherForm weather3 = weathers[3];
				weather_lay1.setVisibility(View.VISIBLE);
				weather_lay2.setVisibility(View.VISIBLE);
				weather_lay3.setVisibility(View.VISIBLE);
				week1.setText("周" + weather1.getWeek());
				String air_quality1 = weather1.getQuality();
				quality1.setText(air_quality1);
				weather_img1.setImageResource(weather1.getImg_id());
				temp1.setText(weather1.getTemp()+ "℃");
				weather_text1.setText(weather1.getWeather());
				if (air_quality1.contains("优")) {
					quality1.setBackgroundResource(R.drawable.bg_quality_you);
				} else if (air_quality1.contains("良")) {
					quality1.setBackgroundResource(R.drawable.bg_quality_liang);
				} else if (air_quality1.contains("轻度")) {
					quality1.setBackgroundResource(R.drawable.bg_quality_qingdu);
				} else if (air_quality1.contains("中度")) {
					quality1.setBackgroundResource(R.drawable.bg_quality_zhongdu);
				} else if (air_quality1.contains("差")
						|| air_quality1.contains("重度")) {
					quality1.setBackgroundResource(R.drawable.bg_quality_heavy);
				} else if (air_quality1.contains("差")
						|| air_quality1.contains("严重")) {
					quality1.setBackgroundResource(R.drawable.bg_quality_yanzhong);
				} else {
					quality.setBackgroundResource(R.drawable.bg_quality_you);
				}

				week2.setText("周" + weather2.getWeek());
				String air_quality2 = weather2.getQuality();
				quality2.setText(air_quality2);
				weather_img2.setImageResource(weather2.getImg_id());
				temp2.setText(weather2.getTemp()+ "℃");
				weather_text2.setText(weather2.getWeather());
				if (air_quality2.contains("优")) {
					quality2.setBackgroundResource(R.drawable.bg_quality_you);
				} else if (air_quality2.contains("良")) {
					quality2.setBackgroundResource(R.drawable.bg_quality_liang);
				} else if (air_quality2.contains("轻度")) {
					quality2.setBackgroundResource(R.drawable.bg_quality_qingdu);
				} else if (air_quality2.contains("中度")) {
					quality2.setBackgroundResource(R.drawable.bg_quality_zhongdu);
				} else if (air_quality2.contains("差")
						|| air_quality2.contains("重度")) {
					quality2.setBackgroundResource(R.drawable.bg_quality_heavy);
				} else if (air_quality2.contains("差")
						|| air_quality2.contains("严重")) {
					quality2.setBackgroundResource(R.drawable.bg_quality_yanzhong);
				} else {
					quality.setBackgroundResource(R.drawable.bg_quality_you);
				}

				week3.setText("周" + weather3.getWeek());
				String air_quality3 = weather3.getQuality();
				quality3.setText(air_quality3);
				weather_img3.setImageResource(weather3.getImg_id());
				temp3.setText(weather3.getTemp()+ "℃");
				weather_text3.setText(weather3.getWeather());
				if (air_quality3.contains("优")) {
					quality3.setBackgroundResource(R.drawable.bg_quality_you);
				} else if (air_quality3.contains("良")) {
					quality3.setBackgroundResource(R.drawable.bg_quality_liang);
				} else if (air_quality3.contains("轻度")) {
					quality3.setBackgroundResource(R.drawable.bg_quality_qingdu);
				} else if (air_quality3.contains("中度")) {
					quality3.setBackgroundResource(R.drawable.bg_quality_zhongdu);
				} else if (air_quality3.contains("差")
						|| air_quality3.contains("重度")) {
					quality3.setBackgroundResource(R.drawable.bg_quality_heavy);
				} else if (air_quality3.contains("差")
						|| air_quality3.contains("严重")) {
					quality3.setBackgroundResource(R.drawable.bg_quality_yanzhong);
				} else {
					quality.setBackgroundResource(R.drawable.bg_quality_you);
				}
			} else if (length >= 3) {
				WeatherForm weather1 = weathers[1];
				WeatherForm weather2 = weathers[2];
				weather_lay1.setVisibility(View.VISIBLE);
				weather_lay2.setVisibility(View.VISIBLE);
				weather_lay3.setVisibility(View.INVISIBLE);
				week1.setText("周" + weather1.getWeek());
				String air_quality1 = weather1.getQuality();
				quality1.setText(air_quality1);
				weather_img1.setImageResource(weather1.getImg_id());
				temp1.setText(weather1.getTemp()+ "℃");
				weather_text1.setText(weather1.getWeather());
				if (air_quality1.contains("优")) {
					quality1.setBackgroundResource(R.drawable.bg_quality_you);
				} else if (air_quality1.contains("良")) {
					quality1.setBackgroundResource(R.drawable.bg_quality_liang);
				} else if (air_quality1.contains("轻度")) {
					quality1.setBackgroundResource(R.drawable.bg_quality_qingdu);
				} else if (air_quality1.contains("中度")) {
					quality1.setBackgroundResource(R.drawable.bg_quality_zhongdu);
				} else if (air_quality1.contains("差")
						|| air_quality1.contains("重度")) {
					quality1.setBackgroundResource(R.drawable.bg_quality_heavy);
				} else if (air_quality1.contains("差")
						|| air_quality1.contains("严重")) {
					quality1.setBackgroundResource(R.drawable.bg_quality_yanzhong);
				} else {
					quality.setBackgroundResource(R.drawable.bg_quality_you);
				}

				week2.setText("周" + weather2.getWeek());
				String air_quality2 = weather2.getQuality();
				quality2.setText(air_quality2);
				weather_img2.setImageResource(weather2.getImg_id());
				temp2.setText(weather2.getTemp()+ "℃");
				weather_text2.setText(weather2.getWeather());
				if (air_quality2.contains("优")) {
					quality2.setBackgroundResource(R.drawable.bg_quality_you);
				} else if (air_quality2.contains("良")) {
					quality2.setBackgroundResource(R.drawable.bg_quality_liang);
				} else if (air_quality2.contains("轻度")) {
					quality2.setBackgroundResource(R.drawable.bg_quality_qingdu);
				} else if (air_quality2.contains("中度")) {
					quality2.setBackgroundResource(R.drawable.bg_quality_zhongdu);
				} else if (air_quality2.contains("差")
						|| air_quality2.contains("重度")) {
					quality2.setBackgroundResource(R.drawable.bg_quality_heavy);
				} else if (air_quality2.contains("差")
						|| air_quality2.contains("严重")) {
					quality2.setBackgroundResource(R.drawable.bg_quality_yanzhong);
				} else {
					quality.setBackgroundResource(R.drawable.bg_quality_you);
				}
			}else if(length>=2){
				WeatherForm weather1 = weathers[1];
				weather_lay1.setVisibility(View.VISIBLE);
				weather_lay2.setVisibility(View.INVISIBLE);
				weather_lay3.setVisibility(View.INVISIBLE);
				week1.setText("周" + weather1.getWeek());
				String air_quality1 = weather1.getQuality();
				quality1.setText(air_quality1);
				weather_img1.setImageResource(weather1.getImg_id());
				temp1.setText(weather1.getTemp()+ "℃");
				weather_text1.setText(weather1.getWeather());
				if (air_quality1.contains("优")) {
					quality1.setBackgroundResource(R.drawable.bg_quality_you);
				} else if (air_quality1.contains("良")) {
					quality1.setBackgroundResource(R.drawable.bg_quality_liang);
				} else if (air_quality1.contains("轻度")) {
					quality1.setBackgroundResource(R.drawable.bg_quality_qingdu);
				} else if (air_quality1.contains("中度")) {
					quality1.setBackgroundResource(R.drawable.bg_quality_zhongdu);
				} else if (air_quality1.contains("差")
						|| air_quality1.contains("重度")) {
					quality1.setBackgroundResource(R.drawable.bg_quality_heavy);
				} else if (air_quality1.contains("差")
						|| air_quality1.contains("严重")) {
					quality1.setBackgroundResource(R.drawable.bg_quality_yanzhong);
				} else {
					quality.setBackgroundResource(R.drawable.bg_quality_you);
				}
			}else if(length<2){
				weather_lay1.setVisibility(View.INVISIBLE);
				weather_lay2.setVisibility(View.INVISIBLE);
				weather_lay3.setVisibility(View.INVISIBLE);
			}

		} else {
			no_city_tip.setVisibility(View.VISIBLE);
			current_weather.setVisibility(View.GONE);
			yubao_weather.setVisibility(View.GONE);
		}
		System.out.println("天气显示成功");
	}

	private class BtnListener implements OnClickListener {

		@Override
		public void onClick(View v) {
			int id = v.getId();
			if (id == R.id.no_city_tip) {
				getAct().openCityManagerFragment();
			}
		}

	}

	private DeviceListActivity getAct() {
		return (DeviceListActivity) getActivity();
	}

	// public void setListViewHeightBasedOnChildren(ListView listView) {
	// // 获取ListView对应的Adapter
	// ListAdapter listAdapter = listView.getAdapter();
	// if (listAdapter == null) {
	// return;
	// }
	//
	// int totalHeight = 0;
	// for (int i = 0, len = listAdapter.getCount(); i < len; i++) {
	// // listAdapter.getCount()返回数据项的数目
	// View listItem = listAdapter.getView(i, null, listView);
	// // 计算子项View 的宽高
	// listItem.measure(0, 0);
	// // 统计所有子项的总高度
	// totalHeight += listItem.getMeasuredHeight();
	// }
	//
	// ViewGroup.LayoutParams params = listView.getLayoutParams();
	// params.height = totalHeight
	// + (listView.getDividerHeight() * (listAdapter.getCount() - 1));
	// // listView.getDividerHeight()获取子项间分隔符占用的高度
	// // params.height最后得到整个ListView完整显示需要的高度
	// listView.setLayoutParams(params);
	// }

}
