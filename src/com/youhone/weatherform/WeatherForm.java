package com.youhone.weatherform;

import com.youhone.xlink.superapp.R;

public class WeatherForm {
	/** 城市名 */
	private String name;
	/** 城市编号 */
	private String id;
	/** 当前日期 */
	private String ddate;
	/** 星期 */
	private String week;
	/** 温度 */
	private String temp;
	/** 湿度 */
	private String humidity;
	/** 天气描述 */
	private String weather;
	/** 风力 */
	private String wind;
	/** 风向 */
	private String fx;
	/** PM2.5 */
	private String pm25;
	/** 空气质量 */
	private String quality;
	/** 健康建议 */
	private String tips;
	/** 天气图片 */
	private int img_id;
	private int high_img_id;

	/** 天气背景 */
	private int bg_id;

	/** 当前温度 */
	private String current_temp = "";

	public WeatherForm() {

	}

	/**
	 * 构造方法
	 * 
	 * @param name
	 * @param id
	 * @param ddate
	 * @param week
	 * @param temp
	 * @param weather
	 * @param wind
	 * @param fx
	 */
	public WeatherForm(String name, String id, String ddate, String week,
			String temp, String weather, String wind, String fx, String PM25,
			String quality, String humidity, int img_id) {
		super();
		this.name = name;
		this.id = id;
		this.ddate = ddate;
		this.week = week;
		this.temp = temp;
		this.weather = weather;
		this.wind = wind;
		this.fx = fx;
		this.pm25 = PM25;
		this.quality = quality;
		this.humidity = humidity;
		this.img_id = img_id;
	}

	public int getHigh_img_id() {
		return high_img_id;
	}

	public void setHigh_img_id(int high_img_id) {
		this.high_img_id = high_img_id;
	}

	public String getCurrent_temp() {
		return current_temp;
	}

	public void setCurrent_temp(String current_temp) {
		this.current_temp = current_temp;
	}

	public int getBg_id() {
		return bg_id;
	}

	public void setBg_id(int bg_id) {
		this.bg_id = bg_id;
	}

	public int getImg_id() {
		return img_id;
	}

	public void setImg_id(int img_id) {
		this.img_id = img_id;
	}

	public String getHumidity() {
		return humidity;
	}

	public void setHumidity(String humidity) {
		this.humidity = humidity;
	}

	public String getTips() {
		return tips;
	}

	public void setTips(String tips) {
		this.tips = tips;
	}

	public String getQuality() {
		return quality;
	}

	public void setQuality(String quality) {
		this.quality = quality;
	}

	public String getPm25() {
		return pm25;
	}

	public void setPm25(String pm25) {
		this.pm25 = pm25;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getDdate() {
		return ddate;
	}

	public void setDdate(String ddate) {
		this.ddate = ddate;
	}

	public String getWeek() {
		return week;
	}

	public void setWeek(String week) {
		this.week = week;
	}

	public String getTemp() {
		return temp;
	}

	public void setTemp(String temp) {
		this.temp = temp;
	}

	public String getWeather() {
		return weather;
	}

	public void setWeather(String weather) {
		this.weather = weather;
		if (weather.contains("晴")
				&& (weather.contains("云") || weather.contains("阴"))) {
			bg_id = R.drawable.weather_sun;
			img_id = R.drawable.sun_cloud_small;
			high_img_id= R.drawable.sun_cloud_high;
		} else if (weather.contains("晴")) {
			bg_id = R.drawable.weather_sun;
			img_id = R.drawable.sun_small;
			high_img_id= R.drawable.sun_high;
		} else if (weather.contains("大雨") && weather.contains("云")) {
			bg_id = R.drawable.weather_rain;
			img_id = R.drawable.rain_small;
			high_img_id= R.drawable.rain_high;
		} else if (weather.contains("雨") && weather.contains("云")) {
			bg_id = R.drawable.weather_rain;
			img_id = R.drawable.rain_small;
			high_img_id= R.drawable.rain_high;
		} else if (weather.contains("雨") && weather.contains("雪")) {
			bg_id = R.drawable.weather_rain;
			img_id = R.drawable.snow_small;
			high_img_id= R.drawable.snow_high;
		} else if (weather.contains("云") || weather.contains("阴")) {
			bg_id = R.drawable.weather_sun;
			img_id = R.drawable.cloud_small;
			high_img_id= R.drawable.cloud_high;
		} else if (weather.contains("雨")) {
			bg_id = R.drawable.weather_rain;
			img_id = R.drawable.rain_small;
			high_img_id= R.drawable.rain_high;
		} else if (weather.contains("雨") && weather.contains("雷")) {
			bg_id = R.drawable.weather_rain;
			img_id = R.drawable.rain_small;
			high_img_id= R.drawable.rain_high;
		} else if (weather.contains("雷")) {
			bg_id = R.drawable.weather_rain;
			img_id = R.drawable.rain_small;
			high_img_id= R.drawable.rain_high;
		} else if (weather.contains("雪")) {
			bg_id = R.drawable.weather_rain;
			img_id = R.drawable.snow_small;
			high_img_id= R.drawable.snow_high;
		} else if (weather.contains("风")) {
			bg_id = R.drawable.weather_sun;
			img_id = R.drawable.cloud_small;
			high_img_id= R.drawable.cloud_high;
		} else if (weather.contains("霾") || weather.contains("雾")
				|| weather.contains("沙")) {
			bg_id = R.drawable.weather_rain;
			img_id = R.drawable.cloud_small;
			high_img_id= R.drawable.cloud_high;
		} else {
			bg_id = R.drawable.weather_sun;
			img_id = R.drawable.sun_small;
			high_img_id= R.drawable.sun_high;
		}
	}

	public String getWind() {
		return wind;
	}

	public void setWind(String wind) {
		this.wind = wind;
	}

	public String getFx() {
		return fx;
	}

	public void setFx(String fx) {
		this.fx = fx;
	}

	@Override
	public String toString() {
		return "WeatherForm [name=" + name + ", id=" + id + ", ddate=" + ddate
				+ ", week=" + week + ", temp=" + temp + ", weather=" + weather
				+ ", wind=" + wind + ", fx=" + fx + "]";
	}
}
