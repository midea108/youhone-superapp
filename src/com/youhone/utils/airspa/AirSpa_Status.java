package com.youhone.utils.airspa;

public class AirSpa_Status {
	public static int para_start = 10;

	private int setTemp = 0;
	private int setHumidity = 0;
	private boolean isOn = false;
	private int model = 1;
	private int gear = 1;
	private boolean negative = false;
	private int temp_in = 0;
	private int temp_out = 0;
	private int humidity_in = 0;
	private int humidity_out = 0;
	private int quality = 0;
	private int co2 = 0;
	private int pm25 = 0;
	private int sleepTime_off = 0;
	private boolean needClean = false;
	private boolean isAlarm = false;
	private int dust_in = 0;
	private int dust_out = 0;
	private int aqi = 0;
	private int voc = 0;
	private int work_time = 0;
	private boolean isTimerOn = false;

	private boolean isTiming1 = false;
	private int time1_on = 0;
	private int time1_off = 0;
	private int hour1_on = 0;
	private int min1_on = 0;
	private int hour1_off = 0;
	private int min1_off = 0;

	private boolean isTiming2 = false;
	private int hour2_on = 0;
	private int min2_on = 0;
	private int hour2_off = 0;
	private int min2_off = 0;

	private boolean isTiming3 = false;
	private int hour3_on = 0;
	private int min3_on = 0;
	private int hour3_off = 0;
	private int min3_off = 0;
	private boolean sleep = false;
	private int test = 0;
	private int index = 0;
	private boolean isSwitch = false;
	private boolean isAlarmCode = false;
	private boolean isStatusQuery = false;
	private boolean isControlBack = false;
	private boolean isWorkTimeBck = false;

	private StringBuilder alarmText = new StringBuilder();

	public AirSpa_Status(byte[] buff) {

		if (AirSpa_OrderUtils.Sum(buff, 2) != buff[buff.length - 3]) {
			System.out.println("校验不通过");
			return;
		}
		if (buff[5] == 0x03) {
			System.out.println("开关=" + buff[6]);
			isOn = (buff[6]) == 1;
			isSwitch = true;
		} else if (buff[5] == 0x66 && buff[3] == 0x35) {// 当前运行状态
			isStatusQuery = true;
			// isTiming1 = !((buff[para_start + 0] & 0x60) == 0);
			needClean = (buff[para_start + 0] & 0x80) == 1;
			System.out.println("是否需要清洁=" + needClean);
			isTimerOn = (buff[para_start + 0] & 0x60) != 0;
			System.out.println("定时=" + isTimerOn);
			isOn = (buff[para_start + 1] & 0x01) == 1;
			System.out.println("开关=" + isOn);
			model = buff[para_start + 1] >> 1 & 0x07;
			System.out.println("模式=" + model);
			gear = buff[para_start + 1] >> 4 & 0x0f;
			System.out.println("档位=" + gear);
			setHumidity = buff[para_start + 2] & 0x7F;
			System.out.println("设定湿度=" + setHumidity);
			negative = (buff[para_start + 2] & 0x80) != 0;
			System.out.println("负离子=" + negative);
			setTemp = buff[para_start + 3] & 0x7F;
			System.out.println("设置温度=" + setTemp);
			sleep = (buff[para_start + 3] & 0x80) != 0;
			System.out.println("睡眠=" + sleep);
			pm25 = (buff[para_start + 6] < 0 ? buff[para_start + 6] + 256
					: buff[para_start + 6])
					+ (buff[para_start + 5] < 0 ? buff[para_start + 5] + 256
							: buff[para_start + 5]) * 256;
			System.out.println("pm25=" + pm25);
			test = buff[para_start + 7] & 0x01;
			temp_in = (buff[para_start + 9] < 0 ? buff[para_start + 9] + 256 - 20
					: buff[para_start + 9]) - 20;
			System.out.println("室内温度=" + temp_in);
			temp_out = (buff[para_start + 10] < 0 ? buff[para_start + 10] + 256 - 20
					: buff[para_start + 10]) - 20;
			System.out.println("室外温度=" + temp_out);
			humidity_in = buff[para_start + 11] < 0 ? buff[para_start + 11] + 256
					: buff[para_start + 11];
			System.out.println("室内湿度=" + humidity_in);
			humidity_out = buff[para_start + 12] < 0 ? buff[para_start + 12] + 256
					: buff[para_start + 12];
			System.out.println("室外湿度=" + humidity_out);
			co2 = (buff[para_start + 13] < 0 ? buff[para_start + 13] + 256
					: buff[para_start + 13]) * 10;
			System.out.println("co2=" + co2);
			quality = buff[para_start + 14] & 0x03;
			System.out.println("空气质量=" + quality);
			isAlarm = (buff[para_start + 14] >> 3 & 0x01) == 1;
			System.out.println("报警=" + isAlarm);
			// time1_on = buff[para_start + 3] < 0 ? buff[para_start + 3] +
			// 256
			// : buff[para_start + 3];
			// time1_off = buff[para_start + 9] < 0 ? buff[para_start + 9] +
			// 256
			// : buff[para_start + 9];
			// sleepTime_off = buff[para_start + 15] < 0 ? buff[para_start +
			// 15]
			// + 256
			// : buff[para_start + 15];
			// dust = buff[para_start + 29] & 0xf0;
		} else if (buff[5] == 0x65) {
			isControlBack = true;
			System.out.println("控制时返回");
			isTimerOn = !((buff[para_start + 0] & 0x60) == 0);
			isOn = (buff[para_start + 1] & 0x01) == 1;
			model = buff[para_start + 1] >> 1 & 0x07;
			gear = buff[para_start + 1] >> 4 & 0x07;
			negative = (buff[para_start + 2] & 0x80) != 0;
			setHumidity = buff[para_start + 2];
			setTemp = buff[para_start + 3] & 0x7F;
			sleep = (buff[para_start + 3] & 0x80) != 0;
			// time1_on = buff[para_start + 3] < 0 ? buff[para_start + 3] +
			// 256
			// : buff[para_start + 3];
			// time1_off = buff[para_start + 9] < 0 ? buff[para_start + 9] +
			// 256
			// : buff[para_start + 9];
			// sleepTime_off = buff[para_start + 15] < 0 ? buff[para_start +
			// 15]
			// + 256
			// : buff[para_start + 15];
			// pm25 = buff[para_start + 16] < 0 ? buff[para_start + 16] +
			// 256
			// : buff[para_start + 16];

		} else if (buff[5] == 0x07) {
			isAlarmCode = true;
			int alarm_count = buff.length - 12;
			for (int i = 0; i < alarm_count; i++) {
				switch (buff[i + 8]) {
				case (byte) 0xE1:
					alarmText.append("室内温度传感器故障\n");
					break;
				case (byte) 0xE2:
					alarmText.append("室外温度传感器故障\n");
					break;
				case (byte) 0xE3:
					alarmText.append("新风机故障\n");
					break;
				case (byte) 0xE4:
					alarmText.append("排风机故障\n");
					break;
				case (byte) 0xE6:
					alarmText.append("室内湿度传感器故障\n");
					break;
				case (byte) 0xE7:
					alarmText.append("室外湿度传感器故障\n");
					break;
				case (byte) 0xE8:
					alarmText.append("水位开关故障\n");
					break;
				case (byte) 0xEA:
					alarmText.append("线控器通信故障\n");
					break;
				case (byte) 0xEC:
					alarmText.append("补水故障\n");
					break;
				case (byte) 0xEE:
					alarmText.append("排水故障\n");
					break;
				case (byte) 0x77:
					alarmText.append("需要清洁\n");
					break;
				default:
					break;
				}
			}
		} else if (buff[5] == 0x0b) {// 传感器信息
			aqi = (buff[9] < 0 ? buff[9] + 256 : buff[9])
					+ (buff[10] & 0x07 * 256);
			voc = (buff[11] < 0 ? buff[11] + 256 : buff[11])
					+ (buff[10] >> 3 & 0x07 * 256);
			quality = buff[10] >> 6 & 0x03;
			dust_out = (buff[12] < 0 ? buff[12] + 256 : buff[12])
					+ (buff[13] & 0x07 * 256);
			dust_in = (buff[14] < 0 ? buff[14] + 256 : buff[14])
					+ (buff[13] >> 3 & 0x07 * 256);
			co2 = (buff[15] < 0 ? buff[15] + 256 : buff[15]) * 10;
			temp_in = buff[16] - 20;
			temp_out = buff[17] - 20;
			humidity_in = buff[18] * 10;
			humidity_in = buff[19] * 10;
		} else if (buff[5] == 0x0c) {// 累计运行时间
			isWorkTimeBck = true;
			work_time = (buff[8] >> 6 & 0x03) * 65536
					+ (buff[9] < 0 ? buff[9] + 256 : buff[9]) * 256
					+ (buff[10] < 0 ? buff[10] + 256 : buff[10]);
//			work_time *= 60;
//			work_time += buff[8] & 0x3F;
		}

	}

	public boolean isTimerOn() {
		return isTimerOn;
	}

	public void setTimerOn(boolean isTimerOn) {
		this.isTimerOn = isTimerOn;
	}

	public boolean isWorkTimeBck() {
		return isWorkTimeBck;
	}

	public void setWorkTimeBck(boolean isWorkTimeBck) {
		this.isWorkTimeBck = isWorkTimeBck;
	}

	public boolean isControlBack() {
		return isControlBack;
	}

	public void setControlBack(boolean isControlBack) {
		this.isControlBack = isControlBack;
	}

	public boolean isStatusQuery() {
		return isStatusQuery;
	}

	public void setStatusQuery(boolean isStatusQuery) {
		this.isStatusQuery = isStatusQuery;
	}

	public boolean isAlarmCode() {
		return isAlarmCode;
	}

	public void setAlarmCode(boolean isAlarmCode) {
		this.isAlarmCode = isAlarmCode;
	}

	public boolean isSwitch() {
		return isSwitch;
	}

	public void setSwitch(boolean isSwitch) {
		this.isSwitch = isSwitch;
	}

	public int getDust_in() {
		return dust_in;
	}

	public void setDust_in(int dust_in) {
		this.dust_in = dust_in;
	}

	public int getDust_out() {
		return dust_out;
	}

	public void setDust_out(int dust_out) {
		this.dust_out = dust_out;
	}

	public int getAqi() {
		return aqi;
	}

	public void setAqi(int aqi) {
		this.aqi = aqi;
	}

	public int getVoc() {
		return voc;
	}

	public void setVoc(int voc) {
		this.voc = voc;
	}

	public int getWork_time() {
		return work_time;
	}

	public void setWork_time(int work_time) {
		this.work_time = work_time;
	}

	public int getIndex() {
		return index;
	}

	public void setIndex(int index) {
		this.index = index;
	}

	public int getTest() {
		return test;
	}

	public void setTest(int test) {
		this.test = test;
	}

	public boolean isSleep() {
		return sleep;
	}

	public void setSleep(boolean sleep) {
		this.sleep = sleep;
	}

	public String getAlarmText() {
		return alarmText.toString();
	}

	public void setAlarmText(StringBuilder alarmText) {
		this.alarmText = alarmText;
	}

	public int getSetTemp() {
		return setTemp;
	}

	public void setSetTemp(int setTemp) {
		this.setTemp = setTemp;
	}

	public int getSetHumidity() {
		return setHumidity;
	}

	public void setSetHumidity(int setHumidity) {
		this.setHumidity = setHumidity;
	}

	public int getTemp_in() {
		return temp_in;
	}

	public void setTemp_in(int temp_in) {
		this.temp_in = temp_in;
	}

	public int getTemp_out() {
		return temp_out;
	}

	public void setTemp_out(int temp_out) {
		this.temp_out = temp_out;
	}

	public int getHumidity_in() {
		return humidity_in;
	}

	public void setHumidity_in(int humidity_in) {
		this.humidity_in = humidity_in;
	}

	public int getHumidity_out() {
		return humidity_out;
	}

	public void setHumidity_out(int humidity_out) {
		this.humidity_out = humidity_out;
	}

	public int getPm25() {
		return pm25;
	}

	public void setPm25(int pm25) {
		this.pm25 = pm25;
	}

	public int getSleepTime_off() {
		return sleepTime_off;
	}

	public void setSleepTime_off(int sleepTime_off) {
		this.sleepTime_off = sleepTime_off;
	}

	public boolean isNeedClean() {
		return needClean;
	}

	public void setNeedClean(boolean needClean) {
		this.needClean = needClean;
	}

	public boolean isAlarm() {
		return isAlarm;
	}

	public void setAlarm(boolean isAlarm) {
		this.isAlarm = isAlarm;
	}

	public int getDustIn() {
		return dust_in;
	}

	public void setDustIn(int dust) {
		this.dust_in = dust;
	}

	public int getDustOut() {
		return dust_in;
	}

	public void setDustOut(int dust) {
		this.dust_out = dust;
	}

	public int getTime1_on() {
		return time1_on;
	}

	public void setTime1_on(int time_on) {
		this.time1_on = time_on;
	}

	public int getTime1_off() {
		return time1_off;
	}

	public void setTime1_off(int time_off) {
		this.time1_off = time_off;
	}

	public boolean isOn() {
		return isOn;
	}

	public void setOn(boolean isOn) {
		this.isOn = isOn;
	}

	public int getModel() {
		return model;
	}

	public void setModel(int model) {
		this.model = model;
	}

	public int getGear() {
		return gear;
	}

	public void setGear(int gear) {
		this.gear = gear;
	}

	public boolean isNegative() {
		return negative;
	}

	public void setNegative(boolean negative) {
		this.negative = negative;
	}

	public int getQuality() {
		return quality;
	}

	public void setQuality(int quality) {
		this.quality = quality;
	}

	public int getCo2() {
		return co2;
	}

	public void setCo2(int co2) {
		this.co2 = co2;
	}

	public boolean isTiming1() {
		return isTiming1;
	}

	public void setTiming1(boolean isTiming1) {
		this.isTiming1 = isTiming1;
	}

	public int getHour1_on() {
		return hour1_on;
	}

	public void setHour1_on(int hour1_on) {
		this.hour1_on = hour1_on;
	}

	public int getMin1_on() {
		return min1_on;
	}

	public void setMin1_on(int min1_on) {
		this.min1_on = min1_on;
	}

	public int getHour1_off() {
		return hour1_off;
	}

	public void setHour1_off(int hour1_off) {
		this.hour1_off = hour1_off;
	}

	public int getMin1_off() {
		return min1_off;
	}

	public void setMin1_off(int min1_off) {
		this.min1_off = min1_off;
	}

	public boolean isTiming2() {
		return isTiming2;
	}

	public void setTiming2(boolean isTiming2) {
		this.isTiming2 = isTiming2;
	}

	public int getHour2_on() {
		return hour2_on;
	}

	public void setHour2_on(int hour2_on) {
		this.hour2_on = hour2_on;
	}

	public int getMin2_on() {
		return min2_on;
	}

	public void setMin2_on(int min2_on) {
		this.min2_on = min2_on;
	}

	public int getHour2_off() {
		return hour2_off;
	}

	public void setHour2_off(int hour2_off) {
		this.hour2_off = hour2_off;
	}

	public int getMin2_off() {
		return min2_off;
	}

	public void setMin2_off(int min2_off) {
		this.min2_off = min2_off;
	}

	public boolean isTiming3() {
		return isTiming3;
	}

	public void setTiming3(boolean isTiming3) {
		this.isTiming3 = isTiming3;
	}

	public int getHour3_on() {
		return hour3_on;
	}

	public void setHour3_on(int hour3_on) {
		this.hour3_on = hour3_on;
	}

	public int getMin3_on() {
		return min3_on;
	}

	public void setMin3_on(int min3_on) {
		this.min3_on = min3_on;
	}

	public int getHour3_off() {
		return hour3_off;
	}

	public void setHour3_off(int hour3_off) {
		this.hour3_off = hour3_off;
	}

	public int getMin3_off() {
		return min3_off;
	}

	public void setMin3_off(int min3_off) {
		this.min3_off = min3_off;
	}

}
