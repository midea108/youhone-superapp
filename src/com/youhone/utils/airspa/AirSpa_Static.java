package com.youhone.utils.airspa;

public class AirSpa_Static {
	/**
	 * 机身码 5位
	 */
	public static final byte DEVICE_CODE = (byte) 0x01;
	/**
	 * 无定时
	 */
	public static final byte NO_TIMER = (byte) 0x00;
	/**
	 * 定时开
	 */
	public static final byte TIMER_ON = (byte) 0x20;
	/**
	 * 定时关
	 */
	public static final byte TIMER_OFF = (byte) 0x40;
	/**
	 * 双定
	 */
	public static final byte DOUBLE_TIMER = (byte) 0x60;
	/**
	 * 无清洁
	 */
	public static final byte NO_CLEAN = (byte) 0x00;
	/**
	 * 清楚清洁提示
	 */
	public static final byte CLEAN_TIPS = (byte) 0x80;
	/**
	 * 开机
	 */
	public static final byte DEVICE_ON = (byte) 0x01;
	/**
	 * 关机机
	 */
	public static final byte DEVICE_OFF = (byte) 0x00;
	/**
	 * 智能模式
	 */
	public static final byte MODEL_SMART = (byte) 0x00;
	/**
	 * 新风模式
	 */
	public static final byte MODEL_XINFENG = (byte) 0x01;
	/**
	 * 内循环模式
	 */
	public static final byte MODEL_INNER = (byte) 0x02;
	/**
	 * 恒温模式
	 */
	public static final byte MODEL_TEMP = (byte) 0x03;
	/**
	 * 恒温恒湿模式
	 */
	public static final byte MODEL_TH = (byte) 0x04;
	/**
	 * 负离子开
	 */
	public static final byte NEGATIVE_ON = (byte) 0x80;
	/**
	 * 负离子关
	 */
	public static final byte NEGATIVE_OFF = (byte) 0x00;
	/**
	 * 风速自动挡
	 */
	public static final byte GEAR_AUTO = (byte) 0x00;
	/**
	 * 风速1挡
	 */
	public static final byte GEAR_ONE = (byte) 0x01;
	/**
	 * 风速2挡
	 */
	public static final byte GEAR_TWO = (byte) 0x02;
	/**
	 * 风速3挡
	 */
	public static final byte GEAR_THREE = (byte) 0x03;
	/**
	 * 风速4挡
	 */
	public static final byte GEAR_FOUR = (byte) 0x04;
	/**
	 * 风速5挡
	 */
	public static final byte GEAR_FIVE = (byte) 0x05;
	/**
	 * 风速6挡
	 */
	public static final byte GEAR_SIX = (byte) 0x06;
	/**
	 * 风速睡眠挡
	 */
	public static final byte GEAR_SLEEP = (byte) 0x07;
}
