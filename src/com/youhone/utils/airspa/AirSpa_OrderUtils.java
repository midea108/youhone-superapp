package com.youhone.utils.airspa;

public class AirSpa_OrderUtils {

	private static byte[] data_on = new byte[] { (byte) 0xF3, (byte) 0xF5,
			0x05, 0x35, 0x00, 0x03, 0x01, 0x00, 0x3E, (byte) 0xF3, (byte) 0xFB };

	private static byte[] data_off = new byte[] { (byte) 0xF3, (byte) 0xF5,
			0x05, 0x35, 0x00, 0x03, 0x00, 0x00, 0x3d, (byte) 0xF3, (byte) 0xFB };

	private static byte[] data_status = new byte[] { (byte) 0xF3, (byte) 0xF5,
			0x05, 0x35, 0x00, 0x66, 0x01, 0x00, (byte) 0xA1, (byte) 0xF3,
			(byte) 0xFB };

	private static byte[] data = new byte[] { (byte) 0xF3, (byte) 0xF5,
			(byte) 0x0e, 0x35, 0x00, (byte) 0x65, 0x00, 0x00, 0x00, 0x00, 0x00,
			0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, (byte) 0xf3, (byte) 0xfb };

	private static byte[] data_query = new byte[] { (byte) 0xF3, (byte) 0xF5,
			0x05, 0x35, 0x00, 0x66, 0x01, 0x00, (byte) 0xa1, (byte) 0xF3,
			(byte) 0xFB };

	private static byte[] data_alarm = new byte[] { (byte) 0xF3, (byte) 0xF5,
			0x05, 0x35, 0x00, 0x07, 0x00, 0x00, (byte) 0x41, (byte) 0xF3,
			(byte) 0xFB };

	private static byte[] data_timer = null;

	private static byte[] timer_query = new byte[] { (byte) 0xF3, (byte) 0xF5,
			0x05, 0x35, 0x00, 0x0A, 0x00, 0x00, 0x44, (byte) 0xF3, (byte) 0xFB };

	private static byte[] sensor_query = new byte[] { (byte) 0xF3, (byte) 0xF5,
			0x05, 0x35, 0x00, 0x0B, 0x00, 0x00, 0x45, (byte) 0xF3, (byte) 0xFB };

	private static byte[] work_time_query = new byte[] { (byte) 0xF3,
			(byte) 0xF5, 0x05, 0x35, 0x00, 0x0c, 0x00, 0x01, 0x47, (byte) 0xF3,
			(byte) 0xFB };

	public static byte[] GET_QUERY() {
		System.out.println("查询命令");
		return data_query;
	}

	public static byte[] GET_ALQRM() {
		return data_alarm;
	}

	public static byte[] QUERY_TIMER() {
		return timer_query;
	}

	public static byte[] QUERY_SENSOR() {
		return sensor_query;
	}

	public static byte[] QUERT_WORKTIME() {
		return work_time_query;
	}

	public static byte[] GET_ON() {
		setOnOff((byte) 0x01);
		System.out.println("开机");
		for (byte b : data_on) {
			System.out.format("%02X ", b);
		}
		System.out.println("");
		return data_on;
	}

	public static byte[] GET_OFF() {
		setOnOff((byte) 0x00);
		System.out.println("关机");
		for (byte b : data_off) {
			System.out.format("%02X ", b);
		}
		System.out.println("");
		return data_off;
	}

	/**
	 * 设置定时器数量
	 * 
	 * @param count
	 */
	public static void setTimerCount(int count) {
		data_timer = new byte[12 + count * 2];
		int length = data_timer.length;
		data_timer[0] = (byte) 0xF3;
		data_timer[1] = (byte) 0xF5;
		data_timer[2] = (byte) (6 + count * 2);
		data_timer[3] = (byte) 0x35;
		data_timer[4] = (byte) 0x00;
		data_timer[5] = (byte) 0x0A;
		data_timer[6] = (byte) 0x00;// 设置定时
		data_timer[7] = (byte) 0x00;
		data_timer[8] = (byte) 0x00;// 机身码
		data_timer[length - 2] = (byte) 0xF3;
		data_timer[length - 1] = (byte) 0xFB;
	}

	/**
	 * 设置定时开机时间
	 * 
	 * @param min
	 */
	public static void setTimer1On(int min) {
		data_timer[9] = (byte) (min);
	}

	/**
	 * 设置是否定时开机
	 * 
	 * @param onoff
	 */
	public static void setTimer1Onoff(byte onoff) {
		data_timer[10] |= onoff;
	}

	/**
	 * 设置定时开机的模式
	 * 
	 * @param model
	 */
	public static void setTimer1Model(byte model) {
		data_timer[10] |= model * 2;
	}

	/**
	 * 设置定时开机的风速
	 * 
	 * @param gear
	 */
	public static void setTimer1Gear(byte gear) {
		data_timer[10] |= gear * 16;
	}

	/**
	 * 设置定时开机的负离子开关
	 * 
	 * @param negative
	 */
	public static void setTimer1Negative(byte negative) {
		data_timer[10] |= negative * 128;
	}

	/**
	 * 设置定时关机时间
	 * 
	 * @param min
	 */
	public static void setTimer2Off(int min) {
		data_timer[11] = (byte) (min);
	}

	/**
	 * 设置是否定时关机
	 * 
	 * @param onoff
	 */
	public static void setTimer2Onoff(byte onoff) {
		data_timer[12] |= onoff;
	}

	/**
	 * 设置定时关机的模式
	 * 
	 * @param model
	 */
	public static void setTimer2Model(byte model) {
		data_timer[12] |= model * 2;
	}

	/**
	 * 设置定时关机的风速
	 * 
	 * @param gear
	 */
	public static void setTimer2Gear(byte gear) {
		data_timer[12] |= gear * 16;
	}

	/**
	 * 设置定时关机的负离子开关
	 * 
	 * @param negative
	 */
	public static void setTimer2Negative(byte negative) {
		data_timer[12] |= negative * 128;
	}

	/**
	 * 设置多个定时器
	 * 
	 * @param time
	 */
	public static void setTimer(byte[] time) {
		if (time.length == data_timer.length - 12) {
			for (int i = 0; i < time.length; i++) {
				data_timer[i + 9] = time[i];
			}
		}
	}

	public static byte[] getTimerByte() {
		int F3 = 0;
		int length = data_timer.length;
		// TODO 算校验
		data_timer[length - 3] = Sum(data_timer, 2);
		// 统计数据包中的F3数目
		for (int i = 2; i < length - 2; i++) {
			if (data_timer[i] == 0xF3)
				F3++;
		}
		System.out.println("发送的数据");
		for (byte b : data_timer) {
			System.out.format("%02X ", b);
		}
		System.out.println("");
		if (F3 == 0) {
			return data_timer;
		} else {
			// 如果有F3则生成新的数据包
			// 新F3不参与校验
			System.out.println("数据中有" + F3 + "个F3");
			byte[] result = new byte[length + F3];
			int new_length = length + F3;
			result[0] = data_timer[0];
			result[1] = data_timer[1];
			result[new_length - 2] = data_timer[length - 2];
			result[new_length - 1] = data_timer[length - 1];
			for (int i = 2; i < new_length - 2; i++) {
				if (result[i] == 0xF3) {
					result[i + 1] = (byte) 0xF3;
					i++;
				}
			}
			System.out.println("增加F3后发送");
			for (byte b : result) {
				System.out.format("%02X ", b);
			}
			System.out.println("");
			return result;
		}
	}

	public static void setCode(byte code) {
		data[10] |= code;
	}

	public static void setTimer(byte timer) {
		data[10] &= (0x9F);
		data[10] |= (timer * 32);
	}

	public static void setClean(byte clean) {
		data[10] &= (0x7F);
		data[10] &= (128 * clean);
	}

	public static void setOnOff(byte onoff) {
		data[11] &= 0xFE;
		data[11] |= onoff;
		System.out.println("开关" + data[11]);
	}

	public static void setModel(byte model) {
		data[11] &= 0xF1;
		data[11] |= (model * 2);
	}

	public static void setGear(byte gear) {
		data[11] &= 0x8F;
		data[11] |= (gear * 16);
	}

	public static void setNegative(byte negative) {
		data[12] &= (0x7F);
		data[12] |= (128 * negative);
		System.out.println("fulizi=" + data[12]);
	}

	public static void setTemp(byte temp) {
		data[13] |= temp;
	}

	public static void setHumidity(byte humidity) {
		data[12] |= humidity;
	}

	public static void setSleep(byte sleep) {
		data[13] &= 0x7F;
		data[13] |= (128 * sleep);
	}

	/**
	 * 下面开始全部没有了
	 * 
	 * @param sleepoff
	 */

	public static void setSleepOff(byte sleepoff) {
		data[25] &= 0;
		data[25] |= sleepoff;
	}

	public static void setTimeOff(byte timeOff) {
		data[13] &= 0;
		data[13] |= timeOff;
	}

	public static void setTimeOn(byte timeOn) {
		data[19] &= 0;
		data[19] |= timeOn;
	}

	public static byte[] getResult() {
		int length = data.length;
		// TODO 算校验
		data[length - 3] = Sum(data, 2);
		return deleteF3(data);
	}

	public static byte Sum(byte[] buff, int offset) {
		byte xor = 0;
		for (int i = offset; i < (buff.length - 3); i++) {
			xor += buff[i];
		}
		return xor;
	}

	public static byte[] getStatus() {
		return data_status;
	}

	public static AirSpa_Status receive(byte[] buff) {
		if (buff.length < 6)
			return null;
		return new AirSpa_Status(addF3(buff));
	}

	private static byte[] addF3(byte[] buff) {
		System.out.println("收到的数据");
		for (byte b : buff) {
			System.out.format("%02X ", b);
		}
		System.out.println("");
		byte[] result = null;
		int F3 = 0;
		int length = buff.length;
		for (int i = 2; i < length - 2; i++) {
			if (buff[i] == 0xF3)
				F3++;
		}
		System.out.println("收到的数据有" + F3 + "个F3");
		if (F3 != 0) {
			result = new byte[length - F3 / 2];
			int result_length = result.length;
			result[0] = buff[0];
			result[1] = buff[1];
			result[result_length - 1] = buff[length - 1];
			result[result_length - 2] = buff[length - 2];
			for (int i = 2, j = 2; i < result.length - 2; i++, j++) {
				if (buff[j] == 0xF3) {
					result[i] = buff[j];
					j++;
				} else {
					result[i] = buff[j];
				}
			}
		} else {
			result = buff;
		}
		System.out.println("去掉F3的数据");
		for (byte b : result) {
			System.out.format("%02X ", b);
		}
		System.out.println("");
		return result;
	}

	private static byte[] deleteF3(byte[] data) {
		int F3 = 0;
		int length = data.length;
		// 统计数据包中的F3数目
		for (int i = 2; i < length - 2; i++) {
			if (data[i] == 0xF3)
				F3++;
		}
		System.out.println("发送的数据");
		for (byte b : data) {
			System.out.format("%02X ", b);
		}
		System.out.println("");
		if (F3 == 0) {
			return data;
		} else {
			// 如果有F3则生成新的数据包
			// 新F3不参与校验
			System.out.println("数据中有" + F3 + "个F3");
			byte[] result = new byte[length + F3];
			int new_length = length + F3;
			result[0] = data[0];
			result[1] = data[1];
			result[new_length - 2] = data[length - 2];
			result[new_length - 1] = data[length - 1];
			for (int i = 2; i < new_length - 2; i++) {
				if (result[i] == 0xF3) {
					result[i + 1] = (byte) 0xF3;
					i++;
				}
			}
			System.out.println("增加F3后发送");
			for (byte b : result) {
				System.out.format("%02X ", b);
			}
			System.out.println("");
			return result;
		}
	}

}
