package com.youhone.utils.tianjun;


public class FanLight_OrderUtils {

	public static byte[] send(int type, int order) {
		byte[] buff = { (byte) 0x80, 0x7F, 0x00, 0x00, 0x00, 0x00, 0x23,
				(byte) 0xDC };

		buff[2] = (byte) (buff.length - 4);
		buff[3] = getCmd(type);
		buff[4] = (byte) order;
		buff[5] = calXor(new byte[] { buff[2], buff[3], buff[4] });

		return buff;
	}

	public static FanLight_Status receive(byte[] buff) {
		if ((buff.length - 4) != buff[2] || buff[3] != 0x40)
			return null;

		byte[] toCheck = new byte[buff.length - 5];
		for (int i = 0; i < toCheck.length; i++) {
			toCheck[i] = buff[2 + i];
		}
	
		if (buff[buff.length - 3] != calXor(toCheck)){
			
			return null;
		}
				
			

		FanLight_Status status = new FanLight_Status();
		status.setLight(buff[4]);
		status.setFan(buff[5]);

		if((buff[6]|buff[7]|buff[8])!=0){
			status.setTiming(true);
			status.setHour(buff[6]);
			status.setMin(buff[7]);
			status.setSec(buff[8]);
		}
		
		return status;
	}

	/**
	 * 根据类型获取cmd
	 * 
	 * @param type
	 * @return
	 */
	private static byte getCmd(int type) {
		switch (type) {
		case FanLight_Static.TYPE_QUERY:
			return (byte)0xC0;
		case FanLight_Static.TYPE_LIGHT:
			return (byte) 0xC1;
		case FanLight_Static.TYPE_FAN:
			return (byte) 0xC2;
		case FanLight_Static.TYPE_BOTH:
			return (byte) 0xC3;
		case FanLight_Static.TYPE_TIMER:
			return (byte) 0xC4;
		}
		return 0;
	}

	private static byte calXor(byte[] buff) {
		byte i = 0, xor = 0;
		for (i = 0; i < buff.length; i++) {
			xor ^= buff[i];
		}
		return xor;
	}
}
