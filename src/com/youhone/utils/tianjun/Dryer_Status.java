package com.youhone.utils.tianjun;

public class Dryer_Status {
	public static int para_start = 4;

	private boolean onoff = false;
	private int func;
	private boolean isSter = false;
	private boolean isTiming = false;
	private int hour = 0;
	private int min = 0;
	private int sec = 0;
	
	public Dryer_Status(byte[] buff) {
		onoff = buff[para_start + 0] == 1;
		func = buff[para_start + 1];
		isSter = buff[para_start + 2] == 1;
		hour = buff[para_start + 3];
		min = buff[para_start + 4];
		sec = buff[para_start + 5];
		isTiming = ((hour | min | sec) != 0);
	}

	public boolean isOnoff() {
		return onoff;
	}

	public int getFunc() {
		return func;
	}

	public boolean isSter() {
		return isSter;
	}

	public boolean isTiming() {
		return isTiming;
	}

	public int getHour() {
		return hour;
	}

	public int getMin() {
		return min;
	}

	public int getSec() {
		return sec;
	}

}
