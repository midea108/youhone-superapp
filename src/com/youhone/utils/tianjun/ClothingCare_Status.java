package com.youhone.utils.tianjun;

public class ClothingCare_Status {
	public static int para_start = 4;

	private int func;
	private int fPara;
	private int fStep;
	private boolean isTiming = false;
	private boolean isError = false;
	private int hour = 0;
	private int min = 0;
	private int sec = 0;
	private int errorNum = 0;

	public ClothingCare_Status(byte[] buff) {
		func = buff[para_start + 0];
		fPara = buff[para_start + 1];
		fStep = buff[para_start + 2];
		hour = buff[para_start + 3];
		min = buff[para_start + 4];
		sec = buff[para_start + 5];
		errorNum = buff[para_start + 6];
		isTiming = ((hour | min | sec) != 0);
		isError = (errorNum != 0);
	}

	public int getFunc() {
		if(func < ClothingCare_Static.SUIT || func> ClothingCare_Static.STERILIZE_TOY){
			func = ClothingCare_Static.OTHERS;
		}
		return func;
	}

	public int getfPara() {
		return fPara;
	}

	public int getfStep() {
		return fStep;
	}

	public boolean isTiming() {
		return isTiming;
	}
	
	public boolean isError() {
		return isError;
	}

	public int getHour() {
		return hour;
	}

	public int getMin() {
		return min;
	}

	public int getSec() {
		return sec;
	}
	
	public String getError() {
		StringBuffer errorString = new StringBuffer();
		
		if((errorNum & 0x01) == 1){
			errorString.append("机器门未关闭");
		}
		
		if(((errorNum >> 1) & 0x01) == 1){
			if(errorString.length()!=0)
				errorString.append("\n");
			errorString.append("水箱缺水");
		}
		
		if(((errorNum >> 2) & 0x01) == 1){
			if(errorString.length()!=0)
				errorString.append("\n");
			errorString.append("水箱水满");
		}
		
		if(((errorNum >> 3) & 0x01) == 1){
			if(errorString.length()!=0)
				errorString.append("\n");
			errorString.append("无水箱");
		}
		
		return errorString.toString();
	}
}
