package com.youhone.utils.tianjun;

public class FanLight_Static {
	public static final int TYPE_QUERY = 0X00;
	public static final int TYPE_LIGHT = 0x01;
	public static final int TYPE_FAN = 0x01 << 1;
	public static final int TYPE_BOTH = 0x01 << 2;
	public static final int TYPE_TIMER = 0x01 << 3;

	public static final int CTRL_CLOSE = 0;
	public static final int CTRL_OPEN = 1;
	public static final int CTRL_LEVEL1 = 1;
	public static final int CTRL_LEVEL2 = 2;
	public static final int CTRL_LEVEL3 = 3;

	public static final int TIMING_0 = 0;
	public static final int TIMING_1H = 1;
	public static final int TIMING_2H = 2;
	public static final int TIMING_3H = 3;
	public static final int TIMING_4H = 4;
	public static final int TIMING_8H = 8;
}
