package com.youhone.utils.tianjun;

public class ClothingCare_Static {

	/**
	 * 0x01(功能01)——西装大衣
	 */
	public static final int SUIT = 0x01;

	/**
	 * 0x02(功能02)——羊毛衫
	 */
	public static final int SWEATER = 0x02;

	/**
	 * 0x03(功能03)——运动服装
	 */
	public static final int SPORT = 0x03;

	/**
	 * 0x04(功能04)——衣帽配件
	 */
	public static final int HAT = 0x04;

	/**
	 * 0x05(功能05)——羊毛衫烘干
	 */
	public static final int DRY_SWEATER = 0x05;

	/**
	 * 0x06(功能06)——衬衫烘干
	 */
	public static final int DRY_SHIRT = 0x06;

	/**
	 * 0x07(功能07)——雨淋烘干
	 */
	public static final int DRY_RAIN = 0x07;

	/**
	 * 0x08(功能08)——定时烘干
	 */
	public static final int DRY_TIMING = 0x08;

	/**
	 * 0x09(功能09)——标准杀菌
	 */
	public static final int STERILIZE_NORMAL = 0x09;

	/**
	 * 0x0A(功能10)——寝具杀菌
	 */
	public static final int STERILIZE_BEDDING = 0x0A;

	/**
	 * 0x0B(功能11)——内衣杀菌
	 */
	public static final int STERILIZE_UNDERWEAR = 0x0B;

	/**
	 * 0x0C(功能12)——玩具杀菌
	 */
	public static final int STERILIZE_TOY = 0x0C;

	/**
	 * 0x0D(功能13)——（保留）
	 */

	/**
	 * 0x0E(功能14)——（保留）
	 */

	/**
	 * 0x0F(功能15)——（保留）
	 */

	/**
	 * 0x10(功能16)——其他衣物
	 */
	public static final int OTHERS = 0x10;

	/**
	 * 0x11(暂停)
	 */
	public static final int PAUSE = 0x11;

	/**
	 * 0x12(开始)
	 */
	public static final int START = 0x12;

	/**
	 * 0x13(停止)
	 */
	public static final int STOP = 0x13;

}
