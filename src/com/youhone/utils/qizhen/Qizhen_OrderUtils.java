package com.youhone.utils.qizhen;

import android.support.annotation.Nullable;

public class Qizhen_OrderUtils {

	public static byte[] getStatus() {
		return SendBase(0xC0);
	}

	public static byte[] sendControl(int func, int para) {
		return SendBase(0xC1, new byte[] { (byte) func, (byte) para });
	}

	public static byte[] setTimer(int time) {
		return SendBase(0xC2, (byte) time);
	}

	public static byte[] Timer = (byte[]) new byte[] { 0x00, 0x00, 0x00, 0x00,
			0x00, 0x00 };

	public static void setTimerOnoff(byte onoff) {
		Timer[0] = onoff;
	}

	public static void setTimerOn(int min_on) {
		System.out.println("定时开的时间" + min_on);
		if (min_on < 255) {
			Timer[1] = (byte) 0;
			Timer[2] = (byte) min_on;
		} else {
			Timer[1] = (byte) (min_on / 255);
			Timer[2] = (byte) (min_on % 255);
		}
	}

	public static void setTimerOff(int min_off) {
		System.out.println("定时开的时间" + min_off);
		if (min_off < 255) {
			Timer[3] = (byte) 0;
			Timer[4] = (byte) min_off;
		} else {
			Timer[3] = (byte) (min_off / 255);
			Timer[4] = (byte) (min_off % 255);
		}
	}

	public static void setTimerGear(int gear) {
		Timer[5] = (byte) gear;
	}

	public static byte[] getTimerbyte() {
		return SendBase(Qizhen_Static.TIEMR, Timer);
	}

	public static byte[] SendBase(int cmd, byte... para) {
		int length = para.length + 3;
		byte[] buff = new byte[length + 4];

		// 包头、包尾
		buff[0] = (byte) 0x80;
		buff[1] = 0x7F;
		buff[buff.length - 2] = 0x23;
		buff[buff.length - 1] = (byte) 0xDC;

		// 长度
		buff[2] = (byte) length;

		// 命令
		buff[3] = (byte) cmd;

		// 数据域
		for (int i = 0; i < para.length; i++) {
			buff[4 + i] = para[i];
		}

		// 校验和
		buff[buff.length - 3] = Xor(buff, 2, buff.length - 4 - 1);

		return buff;
	}

	/**
	 * 解析返回的数据
	 */
	@Nullable
	public static Qizhen_Status receive(byte[] buff) {
		if (buff.length < 4)
			return null;
		if ((buff[2] == buff.length - 4) && (buff[3] == 0x40)) {
			System.out.println("校验 == " + Xor(buff, 2, buff.length - 4));
			if (Xor(buff, 2, buff.length - 4) == 0) {
				return new Qizhen_Status(buff);
			}
		}
		return null;
	}

	private static byte Xor(byte[] buff, int offset, int len) {
		byte xor = 0;
		for (int i = offset; i < (offset + len); i++) {
			xor ^= buff[i];
		}
		return xor;
	}

}
