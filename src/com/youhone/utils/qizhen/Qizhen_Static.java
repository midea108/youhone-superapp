package com.youhone.utils.qizhen;

public class Qizhen_Static {
	/**
	 * 0xC1 总开关
	 */
	public static final byte DEVICE_SWITCH = (byte) 0xC1;
	/**
	 * 0XC2 童锁
	 */
	public static final byte LOCK = (byte) 0xC2;
	/**
	 * 0XC3 睡眠
	 */
	public static final byte SLEEP = (byte) 0xC3;
	/**
	 * 0XC4 负离子
	 */
	public static final byte NEGATIVE = (byte) 0xC4;
	/**
	 * 0xC5 杀菌
	 */
	public static final byte STERILIZATION = (byte) 0xC5;
	/**
	 * 0XC6 风叶摆动
	 */
	public static final byte FAN = (byte) 0xC6;
	/**
	 * 0XC7 灯带开关
	 */
	public static final byte LIGHT = (byte) 0xC7;
	/**
	 * 0xC8 风速
	 */
	public static final byte GEAR = (byte) 0xC8;
	/**
	 * 0xC9 定时
	 */
	public static final byte TIEMR = (byte) 0xC9;
	
}
