package com.youhone.utils.qizhen;

public class Qizhen_Status {
	public static int para_start = 4;
	private boolean fanOn = false;
	private boolean lightOn = false;
	private boolean sterilizeOn = false;
	private boolean On = false;
	private boolean NegativeOn = false;
	private boolean UltravioletOn = false;
	private boolean Auto = false;
	private boolean Sleep = false;
	private boolean TongsuoOn = false;
	private int gear = 0;
	private int time_on = 0;
	private int time_off = 0;
	private boolean lvxin = false;
	private int temp = 0;
	private int humidity = 0;
	private int pm25 = 0;
	private int workTime = 0;
	private int lvxin_remaining_time = 0;
	private int errorCode = 0;

	public Qizhen_Status(byte[] data) {
		On = (data[para_start + 0] == 1);
		System.out.println("开关=" + On);
		TongsuoOn = (data[para_start + 1] == 1);
		System.out.println("童锁=" + TongsuoOn);
		Sleep = (data[para_start + 2] == 1);
		System.out.println("睡眠=" + Sleep);
		NegativeOn = (data[para_start + 3] == 1);
		System.out.println("负离子=" + NegativeOn);
		sterilizeOn = (data[para_start + 4] == 1);
		System.out.println("杀菌=" + sterilizeOn);
		fanOn = (data[para_start + 5] == 1);
		System.out.println("风叶=" + fanOn);
		lightOn = (data[para_start + 6] == 1);
		System.out.println("灯带=" + lightOn);
		gear = data[para_start + 7];
		System.out.println("档位=" + gear);
		Auto = (data[para_start + 7] == 0);
		System.out.println("自动=" + Auto);
		temp = data[para_start + 8];
		System.out.println("温度=" + temp);
		humidity = data[para_start + 9];
		System.out.println("湿度=" + humidity);

		int pm25_h = data[para_start + 10];
		int pm25_l = data[para_start + 11];
		pm25_h = pm25_h < 0 ? pm25_h + 256 : pm25_h;
		pm25_l = pm25_l < 0 ? pm25_l + 256 : pm25_l;
		pm25 = pm25_h * 256 + pm25_l;
		System.out.println("pm25=" + pm25);

		int workTime_h = data[para_start + 12];
		int workTime_l = data[para_start + 13];
		workTime_h = workTime_h < 0 ? workTime_h + 256 : workTime_h;
		workTime_l = workTime_l < 0 ? workTime_l + 256 : workTime_l;
		workTime = workTime_h * 256 + workTime_l;
		System.out.println("工作时间=" + workTime);

		int lvxinTime_h = data[para_start + 14];
		int lvxinTime_l = data[para_start + 15];
		lvxinTime_h = lvxinTime_h < 0 ? lvxinTime_h + 256 : lvxinTime_h;
		lvxinTime_l = lvxinTime_l < 0 ? lvxinTime_l + 256 : lvxinTime_l;
		lvxin_remaining_time = lvxinTime_h * 256 + lvxinTime_l;
		System.out.println("滤芯剩余工作时间=" + lvxin_remaining_time);

		int timeOn_h = data[para_start + 16];
		int timeOn_l = data[para_start + 17];
		timeOn_h = timeOn_h < 0 ? timeOn_h + 256 : timeOn_h;
		timeOn_l = timeOn_l < 0 ? timeOn_l + 256 : timeOn_l;
		time_on = timeOn_h * 256 + timeOn_l;

		int timeoff_h = data[para_start + 18];
		int timeoff_l = data[para_start + 19];
		timeoff_h = timeoff_h < 0 ? timeoff_h + 256 : timeoff_h;
		timeoff_l = timeoff_l < 0 ? timeoff_l + 256 : timeoff_l;
		time_off = timeoff_h * 256 + timeoff_l;

		errorCode = data[para_start + 20];
		System.out.println("错误代码=" + errorCode);
	}

	public boolean isFanOn() {
		return fanOn;
	}

	public void setFanOn(boolean fanOn) {
		this.fanOn = fanOn;
	}

	public boolean isLightOn() {
		return lightOn;
	}

	public void setLightOn(boolean lightOn) {
		this.lightOn = lightOn;
	}

	public int getTime_off() {
		return time_off;
	}

	public void setTime_off(int time_off) {
		this.time_off = time_off;
	}

	public int getWorkTime() {
		return workTime;
	}

	public void setWorkTime(int workTime) {
		this.workTime = workTime;
	}

	public int getErrorCode() {
		return errorCode;
	}

	public void setErrorCode(int errorCode) {
		this.errorCode = errorCode;
	}

	public boolean isSterilizeOn() {
		return sterilizeOn;
	}

	public void setSterilizeOn(boolean sterilizeOn) {
		this.sterilizeOn = sterilizeOn;
	}

	public boolean isOn() {
		return On;
	}

	public void setOn(boolean on) {
		On = on;
	}

	public boolean isNegativeOn() {
		return NegativeOn;
	}

	public void setNegativeOn(boolean negativeOn) {
		NegativeOn = negativeOn;
	}

	public boolean isUltravioletOn() {
		return UltravioletOn;
	}

	public void setUltravioletOn(boolean ultravioletOn) {
		UltravioletOn = ultravioletOn;
	}

	public boolean isAuto() {
		return Auto;
	}

	public void setAuto(boolean auto) {
		Auto = auto;
	}

	public boolean isSleep() {
		return Sleep;
	}

	public void setSleep(boolean sleep) {
		Sleep = sleep;
	}

	public boolean isTongsuoOn() {
		return TongsuoOn;
	}

	public void setTongsuoOn(boolean tongsuoOn) {
		TongsuoOn = tongsuoOn;
	}

	public int getGear() {
		return gear;
	}

	public void setGear(int gear) {
		this.gear = gear;
	}

	public int getTime_on() {
		return time_on;
	}

	public void setTime_on(int time_on) {
		this.time_on = time_on;
	}

	public boolean isLvxin() {
		return lvxin;
	}

	public void setLvxin(boolean lvxin) {
		this.lvxin = lvxin;
	}

	public int getTemp() {
		return temp;
	}

	public void setTemp(int temp) {
		this.temp = temp;
	}

	public int getHumidity() {
		return humidity;
	}

	public void setHumidity(int humidity) {
		this.humidity = humidity;
	}

	public int getPm25() {
		return pm25;
	}

	public void setPm25(int pm25) {
		this.pm25 = pm25;
	}

	public int getLvxin_remaining_time() {
		return lvxin_remaining_time;
	}

	public void setLvxin_remaining_time(int lvxin_remaining_time) {
		this.lvxin_remaining_time = lvxin_remaining_time;
	}

}
