package com.youhone.utils.light;

public class Light_Status {
	private boolean isLight1On = false;
	private boolean isLight2On = false;
	private boolean isLight3On = false;

	public static int para_start = 4;

	public Light_Status(byte[] data) {
		isLight1On = (data[para_start + 0] == 1);
		System.out.println("灯1=" + isLight1On);
		isLight2On = (data[para_start + 1] == 1);
		System.out.println("灯2=" + isLight2On);
		isLight3On = (data[para_start + 2] == 1);
		System.out.println("灯3=" + isLight3On);
	}

	public boolean isLight1On() {
		return isLight1On;
	}

	public void setLight1On(boolean isLight1On) {
		this.isLight1On = isLight1On;
	}

	public boolean isLight2On() {
		return isLight2On;
	}

	public void setLight2On(boolean isLight2On) {
		this.isLight2On = isLight2On;
	}

	public boolean isLight3On() {
		return isLight3On;
	}

	public void setLight3On(boolean isLight3On) {
		this.isLight3On = isLight3On;
	}

}
