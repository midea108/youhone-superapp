package com.youhone.utils.light;

import android.support.annotation.Nullable;

public class Light_OrderUtils {

	public static byte[] getStatus() {
		return SendBase(0xC0);
	}

	public static byte[] sendControl(int func, int para) {
		return SendBase((byte) func, (byte) para);
	}

	public static byte[] SendBase(int cmd, byte... para) {
		int length = para.length + 3;
		byte[] buff = new byte[length + 4];

		// 包头、包尾
		buff[0] = (byte) 0x80;
		buff[1] = 0x7F;
		buff[buff.length - 2] = 0x23;
		buff[buff.length - 1] = (byte) 0xDC;

		// 长度
		buff[2] = (byte) length;

		// 命令
		buff[3] = (byte) cmd;

		// 数据域
		for (int i = 0; i < para.length; i++) {
			buff[4 + i] = para[i];
		}

		// 校验和
		buff[buff.length - 3] = Xor(buff, 2, buff.length - 4 - 1);

		return buff;
	}

	/**
	 * 解析返回的数据
	 */
	@Nullable
	public static Light_Status receive(byte[] buff) {
		if (buff.length < 4)
			return null;
		if ((buff[2] == buff.length - 4) && (buff[3] == 0x40)) {
			System.out.println("校验 == " + Xor(buff, 2, buff.length - 4));
			if (Xor(buff, 2, buff.length - 4) == 0) {
				return new Light_Status(buff);
			}
		}
		return null;
	}

	private static byte Xor(byte[] buff, int offset, int len) {
		byte xor = 0;
		for (int i = offset; i < (offset + len); i++) {
			xor ^= buff[i];
		}
		return xor;
	}

}
