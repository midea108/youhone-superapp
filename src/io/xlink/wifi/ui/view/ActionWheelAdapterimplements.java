package io.xlink.wifi.ui.view;

/**
 * @author LiuXinYi
 * @Date 2015年6月5日 下午7:51:39
 * @Description []
 * @version 1.0.0
 */
public class ActionWheelAdapterimplements implements WheelAdapter {


    /**
     * Constructor
     * 
     * @param minValue
     *            the wheel min value
     * @param maxValue
     *            the wheel max value
     * @param format
     *            the format string
     */
    public ActionWheelAdapterimplements() {
    }

    @Override
    public String getItem(int index) {
	String text = "";
	switch (index) {
	case 0:
	    text = "关闭";
	    break;
	case 1:
	    text = "开启";
	    break;
	default:
	    break;
	}
	return text;
    }

    @Override
    public int getItemsCount() {
	return 2;
    }

    @Override
    public int getMaximumLength() {
	return 2;
    }
}