package io.xlink.wifi.ui.activity;

import io.xlink.wifi.ui.SystemBarTintManager;
import android.annotation.TargetApi;
import android.app.Activity;
import android.os.Build;
import android.view.Window;
import android.view.WindowManager;

public class ActivityManager {

	public ActivityManager() {

	}

	/**
	 * 改变状态栏颜色
	 * 
	 * @param c
	 *            上下文
	 * @param color
	 *            颜色值
	 */
	@TargetApi(Build.VERSION_CODES.KITKAT)
	public void applyKitKatTranslucency(Activity c, int color) {
		if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
			Window win = c.getWindow();
			WindowManager.LayoutParams winParams = win.getAttributes();
			final int bits = WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS;
			if (true) {
				winParams.flags |= bits;
			}
			win.setAttributes(winParams);
			SystemBarTintManager mTintManager = new SystemBarTintManager(c);
			mTintManager.setStatusBarTintEnabled(true);
			mTintManager.setNavigationBarTintEnabled(true);
			mTintManager.setTintColor(color);
		}

	}

}
