package io.xlink.wifi.ui.activity;

import io.xlink.wifi.sdk.XlinkAgent;
import io.xlink.wifi.ui.MyApp;
import io.xlink.wifi.ui.bean.Device;
import io.xlink.wifi.ui.fragment.AddDeviceFragment;
import io.xlink.wifi.ui.fragment.Change_PW_Frag;
import io.xlink.wifi.ui.fragment.DeviceListTab;
import io.xlink.wifi.ui.fragment.DevicePasswordFragment;
import io.xlink.wifi.ui.fragment.Dryer_Tips_Fragment;
import io.xlink.wifi.ui.fragment.ForgetFragment;
import io.xlink.wifi.ui.fragment.Light_Tips_Fragment;
import io.xlink.wifi.ui.fragment.LinkToWifiFragment;
import io.xlink.wifi.ui.fragment.SearchDeviceFragment;
import io.xlink.wifi.ui.util.XlinkUtils;
import io.xlink.wifi.ui.view.CustomDialog;

import java.net.SocketException;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.List;

import android.app.AlertDialog.Builder;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.DialogInterface.OnCancelListener;
import android.content.Intent;
import android.graphics.drawable.BitmapDrawable;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.PopupWindow;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.hiflying.smartlink.ISmartLinker;
import com.hiflying.smartlink.OnSmartLinkListener;
import com.hiflying.smartlink.SmartLinkedModule;
import com.hiflying.smartlink.v7.MulticastSmartLinker;
import com.youhone.activity.MainActivity;
import com.youhone.weather.Weather_City_Manager;
import com.youhone.weather.Weather_Detail;
import com.youhone.weatherform.WeatherForm;
import com.youhone.xlink.superapp.R;

/**
 * @author cgt
 * 
 * @Description [设备列表，配置，发现界面]
 * @version 1.0.0
 */
public class DeviceListActivity extends BaseFragmentActivity implements
		OnSmartLinkListener, OnClickListener {
	boolean isDestroy;
	View title_back;
	View title_more;
	TextView title_text;
	RelativeLayout title_layout;
	public static boolean firstLoad = false;
	ActivityManager manager;

	List<Device> devices = new ArrayList<Device>();

	private AddDeviceFragment addDeviceFg;
	private LinkToWifiFragment linkToWifiFg;
	private DevicePasswordFragment dpFg;
	private SearchDeviceFragment searchDeviceFg;
	private DeviceListTab devicelist_frag;
	// private WeatherList_Fragment weather_list_Frag;
	private Weather_City_Manager city_manager_Frag;
	private Weather_Detail weather_detail_Frag;
	private ForgetFragment change_PW_Frag;

	public Dialog resetDialog;
	PopupWindow mPopupWindow;

	ISmartLinker sm;
	Dialog configDialog;

	@Override
	protected void onDestroy() {
		super.onDestroy();
		isDestroy = true;
		MyApp.getApp().destroyExit();
	}

	@Override
	protected void onCreate(Bundle arg0) {
		isDestroy = false;
		super.onCreate(arg0);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.xlink_activity_main);
		manager = new ActivityManager();
		MyApp.getApp().setCurrentActivity(DeviceListActivity.this);
		title_layout = (RelativeLayout) findViewById(R.id.title_layout);
		title_back = findViewById(R.id.title_back);
		title_back.setOnClickListener(this);
		title_more = findViewById(R.id.title_more);
		title_more.setOnClickListener(this);
		title_text = (TextView) findViewById(R.id.title_text);
		// XlinkAgent.getInstance().start();
		MyApp.getApp().login();
		openDeviceList(false);
	}

	private void setTitleText(String text) {
		title_text.setText(text);
	}

	private void showBack() {
		title_back.setVisibility(View.VISIBLE);
	}

	private void gongBack() {
		title_back.setVisibility(View.GONE);
	}

	private void gongMore() {
		title_more.setVisibility(View.GONE);
	}

	private void showMore() {
		title_more.setVisibility(View.VISIBLE);
	}

	private void gongTitle() {
		manager.applyKitKatTranslucency(DeviceListActivity.this, getResources()
				.getColor(R.color.main_color));
		title_layout.setVisibility(View.GONE);
	}

	public void showResetDialog() {
		if (resetDialog == null) {
			resetDialog = createProgressDialog("修改密码", "正在处理中，请稍后...");
		}
		if (!resetDialog.isShowing())
			resetDialog.show();
	}

	private void showTitle() {
		if (currentViewFr instanceof Weather_Detail) {
			manager.applyKitKatTranslucency(DeviceListActivity.this,
					getResources().getColor(R.color.main_color));
			title_layout.setBackgroundColor(getResources().getColor(
					R.color.main_color));
		} else {
			manager.applyKitKatTranslucency(DeviceListActivity.this,
					getResources().getColor(R.color.main_color));
			title_layout.setBackgroundColor(getResources().getColor(
					R.color.main_color));
		}
		title_layout.setVisibility(View.VISIBLE);
	}

	/**
	 * 20150629 添加 cgt
	 */
	public void openDeviceList(boolean isForgetBck) {
		setTitleText("设备列表");
		firstLoad = true;
		devicelist_frag = new DeviceListTab(isForgetBck);
		replaceViewFragment(devicelist_frag, "DeviceListFrag");
		gongBack();
		gongMore();
		gongTitle();
	}

	public void openAddDevice() {
		setTitleText("添加设备");
		showTitle();
		gongBack();
		gongMore();
		if (addDeviceFg == null) {
			addDeviceFg = new AddDeviceFragment();
		}
		replaceViewFragment(addDeviceFg, "addDeviceFg");
	}

	public void openLinkToWifi() {
		showTitle();
		showBack();
		setTitleText("配置设备wifi");
		if (linkToWifiFg == null) {
			linkToWifiFg = new LinkToWifiFragment();
		}
		replaceViewFragment(linkToWifiFg, "linkToWifiFg");
	}

	public void openDevicePassword(Device device) {
		showBack();
		showTitle();
		setTitleText("连接设备");
		if (dpFg == null) {
			dpFg = new DevicePasswordFragment();
		}
		dpFg.device = device;
		replaceViewFragment(dpFg, "dpFg");
	}

	public void openSearchDevice() {
		showBack();
		showTitle();
		setTitleText("已配置的设备");
		if (searchDeviceFg == null) {
			searchDeviceFg = new SearchDeviceFragment();
		}
		replaceViewFragment(searchDeviceFg, "searchDeviceFg");
	}

	public void openSearchDeviceActiviy(int i, boolean isFirst) {
		Intent intent = new Intent();
		intent.setFlags(i);
		intent.setClass(DeviceListActivity.this, MainActivity.class);
		intent.putExtra("isFirst", isFirst);
		startActivity(intent);
		openDeviceList(false);
	}

	public void openDeviceActiviy(int i, boolean isFirst) {
		Intent intent = new Intent();
		intent.setFlags(i);
		intent.setClass(DeviceListActivity.this, MainActivity.class);
		intent.putExtra("isFirst", isFirst);
		startActivity(intent);
	}

	public void openWeatherDetail(WeatherForm[] weathers) {

		if (weathers != null && weathers.length > 0) {
			setTitleText(weathers[0].getName());
		} else {
			setTitleText("城市天气");
		}
		weather_detail_Frag = new Weather_Detail(weathers);
		replaceViewFragment(weather_detail_Frag, "weatherDetailFg");
		showBack();
		showMore();
		showTitle();
	}

	public void openCityManagerFragment() {
		setTitleText("城市管理");
		showBack();
		showTitle();
		gongMore();
		if (city_manager_Frag == null) {
			city_manager_Frag = new Weather_City_Manager();
		}
		replaceViewFragment(city_manager_Frag, "city_MagagerFg");
	}

	public void openChangePW() {
		setTitleText("修改密码");
		showTitle();
		showBack();
		gongMore();
		if (change_PW_Frag == null) {
			change_PW_Frag = new ForgetFragment(1);
		}
		replaceViewFragment(change_PW_Frag, "change_PW_Frag");
	}

	public void configSmartLink(String ssid, String pwd, boolean isDebug) {
		if (isDebug) {
			openSearchDevice();
		} else {
			configDialog = createProgressDialog("配置设备", "正在配置设备...");
			configDialog.setCanceledOnTouchOutside(true);
			sm = MulticastSmartLinker.getInstance();
			configDialog.setOnCancelListener(new OnCancelListener() {
				@Override
				public void onCancel(DialogInterface dialog) {
					sm.setOnSmartLinkListener(null);
					sm.stop();
					XlinkUtils.shortTips("停止配置");
				}
			});
			try {
				sm.setOnSmartLinkListener(this);
				sm.start(getApplicationContext(), pwd, ssid);
			} catch (SocketException e) {
				e.printStackTrace();
			} catch (UnknownHostException e) {
				e.printStackTrace();
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}

	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) {
		if (event.getKeyCode() == KeyEvent.KEYCODE_BACK) {
			if (event.getAction() == KeyEvent.ACTION_DOWN
					&& event.getRepeatCount() == 0) {
				if (currentViewFr != null
						&& currentViewFr instanceof DeviceListTab) {
					((DeviceListTab) currentViewFr).webViewGoBack();
				} else
					back();
			}
			return true;
		} else if (keyCode == KeyEvent.KEYCODE_MENU) {

		}
		return super.onKeyDown(keyCode, event);
	}

	protected void back() {
		if (currentViewFr instanceof AddDeviceFragment) {
			showExitDialog();
		} else if (currentViewFr instanceof LinkToWifiFragment) {
			openDeviceList(false);
		} else if (currentViewFr instanceof DevicePasswordFragment) {
			openSearchDevice();
		} else if (currentViewFr instanceof SearchDeviceFragment) {
			openDeviceList(false);
		} else if (currentViewFr instanceof Light_Tips_Fragment
				|| currentViewFr instanceof Dryer_Tips_Fragment) {
			openDeviceList(false);
		} else if (currentViewFr instanceof Weather_City_Manager) {
			openDeviceList(false);
		} else if (currentViewFr instanceof Weather_Detail) {
			openDeviceList(false);
		} else if (currentViewFr instanceof Change_PW_Frag) {
			openDeviceList(false);
		} else if (currentViewFr instanceof ForgetFragment) {
			openDeviceList(true);
		} else {
			showExitDialog();
		}
	}

	public void showExitDialog() {
		Builder builder = new Builder(this);
		builder.setCancelable(false);
		builder.setMessage("退出程序吗？");
		builder.setTitle("提示");
		builder.setNegativeButton("确定",
				new android.content.DialogInterface.OnClickListener() {
					@Override
					public void onClick(DialogInterface dialog, int which) {
						dialog.dismiss();
						XlinkAgent.getInstance().stop();
						finish();
						System.exit(0);
					}
				});
		builder.setNeutralButton("取消", null);
		builder.create().show();
	}

	private void configWifiError() {
		CustomDialog dialog = createTipsDialog("配置失败", "配置超时，未找到设备");
		dialog.show();
	}

	public void showSetting(View v) {
		if (mPopupWindow == null) {
			LayoutInflater layoutInflater = LayoutInflater.from(this);
			View myview = layoutInflater.inflate(R.layout.xlink_popup_more,
					null);
			mPopupWindow = new PopupWindow(myview, myview.getWidth(),
					myview.getHeight());
			TextView more_tv = (TextView) myview.findViewById(R.id.more_tv);
			myview.findViewById(R.id.more_relative_1).setOnClickListener(this);
			more_tv.setText("退出帐号");
			mPopupWindow.setWindowLayoutMode(
					ViewGroup.LayoutParams.WRAP_CONTENT,
					ViewGroup.LayoutParams.WRAP_CONTENT);
			mPopupWindow.setFocusable(true);
			// 设置点击弹框外部，弹框消失
			mPopupWindow.setOutsideTouchable(true);
			// 设置一个透明的背景，不然无法实现点击弹框外，弹框消失
			mPopupWindow.setBackgroundDrawable(new BitmapDrawable());
			mPopupWindow.setTouchable(true);
			// 获取popwidow的宽高
		}
		if (!mPopupWindow.isShowing()) {

			mPopupWindow.showAsDropDown(v);
		} else {
			mPopupWindow.dismiss();
		}

		// mPopupWindow.showAsDropDown(head_layout, -500, 0);
	}

	@Override
	public void onClick(View v) {
		int id = v.getId();
		if (id == R.id.title_back) {
			back();
		} else if (id == R.id.more_relative_1) {
			showTipsDialog("确定退出当前帐号吗？",
					new android.content.DialogInterface.OnClickListener() {

						@Override
						public void onClick(DialogInterface dialog, int which) {
							dialog.dismiss();
							MyApp.getApp().removeUser(DeviceListActivity.this);
						}
					});
		} else if (id == R.id.title_more) {
			if (currentViewFr instanceof Weather_Detail) {
				openCityManagerFragment();
			} else {
				showSetting(v);
			}
		}
	}

	public void showTipsDialog(String msg,
			android.content.DialogInterface.OnClickListener listener) {
		Builder builder = new Builder(this);
		builder.setCancelable(false);
		builder.setMessage(msg);
		builder.setTitle("提示");
		builder.setNegativeButton("确定", listener);
		builder.setNeutralButton("取消", null);
		builder.create().show();
	}

	@Override
	public void onCompleted() {
		MyApp.postToMainThread(new Runnable() {

			@Override
			public void run() {
				XlinkUtils.shortTips("配置完成");
				if (configDialog != null && !isDestroy) {
					configDialog.dismiss();
				}
			}
		});
	}

	@Override
	public void onLinked(SmartLinkedModule arg0) {
		if (configDialog != null && !isDestroy) {
			configDialog.dismiss();
			configDialog = createProgressDialog("配置成功", "请等待模块重启...");
			configDialog.show();
		}
		XlinkUtils.shortTips("配置成功");
		MyApp.postToMainThreadDelay(new Runnable() {
			@Override
			public void run() {
				if (configDialog != null && !isDestroy) {
					configDialog.dismiss();
				}
				openSearchDevice();
			}
		}, 5 * 1000);
	}

	@Override
	public void onTimeOut() {
		MyApp.postToMainThread(new Runnable() {
			@Override
			public void run() {
				if (configDialog != null && !isDestroy) {
					configDialog.dismiss();
				}
				configWifiError();
				XlinkUtils.shortTips("配置超时未找到设备!");
			}
		});
	}

}
