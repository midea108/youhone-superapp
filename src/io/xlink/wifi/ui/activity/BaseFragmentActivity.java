package io.xlink.wifi.ui.activity;

import io.xlink.wifi.ui.MyApp;
import io.xlink.wifi.ui.MyApp.userExtresion;
import io.xlink.wifi.ui.view.CustomDialog;
import android.app.AlertDialog.Builder;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.view.inputmethod.InputMethodManager;
import android.widget.Toast;

import com.youhone.xlink.superapp.R;

public abstract class BaseFragmentActivity extends FragmentActivity implements
		userExtresion {
	public static String currentTab = "";
	Fragment currentViewFr;
	private FragmentManager fragmentManager;
	private boolean isShow = false;

	@Override
	protected void onCreate(Bundle arg0) {
		super.onCreate(arg0);
		fragmentManager = getSupportFragmentManager();
		MyApp.getApp().setListener(this);
	}

	public void replaceViewFragment(Fragment viewFr, String tag) {
		currentTab = tag;
		Log.e("", "切换fragment " + tag);
		InputMethodManager imm = (InputMethodManager) getSystemService(INPUT_METHOD_SERVICE);
		if (imm != null) {
			imm.hideSoftInputFromWindow(getWindow().getDecorView()
					.getWindowToken(), 0);
		}
//		if (viewFr == currentViewFr) {
//			return;
//		}
		FragmentTransaction mFragmentTransaction;
		mFragmentTransaction = fragmentManager.beginTransaction();
		mFragmentTransaction.setCustomAnimations(android.R.anim.fade_in,
				android.R.anim.fade_out);
//		if (currentViewFr != null) {
//			mFragmentTransaction.hide(currentViewFr);
//		}
//		if (viewFr.isAdded()) {
//			mFragmentTransaction.show(viewFr);
//		} else {
//			mFragmentTransaction.replace(R.id.ll_main, viewFr, tag);
//		}
		mFragmentTransaction.replace(R.id.ll_main, viewFr, tag);
		currentViewFr = viewFr;
		mFragmentTransaction.commitAllowingStateLoss();

	}

	public Dialog createProgressDialog(String title, String tips) {
		ProgressDialog progressDialog = CustomDialog.createProgressDialog(this,
				title, tips);
		return progressDialog;
	}

	public CustomDialog createTipsDialog(String title, String tips) {
		CustomDialog dialog = CustomDialog.createErrorDialog(this, title, tips,
				null);
		dialog.show();
		return dialog;
	}

	public void showExceptionDialog(String msg,
			android.content.DialogInterface.OnClickListener listener) {
		try {
			Builder builder = new Builder(BaseFragmentActivity.this);
			builder.setCancelable(false);
			builder.setMessage(msg);
			builder.setTitle("登录异常");
			builder.setNegativeButton("退出账号", listener);
			builder.create().show();
			isShow = true;
		} catch (Exception e) {
			Toast.makeText(BaseFragmentActivity.this, "该账号已在其他地方登录！",
					Toast.LENGTH_SHORT).show();
		}

	}

	@Override
	public void onExtresion() {
		if (!isShow) {
			showExceptionDialog("该账号已在其他地方登录！",
					new android.content.DialogInterface.OnClickListener() {
						@Override
						public void onClick(DialogInterface dialog, int which) {
							isShow = false;
							dialog.dismiss();
							MyApp.getApp()
									.removeUser(BaseFragmentActivity.this);
						}
					});
		}
	}

}
