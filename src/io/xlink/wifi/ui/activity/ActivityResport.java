package io.xlink.wifi.ui.activity;

import io.xlink.wifi.ui.MyApp;
import io.xlink.wifi.ui.MyApp.userExtresion;
import android.app.Activity;
import android.app.AlertDialog.Builder;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.youhone.xlink.superapp.R;

public class ActivityResport extends Activity implements userExtresion {
	private Button resport_title_back;
	private EditText edtReportDesc;
	private Button report_commit;
	private EditText edt_phone;

	@Override
	protected void onCreate(Bundle arg0) {
		super.onCreate(arg0);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.activity_resport);
		ActivityManager manager = new ActivityManager();
		manager.applyKitKatTranslucency(ActivityResport.this, getResources().getColor(R.color.main_color));
	//	MyApp.getApp().setCurrentActivity(ActivityResport.this);
		edt_phone = (EditText) findViewById(R.id.edt_phone);
		edtReportDesc = (EditText) findViewById(R.id.edtReportDesc);
		resport_title_back = (Button) findViewById(R.id.resport_title_back);
		report_commit = (Button) findViewById(R.id.report_commit);
		report_commit.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				String resport = edtReportDesc.getText().toString();
				String phone = edt_phone.getText().toString();
				if (resport.equals("") || phone.equals("")) {
					Toast.makeText(ActivityResport.this, "请填写完整的信息",
							Toast.LENGTH_SHORT).show();
					return;
				}
				Toast.makeText(ActivityResport.this, "提交成功", Toast.LENGTH_SHORT)
						.show();
				finish();
			}
		});
		resport_title_back.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				finish();
			}
		});
	}

	public void showExceptionDialog(String msg,
			android.content.DialogInterface.OnClickListener listener) {
		Builder builder = new Builder(this);
		builder.setCancelable(false);
		builder.setMessage(msg);
		builder.setTitle("登录异常");
		builder.setNegativeButton("退出账号", listener);
		builder.create().show();
	}

	@Override
	public void onExtresion() {
		showExceptionDialog("该账号已在其他地方登录！",
				new android.content.DialogInterface.OnClickListener() {

					@Override
					public void onClick(DialogInterface dialog, int which) {
						dialog.dismiss();
						MyApp.getApp().removeUser(ActivityResport.this);
					}
				});

	}

}
