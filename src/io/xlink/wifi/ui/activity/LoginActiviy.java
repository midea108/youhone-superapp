package io.xlink.wifi.ui.activity;

import io.xlink.wifi.sdk.XlinkAgent;
import io.xlink.wifi.ui.Constant;
import io.xlink.wifi.ui.MyApp;
import io.xlink.wifi.ui.fragment.ForgetFragment;
import io.xlink.wifi.ui.fragment.LoginFragment;
import io.xlink.wifi.ui.fragment.RegisterFragment;
import io.xlink.wifi.ui.fragment.RegisterSuccessFragment;
import io.xlink.wifi.ui.http.HttpAgent;
import io.xlink.wifi.ui.util.SharedPreferencesUtil;
import io.xlink.wifi.ui.util.XlinkUtils;
import io.xlink.wifi.ui.view.CustomDialog;

import java.util.HashMap;
import java.util.Map;

import org.apache.http.Header;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.Button;
import android.widget.TextView;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.loopj.android.http.TextHttpResponseHandler;
import com.youhone.volley.JsonGetRequest;
import com.youhone.volley.VolleySingleton;
import com.youhone.xlink.superapp.R;

public class LoginActiviy extends BaseFragmentActivity implements
		OnClickListener {
	private LoginFragment loginFg;
	private RegisterFragment registerFg;
	private ForgetFragment forgetFg;
	private RegisterSuccessFragment regSuccessFg;

	private TextView title_more_tv;
	private TextView title_text;
	private Button title_back;

	public Dialog regDialog;
	public Dialog loginDialog;
	public Dialog resetDialog;

	@Override
	protected void onCreate(Bundle arg0) {
		super.onCreate(arg0);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.xlink_activity_main);
		ActivityManager manager = new ActivityManager();
		manager.applyKitKatTranslucency(LoginActiviy.this, getResources()
				.getColor(R.color.main_color));
		// 隐藏常规更多 （三个点）
		title_more_tv = (TextView) findViewById(R.id.title_more_tv);
		findViewById(R.id.title_more).setVisibility(View.GONE);
		title_text = (TextView) findViewById(R.id.title_text);
		title_back = (Button) findViewById(R.id.title_back);
		title_back.setOnClickListener(this);
		title_more_tv.setOnClickListener(this);
		title_more_tv.setVisibility(View.VISIBLE);
		openLogin();
	}

	protected void getJOFromServer(String url, Map<String, String> map) {
		this.getJOFromServer(url, map, 0);
	}

	protected void postJOFromServer(String url, Map<String, String> map) {
		this.postJOFromServer(url, map, 0);
	}

	protected void getJOFromServer(String url, Map<String, String> map,
			final int tag) {
		Log.e("login_upload", "url = " + url + toGetParams(map));

		JsonGetRequest request = new JsonGetRequest(url + toGetParams(map),
				new Response.Listener<JSONObject>() {
					@Override
					public void onResponse(JSONObject response) {

					}
				}, new Response.ErrorListener() {

					@Override
					public void onErrorResponse(VolleyError error) {
						error.printStackTrace();

					}
				}) {
			@Override
			public Map<String, String> getHeaders() throws AuthFailureError {
				HashMap<String, String> headers = new HashMap<String, String>();
				headers.put("Content-Type",
						"application/x-www-form-urlencoded; charset=utf-8");
				return headers;
			}
		};
		addToVolley(request);
	}

	/**
	 * 从后台获取JsonObject
	 * 
	 * @param url
	 * @param map
	 * @param tag
	 */
	protected void postJOFromServer(String url, final Map<String, String> map,
			final int tag) {

		StringRequest request = new StringRequest(Request.Method.POST, url,
				new Response.Listener<String>() {
					@Override
					public void onResponse(String response) {
						try {
							JSONObject json = string2Json(response);

						} catch (JSONException e) {
							System.out.println("json格式错误");
						}

						System.out.println("post方法返回" + response);
					}
				}, new Response.ErrorListener() {

					@Override
					public void onErrorResponse(VolleyError error) {
						error.printStackTrace();
					}
				}) {
			@Override
			protected Map<String, String> getParams() throws AuthFailureError {

				return map;
			}
		};
		addToVolley(request);

	}

	public <T> void addToVolley(Request<T> request) {
		VolleySingleton.getInstance(getApplicationContext()).addToRequestQueue(
				request);
	}

	private String toGetParams(Map<String, String> map) {
		String params = "";
		boolean isStart = true;
		for (String keyString : map.keySet()) {
			if (isStart) {
				params += '?';
				isStart = false;
			} else
				params += "&";

			params += (keyString + '=' + map.get(keyString));
		}
		return params;
	}

	public static JSONObject string2Json(String string) throws JSONException {
		if (string.startsWith("\"")) {
			string = string.substring(1, string.length() - 1);
		}

		string = string.replace("\\", "");

		JSONObject json = new JSONObject(string);

		return json;
	}

	private void setTitleText(String text) {
		title_text.setText(text);
	}

	private void showBack() {
		title_back.setVisibility(View.VISIBLE);
	}

	private void gongBack() {
		title_back.setVisibility(View.GONE);
	}

	private void showMore() {
		title_more_tv.setVisibility(View.VISIBLE);
	}

	private void gongMore() {
		title_more_tv.setVisibility(View.GONE);
	}

	public void showRegDialog() {
		if (regDialog == null) {
			regDialog = createProgressDialog("注册帐号", "正在注册中，请稍后...");
		}
		regDialog.show();
	}

	public void showResetDialog() {
		if (resetDialog == null) {
			resetDialog = createProgressDialog("修改密码", "正在处理中，请稍后...");
		}
		if (!resetDialog.isShowing())
			resetDialog.show();
	}

	private void showLoginDialog() {
		if (loginDialog == null) {
			loginDialog = createProgressDialog("正在登录", "正在登录中，请稍后...");
		}
		loginDialog.show();
	}

	public void openLogin() {
		// showMore();
		gongBack();
		setTitleText("登录");
		// title_more_tv.setText("注册帐号");
		if (loginFg == null) {
			loginFg = new LoginFragment();
		}
		replaceViewFragment(loginFg, "loginFg");
	}

	public void openRegister() {
		showBack();
		// title_more_tv.setText("已有帐号");
		setTitleText("注册");
		if (registerFg == null) {
			registerFg = new RegisterFragment();
		}
		replaceViewFragment(registerFg, "registerFg");
	}

	public void openForget() {
		showBack();
		gongMore();
		setTitleText("忘记密码");

		if (forgetFg == null) {
			forgetFg = new ForgetFragment(2);
		}
		replaceViewFragment(forgetFg, "forgetFg");
	}

	public void openRegisterSuccess(String uid, int appid, String appauth) {

		if (regSuccessFg == null) {
			regSuccessFg = new RegisterSuccessFragment();
		}
		regSuccessFg.setEmail(uid, appid, appauth);
		replaceViewFragment(regSuccessFg, "regSuccessFg");
		showBack();
		gongMore();
		setTitleText("注册成功");
	}

	private void showLoginError(int status, String tips) {
		String title = "登录失败";
		switch (status) {
		case 404:
			tips = "用户名或密码错误，请重试";
			break;
		default:
			break;
		}
		CustomDialog loginErrorDialog = createTipsDialog(title, tips);
		loginErrorDialog.show();
	}

	public void getAppid(String uid, String pwd, final boolean isRemember) {
		showLoginDialog();
		HttpAgent.getInstance().getAppId(uid, pwd,
				new TextHttpResponseHandler() {

					@Override
					public void onSuccess(int arg0, Header[] arg1, String msg) {

						JSONObject object;
						try {
							loginDialog.dismiss();
							object = new JSONObject(msg);
							int status = object.getInt("status");
							if (status != 200) {

								showLoginError(status, object.getString("msg"));
								return;
							}
							JSONObject value = object.getJSONObject("user");
							int appid = value.getInt("id");
							System.out.println("分配的id    " + appid);
							String authKey = value.getString("key");
							openDeviceList(appid, authKey, isRemember);
						} catch (JSONException e) {
							e.printStackTrace();
							XlinkUtils.shortTips("用户信息，json解析错误");
						}

					}

					@Override
					public void onFailure(int arg0, Header[] arg1, String arg2,
							Throwable arg3) {
						loginDialog.dismiss();
						XlinkUtils.shortTips("网络异常");
					}

				});
	}

	public void openDeviceList(int appid, String auth, boolean isRemember) {
		// if (isRemember) {
		SharedPreferencesUtil.keepShared(Constant.APP_ID, appid);
		SharedPreferencesUtil.keepShared(Constant.APP_KEY, auth);
		// }
		SharedPreferencesUtil.keepShared(Constant.IS_REMEMBER, isRemember);
		MyApp.getApp().setAppid(appid);
		MyApp.getApp().setAuth(auth);
		Intent intent = new Intent(this, DeviceListActivity.class);
		startActivity(intent);
		finish();
	}

	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) {
		if (event.getKeyCode() == KeyEvent.KEYCODE_BACK) {
			if (event.getAction() == KeyEvent.ACTION_DOWN
					&& event.getRepeatCount() == 0) {
				back();
			}
			return true;
		} else if (keyCode == KeyEvent.KEYCODE_MENU) {
		}
		return super.onKeyDown(keyCode, event);
	}

	private void back() {
		if (currentViewFr instanceof RegisterFragment) {
			openLogin();
		} else if (currentViewFr instanceof RegisterSuccessFragment) {
			openRegister();
		} else if (currentViewFr instanceof ForgetFragment) {
			openLogin();
		} else {
			System.out.println("System.exit(0)");
			XlinkAgent.getInstance().stop();
			finish();
			// System.exit(0);
		}

	}

	@Override
	public void onClick(View v) {
		int id = v.getId();
		if (id == R.id.title_more_tv) {
			if (currentViewFr instanceof LoginFragment) {
				openRegister();
			} else {
				openLogin();
			}
		} else if (id == R.id.title_back) {
			back();
		}
	}

}
