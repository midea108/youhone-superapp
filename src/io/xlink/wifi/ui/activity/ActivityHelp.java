package io.xlink.wifi.ui.activity;

import io.xlink.wifi.ui.MyApp;
import io.xlink.wifi.ui.MyApp.userExtresion;
import android.app.Activity;
import android.app.AlertDialog.Builder;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.Button;
import android.widget.LinearLayout;

import com.youhone.usercenter.activity.ActivityAttention;
import com.youhone.usercenter.activity.ActivityError;
import com.youhone.usercenter.activity.ActivityFilter;
import com.youhone.usercenter.activity.ActivityInstall;
import com.youhone.usercenter.activity.ActivityLight;
import com.youhone.usercenter.activity.ActivityProtect;
import com.youhone.usercenter.activity.ActivityTurnOn;
import com.youhone.xlink.superapp.R;

public class ActivityHelp extends Activity implements OnClickListener,
		userExtresion {
	private Button help_title_back;
	private LinearLayout turn_on_lay, light_lay, filter_lay, error_lay,
			protect_lay, install_lay, attention_lay;

	@Override
	protected void onCreate(Bundle arg0) {
		super.onCreate(arg0);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.activity_help);
		ActivityManager manager = new ActivityManager();
		manager.applyKitKatTranslucency(ActivityHelp.this, getResources()
				.getColor(R.color.main_color));
		//MyApp.getApp().setCurrentActivity(ActivityHelp.this);
		help_title_back = (Button) findViewById(R.id.help_main_title_back);
		turn_on_lay = (LinearLayout) findViewById(R.id.turn_on_lay);
		light_lay = (LinearLayout) findViewById(R.id.light_lay);
		filter_lay = (LinearLayout) findViewById(R.id.filter_lay);
		error_lay = (LinearLayout) findViewById(R.id.error_lay);
		protect_lay = (LinearLayout) findViewById(R.id.protect_lay);
		install_lay = (LinearLayout) findViewById(R.id.install_lay);
		attention_lay = (LinearLayout) findViewById(R.id.attention_lay);
		turn_on_lay.setOnClickListener(this);
		light_lay.setOnClickListener(this);
		filter_lay.setOnClickListener(this);
		error_lay.setOnClickListener(this);
		protect_lay.setOnClickListener(this);
		install_lay.setOnClickListener(this);
		attention_lay.setOnClickListener(this);
		help_title_back.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				finish();

			}
		});
	}

	@Override
	public void onClick(View arg0) {
		int id = arg0.getId();
		Intent intent = new Intent();
		if (id == R.id.turn_on_lay) {
			intent.setClass(ActivityHelp.this, ActivityTurnOn.class);
		} else if (id == R.id.light_lay) {
			intent.setClass(ActivityHelp.this, ActivityLight.class);
		} else if (id == R.id.filter_lay) {
			intent.setClass(ActivityHelp.this, ActivityFilter.class);
		} else if (id == R.id.error_lay) {
			intent.setClass(ActivityHelp.this, ActivityError.class);
		} else if (id == R.id.protect_lay) {
			intent.setClass(ActivityHelp.this, ActivityProtect.class);
		} else if (id == R.id.install_lay) {
			intent.setClass(ActivityHelp.this, ActivityInstall.class);
		} else if (id == R.id.attention_lay) {
			intent.setClass(ActivityHelp.this, ActivityAttention.class);
		}
		startActivity(intent);
	}

	public void showExceptionDialog(String msg,
			android.content.DialogInterface.OnClickListener listener) {
		Builder builder = new Builder(this);
		builder.setCancelable(false);
		builder.setMessage(msg);
		builder.setTitle("登录异常");
		builder.setNegativeButton("退出账号", listener);
		builder.create().show();
	}

	@Override
	public void onExtresion() {
		showExceptionDialog("该账号已在其他地方登录！",
				new android.content.DialogInterface.OnClickListener() {

					@Override
					public void onClick(DialogInterface dialog, int which) {
						dialog.dismiss();
						MyApp.getApp().removeUser(ActivityHelp.this);
					}
				});

	}
}
