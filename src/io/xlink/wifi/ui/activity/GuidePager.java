package io.xlink.wifi.ui.activity;

import io.xlink.wifi.ui.MyApp;
import io.xlink.wifi.ui.bean.LocalInfo;
import io.xlink.wifi.ui.bean.NetType;

import java.util.ArrayList;
import java.util.List;

import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.params.CoreConnectionPNames;

import android.app.Activity;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.view.Window;

import com.youhone.xlink.superapp.R;

public class GuidePager extends Activity {
	private static final int GOTO_MAINACTIVITY = 0;
	private static final int TIME = 3000;
	Intent intent;
	private JudgeTask task;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.xlink_startup_fragment);
		ActivityManager manager = new ActivityManager();
		manager.applyKitKatTranslucency(GuidePager.this, 0x003abae4);
		intent = getXIntent();
		task = new JudgeTask();
		task.execute();
		mHandler.sendEmptyMessageDelayed(GOTO_MAINACTIVITY, TIME);
	}

	private Intent getXIntent() {
		Intent intent = new Intent();
		// 是否登录
		if (MyApp.getApp().isLogin()) {
			intent.setClass(GuidePager.this, DeviceListActivity.class);
		} else {// 登录界面
			intent.setClass(GuidePager.this, LoginActiviy.class);
		}
		return intent;
	}

	private final Handler mHandler = new Handler() {
		@Override
		public void handleMessage(Message msg) {
			if (msg.what == GOTO_MAINACTIVITY) {
				startActivity(intent);
				finish();
			}
			super.handleMessage(msg);
		}

	};

	private class JudgeTask extends AsyncTask<Void, Void, Boolean> {

		@Override
		protected Boolean doInBackground(Void... params) {
			List<NameValuePair> param = new ArrayList<NameValuePair>();
			HttpPost httpRequest = new HttpPost("http://www.baidu.com");
			try {
				httpRequest.setEntity(new UrlEncodedFormEntity(param, "GBK"));
				HttpClient httpclinet = new DefaultHttpClient();
				httpclinet.getParams().setParameter(
						CoreConnectionPNames.CONNECTION_TIMEOUT, 2 * 1000);
				httpclinet.getParams().setParameter(
						CoreConnectionPNames.SO_TIMEOUT, 2 * 1000);
				System.out.println("Time Out == "
						+ httpclinet.getParams().getParameter(
								CoreConnectionPNames.CONNECTION_TIMEOUT));
				System.out.println("Time Out == "
						+ httpclinet.getParams().getParameter(
								CoreConnectionPNames.SO_TIMEOUT));
				HttpResponse response = httpclinet.execute(httpRequest);
				System.out.println("ping返回====="
						+ response.getStatusLine().getStatusCode());
				if (response.getStatusLine().getStatusCode() == 200) {
					return true;
				} else {
					return false;
				}
			} catch (Exception e) {
				e.printStackTrace();
				return false;
			}
		}

		@Override
		protected void onPostExecute(Boolean result) {
			if (result) {
				LocalInfo.NetType = NetType.NET;
			} else {
				LocalInfo.NetType = NetType.LAN;
			}

			super.onPostExecute(result);
		}

	}

	@Override
	protected void onDestroy() {
		mHandler.removeCallbacksAndMessages(null);
		if (null != task)
			task.cancel(true);
		super.onDestroy();
	}

}
