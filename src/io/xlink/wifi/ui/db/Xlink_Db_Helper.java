package io.xlink.wifi.ui.db;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteDatabase.CursorFactory;
import android.database.sqlite.SQLiteOpenHelper;

public class Xlink_Db_Helper extends SQLiteOpenHelper {
	private static final int DATABASE_VERSION = 1;
	private static final String DATABASE_NAME = "Xlink_DB";

	public Xlink_Db_Helper(Context context, String name, CursorFactory factory,
			int version) {
		super(context, name, factory, version);
	}

	public Xlink_Db_Helper(Context context) {
		super(context, DATABASE_NAME, null, DATABASE_VERSION);
	}

	@Override
	public void onCreate(SQLiteDatabase db) {
		db.execSQL("create table device_db(id string primary key,data string)");
		db.execSQL("CREATE TABLE citys (id varchar(20) primary key,cityname varchar(20)," +
				"yl1 varchar(20),yl2 varchar(20))");     //执行有更改的sql语句
		db.execSQL("create table user_city(id string primary key,user_id,city string)");
	}

	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
		// TODO 更新数据库

	}

}
