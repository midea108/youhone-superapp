package io.xlink.wifi.ui.db;

import java.util.ArrayList;
import java.util.HashMap;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

public class DBManageImpl implements CityManage {

	private final String TAG = "message";

	private Xlink_Db_Helper myDBHelper = null;

	private static DBManageImpl DBInstance = null;

	private DBManageImpl(Context context) {
		this.myDBHelper = new Xlink_Db_Helper(context);
	}

	/** ���õ���ģʽ */
	public static synchronized DBManageImpl getDBInstance(Context context) {
		if (null == DBInstance) {
			DBInstance = new DBManageImpl(context);
		}
		return DBInstance;

	}

	@Override
	public String findCityId(String CityName) {
		SQLiteDatabase dbwrite = myDBHelper.getWritableDatabase();
		Cursor cursor = dbwrite.rawQuery(
				"select cityid from citys where cityname=?",
				new String[] { CityName });
		String id = "";
		while (cursor.moveToNext()) {
			id = cursor.getString(cursor.getColumnIndex("cityid"));
		}
		cursor.close();
		return id;
	}

	@Override
	public void deleteCity(String CityName) {
		SQLiteDatabase dbwrite = myDBHelper.getWritableDatabase();
		dbwrite.execSQL("delete from citys where cityname=?",
				new String[] { CityName });
		dbwrite.close();
	}

	@Override
	public ArrayList<HashMap<String, String>> findAllCity() {
		SQLiteDatabase dbwrite = myDBHelper.getReadableDatabase(); // �����ݿ⣬�����ݽ��и������ô˷�������SQLiteDatabase����
		Cursor cursor = dbwrite.rawQuery("select * from citys", null);
		ArrayList<HashMap<String, String>> AL = new ArrayList<HashMap<String, String>>();
		while (cursor.moveToNext()) {
			HashMap<String, String> HM = new HashMap<String, String>();
			String name = cursor.getString(cursor.getColumnIndex("cityname"));
			HM.put("cityname", name);
			AL.add(HM);
		}
		cursor.close();

		return AL;
	}

	@Override
	public Boolean findCityByName(String cityName) {
		Boolean bool;
		SQLiteDatabase dbwrite = myDBHelper.getReadableDatabase(); // �����ݿ⣬�����ݽ��и������ô˷�������SQLiteDatabase����
		Cursor cursor = dbwrite.rawQuery("select * from citys where cityname=?",
				new String[] { cityName });
		bool = false;
		try {
			if (cursor.moveToFirst()) {
				bool = true;
			} else {
				bool = false;
			}
		} catch (Exception e) {
			e.printStackTrace();
			Log.i(TAG, e.toString());
		} finally {
			cursor.close();
		}
		return bool;
	}

	@Override
	public ArrayList<String> findAllCityName() {
		SQLiteDatabase dbwrite = myDBHelper.getReadableDatabase(); // �����ݿ⣬�����ݽ��и������ô˷�������SQLiteDatabase����
		Cursor cursor = dbwrite.rawQuery("select cityname from citys", null);
		ArrayList<String> AL = new ArrayList<String>();
		while (cursor.moveToNext()) {
			String name = cursor.getString(cursor.getColumnIndex("cityname"));
			AL.add(name);
		}
		cursor.close();
		return AL;
	}

	@Override
	public void addCity(String CityName) {
		SQLiteDatabase dbwrite = myDBHelper.getWritableDatabase();
		dbwrite.execSQL("insert into citys(cityname) values (?)",
				new String[] { CityName });
		dbwrite.close();

	}

}
