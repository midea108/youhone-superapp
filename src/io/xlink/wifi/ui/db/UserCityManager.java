package io.xlink.wifi.ui.db;

import java.util.ArrayList;

public interface UserCityManager {
	
	/**
	 * 添加一个城市
	 * @param CityName
	 * @param CityId
	 */
	public void addCity(String CityName);
	
	/**
	 * 删除一个城市 
	 * @param CityId
	 */
	public void deleteCity(String CityId);
	
	/**
	 * 该方法用于验证添加是否重复
	 * @param cityid 
	 * @return 如果存在返回true,不存在返回false
	 */
	public Boolean findCityByName(String cityName);
	
	/***
	 * 查询所有城市
	 * @return
	 */
	public ArrayList<String> findAllCity();
	

}
