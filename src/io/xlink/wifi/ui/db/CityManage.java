package io.xlink.wifi.ui.db;

import java.util.ArrayList;

public interface CityManage {
	
	/**
	 * 根据城市名称找到城市名称
	 * @param Cityname
	 * @return
	 */
	public String findCityId(String Cityname);
	/**
	 * 添加一个城市
	 * @param CityName
	 * @param CityId
	 */
	public void addCity(String CityName);
	
	/**
	 * 删除一个城市 
	 * @param CityId
	 */
	public void deleteCity(String CityId);
	
	/**
	 * 该方法用于验证添加是否重复
	 * @param cityid 
	 * @return 如果存在返回true,不存在返回false
	 */
	public Boolean findCityByName(String cityName);
	
	/***
	 * 查询所有城市
	 * @return
	 */
	public ArrayList findAllCity();
	
	/**
	 * 返回所有城市名称
	 * @return
	 */
	public ArrayList<String> findAllCityName();

}
