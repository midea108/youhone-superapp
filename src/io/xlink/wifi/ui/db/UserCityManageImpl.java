package io.xlink.wifi.ui.db;

import io.xlink.wifi.ui.Constant;
import io.xlink.wifi.ui.MyApp;

import java.util.ArrayList;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

public class UserCityManageImpl implements UserCityManager {

	private final String TAG = "UserCityManageImpl";

	private Xlink_Db_Helper myDBHelper = null;

	private static UserCityManageImpl DBInstance = null;

	private UserCityManageImpl(Context context) {
		this.myDBHelper = new Xlink_Db_Helper(context);
	}

	/** ���õ���ģʽ */
	public static synchronized UserCityManageImpl getDBInstance(Context context) {
		if (null == DBInstance) {
			DBInstance = new UserCityManageImpl(context);
		}
		return DBInstance;

	}

	@Override
	public void deleteCity(String CityName) {
		SQLiteDatabase dbwrite = myDBHelper.getWritableDatabase();
		dbwrite.execSQL("delete from user_city where cityname=? and user_id=?",
				new String[] { CityName, MyApp.getApp().getAppid()+"" });
		dbwrite.close();
	}

	@Override
	public Boolean findCityByName(String cityName) {
		Boolean bool;
		SQLiteDatabase dbwrite = myDBHelper.getReadableDatabase(); // �����ݿ⣬�����ݽ��и������ô˷�������SQLiteDatabase����
		Cursor cursor = dbwrite.rawQuery(
				"select * from user_city where cityname=? and user_id=?",
				new String[] { cityName, MyApp.getApp().getAppid()+"" });
		bool = false;
		try {
			if (cursor.moveToFirst()) {
				bool = true;
			} else {
				bool = false;
			}
		} catch (Exception e) {
			e.printStackTrace();
			Log.i(TAG, e.toString());
		} finally {
			cursor.close();
		}
		return bool;
	}

	@Override
	public ArrayList<String> findAllCity() {
		SQLiteDatabase dbwrite = myDBHelper.getReadableDatabase(); // �����ݿ⣬�����ݽ��и������ô˷�������SQLiteDatabase����
		Cursor cursor = dbwrite.query("user_city", null, "user_id=?",
				new String[] { MyApp.getApp().getAppid()+"" }, null, null, null);
		ArrayList<String> AL = new ArrayList<String>();
		while (cursor.moveToNext()) {
			String name = cursor.getString(cursor.getColumnIndex("city"));
			AL.add(name);
		}
		cursor.close();
		return AL;
	}

	public String getCity() {
		SQLiteDatabase dbwrite = myDBHelper.getReadableDatabase();
		Cursor cursor = dbwrite.query("user_city", null, "user_id=?",
				new String[] {MyApp.getApp().getAppid()+"" }, null, null, null);
		String AL = "";
		if (cursor.getCount() > 0)
			AL = cursor.getString(cursor.getColumnIndex("city"));
		cursor.close();
		return AL;
	}

	@Override
	public void addCity(String CityName) {
		clearDB();
		SQLiteDatabase dbwrite = myDBHelper.getWritableDatabase();
		ContentValues mContentValues = new ContentValues();
		mContentValues.put("city", CityName);
		mContentValues.put("user_id",MyApp.getApp().getAppid());
		dbwrite.insert("user_city", null, mContentValues);
		dbwrite.close();

	}

	private void clearDB() {
		SQLiteDatabase db = myDBHelper.getWritableDatabase();
		String delete = "Delete from user_city where user_id like \""
				+ MyApp.getApp().getAppid() + "#%\"";
		db.execSQL(delete);
		db.close();
	}

}
