package io.xlink.wifi.ui.fragment;

import io.xlink.wifi.ui.activity.DeviceListActivity;
import io.xlink.wifi.ui.activity.LoginActiviy;
import io.xlink.wifi.ui.http.HttpAgent;

import java.util.Timer;
import java.util.TimerTask;

import org.apache.http.Header;
import org.json.JSONException;
import org.json.JSONObject;

import android.graphics.Color;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;
import cn.smssdk.EventHandler;
import cn.smssdk.SMSSDK;

import com.loopj.android.http.TextHttpResponseHandler;
import com.youhone.xlink.superapp.R;

public class ForgetFragment extends BaseFragment implements OnClickListener {
	private final int FROMDEVICE = 1;
	private final int FROMLOGIN = 2;
	private EditText account, new_pw1, new_pw2;
	private Button change, get_verify;
	private String phoneNum;
	private String PhonePW;
	private int getCount = 0;
	private Timer timer;
	private TimerTask timerTask;
	private int verify_time = 125;
	private EditText text_verify;
	private int from = 0;

	public ForgetFragment(int from) {
		this.from = from;
	}

	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
	}

	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View view = inflater.inflate(R.layout.xlink_forget_fragment, container,
				false);
		SMSSDK.registerEventHandler(mHandler);
		initWidget(view);
		return view;
	}

	private void initWidget(View view) {
		account = (EditText) view.findViewById(R.id.account);
		new_pw1 = (EditText) view.findViewById(R.id.new_pw1);
		new_pw2 = (EditText) view.findViewById(R.id.new_pw2);
		change = (Button) view.findViewById(R.id.b_change);
		get_verify = (Button) view.findViewById(R.id.b_verify);
		text_verify = (EditText) view.findViewById(R.id.text_verify);
		change.setOnClickListener(this);
		get_verify.setOnClickListener(this);
	}

	@Override
	public void onClick(View v) {

		if (v.getId() == R.id.b_verify) {
			phoneNum = account.getText().toString();
			if (isPhoneNum(phoneNum)) {
				if (++getCount <= 3) {
					getVerificationCode();
					if (getCount == 3) {
						get_verify.setText("获取语音验证");
					}
				} else {
					getVoiceVerifyCode();
				}
			} else {
				Toast.makeText(getActivity(), "请输入正确的手机号码", Toast.LENGTH_SHORT)
						.show();
			}
		} else if (v.getId() == R.id.b_change) {
			String account_text = account.getText().toString();
			String pw1 = new_pw1.getText().toString();
			String pw2 = new_pw2.getText().toString();
			if (TextUtils.isEmpty(account_text) || TextUtils.isEmpty(pw1)
					|| TextUtils.isEmpty(pw2)) {
				Toast.makeText(getActivity(), "请输入完整信息", Toast.LENGTH_SHORT)
						.show();
				return;
			} else if (!pw1.equals(pw2)) {
				Toast.makeText(getActivity(), "两次输入的密码不相同", Toast.LENGTH_SHORT)
						.show();
				return;
			} else {
				if(from==FROMDEVICE){
					getDeviceActiviy().showResetDialog();
				}else if(from==FROMLOGIN){
					getLoginActiviy().showResetDialog();
				}
				
				String code = text_verify.getText().toString();
				PhonePW = new_pw1.getText().toString();
				submitVerificationCode(code);
			}

		}
	}

	private void getVerificationCode() {
		SMSSDK.getVerificationCode("86", phoneNum);
	}

	private void submitVerificationCode(String code) {
		SMSSDK.submitVerificationCode("86", phoneNum, code);
	}

	private void getVoiceVerifyCode() {
		SMSSDK.getVoiceVerifyCode(phoneNum, "86");
	}

	private boolean isPhoneNum(String phone) {
		return phone.matches("^(13|15|18|17)\\d{9}$");
	}

	private void showToast(final String txt, final int time) {
		getActivity().runOnUiThread(new Runnable() {
			@Override
			public void run() {
				Toast.makeText(getActivity(), txt, time).show();
				get_verify.setTextColor(Color.GRAY);
				get_verify.setEnabled(false);
				startTimer();
			}
		});
	}

	@Override
	public void onHiddenChanged(boolean hidden) {
		if (!hidden) {
			SMSSDK.registerEventHandler(mHandler);
		} else {
			SMSSDK.unregisterEventHandler(mHandler);
		}
		super.onHiddenChanged(hidden);
	}

	private EventHandler mHandler = new EventHandler() {

		@Override
		public void afterEvent(int event, int result, Object data) {
			if (result == SMSSDK.RESULT_COMPLETE) {
				// 回调完成
				if (event == SMSSDK.EVENT_SUBMIT_VERIFICATION_CODE) {
					// 提交验证码成功
					//stopTimer();
					getActivity().runOnUiThread(new Runnable() {

						@Override
						public void run() {
							resetPW(phoneNum, PhonePW);
						}
					});

				} else if (event == SMSSDK.EVENT_GET_VERIFICATION_CODE) {
					// 获取验证码成功
					showToast("验证码已发送", Toast.LENGTH_SHORT);
				} else if (event == SMSSDK.EVENT_GET_VOICE_VERIFICATION_CODE) {
					// 获取验证码成功
					showToast("验证码已发送", Toast.LENGTH_SHORT);
				} else if (event == SMSSDK.EVENT_GET_SUPPORTED_COUNTRIES) {
					// 返回支持发送验证码的国家列表
				}
			} else {
				
				if(from==FROMDEVICE){
					getDeviceActiviy().resetDialog.dismiss();
				}else if(from==FROMLOGIN){
					getLoginActiviy().resetDialog.dismiss();
				}
				showToast("修改失败");
				((Throwable) data).printStackTrace();
			}
		}
	};

	private void showToast(final String txt) {
		getActivity().runOnUiThread(new Runnable() {
			@Override
			public void run() {
				Toast.makeText(getActivity(), txt, Toast.LENGTH_SHORT).show();
			}
		});
	}

	private LoginActiviy getLoginActiviy() {
		return (LoginActiviy) getActivity();
	}
	
	private DeviceListActivity getDeviceActiviy() {
		return (DeviceListActivity) getActivity();
	}

	private void resetPW(String account, String pw) {
		HttpAgent.getInstance().resetPW(account, pw,
				new TextHttpResponseHandler() {
					@Override
					public void onSuccess(int arg0, Header[] arg1, String msg) {
						JSONObject object;
						if(from==FROMDEVICE){
							getDeviceActiviy().resetDialog.dismiss();
						}else if(from==FROMLOGIN){
							getLoginActiviy().resetDialog.dismiss();
						}
						try {
							object = new JSONObject(msg);
							int status = object.getInt("status");
							if (status == 200) {
								showToast("修改成功");
								if(from==FROMDEVICE){
									getDeviceActiviy().openDeviceList(true);
								}else if(from==FROMLOGIN){
									getLoginActiviy().openLogin();
								}
								
							} else {
								showToast("修改失败");
							}

						} catch (JSONException e) {
							e.printStackTrace();
						}

					}

					@Override
					public void onFailure(int arg0, Header[] arg1, String arg2,
							Throwable arg3) {
						if(from==FROMDEVICE){
							getDeviceActiviy().resetDialog.dismiss();
						}else if(from==FROMLOGIN){
							getLoginActiviy().resetDialog.dismiss();
						}
						showToast("修改失败");
					}

				});
	}

	private void startTimer() {
		if (timer == null) {
			timer = new Timer();
			if (timerTask == null) {
				timerTask = new TimerTask() {
					@Override
					public void run() {
						verify_time--;
						getActivity().runOnUiThread(new Runnable() {
							@Override
							public void run() {
								if (verify_time < 0) {
									get_verify.setText("获取验证码");
									get_verify.setTextColor(Color.WHITE);
									verify_time = 125;
									get_verify.setEnabled(true);
									stopTimer();
								} else {
									get_verify.setText(verify_time + "秒后获取");
								}
							}
						});
					}
				};
			}
			timer.schedule(timerTask, 0, 1 * 1000);
		}
	}

	private void stopTimer() {
		if (timerTask != null) {
			timerTask.cancel();
			timerTask = null;
		}
		if (timer != null) {
			timer.cancel();
			timer = null;
		}
		verify_time = 125;
	}

	@Override
	public void onDestroy() {
		stopTimer();
		super.onDestroy();
	}

}
