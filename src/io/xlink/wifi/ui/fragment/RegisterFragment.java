package io.xlink.wifi.ui.fragment;

import io.xlink.wifi.ui.activity.LoginActiviy;
import io.xlink.wifi.ui.http.HttpAgent;
import io.xlink.wifi.ui.util.XlinkUtils;

import java.util.Timer;
import java.util.TimerTask;

import org.apache.http.Header;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;
import org.json.JSONException;
import org.json.JSONObject;

import android.content.Context;
import android.graphics.Color;
import android.location.Criteria;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;
import cn.smssdk.EventHandler;
import cn.smssdk.SMSSDK;

import com.loopj.android.http.TextHttpResponseHandler;
import com.youhone.xlink.superapp.R;

public class RegisterFragment extends BaseFragment implements OnClickListener {

	TextView tv_reg_info, tv_reg_info_phone;
	EditText et_reg_username, et_reg_email, et_reg_pwd, et_reg_repwd,
			et_reg_phone, et_reg_verify;
	Button b_reg, b_reg_phone, b_reg_get_verify, b_phone, b_mail,
			b_haveAccount;
	private EditText phone_reg_pwd, phone_reg_repwd, phone_reg_username;
	private CheckBox Purchannels1, Purchannels2, Purchannels3;
	private CheckBox InfoSources1, InfoSources2, InfoSources3, InfoSources4,
			InfoSources5, InfoSources6, InfoSources7, InfoSources8,
			InfoSources9;
	private CheckBox[] Purchannels;
	private CheckBox[] InfoSources;
	private PurchannelsCheckListener listener1 = new PurchannelsCheckListener();
	private InfoSourcesCheckListener listener2 = new InfoSourcesCheckListener();
	private int Purchannel = 0;
	private int InfoSource = 0;
	private String phoneNum;
	private int getCount = 0;
	private String phone_name, phone_psd;
	private Timer timer;
	private TimerTask timerTask;
	private int verify_time = 125;
	private boolean isPhone = false;
	private TextView address_txt;
	// 定位相关
	private LocationManager locationManager;
	private Criteria criteria;
	private Location location;

	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
	}

	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View view = inflater.inflate(R.layout.xlink_register_fragment,
				container, false);
		SMSSDK.registerEventHandler(mHandler);
		initWidget(view);
		return view;
	}

	private void initWidget(View view) {
		Purchannels = new CheckBox[3];
		InfoSources = new CheckBox[9];
		Purchannels1 = (CheckBox) view.findViewById(R.id.Purchannels1);
		Purchannels2 = (CheckBox) view.findViewById(R.id.Purchannels2);
		Purchannels3 = (CheckBox) view.findViewById(R.id.Purchannels3);
		Purchannels[0] = Purchannels1;
		Purchannels[1] = Purchannels2;
		Purchannels[2] = Purchannels3;
		for (int i = 0; i < Purchannels.length; i++) {
			Purchannels[i].setOnCheckedChangeListener(listener1);
		}
		InfoSources1 = (CheckBox) view.findViewById(R.id.InfoSources1);
		InfoSources2 = (CheckBox) view.findViewById(R.id.InfoSources2);
		InfoSources3 = (CheckBox) view.findViewById(R.id.InfoSources3);
		InfoSources4 = (CheckBox) view.findViewById(R.id.InfoSources4);
		InfoSources5 = (CheckBox) view.findViewById(R.id.InfoSources5);
		InfoSources6 = (CheckBox) view.findViewById(R.id.InfoSources6);
		InfoSources7 = (CheckBox) view.findViewById(R.id.InfoSources7);
		InfoSources8 = (CheckBox) view.findViewById(R.id.InfoSources8);
		InfoSources9 = (CheckBox) view.findViewById(R.id.InfoSources9);
		InfoSources[0] = InfoSources1;
		InfoSources[1] = InfoSources2;
		InfoSources[2] = InfoSources3;
		InfoSources[3] = InfoSources4;
		InfoSources[4] = InfoSources5;
		InfoSources[5] = InfoSources6;
		InfoSources[6] = InfoSources7;
		InfoSources[7] = InfoSources8;
		InfoSources[8] = InfoSources9;
		for (int i = 0; i < InfoSources.length; i++) {
			InfoSources[i].setOnCheckedChangeListener(listener2);
		}
		address_txt = (TextView) view.findViewById(R.id.address_txt);
		tv_reg_info = (TextView) view.findViewById(R.id.tv_reg_info_phone);
		et_reg_username = (EditText) view.findViewById(R.id.et_reg_username);
		et_reg_email = (EditText) view.findViewById(R.id.et_reg_email);
		et_reg_pwd = (EditText) view.findViewById(R.id.et_reg_pwd);
		et_reg_repwd = (EditText) view.findViewById(R.id.et_reg_repwd);
		b_haveAccount = (Button) view.findViewById(R.id.b_haveAccount);
		b_haveAccount.setOnClickListener(this);
		b_reg = (Button) view.findViewById(R.id.b_reg);
		b_reg.setOnClickListener(this);
		tv_reg_info_phone = (TextView) view
				.findViewById(R.id.tv_reg_info_phone);
		et_reg_phone = (EditText) view.findViewById(R.id.et_reg_phone_num);
		et_reg_verify = (EditText) view.findViewById(R.id.et_reg_verify);
		phone_reg_pwd = (EditText) view.findViewById(R.id.phone_reg_pwd);
		phone_reg_repwd = (EditText) view.findViewById(R.id.phone_reg_repwd);
		phone_reg_username = (EditText) view
				.findViewById(R.id.phone_reg_username);
		b_reg_get_verify = (Button) view.findViewById(R.id.b_get_verify);
		b_reg_phone = (Button) view.findViewById(R.id.b_reg_phone);
		b_reg_get_verify.setOnClickListener(this);
		b_reg_phone.setOnClickListener(this);
		b_phone = (Button) view.findViewById(R.id.btn_phone_reg);
		b_mail = (Button) view.findViewById(R.id.btn_mail_reg);
		b_phone.setOnClickListener(this);
		b_mail.setOnClickListener(this);
		// initLocation();
		String serviceName = Context.LOCATION_SERVICE;
		locationManager = (LocationManager) getAct().getSystemService(
				serviceName);
		String provider = LocationManager.NETWORK_PROVIDER;
		criteria = new Criteria();
		criteria.setAccuracy(Criteria.ACCURACY_FINE);
		criteria.setAltitudeRequired(false);
		criteria.setBearingRequired(false);
		criteria.setCostAllowed(true);
		criteria.setPowerRequirement(Criteria.POWER_MEDIUM);
		location = locationManager.getLastKnownLocation(provider);
		locationManager.requestLocationUpdates(provider, 2000, 10,
				locationListener);
		// test
		// new Thread(new Runnable() {
		//
		// @Override
		// public void run() {
		// getAct().uploadRegisterInfo("13928273366", "13928273366",
		// "111111", "中国，广东省，广州市，番禺区", Purchannel, InfoSource);
		//
		// }
		// }).start();

	}

	private LoginActiviy getAct() {
		return (LoginActiviy) getActivity();
	}

	private class PurchannelsCheckListener implements OnCheckedChangeListener {

		@Override
		public void onCheckedChanged(CompoundButton buttonView,
				boolean isChecked) {
			if (isChecked) {
				switch (buttonView.getId()) {
				case R.id.Purchannels1:
					Purchannel = 1;
					break;
				case R.id.Purchannels2:
					Purchannel = 2;
					break;
				case R.id.Purchannels3:
					Purchannel = 3;
					break;
				default:
					break;
				}
			} else {
				Purchannel = 0;
			}
			changePurchannelsStatus();
		}

	}

	private class InfoSourcesCheckListener implements OnCheckedChangeListener {

		@Override
		public void onCheckedChanged(CompoundButton buttonView,
				boolean isChecked) {
			if (isChecked) {
				switch (buttonView.getId()) {
				case R.id.InfoSources1:
					InfoSource = 1;
					break;
				case R.id.InfoSources2:
					InfoSource = 2;
					break;
				case R.id.InfoSources3:
					InfoSource = 3;
					break;
				case R.id.InfoSources4:
					InfoSource = 4;
					break;
				case R.id.InfoSources5:
					InfoSource = 5;
					break;
				case R.id.InfoSources6:
					InfoSource = 6;
					break;
				case R.id.InfoSources7:
					InfoSource = 7;
					break;
				case R.id.InfoSources8:
					InfoSource = 8;
					break;
				case R.id.InfoSources9:
					InfoSource = 9;
					break;
				default:
					break;
				}
			} else {
				InfoSource = 0;
			}
			changeInfoSourcesStatus();
		}

	}

	private void changePurchannelsStatus() {
		for (int i = 0; i < Purchannels.length; i++) {
			if (i != Purchannel - 1)
				Purchannels[i].setChecked(false);
			else
				Purchannels[i].setChecked(true);
		}
	}

	private void changeInfoSourcesStatus() {
		for (int i = 0; i < InfoSources.length; i++) {
			if (i != InfoSource - 1)
				InfoSources[i].setChecked(false);
			else
				InfoSources[i].setChecked(true);
		}
	}

	private final LocationListener locationListener = new LocationListener() {
		public void onLocationChanged(final Location location) {
			new Thread(new Runnable() {
				@Override
				public void run() {
					updateWithNewLocation(location);
				}
			}).start();
		}

		public void onProviderDisabled(String provider) {
			updateWithNewLocation(null);
		}

		public void onProviderEnabled(String provider) {
		}

		public void onStatusChanged(String provider, int status, Bundle extras) {
		}
	};

	private void updateWithNewLocation(Location location) {
		String latLongString;
		if (location != null) {
			final double lat = location.getLatitude();
			final double lng = location.getLongitude();
			latLongString = "纬度:" + lat + "\n经度:" + lng;
			String url = "http://recode.ditu.aliyun.com/dist_query?l=" + lat
					+ "," + lng;
			HttpGet get = new HttpGet(url);
			HttpClient mClient = new DefaultHttpClient();
			try {
				HttpResponse response = mClient.execute(get);
				if (response.getStatusLine().getStatusCode() != 200) {
					Log.d("error", "联网失败");
					latLongString = "无法获取位置信息";
				} else {
					HttpEntity entity = response.getEntity();
					latLongString = EntityUtils.toString(entity, "gb2312");
					JSONObject json = new JSONObject(latLongString);
					latLongString = json.getString("dist");
					System.out.println(latLongString);
				}

			} catch (Exception e) {
				Log.d("tag", "出错了");
				latLongString = "无法获取位置信息";
				e.printStackTrace();
			}
		} else {
			latLongString = "无法获取位置信息";
		}
		final String address = latLongString;
		getActivity().runOnUiThread(new Runnable() {
			@Override
			public void run() {
				address_txt.setText(address);
			}
		});

	}

	@Override
	public void onClick(View v) {
		int id = v.getId();
		if (id == R.id.b_reg) {
			String name = et_reg_username.getText().toString();
			String email = et_reg_email.getText().toString();
			String pwd = et_reg_pwd.getText().toString();
			String repwd = et_reg_repwd.getText().toString();
			if (TextUtils.isEmpty(name) || TextUtils.isEmpty(pwd)
					|| pwd.length() < 6 || TextUtils.isEmpty(email)
					|| TextUtils.isEmpty(repwd)) {
				setInfo("请填写完整信息");
			} else {
				if (name.length() > 30 || email.length() > 30
						|| pwd.length() > 30 || repwd.length() > 30) {
					setInfo("注册信息过长，请重新注册");
				}
				if (pwd.equals(repwd)) {
					if (!XlinkUtils.checkEmail(email)) {
						XlinkUtils.shortTips("请输入正确的邮箱格式");
						return;
					}
					registerUser(email, name, pwd);
				} else {
					setInfo("密码不一致");
				}
			}
		} else if (id == R.id.btn_mail_reg) {
			setRegWay(false);
		} else if (id == R.id.btn_phone_reg) {
			setRegWay(true);
		} else if (id == R.id.b_get_verify) {
			phoneNum = et_reg_phone.getText().toString();
			if (isPhoneNum(phoneNum)) {
				if (++getCount <= 3) {
					getVerificationCode();
					if (getCount == 3) {
						b_reg_get_verify.setText("获取语音验证");
					}
				} else {
					getVoiceVerifyCode();
				}
			} else {
				setInfoPhone("请输入正确的手机号码");
			}
		} else if (id == R.id.b_reg_phone) {
			phoneNum = et_reg_phone.getText().toString();
			phone_psd = phone_reg_pwd.getText().toString();
			phone_name = phone_reg_username.getText().toString();
			String repwd = phone_reg_repwd.getText().toString();
			if (phoneNum.equals("")) {
				setInfoPhone("手机号不能为空");
				return;
			}
			if (isPhoneNum(phoneNum)) {
				if (phone_psd.equals(repwd)) {
					getAct().showRegDialog();
					String code = et_reg_verify.getText().toString();
					submitVerificationCode(code);
				} else {
					setInfoPhone("密码不一致");
				}
			} else {
				setInfoPhone("请输入正确的手机号码");
			}

		} else if (id == R.id.b_haveAccount) {
			getAct().openLogin();
		}
		// switch (id) {
		// case R.id.b_reg:
		// String name = et_reg_username.getText().toString();
		// String email = et_reg_email.getText().toString();
		// String pwd = et_reg_pwd.getText().toString();
		// String repwd = et_reg_repwd.getText().toString();
		// if (TextUtils.isEmpty(name) || TextUtils.isEmpty(pwd)
		// || pwd.length() < 6 || TextUtils.isEmpty(email)
		// || TextUtils.isEmpty(repwd)) {
		// tv_reg_info.setText("请填写完整信息");
		// } else {
		// if (pwd.equals(repwd)) {
		// registerUser(email, name, pwd);
		// } else {
		// tv_reg_info.setText("密码不一致");
		// }
		// }
		// break;
		// }
	}

	public void setInfo(String str) {
		if (tv_reg_info != null)
			tv_reg_info.setText(str);
	}

	public void setInfoPhone(String str) {
		if (tv_reg_info_phone != null)
			tv_reg_info_phone.setText(str);
	}

	public void registerUser(final String uid, String name, String pwd) {
		getAct().showRegDialog();
		// 提交注册（帐号，别名， 密码） 都一模一样
		HttpAgent.getInstance().onRegister(uid, name, pwd,
				new TextHttpResponseHandler() {
					@Override
					public void onSuccess(int arg0, Header[] arg1, String msg) {
						JSONObject object;
						getAct().regDialog.dismiss();
						try {
							object = new JSONObject(msg);
							int status = object.getInt("status");
							if (status == 200) {
								JSONObject value = object.getJSONObject("user");
								int appid = value.getInt("id");
								String authKey = value.getString("key");
								getAct().openRegisterSuccess(uid, appid,
										authKey);

							} else if (status == 201) { // 已经注册过
								if (!isPhone)
									setInfo("该账号已被占用");
								else
									setInfoPhone("该账号已被占用");
							} else {
								if (!isPhone)
									setInfo("其他错误:" + status);
								else
									setInfoPhone("其他错误:" + status);
							}
							System.out.println("错误代码" + status);
						} catch (JSONException e) {
							e.printStackTrace();
						}

					}

					@Override
					public void onFailure(int arg0, Header[] arg1, String arg2,
							Throwable arg3) {
						getAct().regDialog.dismiss();
						if (!isPhone)
							setInfo("网络错误");
						else
							setInfoPhone("网络错误");
						// TODO Auto-generated method stub
						// 注册失败
						// tips_text.setText("registerUser fail msg: " + arg2);
						// openDeviceListActivity();
					}

				});
	}

	@Override
	public void onHiddenChanged(boolean hidden) {
		if (!hidden) {
			SMSSDK.registerEventHandler(mHandler);
		} else {
			SMSSDK.unregisterEventHandler(mHandler);
		}
		super.onHiddenChanged(hidden);
	}

	private EventHandler mHandler = new EventHandler() {

		@Override
		public void afterEvent(int event, int result, Object data) {
			if (result == SMSSDK.RESULT_COMPLETE) {
				// 回调完成
				if (event == SMSSDK.EVENT_SUBMIT_VERIFICATION_CODE) {
					// 提交验证码成功
					// stopTimer();
					getActivity().runOnUiThread(new Runnable() {

						@Override
						public void run() {
							registerUser(phoneNum, phone_name, phone_psd);
						}
					});

				} else if (event == SMSSDK.EVENT_GET_VERIFICATION_CODE) {
					// 获取验证码成功
					showToast("验证码已发送", Toast.LENGTH_SHORT);
				} else if (event == SMSSDK.EVENT_GET_VOICE_VERIFICATION_CODE) {
					// 获取验证码成功
					showToast("验证码已发送", Toast.LENGTH_SHORT);
				} else if (event == SMSSDK.EVENT_GET_SUPPORTED_COUNTRIES) {
					// 返回支持发送验证码的国家列表
				}
			} else {
				((Throwable) data).printStackTrace();
			}
		}
	};

	private void setRegWay(boolean isPhone) {
		this.isPhone = isPhone;
		View phone = getView().findViewById(R.id.layout_phone);
		View mail = getView().findViewById(R.id.layout_mail);
		if (isPhone) {
			phone.setVisibility(View.VISIBLE);
			mail.setVisibility(View.GONE);
		} else {
			phone.setVisibility(View.GONE);
			mail.setVisibility(View.VISIBLE);
		}
	}

	private void getVerificationCode() {
		SMSSDK.getVerificationCode("86", phoneNum);
	}

	private void submitVerificationCode(String code) {
		SMSSDK.submitVerificationCode("86", phoneNum, code);
	}

	private void getVoiceVerifyCode() {
		SMSSDK.getVoiceVerifyCode(phoneNum, "86");
	}

	private boolean isPhoneNum(String phone) {
		return phone.matches("^(13|15|18|17)\\d{9}$");
	}

	private void showToast(final String txt, final int time) {
		getActivity().runOnUiThread(new Runnable() {
			@Override
			public void run() {
				Toast.makeText(getActivity(), txt, time).show();
				b_reg_get_verify.setTextColor(Color.GRAY);
				b_reg_get_verify.setEnabled(false);
				startTimer();
			}
		});
	}

	private void startTimer() {
		if (timer == null) {
			timer = new Timer();
			if (timerTask == null) {
				timerTask = new TimerTask() {
					@Override
					public void run() {
						verify_time--;
						getActivity().runOnUiThread(new Runnable() {
							@Override
							public void run() {
								if (verify_time < 0) {
									b_reg_get_verify.setText("获取验证码");
									b_reg_get_verify.setTextColor(Color.WHITE);
									verify_time = 125;
									b_reg_get_verify.setEnabled(true);
								} else {
									b_reg_get_verify.setText(verify_time
											+ "秒后获取");
								}
							}
						});
					}
				};
			}
			timer.schedule(timerTask, 0, 1 * 1000);
		}
	}

	private void stopTimer() {
		if (timerTask != null) {
			timerTask.cancel();
			timerTask = null;
		}
		if (timer != null) {
			timer.cancel();
			timer = null;
		}
		verify_time = 125;
	}

	@Override
	public void onDestroy() {
		stopTimer();
		super.onDestroy();
	}
}
