package io.xlink.wifi.ui.fragment;

import io.xlink.wifi.sdk.XlinkAgent;
import io.xlink.wifi.ui.activity.DeviceListActivity;
import io.xlink.wifi.ui.view.IconPagerAdapter;
import io.xlink.wifi.ui.view.IconTabPageIndicator_Main;
import io.xlink.wifi.ui.view.LazyViewPager;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.List;

import android.app.AlertDialog.Builder;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewConfiguration;
import android.view.ViewGroup;

import com.uh.homepage.fragment.CommunityFragment;
import com.uh.homepage.fragment.DeviceListFragment;
import com.uh.homepage.fragment.SettingFragment;
import com.uh.homepage.fragment.ShoppingFragment;
import com.youhone.xlink.superapp.R;

public class DeviceListTab extends BaseFragment {

	public static int position = 0;
	private LazyViewPager mViewPager;
	private IconTabPageIndicator_Main mIndicator;
	private List<Fragment> mTabs = new ArrayList<Fragment>();
	private FragmentAdapter mAdapter;
	private Fragment deviceFragment, shoppingFragment, settingFragment,communityFragment;
	private Fragment currentFrag;
	private boolean isForgetBck = false;

	public DeviceListTab(boolean isForgetBck) {
		this.isForgetBck = isForgetBck;
	}

	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View view = inflater.inflate(R.layout.device_tab_new, container, false);
		setOverflowShowingAlways();
		mViewPager = (LazyViewPager) view.findViewById(R.id.view_pager);
		mIndicator = (IconTabPageIndicator_Main) view
				.findViewById(R.id.indicator);
		initDatas(view);
		mAdapter = new FragmentAdapter(mTabs, getFragmentManager());
		mViewPager.setAdapter(mAdapter);
		// mViewPager.setOnPageChangeListener(this);
		mIndicator.setViewPager(mViewPager);
		System.out.println("是否忘记密码返回"+isForgetBck);
		if (isForgetBck)
			mViewPager.setCurrentItem(2);
		return view;
	}

	public void webViewGoBack() {
		if (currentFrag != null && currentFrag instanceof ShoppingFragment) {
			System.out.println("DeviceListTab--商城网页返回");
			((ShoppingFragment) shoppingFragment).goBack();
		} else {
			showExitDialog();
		}
	}

	private void initDatas(View view) {

		deviceFragment = new DeviceListFragment();
		Bundle deviceargs = new Bundle();
		deviceargs.putString("title", "设备列表");
		deviceFragment.setArguments(deviceargs);
		((BaseFragment) deviceFragment).setIconId(R.drawable.tab_device);
		((BaseFragment) deviceFragment).setTitle("设备");
		mTabs.add(deviceFragment);

		communityFragment=new CommunityFragment();
		Bundle communityargs = new Bundle();
		communityargs.putString("title", "社区");
		communityFragment.setArguments(communityargs);
		((BaseFragment) communityFragment).setIconId(R.drawable.tab_shopping);
		((BaseFragment) communityFragment).setTitle("物业");
		mTabs.add(communityFragment);
		
		shoppingFragment = new ShoppingFragment();
		Bundle shoppingargs = new Bundle();
		shoppingargs.putString("title", "商城页");
		shoppingFragment.setArguments(shoppingargs);
		((BaseFragment) shoppingFragment).setIconId(R.drawable.tab_shopping);
		((BaseFragment) shoppingFragment).setTitle("商城");
		mTabs.add(shoppingFragment);
		
		settingFragment = new SettingFragment();
		Bundle settingargs = new Bundle();
		settingargs.putString("title", "设置页");
		settingFragment.setArguments(settingargs);
		((BaseFragment) settingFragment).setIconId(R.drawable.tab_my);
		((BaseFragment) settingFragment).setTitle("我的");
		mTabs.add(settingFragment);
	}

	class FragmentAdapter extends FragmentStatePagerAdapter implements
			IconPagerAdapter {
		private List<Fragment> mFragments;

		public FragmentAdapter(List<Fragment> fragments, FragmentManager fm) {
			super(fm);
			mFragments = fragments;
		}

		@Override
		public Fragment getItem(int i) {
			return mFragments.get(i);
		}

		@Override
		public int getIconResId(int index) {
			return ((BaseFragment) mFragments.get(index)).getIconId();
		}

		@Override
		public int getCount() {
			return mFragments.size();
		}

		@Override
		public CharSequence getPageTitle(int position) {
			return ((BaseFragment) mFragments.get(position)).getTitle();
		}
	}

	private void setOverflowShowingAlways() {
		try {
			// true if a permanent menu key is present, false otherwise.
			ViewConfiguration config = ViewConfiguration.get(getActivity());
			Field menuKeyField = ViewConfiguration.class
					.getDeclaredField("sHasPermanentMenuKey");
			menuKeyField.setAccessible(true);
			menuKeyField.setBoolean(config, false);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private DeviceListActivity getAct() {
		return (DeviceListActivity) getActivity();
	}

	public void showExitDialog() {
		Builder builder = new Builder(getAct());
		builder.setCancelable(false);
		builder.setMessage("退出程序吗？");
		builder.setTitle("提示");
		builder.setNegativeButton("确定",
				new android.content.DialogInterface.OnClickListener() {
					@Override
					public void onClick(DialogInterface dialog, int which) {
						dialog.dismiss();
						XlinkAgent.getInstance().stop();
						getAct().finish();
						System.exit(0);
					}
				});
		builder.setNeutralButton("取消", null);
		builder.create().show();
	}

}
