package io.xlink.wifi.ui.fragment;

import io.xlink.wifi.ui.activity.DeviceListActivity;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.youhone.xlink.superapp.R;

public class AddDeviceFragment extends BaseFragment implements OnClickListener {
	ImageView iv_add;

	public void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
	}

	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View view = inflater.inflate(R.layout.xlink_add_device_fragment,
				container, false);
		initWidget(view);
		return view;
	}

	private void initWidget(View view) {
		iv_add = (ImageView) view.findViewById(R.id.iv_add);

		iv_add.setOnClickListener(this);
	}

	private DeviceListActivity getAct() {
		return (DeviceListActivity) getActivity();
	}

	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub

		int id = v.getId();
		if (id == R.id.iv_add) {
			getAct().openLinkToWifi();
		}
		//
		// switch (id) {
		// case R.id.iv_add:
		// getAct().openLinkToWifi();
		//
		// // if (XlinkUtils.isWifi()) {
		// // } else {
		// // // MainActivity.act.openLinkToWifi();
		// // }
		//
		// break;
		//
		// }
	}

}
