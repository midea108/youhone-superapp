package io.xlink.wifi.ui.fragment;

import io.xlink.wifi.ui.activity.DeviceListActivity;
import io.xlink.wifi.ui.util.XlinkUtils;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.youhone.xlink.superapp.R;

public class LinkToWifiFragment extends BaseFragment implements OnClickListener {

	TextView tv_ssid;
	EditText et_pswd;
	Button b_search_wifi_device;
	Boolean isconnecting;
	View linklayout, errorlayout;

	public void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
	}

	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View view = inflater.inflate(R.layout.xlink_link_to_wifi, container,
				false);
		initWidget(view);
		return view;
	}

	@Override
	public void onResume() {
		// TODO Auto-generated method stub
		super.onResume();
		if (XlinkUtils.isWifi()) {
			tv_ssid.setText(getSSid());
			linklayout.setVisibility(View.VISIBLE);
			errorlayout.setVisibility(View.GONE);
		} else {
			linklayout.setVisibility(View.GONE);
			errorlayout.setVisibility(View.VISIBLE);
		}
		SharedPreferences sp=getAct().getSharedPreferences("wifi", Context.MODE_PRIVATE);
		et_pswd.setText(sp.getString(getSSid(), ""));
	}



	private void initWidget(View view) {

		errorlayout = view.findViewById(R.id.link_error);
		linklayout = view.findViewById(R.id.line_layout);
		tv_ssid = (TextView) view.findViewById(R.id.tv_ssid);
		et_pswd = (EditText) view.findViewById(R.id.et_pswd);
		b_search_wifi_device = (Button) view
				.findViewById(R.id.b_search_wifi_device);
		b_search_wifi_device.setOnClickListener(this);

	}

	private DeviceListActivity getAct() {
		return (DeviceListActivity) getActivity();
	}

	@Override
	public void onClick(View v) {
		int id = v.getId();
		if (id == R.id.b_search_wifi_device) {
			String pswd = et_pswd.getText().toString().trim();
			// pswd="789456xlink";
			// pswd="XT789456";
			SharedPreferences sp=getAct().getSharedPreferences("wifi", Context.MODE_PRIVATE);
			Editor edt=sp.edit();
			edt.putString("ssid", getSSid());
			edt.putString(getSSid(), pswd);
			edt.commit();
			if (pswd.equals("8888")) {
				getAct().configSmartLink(null, null, true);
			} else {
				getAct().configSmartLink(getSSid(), pswd, false);
			}
			
		}
	}

	private String getSSid() {
		WifiManager wm = (WifiManager) getAct().getSystemService(
				Context.WIFI_SERVICE);
		if (wm != null) {
			WifiInfo wi = wm.getConnectionInfo();
			if (wi != null) {
				String s = wi.getSSID();
				if (s.length() > 2 && s.charAt(0) == '"'
						&& s.charAt(s.length() - 1) == '"') {
					return s.substring(1, s.length() - 1);
				} else {
					return s;
				}
			}
		}
		return "";
	}

}
