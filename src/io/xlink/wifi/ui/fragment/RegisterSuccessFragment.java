package io.xlink.wifi.ui.fragment;

import io.xlink.wifi.ui.activity.LoginActiviy;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.youhone.xlink.superapp.R;

public class RegisterSuccessFragment extends BaseFragment implements
	OnClickListener {

    TextView tv_reg_success_email;
    Button b_reg_success_start;
    private String email;

    public void onCreate(Bundle savedInstanceState) {
	// TODO Auto-generated method stub
	super.onCreate(savedInstanceState);
    }

    public View onCreateView(LayoutInflater inflater, ViewGroup container,
	    Bundle savedInstanceState) {
	View view = inflater.inflate(R.layout.xlink_register_success_fragment, null);
	initWidget(view);
	return view;
    }

    private void initWidget(View view) {
	tv_reg_success_email = (TextView) view
		.findViewById(R.id.tv_reg_success_email);
	b_reg_success_start = (Button) view
		.findViewById(R.id.b_reg_success_start);

	tv_reg_success_email.setText(email);
	b_reg_success_start.setOnClickListener(this);
    }

    int appid;
    String authKey;

    public void setEmail(String uid, int appid, String authKey) {
	this.email = uid;
	this.appid = appid;
	this.authKey = authKey;

    }

    @Override
    public void onClick(View v) {
	// TODO Auto-generated method stub

	int id = v.getId();
	if(id==R.id.b_reg_success_start){
	    ((LoginActiviy) getActivity()).openDeviceList(appid, authKey,false);
	}
	// switch (id) {
	// case R.id.b_reg_success_start:
	// ((LoginActiviy) getActivity()).openDeviceList(appid, authKey);
	// break;
	// }
    }

}
