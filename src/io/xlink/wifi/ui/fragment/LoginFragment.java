package io.xlink.wifi.ui.fragment;

import io.xlink.wifi.ui.activity.LoginActiviy;
import io.xlink.wifi.ui.util.XlinkUtils;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.TextView;

import com.youhone.xlink.superapp.R;

public class LoginFragment extends BaseFragment implements OnClickListener {

	TextView tv_forget_password;
	EditText et_login_email;
	EditText et_login_password;
	Button b_login, b_register;
	private CheckBox rm_pw;

	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View view = inflater.inflate(R.layout.fragment_login, container, false);
		initWidget(view);
		return view;
	}

	private void initWidget(View view) {
		rm_pw=(CheckBox)view.findViewById(R.id.rm_pw);
		tv_forget_password = (TextView) view
				.findViewById(R.id.tv_forget_password);
		et_login_email = (EditText) view.findViewById(R.id.et_login_email);
		et_login_password = (EditText) view
				.findViewById(R.id.et_login_password);
		b_login = (Button) view.findViewById(R.id.b_login);
		b_register = (Button) view.findViewById(R.id.b_register);
		b_login.setOnClickListener(this);
		b_register.setOnClickListener(this);
		tv_forget_password.setOnClickListener(this);
	}

	private LoginActiviy getAct() {
		return (LoginActiviy) getActivity();
	}

	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		int id = v.getId();
		if (id == R.id.b_login) {
			String str_email = et_login_email.getText().toString();
			String str_password = et_login_password.getText().toString();
			if (XlinkUtils.isString(str_email)
					&& XlinkUtils.isString(str_password)) {
				// if (!XlinkUtils.checkEmail(str_email)) {
				// XlinkUtils.shortTips("请输入正确的邮箱格式");
				// return;
				// }

				getAct().getAppid(str_email, str_password,rm_pw.isChecked());// 获取appid
			} else {
				XlinkUtils.showShortToast("请填写完整");
			}
		} else if (id == R.id.tv_forget_password) {
			getAct().openForget();
		} else if (id == R.id.b_register) {
			getAct().openRegister();
		}
		// switch (id) {
		// case R.id.b_login:
		// String str_email = et_login_email.getText().toString();
		// String str_password = et_login_password.getText().toString();
		// if (XlinkUtils.isString(str_email)
		// && XlinkUtils.isString(str_password)) {
		// getAct().getAppid(str_email, str_password);// 获取appid
		// } else {
		// XlinkUtils.showShortToast("请填写完整");
		// }
		//
		// break;
		//
		// case R.id.tv_forget_password:
		// getAct().openForget();
		// break;
		// }
	}

}
