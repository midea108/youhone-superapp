package io.xlink.wifi.ui.fragment;

import io.xlink.wifi.ui.MyApp;
import io.xlink.wifi.ui.activity.DeviceListActivity;
import io.xlink.wifi.ui.http.HttpAgent;
import io.xlink.wifi.ui.util.XlinkUtils;
import io.xlink.wifi.ui.view.CustomDialog;

import org.apache.http.Header;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.loopj.android.http.TextHttpResponseHandler;
import com.youhone.xlink.superapp.R;

public class Change_PW_Frag extends BaseFragment implements OnClickListener {
	private EditText edt_account, edt_old_password, edt_new_password1,
			edt_new_password2;
	private String account, old_password, new_password1, new_password2;
	private Button change;
	private ProgressDialog mProgressDialog;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View view = inflater.inflate(R.layout.change_pw, container, false);
		initView(view);
		return view;
	}

	private void initView(View view) {
		edt_account = (EditText) view.findViewById(R.id.edt_account);
		edt_old_password = (EditText) view.findViewById(R.id.edt_old_password);
		edt_new_password1 = (EditText) view
				.findViewById(R.id.edt_new_password1);
		edt_new_password2 = (EditText) view
				.findViewById(R.id.edt_new_password2);
		change = (Button) view.findViewById(R.id.b_change);
		change.setOnClickListener(this);
		mProgressDialog = CustomDialog.createProgressDialog(getActivity(),
				"请稍等", "正在提交");
		mProgressDialog.setCanceledOnTouchOutside(true);
		mProgressDialog.dismiss();
	}

	@Override
	public void onClick(View v) {
		int id = v.getId();
		if (id == R.id.b_change) {
			mProgressDialog.show();
			account = edt_account.getText().toString();
			old_password = edt_old_password.getText().toString();
			new_password1 = edt_new_password1.getText().toString();
			new_password2 = edt_new_password2.getText().toString();
			if (account.equals("") || old_password.equals("")
					|| new_password1.equals("") || new_password2.equals("")) {
				mProgressDialog.dismiss();
				Toast.makeText(getActivity(), "请填写完整的信息", Toast.LENGTH_SHORT)
						.show();
			} else if (!new_password1.equals(new_password2)) {
				mProgressDialog.dismiss();
				Toast.makeText(getActivity(), "新密码两次输入不正确", Toast.LENGTH_SHORT)
						.show();
			} else {
				checkPW();
			}
		}

	}

	private void checkPW() {
		HttpAgent.getInstance().getAppId(account, old_password,
				new TextHttpResponseHandler() {

					@Override
					public void onSuccess(int arg0, Header[] arg1, String msg) {

						JSONObject object;
						try {
							object = new JSONObject(msg);
							int status = object.getInt("status");
							if (status != 200) {
								mProgressDialog.dismiss();
								Toast.makeText(getActivity(), "用户名/密码错误1",
										Toast.LENGTH_SHORT).show();
								return;
							}
							JSONObject value = object.getJSONObject("user");
							String appid = value.getInt("id") + "";
							System.out.println(appid);
							System.out.println(MyApp.getApp().getAppid());
							if (!appid.trim().equals(MyApp.getApp().getAppid()+"")) {
								mProgressDialog.dismiss();
								Toast.makeText(getActivity(), "用户名/密码错误2",
										Toast.LENGTH_SHORT).show();
								return;
							}
							changePW();
						} catch (JSONException e) {
							mProgressDialog.dismiss();
							e.printStackTrace();
							XlinkUtils.shortTips("用户信息，json解析错误1");
						}

					}

					@Override
					public void onFailure(int arg0, Header[] arg1, String arg2,
							Throwable arg3) {
						mProgressDialog.dismiss();
						Toast.makeText(getActivity(), "网络异常1",
								Toast.LENGTH_SHORT).show();
					}

				});
	}

	private void changePW() {
		HttpAgent.getInstance().onChange(account, "",
				new TextHttpResponseHandler() {

					@Override
					public void onSuccess(int arg0, Header[] arg1, String msg) {
						JSONObject object;
						System.out.println("返回的");
						System.out.println(msg);
						try {
							object = new JSONObject(msg);
							int status = object.getInt("status");
							if (status == 200) {
								mProgressDialog.dismiss();
								Toast.makeText(getActivity(), "修改成功",
										Toast.LENGTH_SHORT).show();
								return;
							} else {
								mProgressDialog.dismiss();
								Toast.makeText(getActivity(), "修改失败2",
										Toast.LENGTH_SHORT).show();
							}

						} catch (JSONException e) {
							mProgressDialog.dismiss();
							e.printStackTrace();
							XlinkUtils.shortTips("用户信息，json解析错误2");
						}

					}

					@Override
					public void onFailure(int arg0, Header[] arg1, String arg2,
							Throwable arg3) {
						mProgressDialog.dismiss();
						Toast.makeText(getActivity(), "网络异常2",
								Toast.LENGTH_SHORT).show();

					}
				});
	}

	private DeviceListActivity getAct() {
		return (DeviceListActivity) getActivity();
	}
}
