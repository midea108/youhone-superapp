package io.xlink.wifi.ui.fragment;

import io.xlink.wifi.sdk.XDevice;
import io.xlink.wifi.sdk.XlinkAgent;
import io.xlink.wifi.sdk.XlinkCode;
import io.xlink.wifi.sdk.listener.SetDeviceAccessKeyListener;
import io.xlink.wifi.sdk.listener.SubscribeDeviceListener;
import io.xlink.wifi.ui.activity.BaseFragmentActivity;
import io.xlink.wifi.ui.activity.DeviceListActivity;
import io.xlink.wifi.ui.bean.Device;
import io.xlink.wifi.ui.manage.DeviceManage;
import io.xlink.wifi.ui.util.XlinkUtils;
import io.xlink.wifi.ui.view.CustomDialog;
import android.app.Dialog;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.youhone.xlink.superapp.R;

public class DevicePasswordFragment extends BaseFragment implements
		OnClickListener {
	private FragmentDeviceAuthorizeListener listener;

	public void setDevicePasswordlistener(FragmentDeviceAuthorizeListener l) {
		listener = l;
	}

	public Device device;
	private EditText et_device_password;
	private Button b_device_password_next;
	private TextView tv_device_password_title;
	private TextView tv_device_password_content;

	public void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
	}

	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View view = inflater.inflate(R.layout.xlink_device_password_fragment,
				container, false);
		initWidget(view);
		return view;
	}

	private void initWidget(View view) {
		et_device_password = (EditText) view
				.findViewById(R.id.et_device_password);
		b_device_password_next = (Button) view
				.findViewById(R.id.b_device_password_next);
		tv_device_password_title = (TextView) view
				.findViewById(R.id.tv_device_password_title);
		tv_device_password_content = (TextView) view
				.findViewById(R.id.tv_device_password_content);
		// 是否初始化
		if (device.getXDevice().isInit()) {
			tv_device_password_title.setText("请输入密码");
			tv_device_password_content.setText("设备已有授权码,请输入设备的授权码");
		} else {
			tv_device_password_title.setText("请输入设备初始化授权码");
			tv_device_password_content.setText("为设备创建访问授权码，密码不小于4位，请牢记您的密码。");
		}

		b_device_password_next.setOnClickListener(this);
	}

	private Dialog progressDialog;

	@Override
	public void onClick(View v) {

		int id = v.getId();

		if (id == R.id.b_device_password_next) {
			int password;
			try {
				password = Integer.parseInt(et_device_password.getText()
						.toString().trim());
			} catch (Exception e) {
				XlinkUtils.shortTips("请输入设备密码，密码小于900000000");
				return;
			}

			// if (!XlinkUtils.isString(password)) {
			// XlinkUtils.showShortToast("请输入密码");
			// return;
			// }
			String title = "";
			String msg = "";
			String tips = "";
			int ret = setDevicePassword(password);
			if (ret < 0) {
				tips = device.getXDevice().isInit() ? "认证设备失败" + ret
						: "设置初始密码失败" + ret;
				XlinkUtils.showShortToast(tips);
			} else {
				if (device.getXDevice().isInit()) {
					title = "认证设备";
					msg = "正在认证设备授权码,请稍后...";
				} else {
					title = "设置初始授权码";
					msg = "正在设置初始授权码,请稍后...";
				}
			}
			progressDialog = ((BaseFragmentActivity) getActivity())
					.createProgressDialog(title, msg);
		}

		// switch (id) {
		// case R.id.b_device_password_next:
		// String password = et_device_password.getText().toString().trim();
		// if (!XlinkUtils.isString(password)) {
		// XlinkUtils.showShortToast("请输入密码");
		// return;
		// }
		// String title = "";
		// String msg = "";
		// String tips = "";
		// int ret = setDevicePassword(password);
		// if (ret < 0) {
		// tips = device.getXDevice().isInit() ? "认证设备失败" + ret
		// : "设置初始密码失败" + ret;
		// XlinkUtils.showShortToast(tips);
		// } else {
		// if (device.getXDevice().isInit()) {
		// title = "认证设备";
		// msg = "正在认证设备授权码,请稍后...";
		// } else {
		// title = "设置初始授权码";
		// msg = "正在设置初始授权码,请稍后...";
		// }
		// }
		// progressDialog = ((BaseFragmentActivity) getActivity())
		// .createProgressDialog(title, msg);
		//
		// break;
		// }
	}

	private void linkDeviceError(String msg) {
		CustomDialog dialog = ((BaseFragmentActivity) getActivity())
				.createTipsDialog("设备认证", msg);
		dialog.show();
	}

	private int setDevicePassword(final int password) {
		int ret = XlinkAgent.getInstance().setDeviceAccessKey(
				device.getXDevice(), password,
				new SetDeviceAccessKeyListener() {
					@Override
					public void onSetLocalDeviceAccessKey(XDevice xdevice,
							int code, int msgId) {
						if (progressDialog != null) {
							progressDialog.dismiss();
						}
						switch (code) {
						case XlinkCode.SUCCEED:
							SUCCEED(xdevice, password);
							break;
						case XlinkCode.SERVER_CODE_UNAUTHORIZED:
							XlinkAgent.getInstance().subscribeDevice(xdevice,
									password, new SubscribeDeviceListener() {

										@Override
										public void onSubscribeDevice(
												XDevice xdevice, int code) {

											// stub
											switch (code) {
											case XlinkCode.SUCCEED:
												SUCCEED(xdevice, password);
												break;
											default:
												fail(code);
												break;
											}

										}
									});

							break;
						default:
							fail(code);
							break;
						}
						Log("设置默认密码:" + code);
					}
				});

		return ret;
	}

	private void SUCCEED(XDevice xd, int pwd) {
		XlinkUtils.shortTips("认证设备成功");
		device.setxDevice(xd);
		// device.setPassword(pwd);
		DeviceManage.getInstance().addDevice(xd);

		if (getActivity() instanceof DeviceListActivity) {
			// ((DeviceListActivity) getActivity()).openDeviceActiviy();
		} else {
			if (listener != null) {
				listener.onSetLocalDeviceAuthorizeCode(xd, 0);
			}
		}
	}

	private void fail(int code) {
		String tips = "";
		if (getActivity() instanceof DeviceListActivity) {
			if (device.getXDevice().isInit()) {
				tips = "设备认证失败,错误码：" + code;

			} else {
				tips = "设置初始密码失败,错误码：" + code;
			}
		} else {
			tips = "设备认证失败,错误码：" + code;
		}
		linkDeviceError(tips);
	}

	public interface FragmentDeviceAuthorizeListener {
		void onSetLocalDeviceAuthorizeCode(XDevice xdevice, int code);
	}
}
