package io.xlink.wifi.ui.fragment;

import java.io.IOException;
import java.io.UnsupportedEncodingException;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;
import org.json.JSONException;
import org.json.JSONObject;

import android.content.Context;
import android.os.Message;
import android.util.Log;

public class Http_Utils {
	private static final String TAG = "HTTP";
	private HttpClient httpClient;
	// 请求
	private HttpPost httpPost;
	private Context mcontext;

	public Http_Utils(Context context) {
		mcontext = context;
	}

	/**
	 * 上传数据 传进去必要的数据，其他数据从类里面拿
	 */
	public int uploadData(String data,String url) {
		final JSONObject extra = new JSONObject();
		try {
			
			extra.put("data", data);

			httpClient = new DefaultHttpClient();
			httpPost = new HttpPost(url);

			Log.d(TAG, "上传的     "+extra.toString());
			StringEntity postEntity = new StringEntity(extra.toString());
			httpPost.setEntity(postEntity);
			HttpResponse reponse = httpClient.execute(httpPost);
			HttpEntity entity = reponse.getEntity();
			String str = EntityUtils.toString(entity);
			Log.d(TAG, ("返回的:    " + str));
			if (reponse.getStatusLine().getStatusCode() == 200) {
				System.out.println("成功");
			}
			return reponse.getStatusLine().getStatusCode();

		} catch (JSONException e) {

			e.printStackTrace();

			return -1;
		} catch (UnsupportedEncodingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return -1;
		} catch (ClientProtocolException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return -1;
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return -1;
		}
	}
}
