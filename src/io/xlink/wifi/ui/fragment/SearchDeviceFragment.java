package io.xlink.wifi.ui.fragment;

import io.xlink.wifi.sdk.XDevice;
import io.xlink.wifi.sdk.XlinkAgent;
import io.xlink.wifi.sdk.XlinkCode;
import io.xlink.wifi.sdk.listener.ScanDeviceListener;
import io.xlink.wifi.sdk.listener.SetDeviceAccessKeyListener;
import io.xlink.wifi.sdk.listener.SubscribeDeviceListener;
import io.xlink.wifi.ui.Constant;
import io.xlink.wifi.ui.activity.BaseFragmentActivity;
import io.xlink.wifi.ui.activity.DeviceListActivity;
import io.xlink.wifi.ui.adapter.DeviceListAdapter;
import io.xlink.wifi.ui.bean.Device;
import io.xlink.wifi.ui.db.Xlink_Db_Helper;
import io.xlink.wifi.ui.http.HttpAgent;
import io.xlink.wifi.ui.manage.DeviceManage;
import io.xlink.wifi.ui.util.SharedPreferencesUtil;
import io.xlink.wifi.ui.util.XlinkUtils;
import io.xlink.wifi.ui.view.CustomDialog;

import java.util.ArrayList;

import org.apache.http.Header;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.Dialog;
import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.os.Handler;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.Button;
import android.widget.ListView;

import com.loopj.android.http.TextHttpResponseHandler;
import com.youhone.xlink.superapp.R;

public class SearchDeviceFragment extends BaseFragment implements
		OnClickListener, OnItemClickListener {
	private static final int SCAN = 100;
	private ListView lv_device;
	private DeviceListAdapter adapter;
	private Button b_search_device, b_add_device;
	private Device device;
	private FragmentDeviceAuthorizeListener listener;
	private Dialog searchDialog;

	private Handler scanHandler = new Handler() {
		public void handleMessage(android.os.Message msg) {
			switch (msg.what) {
			case SCAN:
				searchDialog.dismiss();
				break;
			}
		}
	};

	public void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
	}

	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View view = inflater.inflate(R.layout.xlink_search_device_fragment,
				container, false);
		initWidget(view);
		return view;
	}

	@Override
	public void onResume() {
		super.onResume();
		updataDeviceListView();
		scan();
	}

	@Override
	public void onDestroyView() {
		super.onDestroyView();
		adapter = null;
	}

	private void initWidget(View view) {
		searchDialog = ((BaseFragmentActivity) getActivity())
				.createProgressDialog("", "正在搜索设备，请稍等");
		devices.clear();
		lv_device = (ListView) view.findViewById(R.id.lv_device);
		lv_device.setOnItemClickListener(this);
		updataDeviceListView();
		b_search_device = (Button) view.findViewById(R.id.b_search_device);
		b_add_device = (Button) view.findViewById(R.id.b_add_device);
		b_search_device.setOnClickListener(this);
		b_add_device.setOnClickListener(this);
		searchDialog.show();
		searchDialog.setCanceledOnTouchOutside(true);
	}

	private void scan() {
		if (!searchDialog.isShowing()) {
			searchDialog.show();
		}

		int ret[] = new int[Constant.PRODUCTID.length];
		for (int i = 0; i < ret.length; i++) {
			ret[i] = XlinkAgent.getInstance().scanDeviceByProductId(
					Constant.PRODUCTID[i], scanListener);
			if (ret[i] < 0) {
				searchDialog.dismiss();
				switch (ret[i]) {
				case XlinkCode.NO_CONNECT_SERVER:
					if (XlinkUtils.isWifi()) {
						XlinkAgent.getInstance().start();
					}
					break;
				case XlinkCode.NETWORD_UNAVAILABLE:
					break;
				default:
					break;
				}
			}
		}
		scanHandler.sendEmptyMessageDelayed(SCAN, 2000);
	}

	ArrayList<Device> devices = new ArrayList<Device>();
	private ScanDeviceListener scanListener = new ScanDeviceListener() {

		@Override
		public void onGotDeviceByScan(XDevice xdevice) {
			Device dev = new Device(xdevice);
			System.out.println("搜索到的设备MAC" + dev.getMacAddress());
			System.out.println("搜索到的设备PID" + xdevice.getProductId());
			if (!devices.contains(dev)) {
				devices.add(dev);
			}
			updataDeviceListView();
		}
	};

	private DeviceListActivity getAct() {
		return (DeviceListActivity) getActivity();
	}

	@Override
	public void onClick(View v) {
		int id = v.getId();
		if (id == R.id.b_search_device) {
			scan();
		} else if (id == R.id.b_add_device) {
			getAct().openLinkToWifi();
		}
	}

	public void onItemClick(AdapterView<?> parent, View view, int position,
			long id) {
		// currentDevice = devices.get(position);
		device = devices.get(position);
		if (device.getXDevice().isInit()) {
			setDevicePassword(Constant.password);
		} else {
			setDevicePassword(Constant.password);
		}
	}

	/**
	 * 进行密码连接或设置初始密码
	 * 
	 * @param password
	 * @return
	 */
	private int setDevicePassword(final int password) {
		int ret = XlinkAgent.getInstance().setDeviceAccessKey(
				device.getXDevice(), password,
				new SetDeviceAccessKeyListener() {
					@Override
					public void onSetLocalDeviceAccessKey(XDevice xdevice,
							int code, int msgId) {
						switch (code) {
						case XlinkCode.SUCCEED:
							SUCCEED(xdevice, password);
							break;
						case XlinkCode.SERVER_CODE_UNAUTHORIZED:
							XlinkAgent.getInstance().subscribeDevice(xdevice,
									password, new SubscribeDeviceListener() {

										@Override
										public void onSubscribeDevice(
												XDevice xdevice, int code) {
											switch (code) {
											case XlinkCode.SUCCEED:
												SUCCEED(xdevice, password);
												break;
											default:
												fail(code);
												break;
											}
										}
									});

							break;
						default:
							fail(code);
							break;
						}
						Log("设置默认密码:" + code);
					}
				});

		return ret;
	}

	/**
	 * 认证成功
	 * 
	 * @param xd
	 * @param pwd
	 */
	private void SUCCEED(XDevice xd, int pwd) {

		XlinkUtils.shortTips("认证设备成功");

		device.setxDevice(xd);
		// device.setPassword(pwd);
		DeviceManage.getInstance().clearAllDevice();
		DeviceManage.getInstance().addDevice(xd);

		new Thread(new Runnable() {
			@Override
			public void run() {
				uploadDevice();
			}
		}).run();

		if (getActivity() instanceof DeviceListActivity) {
			for (int i = 0; i < Constant.PRODUCTID.length; i++) {
				if (device.getXDevice().getProductId()
						.equals(Constant.PRODUCTID[i])) {
					((DeviceListActivity) getActivity())
							.openSearchDeviceActiviy(i, true);
					break;
				}
			}

		} else {
			if (listener != null) {
				listener.onSetLocalDeviceAuthorizeCode(xd, 0);
			}
		}
	}

	/**
	 * 上传设备至后台数据库
	 */
	private void uploadDevice() {

		final int appid = SharedPreferencesUtil.queryIntValue(Constant.APP_ID);
		String id = "";

		// JSONObject data = new JSONObject();
		final XDevice xdevice = device.getXDevice();
		final JSONObject data = XlinkAgent.deviceToJson(xdevice);
		try {
			data.put("appid", appid);
			id = appid + "#" + xdevice.getMacAddress();
			data.put("id", xdevice.getMacAddress());
			XlinkAgent.getInstance().initDevice(xdevice);
			for (int i = 0; i < Constant.PRODUCTID.length; i++) {
				if (xdevice.getProductId().equals(Constant.PRODUCTID[i])) {
					data.put("type", Constant.PRODUCT_NAME[i]);
					data.put("device_name", Constant.PRODUCT_NAME[i]);
				}
			}
			xdevice.setAuthkey(SharedPreferencesUtil
					.queryValue(Constant.APP_KEY));

		} catch (JSONException e) {
			e.printStackTrace();
		}
		HttpAgent.getInstance().putData(Constant.TABLE_USER, data,
				new TextHttpResponseHandler() {

					@Override
					public void onFailure(int arg0, Header[] arg1, String arg2,
							Throwable arg3) {
						// TODO Auto-generated method stub
						// XlinkUtils.shortTips("修改失败");
					}

					@Override
					public void onSuccess(int arg0, Header[] arg1, String result) {
						// XlinkUtils.shortTips("修改成功");
						String id = appid + "#" + xdevice.getMacAddress();
						boolean isExist = false;
						Xlink_Db_Helper mDb_Helper = new Xlink_Db_Helper(
								getActivity());
						SQLiteDatabase db = mDb_Helper.getWritableDatabase();
						ContentValues values = new ContentValues();
						String query = "Select * from device_db where id like ?";
						Cursor mCursor = db.rawQuery(query,
								new String[] { appid + "#%" });
						try {
							data.put("protocol", 1);
							values.put("data", data.toString());
						} catch (JSONException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
							return;
						}
						while (mCursor.moveToNext()) {
							if (mCursor.getString(mCursor.getColumnIndex("id"))
									.equals(id)) {
								isExist = true;
								break;
							}
						}
						if (isExist) {
							db.update("device_db", values, "id=?",
									new String[] { id });
						} else {
							values.put("id", id);
							db.insert("device_db", null, values);
						}
						db.close();
						mDb_Helper.close();
					}
				}, id);

	}

	public interface FragmentDeviceAuthorizeListener {
		void onSetLocalDeviceAuthorizeCode(XDevice xdevice, int code);
	}

	private void fail(int code) {
		String tips = "";
		if (getActivity() instanceof DeviceListActivity) {
			if (device.getXDevice().isInit()) {
				tips = "设备认证失败,错误码：" + code;

			} else {
				tips = "设置初始密码失败,错误码：" + code;
			}
		} else {
			tips = "设备认证失败,错误码：" + code;
		}
		linkDeviceError(tips);
	}

	private void linkDeviceError(String msg) {
		CustomDialog dialog = ((BaseFragmentActivity) getActivity())
				.createTipsDialog("设备认证", msg);
		dialog.show();
	}

	private void updataDeviceListView() {
		if (adapter == null) {
			adapter = new DeviceListAdapter(getAct(), this.devices);
			lv_device.setAdapter(adapter);
		} else {
			System.out.println("device.size   " + devices.size());
			adapter.setDevices(devices);
			adapter.notifyDataSetInvalidated();
			searchDialog.dismiss();
		}

	}

}
