package io.xlink.wifi.ui;

import io.xlink.wifi.sdk.XDevice;
import io.xlink.wifi.sdk.XlinkAgent;
import io.xlink.wifi.sdk.XlinkCode;
import io.xlink.wifi.sdk.bean.EventNotify;
import io.xlink.wifi.sdk.listener.XlinkNetListener;
import io.xlink.wifi.sdk.util.MyLog;
import io.xlink.wifi.ui.activity.DeviceListActivity;
import io.xlink.wifi.ui.activity.LoginActiviy;
import io.xlink.wifi.ui.bean.Device;
import io.xlink.wifi.ui.bean.LocalInfo;
import io.xlink.wifi.ui.bean.NetType;
import io.xlink.wifi.ui.db.CityManage;
import io.xlink.wifi.ui.db.DBManageImpl;
import io.xlink.wifi.ui.http.HttpAgent;
import io.xlink.wifi.ui.manage.DeviceManage;
import io.xlink.wifi.ui.util.CrashHandler;
import io.xlink.wifi.ui.util.SharedPreferencesUtil;
import io.xlink.wifi.ui.util.XlinkUtils;
import io.xlink.wifi.ui.view.CustomDialog;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EncodingUtils;

import android.app.Activity;
import android.app.Application;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.PackageManager.NameNotFoundException;
import android.content.res.TypedArray;
import android.net.ConnectivityManager;
import android.net.NetworkInfo.State;
import android.net.wifi.WifiManager;
import android.os.AsyncTask;
import android.os.Handler;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import cn.sharesdk.framework.ShareSDK;
import cn.smssdk.SMSSDK;

import com.youhone.activity.MainActivity;

public class MyApp extends Application implements XlinkNetListener {

	private static final String TAG = "MyApp";
	private static MyApp application;
	/**
     */
	public static SharedPreferences sharedPreferences;
	// 判断程序是否正常启动
	public boolean auth;
	public static String DEVICE_ACTIVITY = "";
	// 接受系统网络广播
	private MsgReceiver msgReceiver;

	@Override
	public void onCreate() {
		super.onCreate();
		application = this;
		auth = false;
		Log.e(TAG, "Application onCreate()");
		ShareSDK.initSDK(this, "d773b145b480");
		SMSSDK.initSDK(this, "d370c9a82403", "ebab06cdd94ef36adcbb2aa92c973c64");
		// bug收集
		CrashHandler.init(this);
		// 初始化sdk
		XlinkAgent.init(this);
		XlinkAgent.getInstance().addXlinkListener(this);
		// 首�?�?用于存储用户
		sharedPreferences = getSharedPreferences("XlinkOfficiaDemo",
				Context.MODE_PRIVATE);
		appid = SharedPreferencesUtil.queryIntValue("appId");
		authKey = SharedPreferencesUtil.queryValue("authKey", "");
		initHandler();
		// for (Device device : DeviceManage.getInstance().getDevices()) {//
		// 向sdk
		// MyLog.e(TAG, "init device:" + device.getMacAddress());
		// XlinkAgent.getInstance().initDevice(device.getXDevice());
		// }
		// 获取当前软件包版本号和版本名�?
		try {
			PackageInfo pinfo = getPackageManager().getPackageInfo(
					getPackageName(), 0);
			versionCode = pinfo.versionCode;
			versionName = pinfo.versionName;
			packageName = pinfo.packageName;
			ApplicationInfo appInfo = getPackageManager().getApplicationInfo(
					getPackageName(), PackageManager.GET_META_DATA);
			Constant.WEATHER_APPKEY = appInfo.metaData
					.getString("WEATHER_APPKEY");
			// 通过manifest 里配置的meta-data 获取 secret key 和 access id和pid
			HttpAgent.SECRET_KEY = appInfo.metaData
					.getString("XLINK_SECRETKEY");
			HttpAgent.ACCESS_ID = appInfo.metaData.getString("XLINK_ACCESSID");
			Constant.PRODUCTID = getResources().getStringArray(
					appInfo.metaData.getInt("PRODUCTID"));
			Constant.PRODUCT_NAME = getResources().getStringArray(
					appInfo.metaData.getInt("PRODUCT_NAME"));
			Constant.TABLE_USER = appInfo.metaData.getString("TABLE_NAME");
			int online = appInfo.metaData.getInt("PRODUCT_ICO_ONLINE");
			int offline = appInfo.metaData.getInt("PRODUCT_ICO_OFFLINE");

			TypedArray onlinePic = getResources().obtainTypedArray(online);
			TypedArray offlinePic = getResources().obtainTypedArray(offline);

			Constant.PRODUCT_ICO_ONLINE = new int[onlinePic.length()];
			Constant.PRODUCT_ICO_OFFLINE = new int[offlinePic.length()];
			for (int i = 0; i < onlinePic.length(); i++) {
				Constant.PRODUCT_ICO_ONLINE[i] = onlinePic.getResourceId(i, 0);
				Constant.PRODUCT_ICO_OFFLINE[i] = offlinePic
						.getResourceId(i, 0);
			}
			onlinePic.recycle();
			offlinePic.recycle();

		} catch (NameNotFoundException e) {
			throw new IllegalAccessError("请正确配置secret，access，pid");
		}
		if (Constant.PRODUCTID.length == 0
				|| TextUtils.isEmpty(HttpAgent.SECRET_KEY)
				|| TextUtils.isEmpty(HttpAgent.ACCESS_ID)
				|| (Constant.PRODUCTID.length != Constant.PRODUCT_ICO_ONLINE.length
						&& Constant.PRODUCTID.length != Constant.PRODUCT_NAME.length && Constant.PRODUCT_ICO_ONLINE.length != Constant.PRODUCT_ICO_OFFLINE.length)) {
			throw new IllegalAccessError("请正确配置secret，access，pid");
		}
		DEVICE_ACTIVITY = "io.xlink.wifi.ui.DeviceActivity";

		appid = SharedPreferencesUtil.queryIntValue(Constant.APP_ID);
		authKey = SharedPreferencesUtil.queryValue(Constant.APP_KEY);

		try {
			ApplicationInfo appInfo = getPackageManager().getApplicationInfo(
					getPackageName(), PackageManager.GET_META_DATA);

			String appkey = appInfo.metaData.getString("MOB_SMS_APP_KEY");
			String appSecrect = appInfo.metaData
					.getString("MOB_SMS_APP_SECRET");
			SMSSDK.initSDK(application, appkey, appSecrect);
		} catch (NameNotFoundException e) {
			throw new IllegalAccessError("请正确配置mob sms sdk key & secret");
		}

		msgReceiver = new MsgReceiver();
		IntentFilter intentFilter = new IntentFilter();
		intentFilter.addAction(ConnectivityManager.CONNECTIVITY_ACTION);
		intentFilter.addAction(WifiManager.RSSI_CHANGED_ACTION);
		registerReceiver(msgReceiver, intentFilter);
		new Thread(new Runnable() {

			@Override
			public void run() {
				loadAssetTxt();
			}
		}).start();

	}

	public String loadAssetTxt() {
		String res = "";
		InputStream in;
		try {
			in = getResources().getAssets().open("citycode.txt");
			int length = in.available();
			byte[] buffer = new byte[length];
			in.read(buffer);
			res = EncodingUtils.getString(buffer, "UTF-8");
		} catch (IOException e) {
			e.printStackTrace();
		}
		String[] ids = res.split("\n");
		CityManage dbManage = DBManageImpl.getDBInstance(this);
		for (String mid : ids) {
			// 如果城市没有添加过
			if (mid.split("=").length > 1) {
				String city = mid.split("=")[1];
				if (!dbManage.findCityByName(city)) {
					dbManage.addCity(city);
				} else {
					break;
				}
			}

		}
		return null;
	}

	private class MsgReceiver extends BroadcastReceiver {

		@Override
		public void onReceive(Context context, Intent intent) {
			State wifiState = null;
			State mobileState = null;
			ConnectivityManager cm = (ConnectivityManager) context
					.getSystemService(Context.CONNECTIVITY_SERVICE);
			wifiState = cm.getNetworkInfo(ConnectivityManager.TYPE_WIFI)
					.getState();
			mobileState = cm.getNetworkInfo(ConnectivityManager.TYPE_MOBILE)
					.getState();
			if (wifiState != null && mobileState != null
					&& State.CONNECTED != wifiState
					&& State.CONNECTED == mobileState) {
				// 手机网络连接成功
				new JudgeTask().execute();
			} else if (wifiState != null && State.CONNECTED == wifiState) {
				// 无线网络连接成功
				new JudgeTask().execute();
			} else if (wifiState != null && mobileState != null
					&& State.CONNECTED != wifiState
					&& State.CONNECTED != mobileState) {
				// 手机没有任何的网络
				LocalInfo.NetType = NetType.NO_NET;
			}
		}
	}

	private class JudgeTask extends AsyncTask<Void, Void, Boolean> {

		@Override
		protected Boolean doInBackground(Void... params) {
			List<NameValuePair> param = new ArrayList<NameValuePair>();
			HttpPost httpRequest = new HttpPost("http://www.baidu.com");
			try {
				httpRequest.setEntity(new UrlEncodedFormEntity(param, "GBK"));
				HttpResponse httpResponse = new DefaultHttpClient()
						.execute(httpRequest);
				System.out.println("ping返回====="
						+ httpResponse.getStatusLine().getStatusCode());
				if (httpResponse.getStatusLine().getStatusCode() == 200) {
					return true;
				} else {
					return false;
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
			return false;
		}

		@Override
		protected void onPostExecute(Boolean result) {
			if (result) {
				LocalInfo.NetType = NetType.NET;
			} else {
				LocalInfo.NetType = NetType.LAN;
			}
			super.onPostExecute(result);
		}

	}

	public void destroyExit() {
		XlinkAgent.getInstance().stop();
	}

	public void removeUser(Activity act) {
		SharedPreferencesUtil.keepShared(Constant.APP_ID, 0);
		SharedPreferencesUtil.keepShared(Constant.APP_KEY, "");
		appid = 0;
		XlinkAgent.getInstance().stop();
		authKey = "";
		currentActivity.startActivity(new Intent(currentActivity,
				LoginActiviy.class));
		if (currentActivity instanceof MainActivity) {
			System.out.println(exActivity instanceof DeviceListActivity);
			currentActivity.finish();
			exActivity.finish();
		} else {
			currentActivity.finish();
		}

	}

	public void login() {
		XlinkAgent.getInstance().addXlinkListener(this);
		XlinkAgent.getInstance().login(getAppid(), getAuth());
	}

	public String versionName;
	public int versionCode;
	public String packageName;
	private static Handler mainHandler = null;

	public static void initHandler() {
		mainHandler = new Handler();
	}

	/**
	 * 执行在主线程任务
	 * 
	 * @param runnable
	 */
	public static void postToMainThread(Runnable runnable) {
		mainHandler.post(runnable);
	}

	public static void postToMainThreadDelay(Runnable runnable, int time) {
		mainHandler.postDelayed(runnable, time);
	}

	public static MyApp getApp() {
		return application;
	}

	public boolean isLogin() {

		System.out.println("appid   " + appid);
		return appid != 0
				&& authKey != null
				&& SharedPreferencesUtil
						.queryBooleanValue(Constant.IS_REMEMBER);

	}

	// 全局登录�?appId 和auth
	private int appid;
	private String authKey;

	public void setAppid(int id) {
		appid = id;
	}

	public void setAuth(String auth) {
		this.authKey = auth;
	}

	public int getAppid() {
		return appid;
	}

	public String getAuth() {
		return authKey;
	}

	// 当前的activity
	private Activity currentActivity;
	private Activity exActivity;

	public Activity getCurrentActivity() {
		return currentActivity;
	}

	public void setCurrentActivity(Activity currentActivity) {

		exActivity = this.currentActivity;
		this.currentActivity = currentActivity;
		System.out.println("activity====="
				+ (exActivity instanceof DeviceListActivity));
	}

	// xlink 回调的onStart
	@Override
	public void onStart(int code) {
		// TODO Auto-generated method stub
		Log.e(TAG, "onStart code" + code);
		sendBroad(Constant.BROADCAST_ON_START, code);

	}

	// 回调登录xlink状�?
	@Override
	public void onLogin(int code) {
		// TODO Auto-generated method stub
		Log.e(TAG, "login code" + code);
		sendBroad(Constant.BROADCAST_ON_LOGIN, code);
		if (code == XlinkCode.SUCCEED) {
			// XlinkUtils.shortTips("云端网络已可用");
		} else if (code == XlinkCode.CLOUD_CONNECT_NO_NETWORK
				|| !XlinkUtils.isConnected()) {
			XlinkUtils.shortTips("网络不可用，请检查网络连接");
		} else if (code == XlinkCode.CLOUD_USER_EXTRUSION) {
			CustomDialog dialog = createTipsDialog("登录异常", "该账号已在其他地方登录！");
			dialog.show();
		} else if (code == XlinkCode.NO_INIT) {
			XlinkAgent.init(this);
		} else {
			XlinkUtils.shortTips("连接到服务器失败，请检查网络连接");
		}
	}

	@Override
	public void onLocalDisconnect(int code) {
		if (code == XlinkCode.LOCAL_SERVICE_KILL) {
			// 这里是xlink服务被异常终结了（第三方清理软件，或者进入应用管理被强制停止应用/服务�?
			// 永不结束的service
			// 除非调用 XlinkAgent.getInstance().stop（）;
			XlinkAgent.getInstance().start();
		}
		XlinkUtils.shortTips("本地网络已经断开");
	}

	public userExtresion mExtresion;

	public void setListener(userExtresion mExtresion) {
		this.mExtresion = mExtresion;
	}

	@Override
	public void onDisconnect(int code) {
		// if (code == XlinkCode.CLOUD_SERVICE_KILL) {
		// // 这里是服务被异常终结了（第三方清理软件，或�?进入应用管理被强制停止服务）
		// if (appid != 0 && !TextUtils.isEmpty(authKey)) {
		//
		// }
		// }
		if (code == XlinkCode.CLOUD_USER_EXTRUSION) {
			// CustomDialog dialog = createTipsDialog("登录异常", "该账号已在其他地方登录！");
			// dialog.show();
			mExtresion.onExtresion();
		} else {
			XlinkAgent.getInstance().login(appid, authKey);
			Log.d(TAG, "正在修复云端连接");
		}

		// XlinkUtils.shortTips("正在修复云端连接");
	}

	public interface userExtresion {
		public abstract void onExtresion();
	}

	/**
	 * 收到 �?��网设备推送的pipe数据
	 */
	@Override
	public void onRecvPipeData(XDevice xdevice, byte flags, byte[] data) {
		// TODO Auto-generated method stub
		Log.e(TAG, "onRecvPipeData::device:" + xdevice.toString() + "data:"
				+ data);

		Log.e(TAG, XlinkUtils.getHexBinString(data));

		Device device = DeviceManage.getInstance().getDevice(
				xdevice.getMacAddress());
		if (device != null) {
			// 发�?广播，那个activity�?��该数据可以监听广播，并获取数据，然后进行响应的处�?
			sendPipeBroad(Constant.BROADCAST_RECVPIPE, device, data);
		}
	}

	/**
	 * 收到设备通过云端服务器推送的pipe数据
	 */
	@Override
	public void onRecvPipeSyncData(XDevice xdevice, byte flags, byte[] data) {
		// TODO Auto-generated method stub
		Log.e(TAG, "onRecvPipeSyncData::device:" + xdevice.toString() + "data:"
				+ data);
		Device device = DeviceManage.getInstance().getDevice(
				xdevice.getMacAddress());
		if (device != null) {
			// 发�?广播，那个activity�?��该数据可以监听广播，并获取数据，然后进行响应的处�?
			// TimerManage.getInstance().parseByte(device,data);
			sendPipeBroad(Constant.BROADCAST_RECVPIPE, device, data);
		}
	}

	/**
	 * 
	 * @param 发
	 *            �? ：start/login广播
	 */
	public void sendBroad(String action, int code) {
		Intent intent = new Intent(action);
		intent.putExtra(Constant.STATUS, code);
		MyApp.this.sendBroadcast(intent);
	}

	/**
	 * 
	 * @param 发
	 *            �?pipe广播
	 */
	public void sendPipeBroad(String action, Device device, byte[] data) {
		Intent intent = new Intent(action);
		intent.putExtra(Constant.DEVICE_MAC, device.getMacAddress());
		if (data != null) {
			intent.putExtra(Constant.DATA, data);
		}
		MyApp.this.sendBroadcast(intent);
	}

	/**
	 * 设备状�?改变：掉�?重连/在线
	 */
	@Override
	public void onDeviceStateChanged(XDevice xdevice, int state) {

		MyLog.e(TAG, "onDeviceStateChanged::" + xdevice.getMacAddress()
				+ " state:" + state);
		Device device = DeviceManage.getInstance().getDevice(
				xdevice.getMacAddress());
		if (device != null) {
			device.setxDevice(xdevice);
			Intent intent = new Intent(Constant.BROADCAST_DEVICE_CHANGED);
			intent.putExtra(Constant.DEVICE_MAC, device.getMacAddress());
			intent.putExtra(Constant.STATUS, state);
			MyApp.getApp().sendBroadcast(intent);
		}

	}

	/**
	 * 设备数据端点改变
	 */
	@Override
	public void onDataPointUpdate(XDevice xDevice, int key, Object value,
			int channel, int type) {

		Device device = DeviceManage.getInstance().getDevice(
				xDevice.getMacAddress());
		if (device == null) {
			MyLog.e(TAG, "device == null point:" + xDevice.getMacAddress()
					+ " key: " + key + " value:" + value);
			return;
		}
		MyLog.e(TAG, "设备:" + xDevice.getMacAddress() + "端点 key: " + key
				+ " value:" + value);
		Intent intent = new Intent(Constant.BROADCAST_DEVICE_SYNC);
		intent.putExtra(Constant.TYPE, type);
		intent.putExtra(Constant.KEY, key);
		intent.putExtra(Constant.DEVICE_MAC, device.getMacAddress());
		switch (type) {
		case XlinkCode.POINT_TYPE_BOOLEAN:
			intent.putExtra(Constant.DATA, (Boolean) value);
			break;
		case XlinkCode.POINT_TYPE_BYTE:
			intent.putExtra(Constant.DATA, (Byte) value);
			break;
		case XlinkCode.POINT_TYPE_SHORT:
			intent.putExtra(Constant.DATA, (Short) value);
			break;
		case XlinkCode.POINT_TYPE_INT:
			intent.putExtra(Constant.DATA, (Integer) value);
			break;
		default:
			break;
		}
		MyApp.getApp().sendBroadcast(intent);
	}

	public CustomDialog createTipsDialog(String title, String tips) {
		CustomDialog dialog = CustomDialog.createErrorDialog(this, title, tips,
				new android.view.View.OnClickListener() {
					@Override
					public void onClick(View v) {
						removeUser(currentActivity);
					}
				});
		dialog.show();
		return dialog;
	}

	/**
	 * 应用内推送回调
	 */
	@Override
	public void onEventNotify(EventNotify arg0) {
		// TODO Auto-generated method stub

	}
}
