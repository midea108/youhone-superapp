package io.xlink.wifi.ui.adapter;

import io.xlink.wifi.ui.Constant;
import io.xlink.wifi.ui.bean.Device;
import io.xlink.wifi.ui.util.XlinkUtils;

import java.util.ArrayList;
import java.util.List;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.youhone.xlink.superapp.R;

public class DeviceListAdapter extends BaseAdapter {

	private Context mContext;
	private List<Device> devices;

	public DeviceListAdapter(Context context, ArrayList<Device> device) {

		this.mContext = context;
		this.devices = device;
	}

	public void setDevices(ArrayList<Device> devices) {
		this.devices = devices;
	}

	@Override
	public int getCount() {
		return devices.size();
	}

	@Override
	public Object getItem(int position) {
		if (devices.size() - 1 < position)
			return null;
		else
			return devices.get(position);
	}

	@Override
	public long getItemId(int position) {
		return position;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {

		if (convertView == null) {
			convertView = LayoutInflater.from(mContext).inflate(
					R.layout.devicelist_item, parent, false);
		}
		TextView textView = XlinkUtils.getAdapterView(convertView,
				R.id.deviceName);
		TextView mac = XlinkUtils.getAdapterView(convertView, R.id.deviceState);
		ImageView deviceImg = XlinkUtils.getAdapterView(convertView,
				R.id.deviceImg);
		if (devices != null && devices.size() > 0) {
			Device device = devices.get(position);
			for (int i = 0; i < Constant.PRODUCTID.length; i++) {
				if (device.getXDevice().getProductId()
						.equals(Constant.PRODUCTID[i])) {
					textView.setText(Constant.PRODUCT_NAME[i]);
					deviceImg.setImageResource(Constant.PRODUCT_ICO_ONLINE[i]);
					break;
				}
			}
			mac.setText("MAC: " + device.getMacAddress());
		}
		return convertView;

	}

}
