package io.xlink.wifi.ui.adapter;

import java.util.ArrayList;
import java.util.List;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.youhone.weatherform.WeatherForm;
import com.youhone.xlink.superapp.R;

public class WeatherAdapter extends BaseAdapter {

	private Context mContext;
	private List<WeatherForm[]> weatherlist;

	public WeatherAdapter(Context context, ArrayList<WeatherForm[]> weatherlist) {

		this.mContext = context;
		this.weatherlist = weatherlist;
	}

	public void setDevices(ArrayList<WeatherForm[]> weatherlist) {
		this.weatherlist = weatherlist;
	}

	@Override
	public int getCount() {
		return weatherlist.size();
	}

	@Override
	public Object getItem(int position) {
		return weatherlist.get(position);
	}

	@Override
	public long getItemId(int position) {
		return position;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		ViewHolder holder = null;
		if (convertView == null) {
			holder = new ViewHolder();
			convertView = LayoutInflater.from(mContext).inflate(
					R.layout.weather_item, parent, false);
			holder.city_name = (TextView) convertView
					.findViewById(R.id.city_name);
			holder.temp = (TextView) convertView.findViewById(R.id.temp);
			holder.wind = (TextView) convertView.findViewById(R.id.wind);
			holder.fx = (TextView) convertView.findViewById(R.id.fx);
			holder.pm25 = (TextView) convertView.findViewById(R.id.pm25);
			holder.air_quality = (TextView) convertView.findViewById(R.id.air_quality);
			holder.city_name.setText(weatherlist.get(position)[0].getName());
			holder.temp.setText(weatherlist.get(position)[0].getTemp() + "℃");
			holder.wind.setText(weatherlist.get(position)[0].getWind());
			holder.fx.setText(weatherlist.get(position)[0].getFx());
			holder.pm25.setText("PM2.5："+weatherlist.get(position)[0].getPm25());
			holder.air_quality.setText("空气质量："+weatherlist.get(position)[0].getQuality());
			convertView.setTag(holder);
		} else {
			holder = (ViewHolder) convertView.getTag();
		}

		return convertView;

	}

	class ViewHolder {
		public ImageView weather_img;
		public TextView city_name;
		public TextView temp;
		public TextView wind;
		public TextView fx;
		public TextView pm25;
		public TextView air_quality;
	}
}
