/**
 * 本机静态数据
 */
package io.xlink.wifi.ui.bean;

/**
 * @author PC_Mo
 * @time 2015-7-27下午2:42:18
 *
 */
public class LocalInfo {
	
	
	/**
	 * 0:没网络
	 * 1：移动数据
	 * 2：WIFI网络
	 */
	public static int NetType=-1;

}
