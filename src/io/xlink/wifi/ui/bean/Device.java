package io.xlink.wifi.ui.bean;

import java.io.Serializable;
import io.xlink.wifi.sdk.XDevice;
import io.xlink.wifi.sdk.XlinkCode;

public class Device implements Serializable {

    private boolean action;

    public boolean isAction() {
	return action;
    }

    public void setAction(boolean action) {
	this.action = action;
    }

    /**
     * 
     */
    private static final long serialVersionUID = 1L;
    // xlink 可识别的设备 实例
    private XDevice xDevice;
    // 设备授权码
    private String password;
    private int state = -1;

    public int getState() {
	return state;
    }

    private String name;

    public String getName() {
	return name;
    }

    public void setName(String name) {
	this.name = name;
    }

    public void setState(int state) {
	this.state = state;
    }

    public String getPassword() {
	return password;
    }

    public boolean isConnect() {
	if (xDevice.getDevcieConnectStates() == XlinkCode.DEVICE_STATE_OFFLIEN) {
	    return true;
	} else {
	    return false;
	}
    }

    @Override
    public String toString() {
	// TODO Auto-generated method stub
	return xDevice.toString() + " pwd:" + password;
    }

    public void setPassword(String password) {
	this.password = password;
    }

    public Device(XDevice xDevice) {
	this.xDevice = xDevice;
    }

    public String getMacAddress() {
	return xDevice.getMacAddress();
    }

    @Override
    public boolean equals(Object o) {
	// TODO Auto-generated method stub
	if (o instanceof Device) {
	    Device d = (Device) o;
	    return xDevice.equals(d.getXDevice());
	} else if (o instanceof XDevice) {
	    return xDevice.equals(o);
	}
	return super.equals(o);
    }

    public XDevice getXDevice() {
	return xDevice;
    }

    public void setxDevice(XDevice xDevice) {
	this.xDevice = xDevice;
    }

    public String getDName() {
	return xDevice.getDeviceName();
    }

    // /**
    // * 是否在线（不管公网，还是内网）
    // *
    // * @return
    // */
    // public boolean isOnline() {
    // //
    // return xDevice.getState() != ResponseCode.DEVICE_STATE_OFFLINE;
    // }
}
