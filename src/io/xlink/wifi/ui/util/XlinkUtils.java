package io.xlink.wifi.ui.util;

import io.xlink.wifi.ui.MyApp;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.security.MessageDigest;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;
import java.util.concurrent.ExecutionException;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.util.Base64;
import android.util.Log;
import android.util.SparseArray;
import android.view.View;
import android.widget.Toast;

public class XlinkUtils {

	/**
	 * 判断字符是否是中文
	 * 
	 * @param c
	 *            字符
	 * @return 是否是中文
	 */
	public static boolean isChinese(char c) {
		Character.UnicodeBlock ub = Character.UnicodeBlock.of(c);
		if (ub == Character.UnicodeBlock.CJK_UNIFIED_IDEOGRAPHS
				|| ub == Character.UnicodeBlock.CJK_COMPATIBILITY_IDEOGRAPHS
				|| ub == Character.UnicodeBlock.CJK_UNIFIED_IDEOGRAPHS_EXTENSION_A
				|| ub == Character.UnicodeBlock.GENERAL_PUNCTUATION
				|| ub == Character.UnicodeBlock.CJK_SYMBOLS_AND_PUNCTUATION
				|| ub == Character.UnicodeBlock.HALFWIDTH_AND_FULLWIDTH_FORMS) {
			return true;
		}
		return false;
	}

	/**
	 * 判断字符串是否是乱码
	 * 
	 * @param strName
	 *            字符串
	 * @return 是否是乱码
	 */
	public static boolean isMessyCode(String strName) {
		Pattern p = Pattern.compile("\\s*|\t*|\r*|\n*");
		Matcher m = p.matcher(strName);
		String after = m.replaceAll("");
		String temp = after.replaceAll("\\p{P}", "");
		char[] ch = temp.trim().toCharArray();
		float chLength = ch.length;
		float count = 0;
		for (int i = 0; i < ch.length; i++) {
			char c = ch[i];
			if (!Character.isLetterOrDigit(c)) {
				if (!isChinese(c)) {
					count = count + 1;
				}
			}
		}
		float result = count / chLength;
		if (result > 0.4) {
			return true;
		} else {
			return false;
		}

	}

	/**
	 * Map 转换为json
	 * 
	 * @param map
	 * @return
	 */
	public static JSONObject getJsonObject(Map<String, Object> map) {
		JSONObject jo = new JSONObject();
		Iterator<Entry<String, Object>> iter = map.entrySet().iterator();
		while (iter.hasNext()) {
			Entry<String, Object> entry = iter.next();
			try {
				jo.put(entry.getKey(), entry.getValue());
			} catch (JSONException e) {
				e.printStackTrace();
			}
		}
		return jo;

	}

	/**
	 * 验证邮箱
	 * 
	 * @param email
	 * @return
	 */
	public static boolean checkEmail(String email) {
		if (email.length() > 50) {
			return false;
		}
		boolean flag = false;
		try {
			String check = "^([a-z0-9A-Z]+[-|_|\\.]?)+[a-z0-9A-Z]@([a-z0-9A-Z]+(-[a-z0-9A-Z]+)?\\.)+[a-zA-Z]{2,}$";
			Pattern regex = Pattern.compile(check);
			Matcher matcher = regex.matcher(email);
			flag = matcher.matches();
		} catch (Exception e) {
			flag = false;
		}
		return flag;
	}

	public static String weekToString(ArrayList<Integer> weeks) {
		StringBuilder sb = new StringBuilder();
		if (weeks == null || weeks.size() == 0) {
			sb.append("单次");
		} else {
			sb.append("周 ");
			if (weeks.contains((Integer) 0)) {
				weeks.remove((Integer) 0);
				weeks.add(7);
			}

			Collections.sort(weeks);
			for (int i : weeks) {
				if (i == 0) {
					i = 7;
				}
				switch (i) {
				case 1:
					sb.append("一");
					break;
				case 2:
					sb.append("二");
					break;
				case 3:
					sb.append("三");
					break;
				case 4:
					sb.append("四");
					break;
				case 5:
					sb.append("五");
					break;
				case 6:
					sb.append("六");
					break;
				case 7:
					sb.append("日");

					break;
				default:
					break;
				}
			}
			if (weeks.contains((Integer) 7)) {
				weeks.remove((Integer) 7);
				weeks.add(0);
			}
		}
		return sb.toString();
	}

	public static int getDate(int h, int m) {
		return h * 60 + m;
	}

	// 秒转时间
	public static String getDateString(int sum) {
		// 求秒
		int s = sum % 60;
		int msum = (sum - s) / 60; // 100
		// 求分
		int mins = msum % 60; // 40
		// 小时
		int h = msum / 60;

		return String.format("%02d:%02d:%02d", h, mins, s);

	}

	private static Toast t;

	public static void showShortToast(String info) {
		if (t != null) {
			t.cancel();
		}
		t = Toast.makeText(MyApp.getApp(), info, Toast.LENGTH_SHORT);
		t.show();
	}

	public static boolean isString(String str) {
		if (str == null || str.equals("")) {
			return false;
		} else {
			return true;
		}
	}

	/**
	 * 截取 byte
	 * 
	 * @param src
	 *            源数据
	 * @param off
	 *            偏移量
	 * @param len
	 *            长度
	 * @return
	 */
	public static byte[] subBytes(byte[] bytes, int offset, int len) {
		byte[] b = new byte[len];

		System.arraycopy(bytes, offset, b, 0, len);
		return b;
	}

	/**
	 * BASE64解密
	 * 
	 * @param key
	 * @return
	 * @throws IOException
	 */
	public static byte[] base64Decrypt(String key) {
		byte[] bs = Base64.decode(key, Base64.DEFAULT);
		if (bs == null || bs.length == 0) {
			bs = key.getBytes();
		}
		return bs;
	}

	/**
	 * 判断网络是否连接
	 * 
	 * @param context
	 * @return
	 */
	public static boolean isConnected() {

		ConnectivityManager connectivity = (ConnectivityManager) MyApp.getApp()
				.getSystemService(Context.CONNECTIVITY_SERVICE);

		if (null != connectivity) {

			NetworkInfo info = connectivity.getActiveNetworkInfo();
			if (info != null && info.isAvailable()) {
				return true;
			}
		}
		return false;
	}

	public static String getHexBinString(byte[] bs) {
		StringBuffer log = new StringBuffer();
		for (int i = 0; i < bs.length; i++) {
			log.append(String.format("%02X", (byte) bs[i]) + " ");
		}
		return log.toString();
	}

	/**
	 * 把byte转化成 二进制.
	 * 
	 * @param aByte
	 * @return
	 */
	public static String getBinString(byte aByte) {
		String out = "";
		int i = 0;
		for (i = 0; i < 8; i++) {
			int v = (aByte << i) & 0x80;
			v = (v >> 7) & 1;
			out += v;
		}
		return out;
	}

	static private final int bitValue0 = 0x01; // 0000 0001
	static private final int bitValue1 = 0x02; // 0000 0010
	static private final int bitValue2 = 0x04; // 0000 0100
	static private final int bitValue3 = 0x08; // 0000 1000
	static private final int bitValue4 = 0x10; // 0001 0000
	static private final int bitValue5 = 0x20; // 0010 0000
	static private final int bitValue6 = 0x40; // 0100 0000
	static private final int bitValue7 = 0x80; // 1000 0000

	/**
	 * 设置flags
	 * 
	 * @param index
	 *            第几个bit，从零开始排
	 * @param value
	 *            byte值
	 * @return
	 */
	public static byte setByteBit(int index, byte value) {
		if (index > 7) {
			throw new IllegalAccessError("setByteBit error index>7!!! ");
		}
		byte ret = value;
		if (index == 0) {
			ret |= bitValue0;
		} else if (index == 1) {
			ret |= bitValue1;
		} else if (index == 2) {
			ret |= bitValue2;
		} else if (index == 3) {
			ret |= bitValue3;
		} else if (index == 4) {
			ret |= bitValue4;
		} else if (index == 5) {
			ret |= bitValue5;
		} else if (index == 6) {
			ret |= bitValue6;
		} else if (index == 7) {
			ret |= bitValue7;
		}
		return ret;
	}

	/**
	 * 读取 flags 里的小bit
	 * 
	 * @param anByte
	 * @param index
	 * @return
	 */
	public static boolean readFlagsBit(byte anByte, int index) {
		if (index > 7) {
			throw new IllegalAccessError("readFlagsBit error index>7!!! ");
		}
		int temp = anByte << (7 - index);
		temp = temp >> 7;
		temp &= 0x01;
		if (temp == 1) {
			return true;
		}
		// if((anByte & (01<<index)) !=0){
		// return true;
		// }
		return false;
	}

	/**
	 * 将16位的short转换成byte数组
	 * 
	 * @param s
	 *            short
	 * @return byte[] 长度为2
	 * */
	public static byte[] shortToByteArray(short s) {
		byte[] targets = new byte[2];
		for (int i = 0; i < 2; i++) {
			int offset = (targets.length - 1 - i) * 8;
			targets[i] = (byte) ((s >>> offset) & 0xff);
		}
		return targets;
	}

	@SuppressWarnings("unchecked")
	public static <T extends View> T getAdapterView(View convertView, int id) {
		SparseArray<View> viewHolder = (SparseArray<View>) convertView.getTag();
		if (viewHolder == null) {
			viewHolder = new SparseArray<View>();
			convertView.setTag(viewHolder);
		}
		View childView = viewHolder.get(id);
		if (childView == null) {
			childView = convertView.findViewById(id);
			viewHolder.put(id, childView);
		}
		return (T) childView;
	}

	public final static String MD5(String s) {
		char hexDigits[] = { '0', '1', '2', '3', '4', '5', '6', '7', '8', '9',
				'A', 'B', 'C', 'D', 'E', 'F' };
		try {
			byte[] btInput = s.getBytes();
			// 获得MD5摘要算法的 MessageDigest 对象
			MessageDigest mdInst = MessageDigest.getInstance("MD5");
			// 使用指定的字节更新摘要
			mdInst.update(btInput);
			// 获得密文
			byte[] md = mdInst.digest();
			// 把密文转换成十六进制的字符串形式
			int j = md.length;
			char str[] = new char[j * 2];
			int k = 0;
			for (int i = 0; i < j; i++) {
				byte byte0 = md[i];
				str[k++] = hexDigits[byte0 >>> 4 & 0xf];
				str[k++] = hexDigits[byte0 & 0xf];
			}
			return new String(str);
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}

	/**
	 * BASE64加密
	 * 
	 * @param key
	 * @return
	 * @throws UnsupportedEncodingException
	 * @throws Exception
	 */
	public static String base64EncryptUTF(byte[] key)
			throws UnsupportedEncodingException {
		return new String(Base64.encode(key, Base64.DEFAULT), "UTF-8");
	}

	public static String base64Encrypt(byte[] key) {
		return new String(Base64.encode(key, Base64.DEFAULT));
	}

	/**
	 * 判断是否是wifi连接
	 */
	public static boolean isWifi() {
		ConnectivityManager cm = (ConnectivityManager) MyApp.getApp()
				.getSystemService(Context.CONNECTIVITY_SERVICE);

		if (cm == null || cm.getActiveNetworkInfo() == null) {
			return false;
		}

		return cm.getActiveNetworkInfo().getType() == ConnectivityManager.TYPE_WIFI;

	}

	/**
	 * 打开网络设置界面
	 */
	public static void openSetting(Activity activity) {
		Intent intent = null;
		if (android.os.Build.VERSION.SDK_INT > 10) {
			intent = new Intent(android.provider.Settings.ACTION_WIFI_SETTINGS);
		} else {
			intent = new Intent("/");
			ComponentName cm = new ComponentName("com.android.settings",
					"com.android.settings.WirelessSettings");
			intent.setComponent(cm);
			intent.setAction("android.intent.action.VIEW");
		}
		activity.startActivityForResult(intent, 0);
	}

	public static void shortTips(String tip) {
		try {
			Log.e("Tips", tip);
			Toast.makeText(MyApp.getApp(), tip, Toast.LENGTH_SHORT).show();
		} catch (Exception e) {

		}

	}

	public static void longTips(String tip) {
		try {
			Log.e("Tips", tip);
			Toast.makeText(MyApp.getApp(), tip, Toast.LENGTH_LONG).show();
		} catch (Exception e) {

		}
	}

}
