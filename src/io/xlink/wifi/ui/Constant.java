package io.xlink.wifi.ui;

public class Constant {

	// 默认密码
	public static final int password = 8888;
	// 企业ID
	public static final String corp_id = "youhone";
	// 产品id

	public static String PRODUCTID[];
	// TODO 产品默认名称
	public static String PRODUCT_NAME[];
	// 产品图标(在线)
	public static int PRODUCT_ICO_ONLINE[];
	// 产品图标(离线)
	public static int PRODUCT_ICO_OFFLINE[];
	// 天气质量
	public static int WEATHER_QUALITY;
	// 天气APPKEY
	public static String WEATHER_APPKEY;
	// ------------启动监听
	public static final String PACKAGE_NAME = MyApp.getApp().getPackageName();
	public static final String BROADCAST_ON_START = PACKAGE_NAME + ".onStart"; //
	public static final String BROADCAST_ON_LOGIN = PACKAGE_NAME
			+ ".xlinkonLogin";

	public static final String BROADCAST_CLOUD_DISCONNECT = PACKAGE_NAME
			+ ".clouddisconnect";
	public static final String BROADCAST_LOCAL_DISCONNECT = PACKAGE_NAME
			+ ".localdisconnect";
	public static final String BROADCAST_RECVPIPE = PACKAGE_NAME + ".recv-pipe";
	public static final String BROADCAST_DEVICE_CHANGED = PACKAGE_NAME
			+ ".device-changed";

	public static final String BROADCAST_DEVICE_SYNC = PACKAGE_NAME
			+ ".device-sync";
	public static final String BROADCAST_EXIT = PACKAGE_NAME + ".exit";
	public static final String BROADCAST_TIMER_UPDATE = PACKAGE_NAME
			+ "timer-update";
	public static final String BROADCAST_SOCKET_STATUS = PACKAGE_NAME
			+ "socket-status";
	// http 注册，获取appid回调
	public static final int HTTP_NETWORK_ERR = 1;

	// 数据包超时时�?
	public static final int TIMEOUT = 10;// 设置请求超时时间

	public static final String DATA = "data";
	// public static final String DEVICE = "device";
	public static final String DEVICE_MAC = "device-mac";
	public static final String STATUS = "status";
	public static final String TYPE = "type";
	public static final String KEY = "key";

	public static final int TIMER_OFF = 0;
	public static final int TIMER_ON = 1;
	public static final int TIMER_BUFF_SIZE = 6;
	public static final int TIMER_MAX = 19;
	public static final String APP_ID = "appid";
	public static final String APP_KEY = "appkey";
	public static final String APP_UID = "app_uid";
	public static final String APP_PSWD = "app_passwrod";
	public static final String TIMING_DATE = "timing_date";
	public static final String TIMING_ACTION = "timing_action";
	public static final String DEVICE_NAME = "name";
	public static final String IS_REMEMBER = "remember";
	/**
	 * 成功
	 */
	public static final int HTTP_SUCCEED = 100;
	/**
	 * 失败
	 */
	public static final int HTTP_FAILURE = 201;

	// TODO-----------------------各大数据表-------------------------
	// 用户表
	public static String TABLE_USER = "";
}
