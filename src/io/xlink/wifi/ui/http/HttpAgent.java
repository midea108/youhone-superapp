package io.xlink.wifi.ui.http;

import io.xlink.wifi.ui.MyApp;
import io.xlink.wifi.ui.util.RequestParams;
import io.xlink.wifi.ui.util.XlinkUtils;

import java.io.UnsupportedEncodingException;
import java.security.MessageDigest;
import java.util.HashMap;
import java.util.Map;

import org.apache.http.Header;
import org.apache.http.HttpEntity;
import org.apache.http.entity.StringEntity;
import org.json.JSONArray;
import org.json.JSONObject;

import android.provider.SyncStateContract.Constants;

import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.TextHttpResponseHandler;

public class HttpAgent {

	private static HttpAgent instance;
	private final String url = "http://app.xlink.cn";
	// 获取验证码
	private final String verifycodeUrl = "/v2/user_register/verifycode";
	// 注册
	private final String registerUrl = url + "/v1/user/register";
	// 登录
	private final String loginUrl = url + "/v1/user/login";

	// 重置密码
	private final String resetUrl = url + "/v1/user/reset";
	// 修改
	private final String changeUrl = url + "/v1/user/integrate";
	// 添加
	private final String putUrl = url + "/v1/bucket/put";
	// 获取
	private final String getUrl = url + "/v1/bucket/get";
	// 删除
	private final String deleteUrl = url + "/v1/bucket/delete";
	// 更新
	private final String updataUrl = url + "/v1/bucket/update";

	// 3个头部
	private final static String AccessID = "X-AccessId";

	private final static String X_ContentMD5 = "X-ContentMD5";

	private final static String X_Sign = "X-Sign";
	/**
	 * xlink 企业后台管理 注册的 资源id和key
	 */
	public static String SECRET_KEY = "";
	public static String ACCESS_ID = "";

	/**
	 * 初始化，必须最先调用.
	 * 
	 * @param accessId
	 *            云智易平台上的accessId
	 * @param secretKey
	 *            云智易平台上的secretKey
	 */
	public static void init(String accessId, String secretKey) {
		if (instance == null) {
			instance = new HttpAgent();
			instance.ACCESS_ID = accessId;
			instance.SECRET_KEY = secretKey;
		} else {
			getInstance().ACCESS_ID = accessId;
			getInstance().SECRET_KEY = secretKey;
		}
	}

	/**
	 * md5算法
	 * 
	 * @param s
	 * @return
	 */
	public final static String MD5(String s) {
		char hexDigits[] = { '0', '1', '2', '3', '4', '5', '6', '7', '8', '9',
				'A', 'B', 'C', 'D', 'E', 'F' };
		try {
			byte[] btInput = s.getBytes();
			// 获得MD5摘要算法的 MessageDigest 对象
			MessageDigest mdInst = MessageDigest.getInstance("MD5");
			// 使用指定的字节更新摘要
			mdInst.update(btInput);
			// 获得密文
			byte[] md = mdInst.digest();
			// 把密文转换成十六进制的字符串形式
			int j = md.length;
			char str[] = new char[j * 2];
			int k = 0;
			for (int i = 0; i < j; i++) {
				byte byte0 = md[i];
				str[k++] = hexDigits[byte0 >>> 4 & 0xf];
				str[k++] = hexDigits[byte0 & 0xf];
			}
			return new String(str);
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}

	public static HttpAgent getInstance() {
		if (instance == null) {
			instance = new HttpAgent();
		}
		return instance;
	}

	/**
	 * 全局的http代理
	 */
	private static AsyncHttpClient client;

	private HttpAgent() {
		client = new AsyncHttpClient();
	}

	/**
	 * 通过SECRET_KEY 和内容md5 进行加密签名
	 * 
	 * @param contentmd5
	 * @return
	 */
	private XHeader getSign(String contentmd5) {
		String singnMd5 = MD5(SECRET_KEY + contentmd5);
		XHeader header = new XHeader(X_Sign, singnMd5, null);
		return header;
	}

	/**
	 * http 获取验证码
	 * 
	 * @param corp_id
	 *            企业ID
	 * @param phone
	 *            手机号码
	 * 
	 */
	public void onGetVerifyCode(String corp_id, String phone,
			TextHttpResponseHandler handler) {
		Map<String, String> map = new HashMap<String, String>();
		map.put("corp_id", corp_id);
		map.put("phone", phone);
		postJson(verifycodeUrl, map, handler);

	}

	/**
	 * http 注册接口
	 * 
	 * @param uid
	 *            用户ID
	 * @param name
	 *            昵称（别名，仅供后台管理平台观看，对用户来说记住uid和pwd就行）
	 * @param pwd
	 *            密码
	 * @param listener
	 *            注册监听器
	 */
	public void onRegister(String uid, String name, String pwd,
			TextHttpResponseHandler handler) {
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("uid", uid);
		map.put("name", name);
		map.put("pwd", pwd);
		JSONObject data = XlinkUtils.getJsonObject(map);
		// 请求entity
		StringEntity entity = null;
		try {
			entity = new StringEntity(data.toString(), "UTF-8");
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		}
		// 3个http 请求头部
		Header[] headers = new Header[3];
		// AccessID
		headers[0] = new XHeader(AccessID, ACCESS_ID, null);
		String contentMD5 = MD5(data.toString());
		// 内容md5验证
		headers[1] = new XHeader(X_ContentMD5, contentMD5, null);
		headers[2] = getSign(contentMD5); // 内容加SECRET_KEY 签名认证
		post(registerUrl, headers, entity, handler);

	}

	public void resetPW(String user, String pwd, TextHttpResponseHandler handler) {
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("uid", user);
		map.put("pwd", pwd);
		JSONObject data = XlinkUtils.getJsonObject(map);
		// 请求entity
		StringEntity entity = null;
		try {
			entity = new StringEntity(data.toString(), "UTF-8");
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		}
		// 3个http 请求头部
		Header[] headers = new Header[3];
		// AccessID
		headers[0] = new XHeader(AccessID, ACCESS_ID, null);
		// entity md5签名后
		String contentMD5 = MD5(data.toString());
		// 内容md5验证
		headers[1] = new XHeader(X_ContentMD5, contentMD5, null);
		// 内容加SECRET_KEY 签名认证
		headers[2] = getSign(contentMD5);
		post(resetUrl, headers, entity, handler);
	}

	/**
	 * http 注册接口
	 * 
	 * @param uid
	 *            用户ID
	 * @param name
	 *            昵称（别名，仅供后台管理平台观看，对用户来说记住uid和pwd就行）
	 * 
	 * @param listener
	 *            注册监听器
	 */
	public void onChange(String uid, String name,
			TextHttpResponseHandler handler) {
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("uid", uid);
		map.put("name", name);
		JSONObject data = XlinkUtils.getJsonObject(map);
		// 请求entity
		StringEntity entity = null;
		try {
			entity = new StringEntity(data.toString(), "UTF-8");
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		}
		// 3个http 请求头部
		Header[] headers = new Header[3];
		// AccessID
		headers[0] = new XHeader(AccessID, ACCESS_ID, null);
		String contentMD5 = MD5(data.toString());
		// 内容md5验证
		headers[1] = new XHeader(X_ContentMD5, contentMD5, null);
		headers[2] = getSign(contentMD5); // 内容加SECRET_KEY 签名认证
		post(changeUrl, headers, entity, handler);

	}

	public void getAppId(String user, String pwd,
			TextHttpResponseHandler handler) {

		Map<String, Object> map = new HashMap<String, Object>();
		map.put("uid", user);
		map.put("pwd", pwd);
		JSONObject data = XlinkUtils.getJsonObject(map);
		// 请求entity
		StringEntity entity = null;
		try {
			entity = new StringEntity(data.toString(), "UTF-8");
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		}
		// 3个http 请求头部
		Header[] headers = new Header[3];
		// AccessID
		headers[0] = new XHeader(AccessID, ACCESS_ID, null);
		// entity md5签名后
		String contentMD5 = MD5(data.toString());
		// 内容md5验证
		headers[1] = new XHeader(X_ContentMD5, contentMD5, null);
		// 内容加SECRET_KEY 签名认证
		headers[2] = getSign(contentMD5);
		post(loginUrl, headers, entity, handler);
	}

	/**
	 * 
	 * @param url
	 *            url地址
	 * @param headers
	 *            http请求头部
	 * @param entity
	 *            http 实体
	 * @param handler
	 *            回调
	 */
	private void post(String url, Header[] headers, HttpEntity entity,
			AsyncHttpResponseHandler handler) {

		client.post(MyApp.getApp(), url, headers, entity, "text/html", handler);
	}

	private void postJson(String url, Map<String, String> params,
			TextHttpResponseHandler callback) {
		// 请求entity
		StringEntity entity = params2StringEntity(params);
		client.post(MyApp.getApp(), url, entity, "application/json", callback);
	}

	/**
	 * 
	 * @param url
	 *            url地址
	 * @param headers
	 *            http请求头部
	 * @param entity
	 *            http 实体
	 * @param handler
	 *            回调
	 */
	private void post(String url, HttpEntity entity,
			AsyncHttpResponseHandler handler, Header[] headers) {
		client.post(MyApp.getApp(), url, headers, entity, "text/html", handler);
	}

	private Header[] getHeaders(String data) {
		// 3个http 请求头部
		Header[] headers = new Header[3];
		// AccessID
		headers[0] = new XHeader(AccessID, ACCESS_ID, null);
		// entity md5签名后
		String contentMD5 = XlinkUtils.MD5(data.toString());
		// 内容md5验证
		headers[1] = new XHeader(X_ContentMD5, contentMD5, null);
		// 内容加SECRET_KEY 签名认证
		headers[2] = getSign(contentMD5);
		return headers;
	}

	/**
	 * put 数据到服务器
	 * 
	 * @param table
	 *            需要put到数据表的表名
	 * @param id
	 *            指定这条数据唯一id
	 * @param data
	 *            数据 json格式
	 * @param listener
	 * <br>
	 *            数据在 {@link ReceiveResponse #msg}<br>
	 *            code :{@link ReceiveResponse #code}请参见 {@link Constants}
	 * 
	 */
	public void putData(String table, JSONObject data,
			TextHttpResponseHandler listener, String id) {
		RequestParams params = new RequestParams();
		params.put("table", table);
		if (id != null) {
			params.put("id", id);
		}
		params.put("data", data);
		System.out.println(" data   " + data.toString());
		post(putUrl, params.getJsonEntity(), listener,
				getHeaders(params.getStringData()));
	}

	// /**
	// * put 数据到服务器
	// *@deprecated
	// * @param table
	// * @param id
	// * @param data
	// * @param listener
	// */
	// private void putData(String accessId, String table, JSONObject data,
	// TextHttpResponseHandler listener) {
	//
	// putData(accessId, table, data, listener, null);
	// }
	/**
	 * 根据数据ID查询数据
	 * 
	 * @param table
	 *            数据表
	 * @param listener
	 * @param id
	 *            数据ID
	 */
	public void queryData(String table, TextHttpResponseHandler listener,
			String id) {
		RequestParams params = new RequestParams();
		params.put("table", table);
		params.put("id", id);
		post(getUrl, params.getJsonEntity(), listener,
				getHeaders(params.getStringData()));
	}

	/**
	 * 按条件查询。
	 * 
	 * @param table
	 *            数据库表名
	 * @param listener
	 *            数据监听器
	 * @param query
	 *            查询条件 如{"key":"value","key2":"value2"}查询有这样的key-value的数据<br>
	 *            如果没指定过滤器 ，会返回带这个key-vuale的整条数据.
	 * @param filtration
	 *            过滤器 如["key","key2"] 指定只要 key 和key2 的数据返回
	 * @param offset
	 *            查询偏移量 ，从第几条数据开始查询。
	 * @param limit
	 *            返回条目数
	 */
	public void queryData(String table, TextHttpResponseHandler listener,
			JSONObject query, JSONArray filtration, int offset, int limit) {
		RequestParams params = new RequestParams();
		params.put("table", table);
		params.put("query", query);
		if (filtration != null) {
			params.put("columns", filtration);
		}
		if (offset != -1) {
			params.put("offset", offset);
		}
		if (limit != 0) {
			params.put("limit", limit);
		}
		post(getUrl, params.getJsonEntity(), listener,
				getHeaders(params.getStringData()));

	}

	/**
	 * 按条件查询。不规定最大值
	 * 
	 * @param table
	 *            数据库表名
	 * @param listener
	 *            数据监听器
	 * @param query
	 *            查询条件 如{"key":"value","key2":"value2"}查询有这样的key-value的数据<br>
	 *            如果没指定过滤器 ，会返回带这个key-vuale的整条数据.
	 * @param filtration
	 *            过滤器 如["key","key2"] 指定只要 key 和key2 的数据返回
	 * @param offset
	 *            查询偏移量 ，从第几条数据开始查询(无limit 会返回从offset -max 条数据)
	 */
	public void queryData(String table, TextHttpResponseHandler listener,
			JSONObject query, JSONArray filtration, int offset) {
		queryData(table, listener, query, filtration, offset, 0);
	}

	/**
	 * 按条件查询。不规定偏移量
	 * 
	 * @param table
	 *            数据库表名
	 * @param listener
	 *            数据监听器
	 * @param query
	 *            查询条件 如{"key":"value","key2":"value2"}查询有这样的key-value的数据<br>
	 *            如果没指定过滤器 ，会返回带这个key-vuale的整条数据.
	 * @param filtration
	 *            过滤器 如["key","key2"] 指定只要 key 和key2 的数据返回
	 * @param limit
	 *            查询的数据最大值，(无offset 会返回从0 -- limit 条数据)
	 */
	public void queryDataLimit(String table, TextHttpResponseHandler listener,
			JSONObject query, JSONArray filtration, int limit) {
		queryData(table, listener, query, filtration, -1, limit);
	}

	/**
	 * 按条件查询,无数量条件，会返回所有数据.(最大1W条数据)
	 * 
	 * @param table
	 *            数据库表名
	 * @param listener
	 *            数据监听器
	 * @param query
	 *            查询条件 如{"key":"value","key2":"value2"}查询有这样的key-value的数据<br>
	 *            如果没指定过滤器 ，会返回带这个key-vuale的整条数据.<br>
	 * @param filtration
	 *            过滤器 如["key","key2"] 指定只要 key 和key2 的数据返回
	 */
	public void queryData(String table, TextHttpResponseHandler listener,
			JSONObject query, JSONArray filtration) {
		queryData(table, listener, query, filtration, -1, 0);
	}

	/**
	 * 按条件查询,无过滤器 无最多数据条目,会返回所有数据.(最大1W条数据)
	 * 
	 * @param table
	 *            数据库表名
	 * @param listener
	 *            数据监听器
	 * @param query
	 *            查询条件 如{"key":"value","key2":"value2"}查询有这样的key-value的数据<br>
	 *            如果没指定过滤器 ，会返回带这个key-vuale的整条数据.<br>
	 * @param offset
	 *            查询偏移量 ，从第几条数据开始查询(无limit 会返回从offset -max 条数据)
	 */
	public void queryData(String table, TextHttpResponseHandler listener,
			JSONObject query, int offset) {
		queryData(table, listener, query, null, offset, 0);
	}

	/**
	 * 按条件查询,无过滤器 有最大值
	 * 
	 * @param table
	 *            数据库表名
	 * @param listener
	 *            数据监听器
	 * @param query
	 *            查询条件 如{"key":"value","key2":"value2"}查询有这样的key-value的数据<br>
	 *            如果没指定过滤器 ，会返回带这个key-vuale的整条数据.<br>
	 * @param limit
	 *            返回条目数 从第0条数据开始查询(无offset 会返回从0 -limit 条数据)
	 */
	public void queryDataLimit(String table, TextHttpResponseHandler listener,
			JSONObject query, int limit) {
		queryData(table, listener, query, null, -1, limit);
	}

	/**
	 * 按条件查询,无过滤器，无数量条件，会返回所有数据.(最大1W条数据)
	 * 
	 * @param table
	 *            数据库表名
	 * @param listener
	 *            数据监听器
	 * @param query
	 *            查询条件 如{"key":"value","key2":"value2"}查询有这样的key-value的数据<br>
	 *            如果没指定过滤器 ，会返回带这个key-vuale的整条数据.<br>
	 */
	public void queryData(String table, TextHttpResponseHandler listener,
			JSONObject query) {
		queryData(table, listener, query, null, -1, 0);
	}

	/**
	 * 删除数据
	 * 
	 * @param table
	 *            数据表
	 * @param listener
	 *            监听器
	 * @param id
	 *            数据ID
	 */
	public void deleteData(String table, TextHttpResponseHandler listener,
			String id) {
		RequestParams params = new RequestParams();
		params.put("table", table);
		params.put("id", id);
		post(deleteUrl, params.getJsonEntity(), listener,
				getHeaders(params.getStringData()));
	}

	/**
	 * 模糊删除数据
	 * 
	 * @param table
	 *            数据表
	 * @param listener
	 *            监听器
	 * @param query
	 *            带有 这个json{key:value}的数据都删除.
	 */
	public void deleteData(String table, TextHttpResponseHandler listener,
			JSONObject query) {
		RequestParams params = new RequestParams();
		params.put("table", table);
		params.put("query", query);
		post(deleteUrl, params.getJsonEntity(), listener,
				getHeaders(params.getStringData()));
	}

	private void upData(String table, TextHttpResponseHandler listener,
			String id, JSONObject data, JSONObject column) {
		RequestParams params = new RequestParams();
		params.put("table", table);
		if (id != null) {
			params.put("id", id);
		}
		if (column != null) {
			params.put("query", column);
		}
		params.put("update", data);
		post(updataUrl, params.getJsonEntity(), listener,
				getHeaders(params.getStringData()));
	}

	/**
	 * 更新数据 此更新是直接覆盖 数据ID里的数据为 data
	 * 
	 * @param table
	 *            表名
	 * @param listener
	 *            监听器
	 * @param id
	 *            数据Id
	 * @param data
	 *            更新的数据
	 */
	public void upData(String table, TextHttpResponseHandler listener,
			String id, JSONObject data) {
		upData(table, listener, id, data, null);
	}

	/**
	 * 模糊更新数据
	 * 
	 * @param table
	 *            表名
	 * @param listener
	 *            监听器
	 * 
	 * 
	 * @param column
	 *            更新条件 如: 数据库里面的数据为{"key":"sss"} 都更新为@param data
	 * @param data
	 *            更新的数据 格式：{"key":"value"}
	 */
	public void upData(String table, TextHttpResponseHandler listener,
			JSONObject column, JSONObject data) {
		upData(table, listener, null, data, column);
	}

	private StringEntity params2StringEntity(Map<String, String> params) {
		StringEntity entity = null;
		try {
			entity = new StringEntity(toGetParams(params), "UTF-8");
		} catch (Exception e) {
		}
		return entity;
	}

	private String toGetParams(Map<String, String> map) {
		String params = "";
		boolean isStart = true;
		for (String keyString : map.keySet()) {
			if (isStart) {
				params += '?';
				isStart = false;
			} else
				params += "&";

			params += (keyString + '=' + map.get(keyString));
		}
		return params;
	}

	private Header[] map2Header(Map<String, String> headers) {
		if (headers == null) {
			return null;
		}
		Header[] headersdata = new Header[headers.size()];
		int i = 0;
		for (String key : headers.keySet()) {
			headersdata[i] = new XHeader(key, headers.get(key));
			i++;
		}
		return headersdata;
	}
}
