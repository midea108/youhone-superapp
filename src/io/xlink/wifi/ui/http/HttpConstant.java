package io.xlink.wifi.ui.http;

/**
 * Created by MYFLY on 2016/1/5.
 */
public class HttpConstant {

    public static final int PARAM_SUCCESS = 200;

    /*******************************������ָ��� ********************************/
    /**
     * ����IO����
     */
    public static final int PARAM_NETIO_ERROR = 1001001;
    /*******************************������ָ��� ********************************/
    /*******************************������ָ��� ********************************/
    /*******************************������ָ��� ********************************/
    /*******************************������ָ��� ********************************/
    /**
     * HTTP 400 �·��صĴ�����
     */
    /**
     * ���������ֶ���֤��ͨ��
     */
    public static final int PARAM_VALID_ERROR = 4001001;
    /**
     * �������ݱ����ֶβ���Ϊ��
     */
    public static final int PARAM_MUST_NOT_NULL = 4001002;
    /**
     * �ֻ���֤�벻����
     */
    public static final int PHONE_VERIFYCODE_NOT_EXISTS = 4001003;
    /**
     * �ֻ���֤�����
     */
    public static final int PHONE_VERIFYCODE_ERROR = 4001004;
    /**
     * ע����ֻ����Ѵ���
     */
    public static final int REGISTER_PHONE_EXISTS = 4001005;
    /**
     * ע��������Ѵ���
     */
    public static final int REGISTER_EMAIL_EXISTS = 4001006;
    /**
     * �������
     */
    public static final int ACCOUNT_PASSWORD_ERROR = 4001007;
    /**
     * �ʺŲ��Ϸ�
     */
    public static final int ACCOUNT_VAILD_ERROR = 4001008;
    /**
     * ��ҵ��Ա״̬���Ϸ�
     */
    public static final int MEMBER_STATUS_ERROR = 4001009;
    /**
     * ˢ��token���Ϸ�
     */
    public static final int REFRESH_TOKEN_ERROR = 4001010;
    /**
     * δ֪��Ա��ɫ����
     */
    public static final int MEMBER_ROLE_TYPE_UNKOWN = 4001011;
    /**
     * ֻ�й���Ա��������
     */
    public static final int MEMBER_INVITE_NOT_ADMIN = 4001012;
    /**
     * �����޸�������Ա��Ϣ
     */
    public static final int CAN_NOT_MODIFY_OTHER_MEMBER_INFO = 4001013;
    /**
     * ����ɾ������
     */
    public static final int CAN_NOT_DELETE_YOURSELF = 4001014;
    /**
     * δ֪�Ĳ�Ʒ��������
     */
    public static final int PRODUCT_LINK_TYPE_UNKOWN = 4001015;
    /**
     * �ѷ����Ĳ�Ʒ����ɾ��
     */
    public static final int CAN_NOT_DELETE_RELEASE_PRODUCT = 4001016;
    /**
     * �̼��汾�Ѵ���
     */
    public static final int FIRMWARE_VERSION_EXISTS = 4001017;
    /**
     * ���ݶ˵�δ֪��������
     */
    public static final int DATAPOINT_TYPE_UNKOWN = 4001018;
    /**
     * ���ݶ˵������Ѵ���
     */
    public static final int DATAPOINT_INDEX_EXISTS = 4001019;
    /**
     * �ѷ��������ݶ˵㲻��ɾ��
     */
    public static final int CANT_NOT_DELETE_RELEASED_DATAPOINT = 4001020;
    /**
     * �ò�Ʒ���豸MAC��ַ�Ѵ���
     */
    public static final int DEVICE_MAC_ADDRESS_EXISTS = 4001021;
    /**
     * ����ɾ���Ѽ�����豸
     */
    public static final int CAN_NOT_DELETE_ACTIVATED_DEVICE = 4001022;
    /**
     * ��չ����KeyΪԤ���ֶ�
     */
    public static final int PROPERTY_KEY_PROTECT = 4001023;
    /**
     * �豸��չ���Գ�������
     */
    public static final int PROPERTY_LIMIT = 4001024;
    /**
     * �����Ѵ��ڵ���չ����
     */
    public static final int PROPERTY_ADD_EXISTS = 4001025;
    /**
     * ���²����ڵ���չ����
     */
    public static final int PROPERTY_UPDATE_NOT_EXISTS = 4001026;
    /**
     * �����ֶ������Ϸ�
     */
    public static final int PROPERTY_KEY_ERROR = 4001027;
    /**
     * �ʼ���֤�벻����
     */
    public static final int EMAIL_VERIFYCODE_NOT_EXISTS = 4001028;
    /**
     * �ʼ���֤�����
     */
    public static final int EMAIL_VERIFYCODE_ERROR = 4001029;
    /**
     * �û�״̬���Ϸ�
     */
    public static final int USER_STATUS_ERROR = 4001030;
    /**
     * �û��ֻ���δ��֤
     */
    public static final int USER_PHONE_NOT_VAILD = 4001031;
    /**
     * �û�������δ��֤
     */
    public static final int USER_EMAIL_NOT_VAILD = 4001032;
    /**
     * �û��Ѿ������豸
     */
    public static final int USER_HAS_SUBSCRIBE_DEVICE= 4001033;
    /**
     * �û�û�ж��ĸ��豸
     */
    public static final int USER_HAVE_NO_SUBSCRIBE_DEVICE = 4001034;
    /**
     * �Զ��������������Ѵ���
     */
    public static final int UPGRADE_TASK_NAME_EXISTS = 4001035;
    /**
     * ��������״̬δ֪
     */
    public static final int UPGRADE_TASK_STATUS_UNKOWN = 4001036;
    /**
     * ������ͬ����ʼ�汾��������
     */
    public static final int UPGRADE_TASK_HAVE_STARTING_VERSION = 4001037;
    /**
     * �豸����ʧ��
     */
    public static final int DEVICE_ACTIVE_FAIL = 4001038;
    /**
     * �豸��֤ʧ��
     */
    public static final int DEVICE_AUTH_FAIL = 4001039;
    /**
     * �����豸��֤�����
     */
    public static final int SUBSCRIBE_AUTHORIZE_CODE_ERROR = 4001041;
    /**
     * ��Ȩ�����Ѵ���
     */
    public static final int EMPOWER_NAME_EXISTS = 4001042;
    /**
     * �ø澯���������Ѵ���
     */
    public static final int ALARM_RULE_NAME_EXISTS = 4001043;
    /**
     * ���ݱ������Ѵ���
     */
    public static final int DATA_TABLE_NAME_EXISTS = 4001045;
    /**
     * ��Ʒ�̼��ļ�������С����
     */
    public static final int PRODUCT_FIRMWARE_FILE_SIZE_LIMIT = 4001046;
    /**
     * apn��Կ�ļ�������С����
     */
    public static final int APP_APN_LICENSE_FILE_SIZE_LIMIT = 4001047;
    /**
     * APP��APN����δ����
     */
    public static final int APP_APN_IS_NOT_ENABLE = 4001048;
    /**
     * ��Ʒδ�����û�ע���豸
     */
    public static final int PRODUCT_CAN_NOT_REGISTER_DEVICE = 4001049;
    /*******************************������ָ��� ********************************/
    /*******************************������ָ��� ********************************/
    /*******************************������ָ��� ********************************/
    /*******************************������ָ��� ********************************/
    /*******************************������ָ��� ********************************/
    /**
     * HTTP 403 �·��صĴ�����
     */
    /**
     * ��ֹ����
     */
    public static final int INVALID_ACCESS = 4031001;
    /**
     * ��ֹ���ʣ���ҪAccess-Token
     */
    public static final int NEED_ACCESS_TOKEN = 4031002;
    /**
     * ��Ч��Access-Token
     */
    public static final int ACCESS_TOKEN_INVALID = 4031003;
    /**
     * ��Ҫ��ҵ�ĵ���Ȩ��
     */
    public static final int NEED_CORP_API= 4031004;
    /**
     * ��Ҫ��ҵ����ԱȨ��
     */
    public static final int NEED_CORP_ADMIN_MEMBER = 4031005;
    /**
     * ��Ҫ���ݲ���Ȩ��
     */
    public static final int NEED_DATA_PERMISSION = 4031006;
    /**
     * ��ֹ����˽������
     */
    public static final int INVAILD_ACCESS_PRIVATE_DATA = 4031007;
    /**
     * �����Ѿ���ȡ��
     */
    public static final int SHARE_CANCELED = 4031008;
    /**
     * �����Ѿ�����
     */
    public static final int SHARE_ACCEPTED = 4031009;

    /*******************************������ָ��� ********************************/
    /*******************************������ָ��� ********************************/
    /*******************************������ָ��� ********************************/
    /*******************************������ָ��� ********************************/
    /*******************************������ָ��� ********************************/
    /**
     * HTTP 404 �·��صĴ�����
     */
    /**
     * URL�Ҳ���
     */
    public static final int URL_NOT_FOUND = 4041001;
    /**
     * ��ҵ��Ա�ʺŲ�����
     */
    public static final int MEMBER_ACCOUNT_NO_EXISTS = 4041002;
    /**
     * ��ҵ��Ա������
     */
    public static final int MEMBER_NOT_EXISTS = 4041003;
    /**
     * ����ĳ�Ա���䲻����
     */
    public static final int MEMBER_INVITE_EMAIL_NOT_EXISTS = 4041004;
    /**
     * ��Ʒ��Ϣ������
     */
    public static final int PRODUCT_NOT_EXISTS = 4041005;
    /**
     * ��Ʒ�̼�������
     */
    public static final int FIRMWARE_NOT_EXISTS = 4041006;
    /**
     * ���ݶ˵㲻����
     */
    public static final int DATAPOINT_NOT_EXISTS = 4041007;
    /**
     * �豸������
     */
    public static final int DEVICE_NOT_EXISTS = 4041008;
    /**
     * �豸��չ���Բ�����
     */
    public static final int DEVICE_PROPERTY_NOT_EXISTS  = 4041009;
    /**
     * ��ҵ������
     */
    public static final int CORP_NOT_EXISTS = 4041010;
    /**
     * �û�������
     */
    public static final int USER_NOT_EXISTS = 4041011;
    /**
     * �û���չ���Բ�����
     */
    public static final int USER_PROPERTY_NOT_EXISTS = 4041012;
    /**
     * �������񲻴���
     */
    public static final int UPGRADE_TASK_NOT_EXISTS = 4041013;
    /**
     * �����������Ȩ������
     */
    public static final int EMPOWER_NOT_EXISTS = 4041014;
    /**
     * �澯���򲻴���
     */
    public static final int ALARM_RULE_NOT_EXISTS = 4041015;
    /**
     * ���ݱ�����
     */
    public static final int DATA_TABLE_NOT_EXISTS = 4041016;
    /**
     * ���ݲ�����
     */
    public static final int DATA_NOT_EXISTS = 4041017;
    /**
     * ������Դ������
     */
    public static final int SHARE_NOT_EXISTS = 4041018;
    /**
     * ��ҵ���䲻����
     */
    public static final int CORP_EMAIL_NOT_EXISTS = 4041019;
    /**
     * APP������
     */
    public static final int APP_NOT_EXISTS = 4041020;


    /*******************************������ָ��� ********************************/
    /*******************************������ָ��� ********************************/
    /*******************************������ָ��� ********************************/
    /*******************************������ָ��� ********************************/
    /*******************************������ָ��� ********************************/
    /**
     * HTTP 503�·��صĴ�����
     */
    /**
     * ����˷����쳣
     */
    public static final int SERVICE_EXCEPTION = 5031001;


}
